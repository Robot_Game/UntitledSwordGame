Shader "MoreMountains/MMStandardEmission"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("MainTex", 2D) = "white" {}
		_MetallicGlossMap("MetallicGlossMap", 2D) = "gray" {}
		_Metallic("Metallic", Range( 0 , 1)) = 1
		_Glossiness("Glossiness", Range( 0 , 1)) = 0.5
		_BumpMap("BumpMap", 2D) = "bump" {}
		_BumpScale("BumpScale", Float) = 1
		_OcclusionMap("OcclusionMap", 2D) = "white" {}
		_OcclussionStrength("OcclussionStrength", Range( 0 , 1)) = 1
		[Enum(Off,0,On,1)][Header(Depth Blend)]_ZWrite("ZWrite", Range( 0 , 1)) = 0
		[Enum(UnityEngine.Rendering.CompareFunction)]_ZTest("ZTest", Range( 0 , 255)) = 167.5171
		[Enum(UnityEngine.Rendering.BlendMode)][Header(Blend Modes)]_BlendSrc("BlendSrc", Range( 0 , 255)) = 1
		[Enum(UnityEngine.Rendering.BlendMode)]_BlendDst("BlendDst", Range( 0 , 255)) = 10
		[Enum(UnityEngine.Rendering.CullMode)][Header(Cull)]_CullMode("CullMode", Range( 0 , 255)) = 0
		[Enum(UnityEngine.Rendering.ColorWriteMask)]_ColorMask("ColorMask", Range( 0 , 255)) = 255
		[IntRange][Header(Stencil)]_Stencil("Stencil", Range( 0 , 255)) = 0
		[IntRange]_StencilReadMask("StencilReadMask", Range( 0 , 255)) = 15
		[IntRange]_StencilWriteMask("StencilWriteMask", Range( 0 , 255)) = 15
		[Enum(UnityEngine.Rendering.CompareFunction)]_StencilComp("StencilComp", Range( 0 , 255)) = 0
		[Enum(UnityEngine.Rendering.StencilOp)]_StencilOpPassFront("StencilOpPassFront", Range( 0 , 255)) = 0
		[Enum(UnityEngine.Rendering.StencilOp)]_StencilOpFailFront("StencilOpFailFront", Range( 0 , 255)) = 0
		[Enum(UnityEngine.Rendering.StencilOp)]_StencilOpZFailFront("StencilOpZFailFront", Range( 0 , 255)) = 0
		_EmissionMap("EmissionMap", 2D) = "white" {}
		_EmissionColor("EmissionColor", Color) = (0,0,0,0)
		_EmissionIntensity("EmissionIntensity", Float) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull [_CullMode]
		ZWrite [_ZWrite]
		ZTest [_ZTest]
		Stencil
		{
			Ref [_Stencil]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			Comp [_StencilComp]
			Pass [_StencilOpPassFront]
			Fail [_StencilOpFailFront]
			ZFail [_StencilOpZFailFront]
		}
		CGPROGRAM
		#include "UnityStandardUtils.cginc"
		#pragma target 3.0
		#pragma exclude_renderers vulkan xbox360 xboxone ps4 psp2 n3ds wiiu 
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform half _ZTest;
		uniform half _StencilOpPassFront;
		uniform half _StencilComp;
		uniform float _StencilWriteMask;
		uniform float _StencilReadMask;
		uniform float _Stencil;
		uniform half _BlendSrc;
		uniform half _ZWrite;
		uniform half _CullMode;
		uniform half _BlendDst;
		uniform half _StencilOpFailFront;
		uniform half _ColorMask;
		uniform half _StencilOpZFailFront;
		uniform sampler2D _BumpMap;
		uniform float4 _BumpMap_ST;
		uniform float _BumpScale;
		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;
		uniform half4 _Color;
		uniform sampler2D _EmissionMap;
		uniform float4 _EmissionMap_ST;
		uniform float4 _EmissionColor;
		uniform float _EmissionIntensity;
		uniform sampler2D _MetallicGlossMap;
		uniform float4 _MetallicGlossMap_ST;
		uniform half _Metallic;
		uniform half _Glossiness;
		uniform sampler2D _OcclusionMap;
		uniform float4 _OcclusionMap_ST;
		uniform half _OcclussionStrength;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_BumpMap = i.uv_texcoord * _BumpMap_ST.xy + _BumpMap_ST.zw;
			o.Normal = UnpackScaleNormal( tex2D( _BumpMap, uv_BumpMap ), _BumpScale );
			float2 uv_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			o.Albedo = ( tex2D( _MainTex, uv_MainTex ) * _Color ).rgb;
			float2 uv_EmissionMap = i.uv_texcoord * _EmissionMap_ST.xy + _EmissionMap_ST.zw;
			half3 Emission49 = ( (tex2D( _EmissionMap, uv_EmissionMap )).rgb * (_EmissionColor).rgb * _EmissionIntensity );
			o.Emission = Emission49;
			float2 uv_MetallicGlossMap = i.uv_texcoord * _MetallicGlossMap_ST.xy + _MetallicGlossMap_ST.zw;
			float4 tex2DNode20 = tex2D( _MetallicGlossMap, uv_MetallicGlossMap );
			o.Metallic = ( tex2DNode20.r * _Metallic );
			o.Smoothness = ( tex2DNode20.a * _Glossiness );
			float2 uv_OcclusionMap = i.uv_texcoord * _OcclusionMap_ST.xy + _OcclusionMap_ST.zw;
			float lerpResult13 = lerp( tex2D( _OcclusionMap, uv_OcclusionMap ).g , 1.0 , _OcclussionStrength);
			o.Occlusion = lerpResult13;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	//CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17200
-1703;103;1609;897;2058.542;-834.4261;1.583486;True;True
Node;AmplifyShaderEditor.CommentaryNode;51;-1290.53,1243.422;Inherit;False;1110.894;637.5398;Comment;6;44;43;46;47;45;48;Emission;1,1,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;43;-1240.53,1293.422;Inherit;True;Property;_EmissionMap;EmissionMap;22;0;Create;True;0;0;True;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;44;-1143.562,1519.464;Inherit;False;Property;_EmissionColor;EmissionColor;23;0;Create;True;0;0;True;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SwizzleNode;45;-678.8722,1355.525;Inherit;False;FLOAT3;0;1;2;3;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;47;-1072.797,1765.962;Inherit;False;Property;_EmissionIntensity;EmissionIntensity;24;0;Create;True;0;0;True;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SwizzleNode;46;-685.9486,1501.773;Inherit;False;FLOAT3;0;1;2;3;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;24;-1259.066,821.7872;Inherit;False;987.681;380.3051;Occlussion;4;13;12;15;10;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;23;-1258.145,431.4199;Inherit;False;815.0563;357.0226;Bump Map;3;17;19;18;;0.5,0.469,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;22;-1272.643,-23.0182;Inherit;False;854.1115;442.6976;Metallic_Glossiness;5;7;5;8;9;20;;0.315,0.4634167,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;21;-1281.232,-546.493;Inherit;False;775.8388;483.5436;Albedo;3;2;4;3;;1,0,0,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;48;-348.6359,1462.852;Inherit;False;3;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;42;-2546.708,571.9696;Inherit;False;956.6885;362.3174;Stencil;7;34;35;36;38;39;40;41;;1,0.9674529,0.0235849,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;15;-755.9084,944.395;Half;False;Constant;_white;white;6;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;25;-2547.797,-207.767;Inherit;False;354;293;;2;31;30;BlendModes;1,0.4228504,0.2216981,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;7;-885.3477,140.4064;Half;False;Property;_Metallic;Metallic;3;0;Create;True;0;0;True;0;1;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;49;-100.9587,1469.928;Half;False;Emission;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;5;-885.9048,304.6793;Half;False;Property;_Glossiness;Glossiness;4;0;Create;True;0;0;True;0;0.5;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;26;-2139.429,-213.7654;Inherit;False;353;294;Comment;2;29;28;CullModes;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;19;-930.6155,673.4424;Float;False;Property;_BumpScale;BumpScale;6;0;Create;True;0;0;False;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;17;-1208.145,481.4199;Inherit;True;Property;_BumpMap;BumpMap;5;0;Create;True;0;0;True;0;-1;None;None;True;0;False;bump;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;2;-1231.232,-496.493;Inherit;True;Property;_MainTex;MainTex;1;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;10;-853.8529,1087.092;Half;False;Property;_OcclussionStrength;OcclussionStrength;8;0;Create;True;0;0;False;0;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;20;-1222.643,49.96849;Inherit;True;Property;_MetallicGlossMap;MetallicGlossMap;2;0;Create;True;0;0;True;0;-1;None;None;True;0;False;gray;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;27;-2544.213,182.9612;Inherit;False;353;294;;2;33;32;DepthModes;0.2235294,1,0.3270589,1;0;0
Node;AmplifyShaderEditor.SamplerNode;12;-1209.066,880.9346;Inherit;True;Property;_OcclusionMap;OcclusionMap;7;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;3;-994.1052,-269.949;Half;False;Property;_Color;Color;0;0;Create;True;0;0;True;0;1,1,1,1;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;41;-1892.021,810.6714;Half;False;Property;_StencilOpZFailFront;StencilOpZFailFront;21;1;[Enum];Create;True;0;1;UnityEngine.Rendering.StencilOp;True;0;0;4;0;255;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;8;-587.5309,26.98179;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;40;-1905.141,711.2267;Half;False;Property;_StencilOpFailFront;StencilOpFailFront;20;1;[Enum];Create;True;0;1;UnityEngine.Rendering.StencilOp;True;0;0;4;0;255;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;30;-2497.797,-29.76694;Half;False;Property;_BlendDst;BlendDst;12;1;[Enum];Create;True;0;1;UnityEngine.Rendering.BlendMode;True;0;10;10;0;255;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;13;-455.3857,871.7872;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;35;-2492.816,722.6534;Float;False;Property;_StencilReadMask;StencilReadMask;16;1;[IntRange];Create;True;0;0;True;0;15;255;0;255;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;4;-674.395,-424.7168;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.UnpackScaleNormalNode;18;-707.0893,569.9298;Inherit;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;31;-2495.797,-157.767;Half;False;Property;_BlendSrc;BlendSrc;11;1;[Enum];Create;True;0;1;UnityEngine.Rendering.BlendMode;True;1;Header(Blend Modes);1;5;0;255;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;29;-2089.429,-34.76547;Half;False;Property;_ColorMask;ColorMask;14;1;[Enum];Create;True;0;1;UnityEngine.Rendering.ColorWriteMask;True;0;255;255;0;255;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;9;-609.9722,228.0552;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;36;-2485.882,819.287;Float;False;Property;_StencilWriteMask;StencilWriteMask;17;1;[IntRange];Create;True;0;0;True;0;15;255;0;255;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;38;-2208.035,630.3571;Half;False;Property;_StencilComp;StencilComp;18;1;[Enum];Create;True;0;1;UnityEngine.Rendering.CompareFunction;True;0;0;4;0;255;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;39;-1904.289,621.9696;Half;False;Property;_StencilOpPassFront;StencilOpPassFront;19;1;[Enum];Create;True;0;1;UnityEngine.Rendering.StencilOp;True;0;0;4;0;255;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;28;-2088.429,-163.7655;Half;False;Property;_CullMode;CullMode;13;1;[Enum];Create;True;0;1;UnityEngine.Rendering.CullMode;True;1;Header(Cull);0;0;0;255;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;50;-246.0267,18.06863;Inherit;False;49;Emission;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;34;-2496.708,628.1711;Float;False;Property;_Stencil;Stencil;15;1;[IntRange];Create;True;0;0;True;1;Header(Stencil);0;0;0;255;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;33;-2494.213,232.9608;Half;False;Property;_ZWrite;ZWrite;9;1;[Enum];Create;True;2;Off;0;On;1;0;True;1;Header(Depth Blend);0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;32;-2494.213,361.9606;Half;False;Property;_ZTest;ZTest;10;1;[Enum];Create;True;0;1;UnityEngine.Rendering.CompareFunction;True;0;167.5171;4;0;255;0;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;0,0;Float;False;True;2;ASEMaterialInspector;0;0;Standard;Ubisoft/Standard/Standard-Complete;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;True;33;0;True;32;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;7;d3d9;d3d11_9x;d3d11;glcore;gles;gles3;metal;True;True;True;True;0;False;-1;True;0;True;34;255;True;35;255;True;36;0;True;38;0;True;39;0;True;40;0;True;41;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;1;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;True;28;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;45;0;43;0
WireConnection;46;0;44;0
WireConnection;48;0;45;0
WireConnection;48;1;46;0
WireConnection;48;2;47;0
WireConnection;49;0;48;0
WireConnection;8;0;20;1
WireConnection;8;1;7;0
WireConnection;13;0;12;2
WireConnection;13;1;15;0
WireConnection;13;2;10;0
WireConnection;4;0;2;0
WireConnection;4;1;3;0
WireConnection;18;0;17;0
WireConnection;18;1;19;0
WireConnection;9;0;20;4
WireConnection;9;1;5;0
WireConnection;0;0;4;0
WireConnection;0;1;18;0
WireConnection;0;2;50;0
WireConnection;0;3;8;0
WireConnection;0;4;9;0
WireConnection;0;5;13;0
ASEEND*/
//CHKSM=7D65EEEDC2AF61A6BAD3555F682C1F1CAA3E476E