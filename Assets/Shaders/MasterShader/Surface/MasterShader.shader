// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "VFX/SurfaceMasterShader"
{
	Properties
	{
		[Enum(UnityEngine.Rendering.CullMode)]_CullMode("Cull Mode", Float) = 0
		[Space(20)][Header(Main Tex)]_MainTex("Main Tex", 2D) = "white" {}
		_MainTexPanningSpeedX("Main Tex Panning Speed X", Float) = 0
		_MainTexPanningSpeedY("Main Tex Panning Speed Y", Float) = 0
		[Space(20)][Header(Color)]_GradientMap("Gradient Map", 2D) = "white" {}
		[HDR]_Color("Color", Color) = (1,1,1,1)
		_Contrast("Contrast", Float) = 1
		_Power("Power", Float) = 1
		[Space(20)][Header(Dissolve)]_DissolveAmount("Dissolve Amount", Range( 0 , 1)) = 0
		[HDR]_EdgeColor("Edge Color", Color) = (0,0,0,0)
		_EdgeSize("Edge Size", Range( 0 , 1)) = 0
		[Space(20)][Header(Distortion)]_DistortionGuide("Distortion Guide", 2D) = "white" {}
		[HideInInspector]_MaskClipValue("Mask Clip Value", Float) = 0.5
		_DistortionPanningSpeedX("Distortion Panning Speed X", Float) = 0
		_DistortionPanningSpeedY("Distortion Panning Speed Y", Float) = 0
		_DistortionAmount("Distortion Amount", Range( -1 , 1)) = 0
		[HideInInspector] _texcoord3( "", 2D ) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "AlphaTest+0" "IsEmissive" = "true"  }
		LOD 100
		Cull [_CullMode]
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma shader_feature USE_VERTEX_OFFSET
		#pragma surface surf Unlit keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
			float2 uv3_texcoord3;
		};

		uniform float _CullMode;
		uniform float4 _Color;
		uniform sampler2D _GradientMap;
		uniform sampler2D _MainTex;
		uniform float _MainTexPanningSpeedX;
		uniform float _MainTexPanningSpeedY;
		uniform sampler2D _DistortionGuide;
		uniform float _DistortionPanningSpeedX;
		uniform float _DistortionPanningSpeedY;
		uniform float _DistortionAmount;
		uniform float _Contrast;
		uniform float _Power;
		uniform float _EdgeSize;
		uniform float _DissolveAmount;
		uniform float4 _EdgeColor;
		uniform float _MaskClipValue;

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float2 appendResult132 = (float2(_MainTexPanningSpeedX , _MainTexPanningSpeedY));
			float2 panner131 = ( 1.0 * _Time.y * appendResult132 + i.uv_texcoord);
			float2 uv135 = panner131;
			float2 appendResult133 = (float2(_DistortionPanningSpeedX , _DistortionPanningSpeedY));
			float2 panner134 = ( 1.0 * _Time.y * appendResult133 + i.uv3_texcoord3);
			float2 displUV136 = panner134;
			float2 distortedTex145 = ( uv135 + ( (float2( -1,-1 ) + ((tex2D( _DistortionGuide, displUV136 )).rg - float2( 0,0 )) * (float2( 1,1 ) - float2( -1,-1 )) / (float2( 1,1 ) - float2( 0,0 ))) * _DistortionAmount ) );
			float lerpResult152 = lerp( 0.5 , tex2D( _MainTex, distortedTex145 ).r , _Contrast);
			float color153 = pow( saturate( lerpResult152 ) , _Power );
			float2 appendResult177 = (float2(color153 , 0.0));
			float4 tex2DNode174 = tex2D( _GradientMap, appendResult177 );
			float4 appendResult172 = (float4(( (_Color).rgb * (tex2DNode174).rgb * tex2DNode174.a ) , 1.0));
			float smoothstepResult159 = smoothstep( 0.001 , 0.1 , _DissolveAmount);
			float4 edgeColor165 = ( step( 0.0 , _EdgeSize ) * smoothstepResult159 * _EdgeColor );
			float4 finalColor178 = ( appendResult172 + edgeColor165 );
			o.Emission = finalColor178.xyz;
			o.Alpha = 1;
			float clipMask164 = ( color153 - (-0.6 + (_DissolveAmount - 0.0) * (0.6 - -0.6) / (1.0 - 0.0)) );
			clip( clipMask164 - _MaskClipValue );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17700
381;73;1047;968;3749.79;2324.042;4.393461;True;False
Node;AmplifyShaderEditor.CommentaryNode;139;-3711.279,-1193.593;Inherit;False;1011.021;725.9355;UV Panning;12;106;105;133;46;134;136;103;104;132;131;135;45;;1,0.8842885,0.0235849,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;106;-3659.032,-584.8201;Inherit;False;Property;_DistortionPanningSpeedY;Distortion Panning Speed Y;13;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;105;-3661.28,-669.9148;Inherit;False;Property;_DistortionPanningSpeedX;Distortion Panning Speed X;12;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;133;-3325.453,-640.0159;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;46;-3628.286,-798.2104;Inherit;False;2;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;134;-3142.457,-800.9992;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.CommentaryNode;147;-2683.285,-1192.418;Inherit;False;1628.96;463.4018;Distortion;9;143;141;138;140;142;2;61;137;145;;0.4532307,0.7303826,0.990566,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;136;-2940.258,-806.7679;Inherit;False;displUV;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;104;-3655.543,-917.3737;Inherit;False;Property;_MainTexPanningSpeedY;Main Tex Panning Speed Y;3;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;137;-2633.285,-1013.821;Inherit;False;136;displUV;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;103;-3657.79,-1002.468;Inherit;False;Property;_MainTexPanningSpeedX;Main Tex Panning Speed X;2;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;45;-3616.47,-1140.266;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;2;-2456.138,-1030.909;Inherit;True;Property;_DistortionGuide;Distortion Guide;11;0;Create;True;0;0;False;2;Space(20);Header(Distortion);-1;None;e28dc97a9541e3642a48c0e3886688c5;True;2;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;132;-3363.162,-986.9619;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ComponentMaskNode;61;-2131.603,-1031.438;Inherit;False;True;True;False;False;1;0;COLOR;0,0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;131;-3184.273,-1139.729;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;135;-2950.529,-1143.593;Inherit;False;uv;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TFHCRemapNode;141;-1859.332,-1035.887;Inherit;False;5;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT2;1,1;False;3;FLOAT2;-1,-1;False;4;FLOAT2;1,1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;143;-1898.661,-843.6158;Inherit;False;Property;_DistortionAmount;Distortion Amount;14;0;Create;True;0;0;False;0;0;0;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;140;-1603.7,-1007.483;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;138;-1772.967,-1141.986;Inherit;False;135;uv;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;142;-1461.027,-1142.418;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.CommentaryNode;155;-2680.687,-714.9836;Inherit;False;1417.564;405.0777;Contrast and Power;8;152;149;148;150;153;151;146;1;;1,0.5624329,0.1179245,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;145;-1294.324,-1140.102;Inherit;False;distortedTex;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;146;-2630.688,-639.4996;Inherit;False;145;distortedTex;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;151;-2311.464,-424.5059;Inherit;False;Property;_Contrast;Contrast;6;0;Create;True;0;0;False;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;1;-2414.555,-664.9836;Inherit;True;Property;_MainTex;Main Tex;1;0;Create;True;0;0;False;2;Space(20);Header(Main Tex);-1;e168f013358541943b416203ba9b08b7;e28dc97a9541e3642a48c0e3886688c5;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;152;-2088.02,-628.7334;Inherit;False;3;0;FLOAT;0.5;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;149;-1898.559,-501.6567;Inherit;False;Property;_Power;Power;7;0;Create;True;0;0;False;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;148;-1891.628,-626.4229;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;150;-1702.107,-627.2343;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;183;-1028.997,-1189.221;Inherit;False;1640.733;458.1632;Coloring;11;174;170;177;173;175;171;172;181;182;178;176;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;153;-1503.122,-633.187;Inherit;False;color;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;176;-978.9972,-939.7797;Inherit;False;153;color;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;167;-2683.8,-294.2313;Inherit;False;1732.971;713.6729;Dissolve;11;165;156;160;161;164;166;157;158;159;162;163;;1,0,0.07060671,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;161;-2590.284,5.034237;Inherit;False;Property;_DissolveAmount;Dissolve Amount;8;0;Create;True;0;0;False;2;Space(20);Header(Dissolve);0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;162;-2017.927,-52.84003;Inherit;False;Property;_EdgeSize;Edge Size;10;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;177;-797.9645,-934.3472;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SmoothstepOpNode;159;-1763.754,39.02714;Inherit;False;3;0;FLOAT;0.001;False;1;FLOAT;0.001;False;2;FLOAT;0.1;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;157;-1698.985,-86.53134;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;163;-1787.974,177.7965;Inherit;False;Property;_EdgeColor;Edge Color;9;1;[HDR];Create;True;0;0;False;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;175;-567.1268,-1139.221;Inherit;False;Property;_Color;Color;5;1;[HDR];Create;True;0;0;False;0;1,1,1,1;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;174;-655.1004,-961.0576;Inherit;True;Property;_GradientMap;Gradient Map;4;0;Create;True;0;0;False;2;Space(20);Header(Color);-1;None;d5920ea4c62735c419d08b541a8ea4a0;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;170;-326.992,-960.4825;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;158;-1451.885,23.30424;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ComponentMaskNode;173;-326.2369,-1137.634;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;165;-1305.345,18.3543;Inherit;False;edgeColor;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;171;-80.06758,-976.1174;Inherit;False;3;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.TFHCRemapNode;160;-2200.308,-121.2769;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-0.6;False;4;FLOAT;0.6;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;166;-2142.286,-244.2311;Inherit;False;153;color;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;172;75.16792,-975.616;Inherit;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;1;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;181;33.26316,-858.0859;Inherit;False;165;edgeColor;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;156;-1899.335,-233.5801;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;182;246.543,-977.9233;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;164;-1719.141,-237.6641;Inherit;False;clipMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;178;371.7365,-982.6417;Inherit;False;finalColor;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;120;-649.6863,-587.0339;Inherit;False;Property;_CullMode;Cull Mode;0;1;[Enum];Create;True;0;1;UnityEngine.Rendering.CullMode;True;0;0;2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;123;-666.0226,-669.4138;Inherit;False;Constant;_MaskClipValue;Mask Clip Value;12;1;[HideInInspector];Create;True;0;0;False;0;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;168;-1120.599,-480.7687;Inherit;False;164;clipMask;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;179;-1126.032,-638.6183;Inherit;False;178;finalColor;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;-901.8911,-682.7352;Float;False;True;-1;2;ASEMaterialInspector;100;0;Unlit;VFX/SurfaceMasterShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Masked;0.5;True;True;0;False;TransparentCutout;;AlphaTest;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;100;;-1;-1;-1;-1;0;False;0;0;True;120;-1;0;True;123;1;Pragma;shader_feature USE_VERTEX_OFFSET;False;;Custom;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;133;0;105;0
WireConnection;133;1;106;0
WireConnection;134;0;46;0
WireConnection;134;2;133;0
WireConnection;136;0;134;0
WireConnection;2;1;137;0
WireConnection;132;0;103;0
WireConnection;132;1;104;0
WireConnection;61;0;2;0
WireConnection;131;0;45;0
WireConnection;131;2;132;0
WireConnection;135;0;131;0
WireConnection;141;0;61;0
WireConnection;140;0;141;0
WireConnection;140;1;143;0
WireConnection;142;0;138;0
WireConnection;142;1;140;0
WireConnection;145;0;142;0
WireConnection;1;1;146;0
WireConnection;152;1;1;1
WireConnection;152;2;151;0
WireConnection;148;0;152;0
WireConnection;150;0;148;0
WireConnection;150;1;149;0
WireConnection;153;0;150;0
WireConnection;177;0;176;0
WireConnection;159;0;161;0
WireConnection;157;1;162;0
WireConnection;174;1;177;0
WireConnection;170;0;174;0
WireConnection;158;0;157;0
WireConnection;158;1;159;0
WireConnection;158;2;163;0
WireConnection;173;0;175;0
WireConnection;165;0;158;0
WireConnection;171;0;173;0
WireConnection;171;1;170;0
WireConnection;171;2;174;4
WireConnection;160;0;161;0
WireConnection;172;0;171;0
WireConnection;156;0;166;0
WireConnection;156;1;160;0
WireConnection;182;0;172;0
WireConnection;182;1;181;0
WireConnection;164;0;156;0
WireConnection;178;0;182;0
WireConnection;0;2;179;0
WireConnection;0;10;168;0
ASEEND*/
//CHKSM=DCECF90A7EE03B8B25E844C04337E44D5E9470C8