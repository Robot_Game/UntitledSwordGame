// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "VFX/MasterShader"
{
	Properties
	{
		[Enum(UnityEngine.Rendering.CullMode)][Space(10)]_CullMode("Cull Mode", Float) = 2
		[NoScaleOffset][Space(20)][Header(Main Tex)]_MainTex("Main Tex", 2D) = "white" {}
		_MainTillingandOffset("Main Tilling and Offset", Vector) = (1,1,1,1)
		_MainTexPanningSpeedX("Main Tex Panning Speed X", Float) = 0
		_MainTexPanningSpeedY("Main Tex Panning Speed Y", Float) = 0
		[Space(20)][Header(Secondary Tex)][Toggle(_HASSECONDARYTEX_ON)] _HASSECONDARYTEX("HAS SECONDARY TEX", Float) = 0
		[NoScaleOffset]_SecondaryTex("Secondary Tex", 2D) = "white" {}
		_SecTillingandOffset("Sec Tilling and Offset", Vector) = (1,1,1,1)
		_SecondaryPanningSpeedX("Secondary Panning Speed X", Float) = 0
		_SecondaryPanningSpeedY("Secondary Panning Speed Y", Float) = 0
		[Space(20)][Header(Color)]_GradientMap("Gradient Map", 2D) = "white" {}
		[HDR]_Color("Color", Color) = (1,1,1,1)
		_Contrast("Contrast", Float) = 1
		_Power("Power", Float) = 1
		[Space(20)][Header(Clipping)]_ClipAmount("Clip Amount", Range( 0 , 1)) = 0
		[HDR]_EdgeColor("Edge Color", Color) = (0,0,0,0)
		_EdgeSize("Edge Size", Range( 0 , 1)) = 0
		[NoScaleOffset][Space(20)][Header(Distortion)]_DistortionGuide("Distortion Guide", 2D) = "white" {}
		_DistTillingandOffset("Dist Tilling and Offset", Vector) = (1,1,1,1)
		_DistortionPanningSpeedX("Distortion Panning Speed X", Float) = 0
		_DistortionPanningSpeedY("Distortion Panning Speed Y", Float) = 0
		_DistortionAmount("Distortion Amount", Range( -1 , 1)) = 0
		[Space(20)][Header(Polar Coords)][Toggle(_USE_POLAR_COORDINATES_ON)] _USE_POLAR_COORDINATES("USE POLAR COORDINATES", Float) = 0
		[Space(20)][Header(Banding)][Toggle(_BANDING_ON)] _BANDING("BANDING", Float) = 0
		_Bands("Bands", Float) = 3
		[Space(20)][Header(Circle Mask)][Toggle(_CIRLCEMASK_ON)] _CIRLCEMASK("CIRLCE MASK", Float) = 0
		_OuterRadius("Outer Radius", Range( 0 , 1)) = 0.5
		_InnerRadius("Inner Radius", Range( -1 , 1)) = 0
		_Smoothness("Smoothness", Range( 0 , 1)) = 0.2

	}
	
	SubShader
	{
		
		
		Tags { "RenderType"="Opaque" }
	LOD 100

		CGINCLUDE
		#pragma target 3.0
		ENDCG
		Blend Off
		Cull [_CullMode]
		ColorMask RGBA
		ZWrite On
		ZTest LEqual
		Offset -1 , -1
		
		
		
		Pass
		{
			Name "Unlit"
			Tags { "LightMode"="ForwardBase"  }
			CGPROGRAM

			

			#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
			//only defining to not throw compilation error over Unity 5.5
			#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
			#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_instancing
			#include "UnityCG.cginc"
			#include "UnityShaderVariables.cginc"
			#pragma shader_feature _BANDING_ON
			#pragma shader_feature _CIRLCEMASK_ON
			#pragma shader_feature _HASSECONDARYTEX_ON
			#pragma shader_feature _USE_POLAR_COORDINATES_ON
			#pragma shader_feature _USE_POLAR_COORDINATES


			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				float4 ase_texcoord : TEXCOORD0;
				float4 ase_texcoord2 : TEXCOORD2;
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				float4 ase_texcoord : TEXCOORD0;
				float4 ase_color : COLOR;
			};

			uniform float _CullMode;
			uniform float4 _Color;
			uniform sampler2D _GradientMap;
			uniform sampler2D _MainTex;
			uniform float _MainTexPanningSpeedX;
			uniform float _MainTexPanningSpeedY;
			uniform float4 _MainTillingandOffset;
			uniform sampler2D _DistortionGuide;
			uniform float _DistortionPanningSpeedX;
			uniform float _DistortionPanningSpeedY;
			uniform float4 _DistTillingandOffset;
			uniform float _DistortionAmount;
			uniform float _Contrast;
			uniform float _Power;
			uniform sampler2D _SecondaryTex;
			uniform float _SecondaryPanningSpeedX;
			uniform float _SecondaryPanningSpeedY;
			uniform float4 _SecTillingandOffset;
			uniform float _OuterRadius;
			uniform float _Smoothness;
			uniform float _InnerRadius;
			uniform float _Bands;
			uniform float _ClipAmount;
			uniform float _EdgeSize;
			uniform float4 _EdgeColor;

			
			v2f vert ( appdata v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				UNITY_TRANSFER_INSTANCE_ID(v, o);

				o.ase_texcoord.xy = v.ase_texcoord.xy;
				o.ase_texcoord.zw = v.ase_texcoord2.xy;
				o.ase_color = v.color;
				float3 vertexValue = float3(0, 0, 0);
				#if ASE_ABSOLUTE_VERTEX_POS
				vertexValue = v.vertex.xyz;
				#endif
				vertexValue = vertexValue;
				#if ASE_ABSOLUTE_VERTEX_POS
				v.vertex.xyz = vertexValue;
				#else
				v.vertex.xyz += vertexValue;
				#endif
				o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}
			
			fixed4 frag (v2f i ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(i);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);
				fixed4 finalColor;
				float2 appendResult12 = (float2(_MainTexPanningSpeedX , _MainTexPanningSpeedY));
				float2 appendResult182 = (float2(_MainTillingandOffset.x , _MainTillingandOffset.y));
				float2 appendResult183 = (float2(_MainTillingandOffset.z , _MainTillingandOffset.w));
				float2 uv014 = i.ase_texcoord.xy * appendResult182 + appendResult183;
				float2 temp_output_4_0_g12 = ( ( uv014 * float2( 2,2 ) ) - float2( 1,1 ) );
				float2 break8_g12 = temp_output_4_0_g12;
				float2 appendResult16_g12 = (float2(( ( ( atan2( break8_g12.y , break8_g12.x ) / UNITY_PI ) / 2.0 ) + 0.5 ) , length( temp_output_4_0_g12 )));
				#ifdef _USE_POLAR_COORDINATES_ON
				float2 staticSwitch132 = appendResult16_g12;
				#else
				float2 staticSwitch132 = uv014;
				#endif
				float2 panner15 = ( 1.0 * _Time.y * appendResult12 + staticSwitch132);
				float2 uv17 = panner15;
				float2 appendResult4 = (float2(_DistortionPanningSpeedX , _DistortionPanningSpeedY));
				float2 appendResult186 = (float2(_DistTillingandOffset.x , _DistTillingandOffset.y));
				float2 appendResult185 = (float2(_DistTillingandOffset.z , _DistTillingandOffset.w));
				float2 uv25 = i.ase_texcoord.zw * appendResult186 + appendResult185;
				float2 temp_output_4_0_g7 = ( ( uv25 * float2( 2,2 ) ) - float2( 1,1 ) );
				float2 break8_g7 = temp_output_4_0_g7;
				float2 appendResult16_g7 = (float2(( ( ( atan2( break8_g7.y , break8_g7.x ) / UNITY_PI ) / 2.0 ) + 0.5 ) , length( temp_output_4_0_g7 )));
				#ifdef _USE_POLAR_COORDINATES
				float2 staticSwitch144 = appendResult16_g7;
				#else
				float2 staticSwitch144 = uv25;
				#endif
				float2 panner6 = ( 1.0 * _Time.y * appendResult4 + staticSwitch144);
				float2 displUV8 = panner6;
				float2 distortionUV24 = ( (float2( -1,-1 ) + ((tex2D( _DistortionGuide, displUV8 )).rg - float2( 0,0 )) * (float2( 1,1 ) - float2( -1,-1 )) / (float2( 1,1 ) - float2( 0,0 ))) * _DistortionAmount );
				float lerpResult28 = lerp( 0.5 , tex2D( _MainTex, ( uv17 + distortionUV24 ) ).r , _Contrast);
				float temp_output_31_0 = pow( saturate( lerpResult28 ) , _Power );
				float2 appendResult149 = (float2(_SecondaryPanningSpeedX , _SecondaryPanningSpeedY));
				float2 appendResult189 = (float2(_SecTillingandOffset.x , _SecTillingandOffset.y));
				float2 appendResult188 = (float2(_SecTillingandOffset.z , _SecTillingandOffset.w));
				float2 uv2150 = i.ase_texcoord.zw * appendResult189 + appendResult188;
				float2 temp_output_4_0_g11 = ( ( uv2150 * float2( 2,2 ) ) - float2( 1,1 ) );
				float2 break8_g11 = temp_output_4_0_g11;
				float2 appendResult16_g11 = (float2(( ( ( atan2( break8_g11.y , break8_g11.x ) / UNITY_PI ) / 2.0 ) + 0.5 ) , length( temp_output_4_0_g11 )));
				#ifdef _USE_POLAR_COORDINATES_ON
				float2 staticSwitch152 = appendResult16_g11;
				#else
				float2 staticSwitch152 = uv2150;
				#endif
				float2 panner153 = ( 1.0 * _Time.y * appendResult149 + staticSwitch152);
				float2 secondaryUV154 = panner153;
				float lerpResult161 = lerp( 0.5 , tex2D( _SecondaryTex, ( secondaryUV154 + distortionUV24 ) ).r , _Contrast);
				#ifdef _HASSECONDARYTEX_ON
				float staticSwitch145 = ( temp_output_31_0 * 2.0 * pow( saturate( lerpResult161 ) , _Power ) );
				#else
				float staticSwitch145 = temp_output_31_0;
				#endif
				float col32 = staticSwitch145;
				float2 OrMainUV200 = uv014;
				float temp_output_199_0 = distance( OrMainUV200 , float2( 0.5,0.5 ) );
				float smoothstepResult205 = smoothstep( _OuterRadius , ( _OuterRadius + _Smoothness ) , temp_output_199_0);
				float smoothstepResult207 = smoothstep( _InnerRadius , ( _Smoothness + _InnerRadius ) , temp_output_199_0);
				#ifdef _CIRLCEMASK_ON
				float staticSwitch213 = ( col32 * ( 1.0 - smoothstepResult205 ) * smoothstepResult207 );
				#else
				float staticSwitch213 = col32;
				#endif
				float colCircleMask212 = staticSwitch213;
				#ifdef _BANDING_ON
				float staticSwitch221 = ( round( ( colCircleMask212 * _Bands ) ) / _Bands );
				#else
				float staticSwitch221 = colCircleMask212;
				#endif
				float colBanding222 = staticSwitch221;
				float2 appendResult69 = (float2(colBanding222 , 0.0));
				float4 tex2DNode70 = tex2D( _GradientMap, appendResult69 );
				float4 appendResult75 = (float4(( (_Color).rgb * (tex2DNode70).rgb * tex2DNode70.a ) , 1.0));
				float temp_output_225_0 = saturate( ( _ClipAmount + ( 1.0 - i.ase_color.a ) ) );
				float temp_output_229_0 = ( colCircleMask212 - temp_output_225_0 );
				float smoothstepResult39 = smoothstep( 0.001 , 0.1 , temp_output_225_0);
				float4 edgeColor47 = ( step( temp_output_229_0 , _EdgeSize ) * smoothstepResult39 * _EdgeColor );
				float4 finalColor78 = ( appendResult75 + edgeColor47 );
				clip( temp_output_229_0 - _ClipAmount);
				float4 clippedFinalColor56 = finalColor78;
				
				
				finalColor = clippedFinalColor56;
				return finalColor;
			}
			ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=17700
422;66;868;777;1558.722;2413.041;1.883617;True;False
Node;AmplifyShaderEditor.CommentaryNode;1;-3722.692,-1169.324;Inherit;False;1977.278;1076.415;UV Panning;35;187;180;186;185;189;188;182;183;148;151;147;84;153;149;152;154;150;17;15;132;12;10;11;143;8;14;6;144;4;142;2;3;5;200;215;UV Panning;1,1,1,1;0;0
Node;AmplifyShaderEditor.Vector4Node;215;-3693.678,-796.1504;Inherit;False;Property;_DistTillingandOffset;Dist Tilling and Offset;18;0;Create;True;0;0;False;0;1,1,1,1;1,1,0,0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;186;-3413.474,-823.7623;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;185;-3413.9,-701.6871;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;5;-3245.569,-782.4601;Inherit;False;2;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;3;-2794.992,-642.8061;Inherit;False;Property;_DistortionPanningSpeedX;Distortion Panning Speed X;19;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;2;-2792.744,-557.7114;Inherit;False;Property;_DistortionPanningSpeedY;Distortion Panning Speed Y;20;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;142;-2940.052,-719.3273;Inherit;False;PolarCoords;-1;;7;d4c3818101f88c4418358bdc95446521;0;1;2;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.StaticSwitch;144;-2655.941,-787.1711;Inherit;False;Property;_USE_POLAR_COORDINATES;USE POLAR COORDINATES;24;0;Create;False;0;0;False;2;Space(20);Header(Polar Coords);0;0;0;False;;Toggle;2;Key0;Key1;Create;False;9;1;FLOAT2;0,0;False;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT2;0,0;False;6;FLOAT2;0,0;False;7;FLOAT2;0,0;False;8;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;4;-2459.167,-612.9073;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;6;-2238.66,-683.6044;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.Vector4Node;187;-3675.093,-458.2336;Inherit;False;Property;_SecTillingandOffset;Sec Tilling and Offset;7;0;Create;True;0;0;False;0;1,1,1,1;1.5,1.5,0,0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;7;-3709.96,175.2334;Inherit;False;1431.54;360.7472;Distortion;7;24;20;18;19;16;13;9;Distortion;1,1,1,1;0;0
Node;AmplifyShaderEditor.Vector4Node;180;-3680.545,-1117.109;Inherit;False;Property;_MainTillingandOffset;Main Tilling and Offset;2;0;Create;True;0;0;False;0;1,1,1,1;1,1,0,0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;8;-2023.451,-687.8943;Inherit;False;displUV;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;182;-3414.345,-1124.428;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;183;-3413.503,-1029.912;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;188;-3417.062,-359.4639;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;189;-3415.714,-461.5802;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;9;-3667.357,265.0634;Inherit;False;8;displUV;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;150;-3259.19,-421.9261;Inherit;False;2;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;14;-3246.2,-1117.484;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;13;-3490.21,247.9755;Inherit;True;Property;_DistortionGuide;Distortion Guide;17;1;[NoScaleOffset];Create;True;0;0;False;2;Space(20);Header(Distortion);-1;e28dc97a9541e3642a48c0e3886688c5;e28dc97a9541e3642a48c0e3886688c5;True;2;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FunctionNode;151;-2953.671,-350.2744;Inherit;False;PolarCoords;-1;;11;d4c3818101f88c4418358bdc95446521;0;1;2;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;148;-2803.146,-173.551;Inherit;False;Property;_SecondaryPanningSpeedY;Secondary Panning Speed Y;9;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;10;-2727.033,-1003.755;Inherit;False;Property;_MainTexPanningSpeedX;Main Tex Panning Speed X;3;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;16;-3165.675,247.4465;Inherit;False;True;True;False;False;1;0;COLOR;0,0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;147;-2805.392,-258.6457;Inherit;False;Property;_SecondaryPanningSpeedX;Secondary Panning Speed X;8;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;143;-2928.222,-1049.55;Inherit;False;PolarCoords;-1;;12;d4c3818101f88c4418358bdc95446521;0;1;2;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;11;-2724.785,-918.6606;Inherit;False;Property;_MainTexPanningSpeedY;Main Tex Panning Speed Y;4;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;152;-2655.815,-423.4986;Inherit;False;Property;_USE_POLAR_COORDINATES;USE POLAR COORDINATES;27;0;Create;False;0;0;False;2;Space(20);Header(Polar Coords);0;0;0;True;;Toggle;2;Key0;Key1;Create;False;9;1;FLOAT2;0,0;False;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT2;0,0;False;6;FLOAT2;0,0;False;7;FLOAT2;0,0;False;8;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TFHCRemapNode;18;-2893.404,242.9975;Inherit;False;5;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT2;1,1;False;3;FLOAT2;-1,-1;False;4;FLOAT2;1,1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.StaticSwitch;132;-2583.357,-1126.063;Inherit;False;Property;_USE_POLAR_COORDINATES;USE POLAR COORDINATES;24;0;Create;False;0;0;False;2;Space(20);Header(Polar Coords);0;0;0;True;;Toggle;2;Key0;Key1;Create;False;9;1;FLOAT2;0,0;False;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT2;0,0;False;6;FLOAT2;0,0;False;7;FLOAT2;0,0;False;8;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;149;-2469.565,-228.7469;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;12;-2432.406,-988.2488;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;19;-3001.424,442.0117;Inherit;False;Property;_DistortionAmount;Distortion Amount;21;0;Create;True;0;0;False;0;0;0;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;153;-2247.812,-371.2974;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;15;-2197.965,-1075.705;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;-2682.192,267.1711;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;24;-2498.446,268.091;Inherit;False;distortionUV;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;154;-2028.735,-374.4865;Inherit;False;secondaryUV;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;17;-1978.62,-1079.702;Inherit;False;uv;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.CommentaryNode;23;-3718.66,772.5612;Inherit;False;2034.192;629.8216;Contrast and Power;20;32;145;166;31;164;177;29;162;30;28;161;26;27;146;192;191;195;25;190;194;Contrast and Power;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;25;-3675.39,923.8115;Inherit;False;24;distortionUV;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;190;-3661.846,843.2872;Inherit;False;17;uv;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;195;-3673.643,1186.892;Inherit;False;154;secondaryUV;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;194;-3670.896,1290.728;Inherit;False;24;distortionUV;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;192;-3442.845,1200.909;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;191;-3441.16,859.6796;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;146;-3286.936,1194.644;Inherit;True;Property;_SecondaryTex;Secondary Tex;6;1;[NoScaleOffset];Create;True;0;0;True;0;-1;cd460ee4ac5c1e746b7a734cc7cc64dd;cd460ee4ac5c1e746b7a734cc7cc64dd;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;27;-3281.192,827.7128;Inherit;True;Property;_MainTex;Main Tex;1;1;[NoScaleOffset];Create;True;0;0;False;2;Space(20);Header(Main Tex);-1;e28dc97a9541e3642a48c0e3886688c5;e28dc97a9541e3642a48c0e3886688c5;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;26;-3177.993,1061.672;Inherit;False;Property;_Contrast;Contrast;12;0;Create;True;0;0;False;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;28;-2954.551,857.4448;Inherit;False;3;0;FLOAT;0.5;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;161;-2942.423,1202.178;Inherit;False;3;0;FLOAT;0.5;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;30;-2778.033,1055.712;Inherit;False;Property;_Power;Power;13;0;Create;True;0;0;False;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;29;-2758.158,859.7552;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;162;-2759.523,1226.075;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;214;-1577.123,-1941.62;Inherit;False;1678.742;550.1378;Circle Mask;14;198;202;196;197;199;206;205;208;211;209;207;210;213;212;Circle mask;1,1,1,1;0;0
Node;AmplifyShaderEditor.PowerNode;31;-2568.637,858.9438;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;177;-2537.595,1056.217;Inherit;False;Constant;_Float2;Float 2;24;0;Create;True;0;0;False;0;2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;164;-2570.002,1225.264;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;200;-2983.117,-944.8351;Inherit;False;OrMainUV;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;202;-1434.579,-1752.506;Inherit;False;200;OrMainUV;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;196;-1514.197,-1890.463;Inherit;False;Property;_OuterRadius;Outer Radius;28;0;Create;True;0;0;False;0;0.5;0.335;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;198;-1527.123,-1640.408;Inherit;False;Property;_Smoothness;Smoothness;30;0;Create;True;0;0;False;0;0.2;0.218;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;166;-2370.318,1036.92;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;197;-1521.817,-1523.926;Inherit;False;Property;_InnerRadius;Inner Radius;29;0;Create;True;0;0;False;0;0;0.17;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;145;-2212.747,869.7482;Inherit;False;Property;_HASSECONDARYTEX;HAS SECONDARY TEX;5;0;Create;True;0;0;False;2;Space(20);Header(Secondary Tex);0;0;0;True;;Toggle;2;Key0;Key1;Create;False;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;206;-1180.817,-1813.008;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;199;-1189.037,-1708.017;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;32;-1902.712,868.1381;Inherit;False;col;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;208;-1132.367,-1506.55;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;205;-994.9518,-1871.166;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;209;-766.1276,-1718.131;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;211;-779.5085,-1819.177;Inherit;False;32;col;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;207;-962.2368,-1639.282;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;210;-556.3851,-1738.013;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;213;-416.6012,-1812.05;Inherit;False;Property;_CIRLCEMASK;CIRLCE MASK;27;0;Create;True;0;0;False;2;Space(20);Header(Circle Mask);0;0;0;True;;Toggle;2;Key0;Key1;Create;False;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;223;-1522.491,-1235.127;Inherit;False;1204.95;292.407;Banding;7;218;217;219;216;220;221;222;Banding;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;212;-146.0663,-1813.01;Inherit;False;colCircleMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;217;-1472.491,-1182.579;Inherit;False;212;colCircleMask;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;218;-1439.478,-1057.32;Inherit;False;Property;_Bands;Bands;26;0;Create;True;0;0;False;0;3;3;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;219;-1256.868,-1113.076;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;66;-1525.269,-195.7786;Inherit;False;1581.465;712.9653;Clipping;16;229;36;228;225;226;227;231;57;56;61;47;45;40;39;41;37;Clipping;1,1,1,1;0;0
Node;AmplifyShaderEditor.RoundOpNode;216;-1102.936,-1098.989;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;220;-918.1097,-1078.307;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.VertexColorNode;231;-1480.406,-75.45817;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StaticSwitch;221;-765.9884,-1181.229;Inherit;False;Property;_BANDING;BANDING;25;0;Create;True;0;0;False;2;Space(20);Header(Banding);0;0;0;True;;Toggle;2;Key0;Key1;Create;False;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;227;-1289.135,14.72363;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;36;-1412.215,146.8042;Inherit;False;Property;_ClipAmount;Clip Amount;14;0;Create;True;0;0;False;2;Space(20);Header(Clipping);0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;67;-1520.173,-801.3148;Inherit;False;1643.31;460.7402;Coloring;11;78;77;75;74;73;72;71;70;69;68;79;Coloring;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleAddOpNode;226;-1098.014,-6.367166;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;222;-524.4125,-1178.875;Inherit;False;colBanding;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;228;-1215.988,-117.5873;Inherit;False;212;colCircleMask;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;225;-964.5547,-5.753335;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;68;-1478.846,-551.4603;Inherit;False;222;colBanding;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;69;-1289.14,-546.4413;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;229;-828.0908,-74.0172;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;37;-987.793,156.009;Inherit;False;Property;_EdgeSize;Edge Size;16;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;40;-529.7692,39.65012;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;70;-1146.276,-573.1515;Inherit;True;Property;_GradientMap;Gradient Map;10;0;Create;True;0;0;False;2;Space(20);Header(Color);-1;d5920ea4c62735c419d08b541a8ea4a0;d5920ea4c62735c419d08b541a8ea4a0;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;41;-657.7264,322.1641;Inherit;False;Property;_EdgeColor;Edge Color;15;1;[HDR];Create;True;0;0;False;0;0,0,0,0;4.237095,0.4880423,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;71;-1058.303,-751.3149;Inherit;False;Property;_Color;Color;11;1;[HDR];Create;True;0;0;False;0;1,1,1,1;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SmoothstepOpNode;39;-633.5066,183.395;Inherit;False;3;0;FLOAT;0.001;False;1;FLOAT;0.001;False;2;FLOAT;0.1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;73;-817.4127,-749.7281;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ComponentMaskNode;72;-818.1675,-572.5765;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;45;-321.6377,167.672;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;74;-571.2441,-588.2112;Inherit;False;3;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;47;-175.0981,162.7221;Inherit;False;edgeColor;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;79;-490.0643,-443.4043;Inherit;False;47;edgeColor;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.DynamicAppendNode;75;-416.0081,-587.7101;Inherit;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;1;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleAddOpNode;77;-244.6327,-590.0173;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;78;-119.4388,-594.7355;Inherit;False;finalColor;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;57;-827.7211,-154.4319;Inherit;False;78;finalColor;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.ClipNode;61;-591.8229,-138.8286;Inherit;False;3;0;FLOAT4;1,1,1,1;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.CommentaryNode;127;-3705.363,-1928.498;Inherit;False;1919.698;489.6259;Vertex Offset;12;89;120;121;88;107;99;112;108;94;106;85;100;Vertex Offset;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;56;-375.8922,-143.676;Inherit;False;clippedFinalColor;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;121;-3439.278,-1842.037;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;108;-2629.048,-1850.761;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;88;-3264.248,-1851.217;Inherit;False;FLOAT4;4;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;1;False;1;FLOAT4;0
Node;AmplifyShaderEditor.StaticSwitch;106;-2312.742,-1800.575;Inherit;False;Property;_USEVERTEXOFFSET;USE VERTEX OFFSET;22;0;Create;True;0;0;False;2;Space(20);Header(Vertex Offset);0;0;0;True;;Toggle;2;Key0;Key1;Create;False;9;1;FLOAT3;0,0,0;False;0;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT3;0,0,0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;100;-2025.663,-1800.862;Inherit;False;VertexOffset;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;112;-2798.731,-1715.832;Inherit;False;Property;_VertexOffsetAmount;Vertex Offset Amount;23;0;Create;True;0;0;False;0;0;0;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;60;-1108.635,658.2414;Inherit;False;56;clippedFinalColor;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleTimeNode;120;-3643.926,-1771.638;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;94;-2458.975,-1774.649;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;107;-2790.627,-1850.761;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;85;-3110.664,-1878.498;Inherit;True;Property;_TextureSample0;Texture Sample 0;1;0;Create;True;0;0;False;0;-1;e28dc97a9541e3642a48c0e3886688c5;e28dc97a9541e3642a48c0e3886688c5;True;0;False;white;Auto;False;Instance;27;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NormalVertexDataNode;99;-2718.948,-1616.272;Inherit;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;84;-2184.81,-846.5309;Inherit;False;MainTexPanSpeed;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;89;-3686.563,-1862.656;Inherit;False;84;MainTexPanSpeed;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;58;-636.795,669.6462;Inherit;False;Property;_CullMode;Cull Mode;0;1;[Enum];Create;True;0;1;UnityEngine.Rendering.CullMode;True;1;Space(10);2;2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;0;-866.0443,657.2399;Float;False;True;-1;2;ASEMaterialInspector;100;1;VFX/MasterShader;0770190933193b94aaa3065e307002fa;True;Unlit;0;0;Unlit;2;True;0;1;False;-1;0;False;-1;0;1;False;-1;0;False;-1;True;0;False;-1;0;False;-1;True;False;True;0;True;58;True;True;True;True;True;0;False;-1;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;True;1;False;-1;True;3;False;-1;True;True;-1;False;-1;-1;False;-1;True;1;RenderType=Opaque=RenderType;True;2;0;False;False;False;False;False;False;False;False;False;True;2;LightMode=ForwardBase;=;False;0;;0;0;Standard;1;Vertex Position,InvertActionOnDeselection;1;0;1;True;False;;0
WireConnection;186;0;215;1
WireConnection;186;1;215;2
WireConnection;185;0;215;3
WireConnection;185;1;215;4
WireConnection;5;0;186;0
WireConnection;5;1;185;0
WireConnection;142;2;5;0
WireConnection;144;1;5;0
WireConnection;144;0;142;0
WireConnection;4;0;3;0
WireConnection;4;1;2;0
WireConnection;6;0;144;0
WireConnection;6;2;4;0
WireConnection;8;0;6;0
WireConnection;182;0;180;1
WireConnection;182;1;180;2
WireConnection;183;0;180;3
WireConnection;183;1;180;4
WireConnection;188;0;187;3
WireConnection;188;1;187;4
WireConnection;189;0;187;1
WireConnection;189;1;187;2
WireConnection;150;0;189;0
WireConnection;150;1;188;0
WireConnection;14;0;182;0
WireConnection;14;1;183;0
WireConnection;13;1;9;0
WireConnection;151;2;150;0
WireConnection;16;0;13;0
WireConnection;143;2;14;0
WireConnection;152;1;150;0
WireConnection;152;0;151;0
WireConnection;18;0;16;0
WireConnection;132;1;14;0
WireConnection;132;0;143;0
WireConnection;149;0;147;0
WireConnection;149;1;148;0
WireConnection;12;0;10;0
WireConnection;12;1;11;0
WireConnection;153;0;152;0
WireConnection;153;2;149;0
WireConnection;15;0;132;0
WireConnection;15;2;12;0
WireConnection;20;0;18;0
WireConnection;20;1;19;0
WireConnection;24;0;20;0
WireConnection;154;0;153;0
WireConnection;17;0;15;0
WireConnection;192;0;195;0
WireConnection;192;1;194;0
WireConnection;191;0;190;0
WireConnection;191;1;25;0
WireConnection;146;1;192;0
WireConnection;27;1;191;0
WireConnection;28;1;27;1
WireConnection;28;2;26;0
WireConnection;161;1;146;1
WireConnection;161;2;26;0
WireConnection;29;0;28;0
WireConnection;162;0;161;0
WireConnection;31;0;29;0
WireConnection;31;1;30;0
WireConnection;164;0;162;0
WireConnection;164;1;30;0
WireConnection;200;0;14;0
WireConnection;166;0;31;0
WireConnection;166;1;177;0
WireConnection;166;2;164;0
WireConnection;145;1;31;0
WireConnection;145;0;166;0
WireConnection;206;0;196;0
WireConnection;206;1;198;0
WireConnection;199;0;202;0
WireConnection;32;0;145;0
WireConnection;208;0;198;0
WireConnection;208;1;197;0
WireConnection;205;0;199;0
WireConnection;205;1;196;0
WireConnection;205;2;206;0
WireConnection;209;0;205;0
WireConnection;207;0;199;0
WireConnection;207;1;197;0
WireConnection;207;2;208;0
WireConnection;210;0;211;0
WireConnection;210;1;209;0
WireConnection;210;2;207;0
WireConnection;213;1;211;0
WireConnection;213;0;210;0
WireConnection;212;0;213;0
WireConnection;219;0;217;0
WireConnection;219;1;218;0
WireConnection;216;0;219;0
WireConnection;220;0;216;0
WireConnection;220;1;218;0
WireConnection;221;1;217;0
WireConnection;221;0;220;0
WireConnection;227;0;231;4
WireConnection;226;0;36;0
WireConnection;226;1;227;0
WireConnection;222;0;221;0
WireConnection;225;0;226;0
WireConnection;69;0;68;0
WireConnection;229;0;228;0
WireConnection;229;1;225;0
WireConnection;40;0;229;0
WireConnection;40;1;37;0
WireConnection;70;1;69;0
WireConnection;39;0;225;0
WireConnection;73;0;71;0
WireConnection;72;0;70;0
WireConnection;45;0;40;0
WireConnection;45;1;39;0
WireConnection;45;2;41;0
WireConnection;74;0;73;0
WireConnection;74;1;72;0
WireConnection;74;2;70;4
WireConnection;47;0;45;0
WireConnection;75;0;74;0
WireConnection;77;0;75;0
WireConnection;77;1;79;0
WireConnection;78;0;77;0
WireConnection;61;0;57;0
WireConnection;61;1;229;0
WireConnection;61;2;36;0
WireConnection;56;0;61;0
WireConnection;121;0;89;0
WireConnection;121;1;120;0
WireConnection;108;0;107;0
WireConnection;88;0;121;0
WireConnection;106;0;94;0
WireConnection;100;0;106;0
WireConnection;94;0;108;0
WireConnection;94;1;112;0
WireConnection;94;2;99;0
WireConnection;107;0;85;1
WireConnection;85;1;88;0
WireConnection;84;0;12;0
WireConnection;0;0;60;0
ASEEND*/
//CHKSM=A624850030B7D57722A87AE06C921D4EC2310148