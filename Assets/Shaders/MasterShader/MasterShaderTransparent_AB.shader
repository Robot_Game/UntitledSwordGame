// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "VFX/MasterShaderTransparent"
{
	Properties
	{
		[Enum(UnityEngine.Rendering.CullMode)][Space(10)]_CullMode("Cull Mode", Float) = 2
		[Space(20)][Header(Main Tex)]_MainTex("Main Tex", 2D) = "white" {}
		_MainTexPanningSpeedX("Main Tex Panning Speed X", Float) = 0
		_MainTexPanningSpeedY("Main Tex Panning Speed Y", Float) = 0
		[Space(20)][Header(Secondary Tex)][Toggle(_HASSECONDARYTEX_ON)] _HASSECONDARYTEX("HAS SECONDARY TEX", Float) = 0
		_SecondaryTex("Secondary Tex", 2D) = "white" {}
		_SecondaryPanningSpeedX("Secondary Panning Speed X", Float) = 0
		_SecondaryPanningSpeedY("Secondary Panning Speed Y", Float) = 0
		[Space(20)][Header(Color)]_GradientMap("Gradient Map", 2D) = "white" {}
		[HDR]_Color("Color", Color) = (1,1,1,1)
		_Contrast("Contrast", Float) = 1
		_Power("Power", Float) = 1
		[Space(20)][Header(Fresnel)][Toggle(_USEFRESNEL_ON)] _USEFRESNEL("USE FRESNEL", Float) = 0
		_FresnelIntensity("Fresnel Intensity", Float) = 0
		_FresnelPower("Fresnel Power", Range( 0 , 10)) = 0
		[HDR]_FresnelColor("Fresnel Color", Color) = (1,1,1,1)
		[Space(20)][Header(Clipping)]_ClipAmount("Clip Amount", Range( 0 , 1)) = 0
		_ClipSoftness("Clip Softness", Range( 0 , 1)) = 0
		[HDR]_EdgeColor("Edge Color", Color) = (1,1,1,1)
		_EdgeSize("Edge Size", Range( 0 , 1)) = 0
		[Space(20)][Header(Soft Blend)][Toggle(_SOFTBLEND_ON)] _SOFTBLEND("SOFT BLEND", Float) = 0
		_IntersectionThresholdMax("IntersectionThresholdMax", Range( 0.1 , 5)) = 1
		[Space(20)][Header(DEPTHl)][Toggle(_USEDEPTH_ON)] _USEDEPTH("USE DEPTH", Float) = 0
		_IntersectionSize("Intersection Size", Float) = 0
		[Toggle]_MultiplyByMainColor("Multiply By Main Color", Float) = 0
		[HDR]_IntersectionColor("Intersection Color", Color) = (1,1,1,1)
		[Space(20)][Header(Distortion)]_DistortionGuide("Distortion Guide", 2D) = "bump" {}
		_DistortionPanningSpeedX("Distortion Panning Speed X", Float) = 0
		_DistortionPanningSpeedY("Distortion Panning Speed Y", Float) = 0
		_DistortionAmountX("Distortion Amount X", Range( -1 , 1)) = 0
		_DistortionAmountY("Distortion Amount Y", Range( -1 , 1)) = 0
		[Space(20)][Header(Vertex Offset)][Toggle(_USEVERTEXOFFSET_ON)] _USEVERTEXOFFSET("USE VERTEX OFFSET", Float) = 0
		_VertexOffsetAmount("Vertex Offset Amount", Range( -1 , 1)) = 0
		[Space(20)][Header(Polar Coords)][Toggle(_USE_POLAR_COORDINATES_ON)] _USE_POLAR_COORDINATES("USE POLAR COORDINATES", Float) = 0
		[Space(20)][Header(Banding)][Toggle(_BANDING_ON)] _BANDING("BANDING", Float) = 0
		_Bands("Bands", Float) = 3
		[Space(20)][Header(Circle Mask)][Toggle(_CIRCLEMASK_ON)] _CIRCLEMASK("CIRCLE MASK", Float) = 0
		_OuterRadius("Outer Radius", Range( 0 , 1)) = 0.5
		_InnerRadius("Inner Radius", Range( -1 , 1)) = 0
		_Smoothness("Smoothness", Range( 0 , 1)) = 0.2
		[Space(20)][Header(Rect Mask)][Toggle(_RECTMASK_ON)] _RECTMASK("RECT MASK", Float) = 0
		_RectWidth("RectWidth", Float) = 0
		_RectHeight("RectHeight", Float) = 0
		_RectMaskCutoff("RectMaskCutoff", Range( 0 , 1)) = 0
		_RectSmothness("RectSmothness", Range( 0 , 1)) = 0

	}
	
	SubShader
	{
		
		
		Tags { "RenderType"="Transparent" "Queue"="Transparent" "IgnoreProjector"="True" "PreviewType"="Plane" }
	LOD 100

		CGINCLUDE
		#pragma target 3.0
		ENDCG
		Blend SrcAlpha OneMinusSrcAlpha
		Cull [_CullMode]
		ColorMask RGB
		ZWrite Off
		ZTest LEqual
		Offset -1 , -1
		
		
		
		Pass
		{
			Name "Unlit"
			Tags { "LightMode"="ForwardBase"  }
			CGPROGRAM

			

			#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
			//only defining to not throw compilation error over Unity 5.5
			#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
			#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_instancing
			#include "UnityCG.cginc"
			#include "UnityShaderVariables.cginc"
			#pragma shader_feature _USEVERTEXOFFSET_ON
			#pragma shader_feature _HASSECONDARYTEX_ON
			#pragma shader_feature _USEDEPTH_ON
			#pragma shader_feature _SOFTBLEND_ON
			#pragma shader_feature _USEFRESNEL_ON
			#pragma shader_feature _RECTMASK_ON
			#pragma shader_feature _CIRCLEMASK_ON
			#pragma shader_feature _USE_POLAR_COORDINATES_ON
			#pragma shader_feature _BANDING_ON
			#pragma multi_compile_fog


			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				float4 ase_texcoord : TEXCOORD0;
				float4 ase_texcoord2 : TEXCOORD2;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord1 : TEXCOORD1;
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				float4 ase_texcoord : TEXCOORD0;
				float4 ase_texcoord1 : TEXCOORD1;
				float4 ase_color : COLOR;
				float4 ase_texcoord2 : TEXCOORD2;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_texcoord4 : TEXCOORD4;
			};

			uniform float _CullMode;
			uniform sampler2D _MainTex;
			uniform float _MainTexPanningSpeedX;
			uniform float _MainTexPanningSpeedY;
			uniform float4 _MainTex_ST;
			uniform sampler2D _SecondaryTex;
			uniform float _SecondaryPanningSpeedX;
			uniform float _SecondaryPanningSpeedY;
			uniform float4 _SecondaryTex_ST;
			uniform float _VertexOffsetAmount;
			uniform sampler2D _DistortionGuide;
			uniform float _DistortionPanningSpeedX;
			uniform float _DistortionPanningSpeedY;
			uniform float4 _DistortionGuide_ST;
			uniform float _DistortionAmountX;
			uniform float _DistortionAmountY;
			uniform float _Contrast;
			uniform float _Power;
			uniform float _OuterRadius;
			uniform float _Smoothness;
			uniform float _InnerRadius;
			uniform float _RectMaskCutoff;
			uniform float _RectSmothness;
			uniform float _RectWidth;
			uniform float _RectHeight;
			uniform float _ClipAmount;
			uniform float _ClipSoftness;
			uniform float _EdgeSize;
			uniform float4 _EdgeColor;
			uniform sampler2D _GradientMap;
			uniform float _Bands;
			uniform float4 _Color;
			uniform float _FresnelIntensity;
			uniform float _FresnelPower;
			uniform float4 _FresnelColor;
			UNITY_DECLARE_DEPTH_TEXTURE( _CameraDepthTexture );
			uniform float4 _CameraDepthTexture_TexelSize;
			uniform float _IntersectionThresholdMax;
			uniform float _MultiplyByMainColor;
			uniform float4 _IntersectionColor;
			uniform float _IntersectionSize;

			
			v2f vert ( appdata v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				UNITY_TRANSFER_INSTANCE_ID(v, o);

				float2 appendResult12 = (float2(_MainTexPanningSpeedX , _MainTexPanningSpeedY));
				float2 MainTexPanSpeed84 = appendResult12;
				float2 uv0_MainTex = v.ase_texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;
				float4 tex2DNode85 = tex2Dlod( _MainTex, float4( ( ( MainTexPanSpeed84 * _Time.y ) + uv0_MainTex ), 0, 0.0) );
				float2 appendResult149 = (float2(_SecondaryPanningSpeedX , _SecondaryPanningSpeedY));
				float2 SecTexPanSpeed369 = appendResult149;
				float2 uv2_SecondaryTex = v.ase_texcoord2.xy * _SecondaryTex_ST.xy + _SecondaryTex_ST.zw;
				#ifdef _HASSECONDARYTEX_ON
				float staticSwitch370 = ( ( tex2DNode85.r * tex2Dlod( _SecondaryTex, float4( ( ( SecTexPanSpeed369 * _Time.y ) + uv2_SecondaryTex ), 0, 0.0) ).r ) * 2.0 );
				#else
				float staticSwitch370 = tex2DNode85.r;
				#endif
				#ifdef _USEVERTEXOFFSET_ON
				float3 staticSwitch106 = ( ( ( staticSwitch370 * 2.0 ) - 1.0 ) * _VertexOffsetAmount * v.ase_normal );
				#else
				float3 staticSwitch106 = float3( 0,0,0 );
				#endif
				float3 VertexOffset100 = staticSwitch106;
				
				float3 ase_worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				o.ase_texcoord2.xyz = ase_worldPos;
				float3 ase_worldNormal = UnityObjectToWorldNormal(v.ase_normal);
				o.ase_texcoord3.xyz = ase_worldNormal;
				float4 ase_clipPos = UnityObjectToClipPos(v.vertex);
				float4 screenPos = ComputeScreenPos(ase_clipPos);
				o.ase_texcoord4 = screenPos;
				
				o.ase_texcoord.xy = v.ase_texcoord.xy;
				o.ase_texcoord.zw = v.ase_texcoord1.xy;
				o.ase_texcoord1.xy = v.ase_texcoord2.xy;
				o.ase_color = v.color;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord1.zw = 0;
				o.ase_texcoord2.w = 0;
				o.ase_texcoord3.w = 0;
				float3 vertexValue = float3(0, 0, 0);
				#if ASE_ABSOLUTE_VERTEX_POS
				vertexValue = v.vertex.xyz;
				#endif
				vertexValue = VertexOffset100;
				#if ASE_ABSOLUTE_VERTEX_POS
				v.vertex.xyz = vertexValue;
				#else
				v.vertex.xyz += vertexValue;
				#endif
				o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}
			
			fixed4 frag (v2f i ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(i);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);
				fixed4 finalColor;
				float2 appendResult12 = (float2(_MainTexPanningSpeedX , _MainTexPanningSpeedY));
				float2 uv0_MainTex = i.ase_texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;
				float2 temp_output_4_0_g14 = ( ( uv0_MainTex * float2( 2,2 ) ) - float2( 1,1 ) );
				float2 break8_g14 = temp_output_4_0_g14;
				float2 appendResult16_g14 = (float2(( ( ( atan2( break8_g14.y , break8_g14.x ) / UNITY_PI ) / 2.0 ) + 0.5 ) , length( temp_output_4_0_g14 )));
				#ifdef _USE_POLAR_COORDINATES_ON
				float2 staticSwitch132 = appendResult16_g14;
				#else
				float2 staticSwitch132 = uv0_MainTex;
				#endif
				float2 panner15 = ( 1.0 * _Time.y * appendResult12 + staticSwitch132);
				float2 uv17 = panner15;
				float2 appendResult4 = (float2(_DistortionPanningSpeedX , _DistortionPanningSpeedY));
				float2 uv1_DistortionGuide = i.ase_texcoord.zw * _DistortionGuide_ST.xy + _DistortionGuide_ST.zw;
				float2 temp_output_4_0_g7 = ( ( uv1_DistortionGuide * float2( 2,2 ) ) - float2( 1,1 ) );
				float2 break8_g7 = temp_output_4_0_g7;
				float2 appendResult16_g7 = (float2(( ( ( atan2( break8_g7.y , break8_g7.x ) / UNITY_PI ) / 2.0 ) + 0.5 ) , length( temp_output_4_0_g7 )));
				#ifdef _USE_POLAR_COORDINATES_ON
				float2 staticSwitch144 = appendResult16_g7;
				#else
				float2 staticSwitch144 = uv1_DistortionGuide;
				#endif
				float2 panner6 = ( 1.0 * _Time.y * appendResult4 + staticSwitch144);
				float2 displUV8 = panner6;
				float2 break421 = (float2( -1,-1 ) + ((UnpackNormal( tex2D( _DistortionGuide, displUV8 ) )).xy - float2( 0,0 )) * (float2( 1,1 ) - float2( -1,-1 )) / (float2( 1,1 ) - float2( 0,0 )));
				float4 appendResult424 = (float4(( break421.x * _DistortionAmountX ) , ( break421.y * _DistortionAmountY ) , 0.0 , 0.0));
				float4 distortionUV24 = appendResult424;
				float lerpResult28 = lerp( 0.5 , tex2D( _MainTex, ( float4( uv17, 0.0 , 0.0 ) + distortionUV24 ).xy ).r , _Contrast);
				float temp_output_31_0 = pow( saturate( lerpResult28 ) , _Power );
				float2 appendResult149 = (float2(_SecondaryPanningSpeedX , _SecondaryPanningSpeedY));
				float2 uv2_SecondaryTex = i.ase_texcoord1.xy * _SecondaryTex_ST.xy + _SecondaryTex_ST.zw;
				float2 temp_output_4_0_g15 = ( ( uv2_SecondaryTex * float2( 2,2 ) ) - float2( 1,1 ) );
				float2 break8_g15 = temp_output_4_0_g15;
				float2 appendResult16_g15 = (float2(( ( ( atan2( break8_g15.y , break8_g15.x ) / UNITY_PI ) / 2.0 ) + 0.5 ) , length( temp_output_4_0_g15 )));
				#ifdef _USE_POLAR_COORDINATES_ON
				float2 staticSwitch152 = appendResult16_g15;
				#else
				float2 staticSwitch152 = uv2_SecondaryTex;
				#endif
				float2 panner153 = ( 1.0 * _Time.y * appendResult149 + staticSwitch152);
				float2 secondaryUV154 = panner153;
				float lerpResult161 = lerp( 0.5 , tex2D( _SecondaryTex, ( distortionUV24 + float4( secondaryUV154, 0.0 , 0.0 ) ).xy ).r , _Contrast);
				#ifdef _HASSECONDARYTEX_ON
				float staticSwitch145 = ( temp_output_31_0 * 2.0 * pow( saturate( lerpResult161 ) , _Power ) );
				#else
				float staticSwitch145 = temp_output_31_0;
				#endif
				float col32 = staticSwitch145;
				float2 uv0371 = i.ase_texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float temp_output_199_0 = distance( uv0371 , float2( 0.5,0.5 ) );
				float smoothstepResult205 = smoothstep( _OuterRadius , ( _OuterRadius + _Smoothness ) , temp_output_199_0);
				float smoothstepResult207 = smoothstep( _InnerRadius , ( _Smoothness + _InnerRadius ) , temp_output_199_0);
				#ifdef _CIRCLEMASK_ON
				float staticSwitch213 = ( col32 * ( 1.0 - smoothstepResult205 ) * smoothstepResult207 );
				#else
				float staticSwitch213 = col32;
				#endif
				float colCircleMask212 = staticSwitch213;
				float2 uv0281 = i.ase_texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float2 temp_output_283_0 = ( ( uv0281 * float2( 2,0 ) ) - float2( 1,1 ) );
				float smoothstepResult302 = smoothstep( _RectMaskCutoff , ( _RectMaskCutoff + _RectSmothness ) , max( abs( ( (temp_output_283_0).x / _RectWidth ) ) , abs( ( (temp_output_283_0).y / _RectHeight ) ) ));
				#ifdef _RECTMASK_ON
				float staticSwitch295 = ( colCircleMask212 * ( 1.0 - smoothstepResult302 ) );
				#else
				float staticSwitch295 = colCircleMask212;
				#endif
				float colRectMask307 = staticSwitch295;
				float temp_output_238_0 = saturate( ( _ClipAmount + ( 1.0 - i.ase_color.a ) ) );
				float cutoff274 = temp_output_238_0;
				float temp_output_243_0 = ( colRectMask307 - cutoff274 );
				float smoothstepResult245 = smoothstep( temp_output_243_0 , ( temp_output_243_0 + _ClipSoftness ) , _EdgeSize);
				float smoothstepResult246 = smoothstep( 0.001 , 0.5 , cutoff274);
				float4 edgeColor251 = ( smoothstepResult245 * smoothstepResult246 * _EdgeColor );
				#ifdef _BANDING_ON
				float staticSwitch221 = ( round( ( colRectMask307 * _Bands ) ) / _Bands );
				#else
				float staticSwitch221 = colRectMask307;
				#endif
				float colBanding222 = staticSwitch221;
				float2 appendResult250 = (float2(colBanding222 , 0.0));
				float4 tex2DNode255 = tex2D( _GradientMap, appendResult250 );
				float smoothstepResult254 = smoothstep( cutoff274 , ( temp_output_238_0 + _ClipSoftness ) , colRectMask307);
				float alpha258 = smoothstepResult254;
				float temp_output_266_0 = ( alpha258 * _Color.a * tex2D( _MainTex, ( float4( uv17, 0.0 , 0.0 ) + distortionUV24 ).xy ).a );
				#ifdef _HASSECONDARYTEX_ON
				float staticSwitch413 = temp_output_266_0;
				#else
				float staticSwitch413 = ( i.ase_color.a * temp_output_266_0 );
				#endif
				float4 appendResult267 = (float4(( (( edgeColor251 + tex2DNode255 )).rgb * tex2DNode255.a * (i.ase_color).rgb * (_Color).rgb ) , staticSwitch413));
				float4 finalColor268 = appendResult267;
				float3 ase_worldPos = i.ase_texcoord2.xyz;
				float3 ase_worldViewDir = UnityWorldSpaceViewDir(ase_worldPos);
				ase_worldViewDir = normalize(ase_worldViewDir);
				float3 ase_worldNormal = i.ase_texcoord3.xyz;
				float fresnelNdotV381 = dot( ase_worldNormal, ase_worldViewDir );
				float fresnelNode381 = ( 0.0 + _FresnelIntensity * pow( 1.0 - fresnelNdotV381, (10.0 + (_FresnelPower - 0.0) * (0.0 - 10.0) / (10.0 - 0.0)) ) );
				#ifdef _USEFRESNEL_ON
				float4 staticSwitch384 = ( finalColor268 + ( fresnelNode381 * _FresnelColor ) );
				#else
				float4 staticSwitch384 = finalColor268;
				#endif
				float4 colFresnel387 = staticSwitch384;
				float4 screenPos = i.ase_texcoord4;
				float4 ase_screenPosNorm = screenPos / screenPos.w;
				ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
				float eyeDepth327 = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE( _CameraDepthTexture, ase_screenPosNorm.xy ));
				#ifdef _SOFTBLEND_ON
				float4 staticSwitch309 = ( colFresnel387 * saturate( ( ( eyeDepth327 - screenPos.w ) * _IntersectionThresholdMax ) ) );
				#else
				float4 staticSwitch309 = colFresnel387;
				#endif
				float4 colSoftBlend320 = staticSwitch309;
				float screenDepth395 = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE( _CameraDepthTexture, ase_screenPosNorm.xy ));
				float distanceDepth395 = saturate( abs( ( screenDepth395 - LinearEyeDepth( ase_screenPosNorm.z ) ) / ( _IntersectionSize ) ) );
				float4 lerpResult420 = lerp( _IntersectionColor , ( _IntersectionColor * colSoftBlend320 ) , distanceDepth395);
				float4 lerpResult398 = lerp( _IntersectionColor , colSoftBlend320 , distanceDepth395);
				#ifdef _USEDEPTH_ON
				float4 staticSwitch401 = (( _MultiplyByMainColor )?( ( colSoftBlend320 * lerpResult398 ) ):( lerpResult420 ));
				#else
				float4 staticSwitch401 = colSoftBlend320;
				#endif
				float4 colDepthInter402 = staticSwitch401;
				
				
				finalColor = colDepthInter402;
				return finalColor;
			}
			ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=17700
375;70;1400;825;3546.146;55.39899;1;True;False
Node;AmplifyShaderEditor.CommentaryNode;1;-4190.189,-1181.141;Inherit;False;1600.313;1104.547;UV Panning;26;150;5;14;84;154;17;153;15;152;149;12;132;147;10;151;148;143;11;8;6;144;4;142;3;2;369;UV Panning;1,1,1,1;0;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;5;-4087.221,-794.2773;Inherit;False;1;13;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FunctionNode;142;-3781.704,-731.1445;Inherit;False;PolarCoords;-1;;7;d4c3818101f88c4418358bdc95446521;0;1;2;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;3;-3636.644,-654.6233;Inherit;False;Property;_DistortionPanningSpeedX;Distortion Panning Speed X;27;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;2;-3634.396,-569.5286;Inherit;False;Property;_DistortionPanningSpeedY;Distortion Panning Speed Y;28;0;Create;True;0;0;False;0;0;-0.2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;144;-3497.593,-798.9883;Inherit;False;Property;_USE_POLAR_COORDINATES;USE POLAR COORDINATES;33;0;Create;False;0;0;False;2;Space(20);Header(Polar Coords);0;0;0;False;;Toggle;2;Key0;Key1;Reference;132;False;9;1;FLOAT2;0,0;False;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT2;0,0;False;6;FLOAT2;0,0;False;7;FLOAT2;0,0;False;8;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;4;-3300.819,-624.7245;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;6;-3080.312,-695.4216;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;8;-2865.103,-699.7115;Inherit;False;displUV;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.CommentaryNode;7;-4150.831,97.80959;Inherit;False;2104.571;418.1203;Distortion;11;18;16;13;24;20;19;9;421;422;423;424;Distortion;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;9;-4108.228,187.6396;Inherit;False;8;displUV;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;13;-3931.081,170.5517;Inherit;True;Property;_DistortionGuide;Distortion Guide;26;0;Create;True;0;0;False;2;Space(20);Header(Distortion);-1;e28dc97a9541e3642a48c0e3886688c5;9f51ad018f6f5c84587e54a85203804b;True;2;False;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;16;-3606.546,170.0227;Inherit;False;True;True;False;False;1;0;FLOAT3;0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;14;-4119.878,-1121.295;Inherit;False;0;27;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;150;-4100.842,-431.7417;Inherit;False;2;146;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCRemapNode;18;-3334.275,165.5737;Inherit;False;5;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT2;1,1;False;3;FLOAT2;-1,-1;False;4;FLOAT2;1,1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.BreakToComponentsNode;421;-3135.146,166.601;Inherit;False;FLOAT2;1;0;FLOAT2;0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.RangedFloatNode;422;-3165.146,320.601;Inherit;False;Property;_DistortionAmountX;Distortion Amount X;29;0;Create;True;0;0;False;0;0;0.1;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;19;-3167.295,403.5878;Inherit;False;Property;_DistortionAmountY;Distortion Amount Y;30;0;Create;True;0;0;False;0;0;0.1;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;11;-3566.437,-930.4778;Inherit;False;Property;_MainTexPanningSpeedY;Main Tex Panning Speed Y;3;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;148;-3644.798,-185.3682;Inherit;False;Property;_SecondaryPanningSpeedY;Secondary Panning Speed Y;7;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;147;-3647.044,-270.463;Inherit;False;Property;_SecondaryPanningSpeedX;Secondary Panning Speed X;6;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;10;-3568.685,-1015.572;Inherit;False;Property;_MainTexPanningSpeedX;Main Tex Panning Speed X;2;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;151;-3795.323,-362.0917;Inherit;False;PolarCoords;-1;;15;d4c3818101f88c4418358bdc95446521;0;1;2;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.FunctionNode;143;-3769.874,-1061.367;Inherit;False;PolarCoords;-1;;14;d4c3818101f88c4418358bdc95446521;0;1;2;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;-2721.063,174.7473;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;423;-2721.146,325.601;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;132;-3425.009,-1137.88;Inherit;False;Property;_USE_POLAR_COORDINATES;USE POLAR COORDINATES;33;0;Create;False;0;0;False;2;Space(20);Header(Polar Coords);0;0;0;True;;Toggle;2;Key0;Key1;Create;False;9;1;FLOAT2;0,0;False;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT2;0,0;False;6;FLOAT2;0,0;False;7;FLOAT2;0,0;False;8;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;149;-3311.217,-240.5641;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.StaticSwitch;152;-3497.467,-435.3159;Inherit;False;Property;_USE_POLAR_COORDINATES;USE POLAR COORDINATES;33;0;Create;False;0;0;False;2;Space(20);Header(Polar Coords);0;0;0;True;;Toggle;2;Key0;Key1;Reference;132;False;9;1;FLOAT2;0,0;False;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT2;0,0;False;6;FLOAT2;0,0;False;7;FLOAT2;0,0;False;8;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;12;-3274.058,-1000.066;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;15;-3039.617,-1087.522;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;153;-3089.464,-383.1147;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;424;-2491.146,189.601;Inherit;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;17;-2822.872,-1091.519;Inherit;False;uv;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;154;-2870.387,-386.3038;Inherit;False;secondaryUV;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.CommentaryNode;23;-4144.542,617.3571;Inherit;False;2034.192;629.8216;Contrast and Power;19;32;145;166;31;164;177;29;162;30;28;161;26;27;146;192;191;195;25;190;Contrast and Power;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;24;-2264.317,189.6672;Inherit;False;distortionUV;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;195;-4116.427,1045.988;Inherit;False;154;secondaryUV;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;190;-4087.729,688.0831;Inherit;False;17;uv;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;25;-4120.774,877.8074;Inherit;False;24;distortionUV;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleAddOpNode;191;-3867.044,704.4755;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleAddOpNode;192;-3868.729,1045.705;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT2;0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;26;-3603.877,906.4678;Inherit;False;Property;_Contrast;Contrast;10;0;Create;True;0;0;False;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;27;-3707.076,672.5087;Inherit;True;Property;_MainTex;Main Tex;1;0;Create;True;0;0;False;2;Space(20);Header(Main Tex);-1;e28dc97a9541e3642a48c0e3886688c5;631aa9b7a0f217e48adf036c22ba527a;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;146;-3712.82,1039.44;Inherit;True;Property;_SecondaryTex;Secondary Tex;5;0;Create;True;0;0;True;0;-1;cd460ee4ac5c1e746b7a734cc7cc64dd;cd460ee4ac5c1e746b7a734cc7cc64dd;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;28;-3380.435,702.2407;Inherit;False;3;0;FLOAT;0.5;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;161;-3368.307,1046.974;Inherit;False;3;0;FLOAT;0.5;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;30;-3203.917,900.5079;Inherit;False;Property;_Power;Power;11;0;Create;True;0;0;False;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;162;-3185.407,1070.871;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;308;-1553.603,-1122.854;Inherit;False;2488.026;593.5007;Rectangle Mask;21;282;281;301;291;297;298;285;290;299;284;300;305;303;306;283;296;302;295;307;304;294;Rectangle Mask;1,1,1,1;0;0
Node;AmplifyShaderEditor.SaturateNode;29;-3184.042,704.5511;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;31;-2994.521,703.7397;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;164;-2995.886,1070.06;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;281;-1503.603,-984.6995;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;177;-2963.479,901.0129;Inherit;False;Constant;_Float2;Float 2;24;0;Create;True;0;0;False;0;2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;214;-1577.123,-1941.62;Inherit;False;1678.742;550.1378;Circle Mask;14;198;196;197;199;206;205;208;211;209;207;210;213;212;371;Circle mask;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;196;-1514.197,-1890.463;Inherit;False;Property;_OuterRadius;Outer Radius;37;0;Create;True;0;0;False;0;0.5;0.5;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;371;-1500.047,-1782.556;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;166;-2796.202,881.7159;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;198;-1527.123,-1640.408;Inherit;False;Property;_Smoothness;Smoothness;39;0;Create;True;0;0;False;0;0.2;0.2;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;282;-1252.813,-978.2691;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;2,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.StaticSwitch;145;-2638.631,714.5441;Inherit;False;Property;_HASSECONDARYTEX;HAS SECONDARY TEX;4;0;Create;True;0;0;False;2;Space(20);Header(Secondary Tex);0;0;0;True;;Toggle;2;Key0;Key1;Create;False;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;283;-1068.749,-977.2557;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;1,1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;197;-1521.817,-1523.926;Inherit;False;Property;_InnerRadius;Inner Radius;38;0;Create;True;0;0;False;0;0;0;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;199;-1189.037,-1708.017;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;206;-1180.817,-1813.008;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;205;-994.9518,-1871.166;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;208;-1132.367,-1506.55;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;285;-837.6102,-977.7801;Inherit;False;Property;_RectWidth;RectWidth;41;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;290;-842.4625,-778.0399;Inherit;False;Property;_RectHeight;RectHeight;42;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;297;-868.1254,-871.8031;Inherit;False;False;True;True;True;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;296;-866.6989,-1072.854;Inherit;False;True;False;True;True;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;32;-2328.596,712.934;Inherit;False;col;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;209;-766.1276,-1718.131;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;211;-779.5085,-1819.177;Inherit;False;32;col;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;298;-615.3495,-1030.467;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;299;-621.3495,-858.4667;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;207;-975.0293,-1632.886;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;210;-556.3851,-1738.013;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.AbsOpNode;300;-462.3495,-857.4667;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;291;-550.9013,-751.7255;Inherit;False;Property;_RectMaskCutoff;RectMaskCutoff;43;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;294;-534.8961,-644.3527;Inherit;False;Property;_RectSmothness;RectSmothness;44;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.AbsOpNode;284;-461.8871,-1028.104;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;213;-416.6012,-1812.05;Inherit;False;Property;_CIRCLEMASK;CIRCLE MASK;36;0;Create;True;0;0;False;2;Space(20);Header(Circle Mask);0;0;0;True;;Toggle;2;Key0;Key1;Create;False;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;303;-304.3496,-966.4666;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;304;-229.3496,-690.4877;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;302;-87.34956,-882.4667;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;212;-146.0663,-1813.01;Inherit;False;colCircleMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;305;88.20785,-886.3553;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;232;-1510.935,296.5246;Inherit;False;2213.412;825.7174;Transparency;22;251;248;247;246;245;242;258;254;240;270;237;243;238;239;236;235;234;233;274;275;277;278;Transparency;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;301;58.00077,-1008.432;Inherit;False;212;colCircleMask;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.VertexColorNode;233;-1396.034,430.9652;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;306;284.4746,-941.1273;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;234;-1403.838,338.5547;Inherit;False;Property;_ClipAmount;Clip Amount;16;0;Create;True;0;0;False;2;Space(20);Header(Clipping);0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;295;443.7487,-1008.535;Inherit;False;Property;_RECTMASK;RECT MASK;40;0;Create;True;0;0;False;2;Space(20);Header(Rect Mask);0;0;0;True;;Toggle;2;Key0;Key1;Create;False;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;235;-1197.595,519.7537;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;223;-1507.734,-313.9757;Inherit;False;1204.95;292.407;Banding;7;218;217;219;216;220;221;222;Banding;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;307;691.4228,-1009.052;Inherit;False;colRectMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;236;-1006.475,468.9662;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;217;-1457.734,-261.4277;Inherit;False;307;colRectMask;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;238;-871.839,467.7123;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;218;-1424.721,-136.1686;Inherit;False;Property;_Bands;Bands;35;0;Create;True;0;0;False;0;3;3;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;274;-655.5857,464.3092;Inherit;False;cutoff;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;219;-1242.111,-191.9247;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;277;-674.7813,750.4597;Inherit;False;274;cutoff;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;275;-698.113,671.9752;Inherit;False;307;colRectMask;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RoundOpNode;216;-1088.179,-177.8376;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;243;-474.6973,655.9039;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;237;-1044.355,779.742;Inherit;False;Property;_ClipSoftness;Clip Softness;17;0;Create;True;0;0;False;0;0;0.15;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;220;-903.353,-157.1557;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;242;-322.0307,549.3991;Inherit;False;Property;_EdgeSize;Edge Size;19;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;221;-751.2318,-260.0777;Inherit;False;Property;_BANDING;BANDING;34;0;Create;True;0;0;False;2;Space(20);Header(Banding);0;0;0;True;;Toggle;2;Key0;Key1;Create;False;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;270;-329.3106,767.3249;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;278;-152.9695,788.6412;Inherit;False;274;cutoff;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;240;-705.4165,573.7971;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;246;34.40163,788.9721;Inherit;False;3;0;FLOAT;0.001;False;1;FLOAT;0.001;False;2;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;247;-44.96908,915.0139;Inherit;False;Property;_EdgeColor;Edge Color;18;1;[HDR];Create;True;0;0;False;0;1,1,1,1;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;244;1064.896,-1962.026;Inherit;False;2592.984;933.9333;Coloring;22;268;267;265;266;375;262;263;264;261;257;374;259;260;256;253;252;255;250;249;413;415;416;Coloring;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;239;-639.8317,379.9024;Inherit;False;307;colRectMask;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;245;13.1473,550.1315;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;222;-509.6558,-257.7237;Inherit;False;colBanding;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;248;244.4537,749.9161;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;253;1489.817,-1176.595;Inherit;False;24;distortionUV;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SmoothstepOpNode;254;-326.8975,420.6249;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;249;1099.994,-1822.146;Inherit;False;222;colBanding;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;252;1509.693,-1262.676;Inherit;False;17;uv;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;258;-115.0452,418.2986;Inherit;False;alpha;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;260;1774.312,-1231.19;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.DynamicAppendNode;250;1298.141,-1816.325;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;251;455.9549,764.2226;Inherit;False;edgeColor;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;263;1927.735,-1257.922;Inherit;True;Property;_TextureSample1;Texture Sample 1;1;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Instance;27;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;257;1536.518,-1437.997;Inherit;False;Property;_Color;Color;9;1;[HDR];Create;True;0;0;False;0;1,1,1,1;0.3490566,0.004939471,0.02212816,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;261;2023.898,-1421.955;Inherit;False;258;alpha;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;256;1546.236,-1920.014;Inherit;False;251;edgeColor;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;255;1446.809,-1839.167;Inherit;True;Property;_GradientMap;Gradient Map;8;0;Create;True;0;0;False;2;Space(20);Header(Color);-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;259;1757.177,-1855.859;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.VertexColorNode;374;1561.142,-1620.324;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.VertexColorNode;415;2291.887,-1494.46;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;266;2323.474,-1316.895;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;262;1893.883,-1861.967;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ComponentMaskNode;264;1783.512,-1440.346;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ComponentMaskNode;375;1786.984,-1616.254;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;416;2503.787,-1406.06;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;392;1115.725,-870.1198;Inherit;False;1940.684;520.7281;Fresnel;10;410;381;388;390;385;387;384;391;382;412;Fresnel;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;265;2128.994,-1698.597;Inherit;False;4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StaticSwitch;413;2695.805,-1347.91;Inherit;False;Property;_USEFRESNEL1;USE FRESNEL;4;0;Create;True;0;0;False;0;0;0;0;True;;Toggle;2;Key0;Key1;Reference;145;False;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;385;1133.225,-587.4473;Inherit;False;Property;_FresnelPower;Fresnel Power;14;0;Create;True;0;0;False;0;0;0;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;267;2992.574,-1430.484;Inherit;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;1;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;390;1345.66,-714.5048;Inherit;False;Property;_FresnelIntensity;Fresnel Intensity;13;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;388;1428.42,-587.0897;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;10;False;3;FLOAT;10;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;268;3172.734,-1434.944;Inherit;False;finalColor;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.ColorNode;412;1699.827,-532.6616;Inherit;False;Property;_FresnelColor;Fresnel Color;15;1;[HDR];Create;True;0;0;False;0;1,1,1,1;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FresnelNode;381;1654.307,-702.711;Inherit;False;Standard;WorldNormal;ViewDir;False;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;382;1698.451,-818.726;Inherit;False;268;finalColor;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.CommentaryNode;328;1083.545,-165.4134;Inherit;False;1603.444;436.5707;Soft Bend;10;320;309;316;315;319;318;314;317;327;313;Soft Blend;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;410;1959.693,-696.5314;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;313;1172.767,-14.91917;Float;False;1;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;391;2148.802,-753.0813;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;369;-3112.664,-218.157;Inherit;False;SecTexPanSpeed;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.CommentaryNode;127;-4214.408,-2236.198;Inherit;False;2590.57;834.4979;Vertex Offset;22;100;106;94;108;99;112;107;370;368;366;359;85;358;350;349;357;356;121;120;89;355;354;Vertex Offset;1,1,1,1;0;0
Node;AmplifyShaderEditor.ScreenDepthNode;327;1183.545,-98.4133;Inherit;False;0;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;84;-3026.462,-858.3481;Inherit;False;MainTexPanSpeed;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.StaticSwitch;384;2371.938,-818.8394;Inherit;False;Property;_USEFRESNEL;USE FRESNEL;12;0;Create;True;0;0;False;2;Space(20);Header(Fresnel);0;0;0;True;;Toggle;2;Key0;Key1;Create;False;9;1;FLOAT4;0,0,0,0;False;0;FLOAT4;0,0,0,0;False;2;FLOAT4;0,0,0,0;False;3;FLOAT4;0,0,0,0;False;4;FLOAT4;0,0,0,0;False;5;FLOAT4;0,0,0,0;False;6;FLOAT4;0,0,0,0;False;7;FLOAT4;0,0,0,0;False;8;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;317;1343.986,162.2847;Inherit;False;Property;_IntersectionThresholdMax;IntersectionThresholdMax;21;0;Create;True;0;0;False;0;1;1;0.1;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;89;-4178.41,-2109.968;Inherit;False;84;MainTexPanSpeed;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleTimeNode;354;-4139.423,-1655.403;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;120;-4135.772,-2018.949;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;314;1471.837,-20.09258;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;355;-4182.061,-1746.422;Inherit;False;369;SecTexPanSpeed;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;357;-4055.231,-1573.38;Inherit;False;2;146;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;349;-4018.045,-1946.507;Inherit;False;0;27;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;318;1663.616,58.87519;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;387;2657.773,-820.1197;Inherit;False;colFresnel;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;356;-3934.772,-1725.803;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;121;-3931.122,-2089.349;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;350;-3754.142,-2041.407;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;315;1778.306,-57.69566;Inherit;False;387;colFresnel;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleAddOpNode;358;-3757.793,-1677.861;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SaturateNode;319;1825.311,62.63581;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;85;-3594.974,-2095.733;Inherit;True;Property;_TextureSample0;Texture Sample 0;1;0;Create;True;0;0;False;0;-1;e28dc97a9541e3642a48c0e3886688c5;e28dc97a9541e3642a48c0e3886688c5;True;0;False;white;Auto;False;Instance;27;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;316;2018.968,15.63087;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SamplerNode;359;-3598.625,-1732.188;Inherit;True;Property;_TextureSample2;Texture Sample 2;5;0;Create;True;0;0;False;0;-1;e28dc97a9541e3642a48c0e3886688c5;e28dc97a9541e3642a48c0e3886688c5;True;0;False;white;Auto;False;Instance;146;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;409;1083.119,547.0963;Inherit;False;1788.337;574.0727;Depth Intersection;11;418;401;419;420;398;407;394;402;408;395;400;Depth Intersection;1,1,1,1;0;0
Node;AmplifyShaderEditor.StaticSwitch;309;2208.289,-53.32035;Inherit;False;Property;_SOFTBLEND;SOFT BLEND;20;0;Create;True;0;0;False;2;Space(20);Header(Soft Blend);0;0;0;True;;Toggle;2;Key0;Key1;Create;False;9;1;FLOAT4;0,0,0,0;False;0;FLOAT4;0,0,0,0;False;2;FLOAT4;0,0,0,0;False;3;FLOAT4;0,0,0,0;False;4;FLOAT4;0,0,0,0;False;5;FLOAT4;0,0,0,0;False;6;FLOAT4;0,0,0,0;False;7;FLOAT4;0,0,0,0;False;8;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;366;-3273.669,-1809.749;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;368;-3114.585,-1816.09;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;320;2461.053,-47.62817;Inherit;False;colSoftBlend;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;400;1133.119,972.5692;Inherit;False;Property;_IntersectionSize;Intersection Size;23;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;394;1327.789,795.0858;Inherit;False;320;colSoftBlend;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.StaticSwitch;370;-2962.988,-1895.138;Inherit;False;Property;_USEVERTEXOFFSET;USE VERTEX OFFSET;4;0;Create;True;0;0;False;2;Space(20);Header(Vertex Offset);0;0;0;True;;Toggle;2;Key0;Key1;Reference;145;False;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;408;1323.7,594.3297;Inherit;False;Property;_IntersectionColor;Intersection Color;25;1;[HDR];Create;True;0;0;False;0;1,1,1,1;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DepthFade;395;1343.146,953.5667;Inherit;False;True;True;True;2;1;FLOAT3;0,0,0;False;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;107;-2652.332,-1883.304;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;398;1621.194,902.5289;Inherit;False;3;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;419;1636.068,708.461;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;108;-2490.753,-1883.304;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.NormalVertexDataNode;99;-2565.48,-1646.917;Inherit;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;112;-2645.263,-1746.479;Inherit;False;Property;_VertexOffsetAmount;Vertex Offset Amount;32;0;Create;True;0;0;False;0;0;0;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;407;1842.398,860.6329;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.LerpOp;420;1821.668,668.4608;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;94;-2305.507,-1805.294;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ToggleSwitchNode;418;2048.867,823.6609;Inherit;False;Property;_MultiplyByMainColor;Multiply By Main Color;24;0;Create;True;0;0;False;0;0;2;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.StaticSwitch;401;2301.37,733.5895;Inherit;False;Property;_USEDEPTH;USE DEPTH;22;0;Create;True;0;0;False;2;Space(20);Header(DEPTHl);0;0;0;True;;Toggle;2;Key0;Key1;Create;False;9;1;FLOAT4;0,0,0,0;False;0;FLOAT4;0,0,0,0;False;2;FLOAT4;0,0,0,0;False;3;FLOAT4;0,0,0,0;False;4;FLOAT4;0,0,0,0;False;5;FLOAT4;0,0,0,0;False;6;FLOAT4;0,0,0,0;False;7;FLOAT4;0,0,0,0;False;8;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.StaticSwitch;106;-2161.149,-1831.221;Inherit;False;Property;_USEVERTEXOFFSET;USE VERTEX OFFSET;31;0;Create;True;0;0;False;2;Space(20);Header(Vertex Offset);0;0;0;True;;Toggle;2;Key0;Key1;Create;False;9;1;FLOAT3;0,0,0;False;0;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT3;0,0,0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;402;2606.857,772.031;Inherit;False;colDepthInter;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;100;-1872.196,-1831.508;Inherit;False;VertexOffset;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;58;3595.41,-77.68436;Inherit;False;Property;_CullMode;Cull Mode;0;1;[Enum];Create;True;0;1;UnityEngine.Rendering.CullMode;True;1;Space(10);2;2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;346;3128.637,-39.766;Inherit;False;100;VertexOffset;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;60;3137.525,-120.8984;Inherit;False;402;colDepthInter;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;0;3368.908,-87.68886;Float;False;True;-1;2;ASEMaterialInspector;100;1;VFX/MasterShaderTransparent;0770190933193b94aaa3065e307002fa;True;Unlit;0;0;Unlit;2;True;2;5;False;-1;10;False;-1;0;5;False;-1;10;False;-1;True;0;False;-1;0;False;-1;True;False;True;0;True;58;True;True;True;True;False;0;False;-1;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;True;2;False;-1;True;3;False;-1;True;True;-1;False;-1;-1;False;-1;True;4;RenderType=Transparent=RenderType;Queue=Transparent=Queue=0;IgnoreProjector=True;PreviewType=Plane;True;2;0;False;False;False;False;False;False;False;False;False;True;2;LightMode=ForwardBase;=;False;2;Include;;False;;Native;Pragma;multi_compile_fog;False;;Custom;;0;0;Standard;1;Vertex Position,InvertActionOnDeselection;1;0;1;True;False;;0
WireConnection;142;2;5;0
WireConnection;144;1;5;0
WireConnection;144;0;142;0
WireConnection;4;0;3;0
WireConnection;4;1;2;0
WireConnection;6;0;144;0
WireConnection;6;2;4;0
WireConnection;8;0;6;0
WireConnection;13;1;9;0
WireConnection;16;0;13;0
WireConnection;18;0;16;0
WireConnection;421;0;18;0
WireConnection;151;2;150;0
WireConnection;143;2;14;0
WireConnection;20;0;421;0
WireConnection;20;1;422;0
WireConnection;423;0;421;1
WireConnection;423;1;19;0
WireConnection;132;1;14;0
WireConnection;132;0;143;0
WireConnection;149;0;147;0
WireConnection;149;1;148;0
WireConnection;152;1;150;0
WireConnection;152;0;151;0
WireConnection;12;0;10;0
WireConnection;12;1;11;0
WireConnection;15;0;132;0
WireConnection;15;2;12;0
WireConnection;153;0;152;0
WireConnection;153;2;149;0
WireConnection;424;0;20;0
WireConnection;424;1;423;0
WireConnection;17;0;15;0
WireConnection;154;0;153;0
WireConnection;24;0;424;0
WireConnection;191;0;190;0
WireConnection;191;1;25;0
WireConnection;192;0;25;0
WireConnection;192;1;195;0
WireConnection;27;1;191;0
WireConnection;146;1;192;0
WireConnection;28;1;27;1
WireConnection;28;2;26;0
WireConnection;161;1;146;1
WireConnection;161;2;26;0
WireConnection;162;0;161;0
WireConnection;29;0;28;0
WireConnection;31;0;29;0
WireConnection;31;1;30;0
WireConnection;164;0;162;0
WireConnection;164;1;30;0
WireConnection;166;0;31;0
WireConnection;166;1;177;0
WireConnection;166;2;164;0
WireConnection;282;0;281;0
WireConnection;145;1;31;0
WireConnection;145;0;166;0
WireConnection;283;0;282;0
WireConnection;199;0;371;0
WireConnection;206;0;196;0
WireConnection;206;1;198;0
WireConnection;205;0;199;0
WireConnection;205;1;196;0
WireConnection;205;2;206;0
WireConnection;208;0;198;0
WireConnection;208;1;197;0
WireConnection;297;0;283;0
WireConnection;296;0;283;0
WireConnection;32;0;145;0
WireConnection;209;0;205;0
WireConnection;298;0;296;0
WireConnection;298;1;285;0
WireConnection;299;0;297;0
WireConnection;299;1;290;0
WireConnection;207;0;199;0
WireConnection;207;1;197;0
WireConnection;207;2;208;0
WireConnection;210;0;211;0
WireConnection;210;1;209;0
WireConnection;210;2;207;0
WireConnection;300;0;299;0
WireConnection;284;0;298;0
WireConnection;213;1;211;0
WireConnection;213;0;210;0
WireConnection;303;0;284;0
WireConnection;303;1;300;0
WireConnection;304;0;291;0
WireConnection;304;1;294;0
WireConnection;302;0;303;0
WireConnection;302;1;291;0
WireConnection;302;2;304;0
WireConnection;212;0;213;0
WireConnection;305;0;302;0
WireConnection;306;0;301;0
WireConnection;306;1;305;0
WireConnection;295;1;301;0
WireConnection;295;0;306;0
WireConnection;235;0;233;4
WireConnection;307;0;295;0
WireConnection;236;0;234;0
WireConnection;236;1;235;0
WireConnection;238;0;236;0
WireConnection;274;0;238;0
WireConnection;219;0;217;0
WireConnection;219;1;218;0
WireConnection;216;0;219;0
WireConnection;243;0;275;0
WireConnection;243;1;277;0
WireConnection;220;0;216;0
WireConnection;220;1;218;0
WireConnection;221;1;217;0
WireConnection;221;0;220;0
WireConnection;270;0;243;0
WireConnection;270;1;237;0
WireConnection;240;0;238;0
WireConnection;240;1;237;0
WireConnection;246;0;278;0
WireConnection;245;0;242;0
WireConnection;245;1;243;0
WireConnection;245;2;270;0
WireConnection;222;0;221;0
WireConnection;248;0;245;0
WireConnection;248;1;246;0
WireConnection;248;2;247;0
WireConnection;254;0;239;0
WireConnection;254;1;274;0
WireConnection;254;2;240;0
WireConnection;258;0;254;0
WireConnection;260;0;252;0
WireConnection;260;1;253;0
WireConnection;250;0;249;0
WireConnection;251;0;248;0
WireConnection;263;1;260;0
WireConnection;255;1;250;0
WireConnection;259;0;256;0
WireConnection;259;1;255;0
WireConnection;266;0;261;0
WireConnection;266;1;257;4
WireConnection;266;2;263;4
WireConnection;262;0;259;0
WireConnection;264;0;257;0
WireConnection;375;0;374;0
WireConnection;416;0;415;4
WireConnection;416;1;266;0
WireConnection;265;0;262;0
WireConnection;265;1;255;4
WireConnection;265;2;375;0
WireConnection;265;3;264;0
WireConnection;413;1;416;0
WireConnection;413;0;266;0
WireConnection;267;0;265;0
WireConnection;267;3;413;0
WireConnection;388;0;385;0
WireConnection;268;0;267;0
WireConnection;381;2;390;0
WireConnection;381;3;388;0
WireConnection;410;0;381;0
WireConnection;410;1;412;0
WireConnection;391;0;382;0
WireConnection;391;1;410;0
WireConnection;369;0;149;0
WireConnection;84;0;12;0
WireConnection;384;1;382;0
WireConnection;384;0;391;0
WireConnection;314;0;327;0
WireConnection;314;1;313;4
WireConnection;318;0;314;0
WireConnection;318;1;317;0
WireConnection;387;0;384;0
WireConnection;356;0;355;0
WireConnection;356;1;354;0
WireConnection;121;0;89;0
WireConnection;121;1;120;0
WireConnection;350;0;121;0
WireConnection;350;1;349;0
WireConnection;358;0;356;0
WireConnection;358;1;357;0
WireConnection;319;0;318;0
WireConnection;85;1;350;0
WireConnection;316;0;315;0
WireConnection;316;1;319;0
WireConnection;359;1;358;0
WireConnection;309;1;315;0
WireConnection;309;0;316;0
WireConnection;366;0;85;1
WireConnection;366;1;359;1
WireConnection;368;0;366;0
WireConnection;320;0;309;0
WireConnection;370;1;85;1
WireConnection;370;0;368;0
WireConnection;395;0;400;0
WireConnection;107;0;370;0
WireConnection;398;0;408;0
WireConnection;398;1;394;0
WireConnection;398;2;395;0
WireConnection;419;0;408;0
WireConnection;419;1;394;0
WireConnection;108;0;107;0
WireConnection;407;0;394;0
WireConnection;407;1;398;0
WireConnection;420;0;408;0
WireConnection;420;1;419;0
WireConnection;420;2;395;0
WireConnection;94;0;108;0
WireConnection;94;1;112;0
WireConnection;94;2;99;0
WireConnection;418;0;420;0
WireConnection;418;1;407;0
WireConnection;401;1;394;0
WireConnection;401;0;418;0
WireConnection;106;0;94;0
WireConnection;402;0;401;0
WireConnection;100;0;106;0
WireConnection;0;0;60;0
WireConnection;0;1;346;0
ASEEND*/
//CHKSM=3035154569D3FE93E3360C60827F0469B631B3F0