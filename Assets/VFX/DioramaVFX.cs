﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using TMPro;
using MoreMountains.Feedbacks;
using Doozy.Engine;

public class DioramaVFX : SerializedMonoBehaviour
{
    public Transform buttonsParent;
    public GameObject buttonPrefab;

    public List<GameObject> VFXCollection;
    public List<Vector3> VFXCollectionPos;

    // Start is called before the first frame update
    void Start()
    {
        buttonsParent.gameObject.transform.parent.gameObject.SetActive(true);

        foreach (Transform t in transform)
        {
            Button b = Instantiate(buttonPrefab, buttonsParent).GetComponent<Button>();
            b.name = t.name + " Button";
            b.GetComponentInChildren<TextMeshProUGUI>().text = t.name;
            t.GetComponentInChildren<TextMeshPro>().text = t.name;

            if (t.gameObject.activeSelf)
            {
                VFXCollection.Add(t.gameObject);
                VFXCollectionPos.Add(t.position);
            }

            b.onClick.AddListener(() => ActivateDiorama(t.gameObject));
        }
    }

    private void ActivateDiorama(GameObject diorama)
    {
        int count = 0;

        foreach (GameObject d in VFXCollection)
        {
            if (d == diorama)
            {
                d.SetActive(false);
                d.SetActive(true);
                d.transform.localPosition = Vector3.zero;

                continue;
            }

            d.SetActive(false);
            d.transform.position = VFXCollectionPos[count];

            count++;
        }
    }
}
