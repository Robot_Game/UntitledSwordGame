﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class DioramaReset : MonoBehaviour
{
    [HideIf("@animController == null")]
    [SerializeField] private string animParameter;

    private Vector3 initialPos;
    private Animator animController;

    private void Awake()
    {
        animController = GetComponent<Animator>();
        initialPos = transform.localPosition;
    }

    private void OnEnable()
    {
        if (animController && animParameter != null) animController.SetTrigger(animParameter);
        transform.localPosition = initialPos;
    }
}
