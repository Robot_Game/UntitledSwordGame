﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantMovementVFX : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private float speed = 10;

    private void Update()
    {
        Move();
    }

    private void Move()
    {
        transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
    }
}
