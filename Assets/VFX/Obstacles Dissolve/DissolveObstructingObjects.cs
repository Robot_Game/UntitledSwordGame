﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DissolveObstructingObjects : MonoBehaviour
{
    [SerializeField] private Transform raycastTarget;
    [SerializeField] private float raycastDistance = 50;
    [SerializeField] private LayerMask layerToDissolve;
    [SerializeField] private float dissolveDuration = 0.5f;

    List<Collider> previousCols;

    private void Awake()
    {
        previousCols = new List<Collider>();
    }

    // Update is called once per frame
    void Update()
    {
        SendRaycast();
    }

    private void SendRaycast()
    {
        Vector3 direction = raycastTarget.position - transform.position;
        RaycastHit[] hitInfo;
        hitInfo = Physics.RaycastAll(transform.position, direction, raycastDistance, layerToDissolve);

        if (previousCols != null)
        {
            foreach (Collider col in previousCols)
            {
                MeshRenderer renderer = col.GetComponent<MeshRenderer>();
                if (renderer)
                {
                    Material previousMat = renderer.material;
                    if (previousMat)
                    {
                        previousMat.DOKill();
                        previousMat.DOFloat(0, "_DissolveAmount", dissolveDuration);
                    }
                }
            }

            previousCols.Clear();
        }

        foreach (RaycastHit hit in hitInfo)
        {
            MeshRenderer renderer = hit.collider.GetComponent<MeshRenderer>();
            if (renderer)
            {
                Material currentMat = renderer.material;

                if (currentMat)
                {
                    currentMat.DOKill();
                    currentMat.DOFloat(1, "_DissolveAmount", dissolveDuration);

                    previousCols.Add(hit.collider);
                }
            }
        }
    }
}
