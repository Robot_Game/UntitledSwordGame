﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Feedbacks;

public class VFXPlayerAttacksDemoController : MonoBehaviour
{
    public Vector3 initialPos;

    [Space(5)]
    public MMFeedbacks[] slashesVFX;
    [Space(5)]
    public MMFeedbacks spinVFX;
    public MMFeedbacks mortalVFX;
    public MMFeedbacks mortalImpactVFX;

    private Animator animController;
    private int attackID = 0;

    private void Awake()
    {
        animController = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        animController.SetTrigger("Attack");
        transform.localPosition = initialPos;
        attackID = 0;
    }

    private void PlaySlashesVFX()
    {
        slashesVFX[attackID].PlayFeedbacks();

        attackID++;
        if (attackID >= 2) attackID = 0;
    }

    private void PlayMortalVFX() => mortalVFX.PlayFeedbacks();
    private void PlayMortalImpactVFX() => mortalImpactVFX.PlayFeedbacks();
    private void PlaySpinVFX() => spinVFX.PlayFeedbacks();
}
