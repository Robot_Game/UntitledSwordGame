﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scripts : MonoBehaviour
{

    public static GameManager GameManager { get; private set; }
    public static ScenesManager ScenesManager { get; private set; }
    public static UIManager UIManager { get; private set; }
    public static SaveManager SaveManager { get; private set; }
    public static SavedObjects SavedObjects { get; private set; }
    public static PlayerMovement PlayerMovement { get; private set; }
    public static PlayerActions PlayerActions { get; private set; }
    public static PlayerAttacks PlayerAttacks { get; private set; }
    public static CameraController CameraController { get; private set; }

    void Awake()
    {
        GameManager = FindObjectOfType<GameManager>();
        ScenesManager = FindObjectOfType<ScenesManager>();
        UIManager = FindObjectOfType<UIManager>();
        SaveManager = FindObjectOfType<SaveManager>();
        SavedObjects = FindObjectOfType<SavedObjects>();
        PlayerMovement = FindObjectOfType<PlayerMovement>();
        PlayerActions = FindObjectOfType<PlayerActions>();
        PlayerAttacks = FindObjectOfType<PlayerAttacks>();
        CameraController = FindObjectOfType<CameraController>();
        Debug.Log("ScenesManager: " + ScenesManager.gameObject);
    }

}
