﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavedObjects : MonoBehaviour
{
    private bool isAddingObjects;
    private bool isActuallySaving;

    public List<ScenesData> ScenesData { get; set; } = new List<ScenesData>();
    public List<PlayerSaveData> PlayerSavedData { get; set; } = new List<PlayerSaveData>();
    public List<InteractibleSaveData> SavedInteractibles { get; private set; } = new List<InteractibleSaveData>();
    public List<BreakableSaveData> SavedBreakables { get; private set; } = new List<BreakableSaveData>();
    public List<SwingableSaveData> SavedSwingables { get; private set; } = new List<SwingableSaveData>();
    public List<CheckpointSaveData> SavedCheckpoints { get; private set; } = new List<CheckpointSaveData>();
    public HashSet<EnemySaveData> KilledEnemies { get; private set; } = new HashSet<EnemySaveData>();
    //public HashSet<string> SavedIDs { get; private set; } = new HashSet<string>();

    // IsSaving is used to make sure other scritps don't close 
    // the application while we are saving
    public bool IsSaving { get; private set; } = false;


    private void Awake()
    {
        Load();
    }
    private void OnEnable()
    {
        Debug.LogWarning("ON ENABLE WAS CALLED IN SAVED OBJECTS!");
        GameEvents.SaveInitiated += Save;
    }
    private void OnDisable()
    {
        Debug.LogWarning("ON DISABLE WAS CALLED IN SAVED OBJECTS!");
        GameEvents.SaveInitiated -= Save;
    }

    private void Save()
    {
        StopAllCoroutines();
        StartCoroutine(SaveAfterInterval(0.2f));
    }
    private IEnumerator SaveAfterInterval(float interval)
    {
        IsSaving = true;
        isActuallySaving = true;
        Scripts.ScenesManager.CanChangeScenes = false;
        Debug.Log("STARTED SAVING");

        while (isAddingObjects)
        {
            yield return null;
        }
        float tmpTime = interval;
        while (tmpTime > 0)
        {
            tmpTime -= Time.unscaledDeltaTime;
            yield return null;
        }
        while (isAddingObjects)
        {
            yield return null;
        }
        // THERE'S A REASON WHY WE WAIT BEFORE SAVING! DON'T MOVE
        // THIS CODE
        Scripts.SaveManager.Save(ScenesData, "Scene");
        Scripts.SaveManager.Save(PlayerSavedData, "Player");
        Scripts.SaveManager.Save(SavedInteractibles, "Interactibles");
        Scripts.SaveManager.Save(SavedBreakables, "Breakables");
        Scripts.SaveManager.Save(SavedSwingables, "Swingables");
        Scripts.SaveManager.Save(SavedCheckpoints, "Checkpoints");
        Scripts.SaveManager.Save(KilledEnemies, "Enemies");

        tmpTime = interval;
        while (tmpTime > 0)
        {
            tmpTime -= Time.unscaledDeltaTime;
            yield return null;
        }
        Debug.Log("FINISHED SAVING");
        IsSaving = false;
        isActuallySaving = false;
        Scripts.ScenesManager.CanChangeScenes = true;
    }

    private void Load()
    {

        ScenesData = Scripts.SaveManager.Load<List<ScenesData>>("Scene");
        PlayerSavedData = Scripts.SaveManager.Load<List<PlayerSaveData>>("Player");
        SavedInteractibles = Scripts.SaveManager.Load<List<InteractibleSaveData>>("Interactibles");
        SavedBreakables = Scripts.SaveManager.Load<List<BreakableSaveData>>("Breakables");
        SavedSwingables = Scripts.SaveManager.Load<List<SwingableSaveData>>("Swingables");
        SavedCheckpoints = Scripts.SaveManager.Load<List<CheckpointSaveData>>("Checkpoints");
        KilledEnemies = Scripts.SaveManager.Load<HashSet<EnemySaveData>>("Enemies");

        CreateInstances();
    }

    private void CreateInstances()
    {
        if (ScenesData == null) ScenesData = new List<ScenesData>();
        if (PlayerSavedData == null) PlayerSavedData = new List<PlayerSaveData>();
        if (SavedInteractibles == null) SavedInteractibles = new List<InteractibleSaveData>();
        if (SavedBreakables == null) SavedBreakables = new List<BreakableSaveData>();
        if (SavedSwingables == null) SavedSwingables = new List<SwingableSaveData>();
        if (SavedCheckpoints == null) SavedCheckpoints = new List<CheckpointSaveData>();
        if (KilledEnemies == null) KilledEnemies = new HashSet<EnemySaveData>();
    }

    public void AddObject<T>(List<T> list, T objectToAdd)
    {
        IsSaving = true;
        isAddingObjects = true;
        Scripts.ScenesManager.CanChangeScenes = false;
        if (list.Contains(objectToAdd))
            list.Remove(objectToAdd);
        list.Add(objectToAdd);
        isAddingObjects = false;
        // If we're not calling Save()
        if (!isActuallySaving)
        {
            IsSaving = false;
        }
        Scripts.ScenesManager.CanChangeScenes = true;
    }

    public void AddObject<T>(HashSet<T> hashSet, T objectToAdd)
    {
        IsSaving = true;
        isAddingObjects = true;
        Scripts.ScenesManager.CanChangeScenes = false;
        if (!hashSet.Contains(objectToAdd))
            hashSet.Add(objectToAdd);
        isAddingObjects = false;
        // If we're not calling Save()
        if (!isActuallySaving)
        {
            IsSaving = false;
        }
        Scripts.ScenesManager.CanChangeScenes = true;
    }

    public bool IsObjectSaved<T>(T objSavedData)
    {
        if (objSavedData != null)
            if (Scripts.SaveManager.SaveExists("Player"))
                return true;

        return false;
    }

    public bool IsGenericObjectSaved<T>(ICollection<T> enumerable, T obj)
    {
        if (enumerable == null) Debug.Log("List was null for some reason!");
        if (enumerable.Contains(obj)) return true;

        return false;
    }

    public bool IsObjectSaved<T>(HashSet<T> hashSet, T obj)
    {
        if (hashSet == null) Debug.Log("List was null for some reason!");
        SavableObject objToCheck = obj as SavableObject;

        foreach (T i in hashSet)
        {
            SavableObject objInSet = i as SavableObject;

            if (objInSet.Id == objToCheck.Id) return true;
        }

        return false;
    }
    /// <summary>
    /// Is object 'data' added to List 'list'
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    /// <param name="data"></param>
    /// <returns></returns>
    public bool IsObjectSaved<T>(List<T> list, ref T objectData)
    {
        if (list == null) Debug.Log("List was null for some reason!");

        SavableObject objToCheck = objectData as SavableObject;

        foreach (T i in list)
        {
            SavableObject obj = i as SavableObject;

            if (obj.Id == objToCheck.Id)
            {
                objectData = i;
                return true;
            }
        }

        return false;
    }
}


// Savable objects' datas

[Serializable]
public class SavableObject { public string Id { get; set; } }

[Serializable]
public class ScenesData : SavableObject
{
    public string CurrentScene { get; set; }
    public ScenesData(ScenesManager scenes)
    {
        Id = scenes.CurrentScene;
    }
}
[Serializable]
public class PlayerSaveData : SavableObject
{
    public float SpawnPosX { get; set; }
    public float SpawnPosY { get; set; }
    public float SpawnPosZ { get; set; }
    public int CurrentHearts { get; set; }
    public string CurrentScene { get; set; }
    public PlayerSaveData(PlayerActions pa)
    {
        Id = pa.name;
        SpawnPosX = pa.SpawnPos.x;
        SpawnPosY = pa.SpawnPos.y;
        SpawnPosZ = pa.SpawnPos.z;
        CurrentHearts = pa.CurrentHearts;
        CurrentScene = pa.CurrentScene;
    }
}

[Serializable]
public class InteractibleSaveData : SavableObject
{
    public bool IsActive { get; set; }

    public InteractibleSaveData(Interactible i)
    {
        Id = i.transform.position.sqrMagnitude + "-" + i.name + "-" + i.transform.GetSiblingIndex();
        IsActive = i.IsActive;
    }
}

[Serializable]
public class BreakableSaveData : SavableObject
{
    public int Hp { get; set; }

    public BreakableSaveData(Breakable i)
    {
        Id = i.transform.position.sqrMagnitude + "-" + i.name + "-" + i.transform.GetSiblingIndex();
        Hp = i.Hp;
    }
}

[Serializable]
public class SwingableSaveData : SavableObject
{
    public float PosX { get; set; }
    public float PosY { get; set; }
    public float PosZ { get; set; }
    public bool IsDestroyed { get; set; }

    public SwingableSaveData(Swingable i)
    {
        Id = i.transform.position.sqrMagnitude + "-" + i.name + "-" + i.transform.GetSiblingIndex();
        PosX = i.Pos.x;
        PosY = i.Pos.y;
        PosZ = i.Pos.z;
        IsDestroyed = i.IsDestroyed;
    }
}

[Serializable]
public class CheckpointSaveData : SavableObject
{
    public bool IsActive { get; set; }
    public bool IsVirgin { get; set; }

    public CheckpointSaveData(Checkpoint c)
    {
        Id = c.transform.position.sqrMagnitude + "-" + c.name + "-" + c.transform.GetSiblingIndex();
        IsActive = c.IsActive;
        IsVirgin = c.IsVirgin;
    }
}


[Serializable]
public class EnemySaveData : SavableObject
{
    public int Hp { get; set; }

    public EnemySaveData(Enemy e)
    {
        Id = e.transform.position.sqrMagnitude + "-" + e.name + "-" + e.transform.GetSiblingIndex();
    }
}
