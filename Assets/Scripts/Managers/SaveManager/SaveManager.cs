﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using MoreMountains.Tools;

public class SaveManager : MonoBehaviour
{

    public void Save<T>(T objectToSave, string fileName)
    {
        string path = Application.persistentDataPath + "/saves/";
        Directory.CreateDirectory(path);
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream fileStream = new FileStream(path + fileName + ".binary", FileMode.Create);
        formatter.Serialize(fileStream, objectToSave);
        fileStream.Close();


        //Debug.Log("Saved " + objectToSave + "in " + key + ".txt");
    }

    public T Load<T>(string fileName)
    {
        T returnValue = default;
        if (Scripts.SaveManager.SaveExists(fileName))
        {
            //Debug.Log("Loading from " + key + ".txt");
            string path = Application.persistentDataPath + "/saves/";
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fileStream = new FileStream(path + fileName + ".binary", FileMode.Open);
            returnValue = (T)formatter.Deserialize(fileStream);
            fileStream.Close();
        }
        return returnValue;

    }

    public bool SaveExists(string fileName)
    {
        string directoryPath = Application.persistentDataPath + "/saves/";
        if (Directory.Exists(directoryPath))
        {
            string path = directoryPath + fileName + ".binary";

            return File.Exists(path);
        }

        return false;
    }

    public void DeleteAllSavedFiles()
    {
        Debug.Log("Delete all progress");
        string path = Application.persistentDataPath + "/saves/";
        DirectoryInfo directory = new DirectoryInfo(path);
        directory.Delete(true);
        //Directory.CreateDirectory(path);
    }
}

