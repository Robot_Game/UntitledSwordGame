﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Sirenix.OdinInspector;

public class ScenesManager : MonoBehaviour
{
    private ScenesData scenesSaveData;
    public string CurrentScene { get; set; }
    public string NextScene { get; set; }
    public bool CanChangeScenes { get; set; } = true;

    private void Awake()
    {
        CurrentScene = SceneManager.GetActiveScene().name;
        scenesSaveData = new ScenesData(this);
    }

    private void Start()
    {
        //ManageStartState();
    }

    /*private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene loadedScene, LoadSceneMode loadedSceneMode)
    {
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex((int)currentScene));
        if (SceneManager.GetActiveScene().buildIndex != (int)currentScene) 
        Debug.Log("FAILED TO SET THE ACTIVE SCENE!");
        scene = SceneManager.GetActiveScene();
        CurrentScene = scene.name;
        scenesSaveData = new ScenesData(this);
        ManageStartState();
    }*/

    private void ManageStartState()
    {
        if (Scripts.SavedObjects.IsObjectSaved(
            Scripts.SavedObjects.ScenesData, ref scenesSaveData))
        {
            // Set the current scene name to the last saved scene in the
            // game savings.
            CurrentScene = scenesSaveData.Id;
        }
        else CurrentScene = "CenterZone";
        // If we're not in the scene that was last saved, load the correct scene
        if (SceneManager.GetActiveScene().name != CurrentScene) LoadScene(CurrentScene);
    }

    public void SetNextScene(string scene) => NextScene = scene;

    public void LoadNextSceneAfterIntervalCaller(float interval)
        => StartCoroutine(LoadNextSceneAfterInterval(interval));

    public IEnumerator LoadNextSceneAfterInterval(float interval)
    {
        yield return new WaitForSeconds(interval);
        if (!CanChangeScenes)
        {
            StartCoroutine(WaitForLoadPermission());
        }
        else SceneManager.LoadSceneAsync(NextScene);
    }
    public void LoadNextScene()
    {
        if (!CanChangeScenes)
        {
            StartCoroutine(WaitForLoadPermission());
            return;
        }
        SceneManager.LoadSceneAsync(NextScene);
    }
    public void LoadScene(string sceneName)
    {
        if (!CanChangeScenes)
        {
            StartCoroutine(WaitForLoadPermission(sceneName));
            return;
        }
        SceneManager.LoadSceneAsync(sceneName);
    }
    public void LoadScene(int zone, int part)
    {
        if (!CanChangeScenes)
        {
            StartCoroutine(WaitForLoadPermission(zone, part));
            return;
        }
        SceneManager.LoadSceneAsync($"Zone{zone}_Part{part}");
    }
    public void RestartScene()
    {
        if (!CanChangeScenes)
        {
            StartCoroutine(WaitForRestartPermission());
            return;
        }
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
    }

    public void RestartScene(float time)
    {
        if (!CanChangeScenes)
        {
            StartCoroutine(WaitForRestartPermission(time));
            return;
        }
        StartCoroutine(RestartSceneAfterInterval(time));
    }

    private IEnumerator RestartSceneAfterInterval(float time)
    {
        float timer = time;
        while (timer > 0)
        {
            timer -= Time.unscaledDeltaTime;
            yield return null;
        }

        RestartScene();
    }

    private IEnumerator WaitForRestartPermission()
    {
        while (!CanChangeScenes)
        {
            yield return null;
        }
        RestartScene();
    }

    private IEnumerator WaitForRestartPermission(float time)
    {
        while (!CanChangeScenes)
        {
            Debug.Log("WAITING FOR SAVE TO FINISH!");
            yield return null;
        }
        RestartScene(time);
    }

    private IEnumerator WaitForLoadPermission()
    {
        while (!CanChangeScenes)
        {
            yield return null;
        }
        LoadNextScene();
    }
    private IEnumerator WaitForLoadPermission(int interval)
    {
        while (!CanChangeScenes)
        {
            yield return null;
        }
        LoadNextSceneAfterInterval(interval);
    }


    private IEnumerator WaitForLoadPermission(string sceneName)
    {
        while (!CanChangeScenes)
        {
            yield return null;
        }
        LoadScene(sceneName);
    }
    private IEnumerator WaitForLoadPermission(int zone, int part)
    {
        while (!CanChangeScenes)
        {
            yield return null;
        }
        LoadScene(zone, part);
    }

}
