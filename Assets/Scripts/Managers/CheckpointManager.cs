﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointManager : MonoBehaviour
{
    public Checkpoint CurrentCheckpoint { get; private set; }

    public void SetCurrentCheckpoint(Checkpoint c) => CurrentCheckpoint = c;
    public void DeactivateCurrentCheckpoint() => CurrentCheckpoint.Deactivate();
}
