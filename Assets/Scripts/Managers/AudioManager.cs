﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private float fadeDuration;
    private static AudioManager instance;

    private AudioSource musicAudioSource;
    private AudioSource ambientAudioSource;
    private AudioSource sfxAudioSource;

    private AudioClip lastClipPlayed;

    public static AudioManager Instance => instance;

    private void Awake()
    {
        instance = this;

        musicAudioSource = transform.GetChild(0).GetComponent<AudioSource>();
        ambientAudioSource = transform.GetChild(1).GetComponent<AudioSource>();
        sfxAudioSource = transform.GetChild(2).GetComponent<AudioSource>();
    }

    public void PlaySfx(AudioClip audio)
    {
        sfxAudioSource.clip = audio;
        sfxAudioSource.Play();
    }

    public void PlaySfxOneShot(params AudioClip[] audio)
    {
        if (audio.Length < 1) return;

        if (audio.Length > 1)
        {
            int n = 0;
            do
            {
                n = Random.Range(0, audio.Length);
            }
            while (lastClipPlayed == audio[n]);

            lastClipPlayed = audio[n];
            sfxAudioSource.PlayOneShot(audio[n]);
        }
        else
            sfxAudioSource.PlayOneShot(audio[0]);
    }

    public void PlayMusic()
    {
        musicAudioSource.Play();
        Fade("music", 1);
    }

    public void Fade(string s, int endValue)
    {
        switch (s)
        {
            case "music":
            musicAudioSource.DOFade(endValue, fadeDuration);
            break;
            case "ambience":
            ambientAudioSource.DOFade(endValue, fadeDuration);
            break;
            case "sfx":
            sfxAudioSource.DOFade(endValue, fadeDuration);
            break;
        }
    }
}
