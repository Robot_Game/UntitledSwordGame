﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;
using Doozy.Engine;
using Doozy.Engine.Progress;

public class UIManager : MonoBehaviour
{

    [Title("Action Feedback")]
    [SerializeField] private Sprite controllerInteractionButton;
    [SerializeField] private Sprite keyboardInteractionButton;
    [SerializeField] private GameObject actionDescriptionObj;

    [Title("Player Information")]
    [SerializeField] private GameObject healthBarObj;
    [SerializeField] private Image healthBar;
    [SerializeField] private Progressor hpProgressor;
    [SerializeField] private Gradient healthBarColors;

    [Title("Counters")]
    [SerializeField] private TextMeshProUGUI heartNumDisplay;

    private PlayerInputActions inputs;
    private SpriteRenderer actionKeySprite;
    private TextMeshPro actionDescriptionText;
    private GameObject eventSystem;
    private Vector2 initHpBarSize;
    public bool IsInStore { get; private set; }

    private void Awake()
    {
        actionDescriptionText = actionDescriptionObj.GetComponentInChildren<TextMeshPro>();
        actionKeySprite = actionDescriptionObj.GetComponentInChildren<SpriteRenderer>();
        eventSystem = GameObject.Find("EventSystem");
        UpdateHealthBarColor(100);
        initHpBarSize = healthBar.rectTransform.rect.size;
        Debug.Log("Init bar size = " + initHpBarSize.x);
    }

    private void Start()
    {
        if (Scripts.GameManager.IsPlayingOnController)
            actionKeySprite.sprite = controllerInteractionButton;
        else
            actionKeySprite.sprite = keyboardInteractionButton;
    }

    public void UpdateHeartDisplay(int num)
    {
        GameEventMessage.SendEvent("ShowCurrencyUI");
        heartNumDisplay.text = $"x {num}";
    }

    public void UpdateHealthBarSize()
    {
        float hp = hpProgressor.Value;
        Debug.Log("TRIED TO UPDATE HEALTH SIZE: " + hp);
        float w = hp.Lerp(0, 100, 0, initHpBarSize.x);
        Debug.Log("The width should be " + w);
        healthBar.rectTransform.sizeDelta = new Vector2(w, initHpBarSize.y);
    }
    public void UpdateHealthBarColor(float hp)
    {
        //Debug.Log("WITH HEALTH = " + hp + " CURRENT VALUE IS " + hp / 100);
        healthBar.color = healthBarColors.Evaluate(hp / 100);
    }

    public void ShowActionKey(bool value = true) => actionDescriptionObj.gameObject.SetActive(value);

    public void ShowActionKey(string description = "Interact", bool value = true)
    {
        actionDescriptionText.text = description;
        actionDescriptionObj.gameObject.SetActive(value);
    }
    public void ShowActionKey(string action, string description, bool value = true)
    {
        // Action could be for example: "Use" or something
        // for later implementation, if we need to other buttons
    }

    public void ShowBlackScreen(string gameEvent) => GameEventMessage.SendEvent(gameEvent);
}
