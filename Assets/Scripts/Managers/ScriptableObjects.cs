﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptableObjects : MonoBehaviour
{
    [SerializeField] private GameData gameData;
    [SerializeField] private PlayerData playerData;

    public static GameData GameData{ get; private set; }
    public static PlayerData PlayerData { get; private set; }

    private void Awake()
    {
        GetDataObjects();
        GameData = gameData;
        PlayerData = playerData;
    }

    private void GetDataObjects()
    {
        if (gameData == null) gameData = (GameData)Resources.Load("ScriptableObjects/Datas/GameData");
        if (gameData == null) Debug.Log("This data object was not found!!");
        if (playerData == null) playerData = (PlayerData)Resources.Load("ScriptableObjects/Datas/PlayerData");
        if (playerData == null) Debug.Log("This data object was not found!!");
    }
}
