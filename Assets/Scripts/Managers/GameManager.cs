﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Doozy.Engine;

public class GameManager : MonoBehaviour
{

    [SerializeField] private bool isPlayingOnController;

    private PlayerInputActions inputs;

    public bool IsPlayingOnController => isPlayingOnController;
    public bool IsInPause { get; private set; }

    private void Awake()
    {

        //DontDestroyOnLoad(gameObject);

        // Normal awake code here
        inputs = new PlayerInputActions();
        AddControls();
        DisableMouse();
    }

    private void Start()
    {
        Time.timeScale = 1;
    }

    private void OnEnable()
    {
        Time.timeScale = 1;
        inputs.Enable();
        inputs.Gameplay.Cancel.Disable();
        Message.AddListener<GameEventMessage>(OnMessage);
    }

    private void OnDisable()
    {
        Time.timeScale = 1;
        inputs.Disable();
        Message.RemoveListener<GameEventMessage>(OnMessage);
    }

    private void AddControls()
    {
        inputs.Gameplay.Pause.performed += ctx => { StartCoroutine(Pause()); };
    }

    private void DisableMouse()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private IEnumerator Pause()
    {
        inputs.Gameplay.Pause.Disable();
        GameEventMessage.SendEvent("ShowPauseUI");
        Scripts.PlayerActions.DisableActionInputs();
        Scripts.PlayerAttacks.DisableAttackInputs();
        Scripts.PlayerMovement.DisableMovementInputs();

        yield return new WaitForSeconds(.2f);

        inputs.Gameplay.Pause.Enable();
    }

    public IEnumerator OpenStore()
    {
        inputs.Gameplay.Pause.Disable();
        GameEventMessage.SendEvent("ShowStoreUI");
        Scripts.PlayerActions.DisableActionInputs();
        Scripts.PlayerAttacks.DisableAttackInputs();
        Scripts.PlayerMovement.DisableMovementInputs();

        yield return new WaitForSeconds(.2f);

        inputs.Gameplay.Pause.Enable();
    }

    private void OnMessage(GameEventMessage message)
    {
        if (message == null) return;

        if (message.EventName == "EnableGameplayInputs")
        {
            Scripts.PlayerActions.EnableActionInputs();
            Scripts.PlayerAttacks.EnableAttackInputs();
            Scripts.PlayerMovement.EnableMovementInputs();
        }
    }

    private void Update()
    {
        // HARD RESET, TO DELETE LATER
        if (Input.GetKeyDown(KeyCode.R)) ResetProgress();
    }
    public void Save() => GameEvents.OnSaveInitiated();

    public void ResetProgress()
    {
        Scripts.SaveManager.DeleteAllSavedFiles();
        Scripts.ScenesManager.RestartScene(1);
    }

    public void StartSlowMotionEffect(float speed, float duration)
    {
        StartCoroutine(SlowMotionEffect(speed, duration));
    }

    private IEnumerator SlowMotionEffect(float speed, float duration)
    {
        ChangeTimeScale(speed);
        float timer = duration;
        while (timer > 0)
        {
            timer -= Time.deltaTime;
            yield return null;
        }
        ChangeTimeScale(1);
    }

    public void ChangeTimeScale(float value)
    {
        Time.timeScale = value;
        Time.fixedDeltaTime = 0.02F * Time.timeScale;
    }

    //private void OnApplicationQuit()
    //{
    //    Save();
    //}
}
