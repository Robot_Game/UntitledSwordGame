﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Sirenix.OdinInspector;
using Random = UnityEngine.Random;

public class EnemySpawner : SerializedMonoBehaviour
{
    [InfoBox("The minimum interval between each enemy spawn")]
    [SerializeField] private float minSpawnInterval;
    [InfoBox("The maximum interval between each enemy spawn")]
    [SerializeField] private float maxSpawnInterval;
    [InfoBox("The max number of enemies that can be alive at a time")]
    [SerializeField] private float maxEnemiesAtATime;
    //[InfoBox("The max number of enemies this spawner will spawn before disabling")]
    //[SerializeField] private int maxSpawnCount;
    [InfoBox("Whether this spawner should reset after the player exits it's trigger" +
    "collider range")]
    [SerializeField] private bool spawnOnStart;
    [InfoBox("Whether this spawner should reset after the player exits it's trigger" +
        "collider range")]
    [SerializeField] private bool isResetable;
    [Title("Enemies")]
    [InfoBox("The type and the total amount of enemies of said type to spawn")]
    [SerializeField]
    private Dictionary<GameObject, int> enemiesToSpawn;

    private List<Transform> spawningPositions;

    private FightingPit fightingPit;
    // Multiple lists of the different enemies types spawned
    // Ex: enemies[0] -> List<BasicEnemy>.Count = 3
    // Ex: enemies[1] -> List<RangedEnemy>.Count = 1
    // Ex: enemies[2] -> List<BomberEnemy>.Count = 2
    private List<List<GameObject>> enemies;
    private List<int> enemyCount;

    private GameObject currentSpawnedEnemy;
    // The total amount of enemies of all types that will spawn
    private int numberOfEnemiesToSpawn = 0;
    // The amount of enemies that have been killed
    private int deadEnemies;
    public int LiveEnemies { get; set; } = 0;
    public bool Finished { get; private set; }

    private void Awake()
    {
        enemies = new List<List<GameObject>>();
        enemyCount = new List<int>();

        for (int i = 0; i < enemiesToSpawn.Count; i++)
        {
            List<GameObject> tmpList = new List<GameObject>();
            enemies.Add(tmpList);
            //Had a counter with a value of 0 to this list
            enemyCount.Add(0);
        }
        for (int i = 0; i < enemiesToSpawn.Count; i++)
        {
            numberOfEnemiesToSpawn += enemiesToSpawn.Values.ElementAt(i);
        }
        spawningPositions = new List<Transform>();

        // Get the object that contains all of the spawning positions
        Transform parent = transform.Find("SpawningPositions");

        if (parent != null && parent.childCount != 0)
        {
            for (int i = 0; i < parent.childCount; i++)
                spawningPositions.Add(parent.GetChild(i));
        }

        // if there are no spawning positions set just use the main transform
        else spawningPositions.Add(transform);
    }

    // Start is called before the first frame update
    void Start()
    {
        if (spawnOnStart) StartCoroutine(TryToSpawn());
    }

    public void StartSpawning()
    {
        StartCoroutine(TryToSpawn());
    }
    private IEnumerator TryToSpawn()
    {
        float time = Random.Range(minSpawnInterval, maxSpawnInterval);

        while (!HaveAllEnemiesSpawned())
        {
            yield return new WaitForSeconds(time);

            Spawn(GetRandomEnemyType());
        }
    }

    private bool HaveAllEnemiesSpawned()
    {
        if (LiveEnemies >= numberOfEnemiesToSpawn) return true;

        if (LiveEnemies >= maxEnemiesAtATime) return true;

        for (int i = 0; i < enemiesToSpawn.Count; i++)
        {
            if (enemyCount[i] < enemiesToSpawn.Values.ElementAt(i)) return false;
        }
        return true;
    }

    private int GetRandomEnemyType()
    {
        int i = 0;
        do
        {
            if (HaveAllEnemiesSpawned())
            {
                break;
            }

            i = Random.Range(0, enemiesToSpawn.Count);
        }
        while (enemyCount[i] >= enemiesToSpawn.Values.ElementAt(i));

        return i;
    }

    private void Spawn(int enemyNumber)
    {
        Transform spawner;
        if (spawningPositions.Count == 1) spawner = spawningPositions[0];
        else spawner = GetRandomPosition();
        if (spawner.position == Vector3.zero) Debug.LogError("WE COULDN'T GET A CORRECT POSITION!!");
        currentSpawnedEnemy = Instantiate(enemiesToSpawn.Keys.ElementAt(enemyNumber),
                                          spawner.position,
                                          spawner.rotation,
                                          spawner);
        if (currentSpawnedEnemy == null) Debug.LogError("THE ENEMY WASN'T INSTANTIATED!! " + enemyNumber);
        enemies[enemyNumber].Add(currentSpawnedEnemy);
        currentSpawnedEnemy.GetComponent<Enemy>().Spawner = this;
        currentSpawnedEnemy.GetComponent<Enemy>().SpawnerInstantiationID = enemyNumber;
        enemyCount[enemyNumber]++;
        LiveEnemies++;
    }

    private Transform GetRandomPosition()
    {
        int i = Random.Range(0, spawningPositions.Count);
        return spawningPositions[i];
    }
    public void RemoveEnemy(Enemy enemy, int i)
    {
        bool flag = HaveAllEnemiesSpawned();
        // Debug.Log("REMOVE ENEMY " + enemy.name + " FROM SPAWNER.");
        enemies[i].Remove(enemy.gameObject);
        LiveEnemies--;
        deadEnemies++;
        if (enemy.Type == EnemyType.Bomber) Debug.LogWarning("REMOVE " + enemy.gameObject.name + " FROM SPAWNER " + gameObject.name);
        Debug.LogWarning("Dead Enemies: " + deadEnemies);
        if (flag && !HaveAllEnemiesSpawned()) StartCoroutine(TryToSpawn());

        if (deadEnemies >= numberOfEnemiesToSpawn)
        {
            Finished = true;
            Debug.Log("FINISHED ONE SPAWNER");
            if (fightingPit != null) fightingPit.CheckCompletionStatus();
        }
        enemy.Spawner = null;
    }

    private void OnTriggerExit(Collider other)
    {
        if (isResetable)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
            {
                for (int i = 0; i < enemiesToSpawn.Count; i++)
                {
                    enemyCount[i] = enemies[i].Count;
                }
                deadEnemies = 0;
                if (!HaveAllEnemiesSpawned()) StartCoroutine(TryToSpawn());
            }
        }
    }

    public void SetFightingPit(FightingPit f) => fightingPit = f;

}
