﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "Enemy Data", menuName = "Game Entities/Enemy")]
public class EnemyData : ScriptableObject
{
    [Title("Basic Stats")]
    [SerializeField, EnumToggleButtons] private EnemyType type;
    [SerializeField] private new string name;
    [SerializeField] private float maxHealth;

    [Title("Movement")]
    [SerializeField] private float movementSpeed;
    [SerializeField] private float rotationSpeed;
    [SerializeField] private float minTimeUntilWander;
    [SerializeField] private float maxTimeUntilWander;
    [SerializeField] private float patrolRadius;

    [Title("Seek Player")]
    [SerializeField] private LayerMask playerDetectionLayerMask;
    [SerializeField] private float detectPlayerRadius;
    [SerializeField] private float stoppingDistance;

    [Title("Attack")]
    [SerializeField] private float damage;
    [SerializeField] private float attackSpeed;
    [SerializeField] private float knockbackForce;
    [SerializeField] private float knockbackDuration;

    [Title("Drops")]
    [SerializeField]
    private float popOutForce;

    [Title("Death")]
    [SerializeField] private GameObject[] deathRagdolls;
    [SerializeField] private GameObject impaleRagdolls;

    [SerializeField, AssetsOnly, Required]
    [InlineEditor(InlineEditorModes.GUIAndPreview)]
    private Drop cursePrefab;

    [SerializeField, HideIf("@this.cursePrefab == null")]
    private int maxCurseDrops;

    [SerializeField, HideIf("@this.cursePrefab == null")]
    private float curseDropPercentage;

    [SerializeField, AssetsOnly, Required]
    [InlineEditor(InlineEditorModes.GUIAndPreview)]
    private Drop heart;

    [SerializeField, HideIf("@this.heart == null")]
    private int maxHeartDrops;

    [SerializeField, HideIf("@this.heart == null")]
    private float heartDropPercentage;

    [Title("Prefab Versions")]
    [SerializeField, Required]
    private GameObject blessedVersion;

    [SerializeField, Required]
    private GameObject cursedVersion;

    [SerializeField, Required]
    private GameObject impaledVersion;

    [Title("Materials Versions")]
    [SerializeField, Required]
    private Material blessedMat;

    [SerializeField, Required]
    private Material cursedMat;

    [Title("Enemy Sounds")]
    [SerializeField] private AudioClip[] hitPlayerSfx;

    public EnemyType Type => type;
    public string Name => name;
    public float Maxhealth => maxHealth;
    public float MovementSpeed => movementSpeed;
    public float RotationSpeed => rotationSpeed;
    public float MinTimeUntilWander => minTimeUntilWander;
    public float MaxTimeUntilWander => maxTimeUntilWander;
    public float PatrolRadius => patrolRadius;
    public LayerMask PlayerDetectionLayerMask => playerDetectionLayerMask;
    public float DetectPlayerRadius => detectPlayerRadius;
    public float StoppingDistance => stoppingDistance;
    public float Damage => damage;
    public float AttackSpeed => attackSpeed;
    public float KnockbackForce => knockbackForce;
    public float KnockbackDuration => knockbackDuration;
    public float PopOutForce => popOutForce;
    public Drop CursePrefab => cursePrefab;
    public int MaxCurseDrops => maxCurseDrops;
    public float CurseDropPercentage => curseDropPercentage;
    public Drop Heart => heart;
    public int MaxHeartDrops => maxHeartDrops;
    public float HeartDropPercentage => heartDropPercentage;
    public GameObject BlessedVersion => blessedVersion;
    public GameObject CursedVersion => cursedVersion;
    public GameObject ImpaledVersion => impaledVersion;
    public GameObject[] DeathRagdolls => deathRagdolls;
    public GameObject ImpaleRagdolls => impaleRagdolls;
    public Material BlessedMat => blessedMat;
    public Material CursedMat => cursedMat;
    public AudioClip[] HitPlayerSfx => hitPlayerSfx;
}
