﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class RangedEnemy : Enemy
{
    [Title("Attack Properties")]
    [SerializeField, AssetsOnly, Required]
    [HideIf("@this.state == EnemyState.Impaled")]
    private Projectile projectile;

    [HideIf("@this.state == EnemyState.Impaled")]
    [SerializeField] private float projectileSpeed;

    [HideIf("@this.state == EnemyState.Impaled")]
    [SerializeField] private float projectileLifetime;

    [ShowIfGroup("state", EnemyState.Impaled)]
    [TitleGroup("state/Special Effects")]
    [HideIf("@tFeedback == null")]
    [SerializeField]
    [LabelText("Projectile Damage")]
    private float tProjDamage;

    [TitleGroup("state/Special Effects")]
    [HideIf("@tFeedback == null")]
    [SerializeField]
    [LabelText("Knockback Force")]
    private float tKnockbackForce;

    private Projectile actualProjectile;

    private FriendlyParticleProjectile tProjectile;

    private Action<Transform> onHitEnemy;

    protected override void Awake()
    {
        base.Awake();
        if (state == EnemyState.Impaled)
            tProjectile = tFeedback.GetComponentInChildren<FriendlyParticleProjectile>();



    }
    // TESTING
    // Called on all saved objects when the player dies, so we don't have to
    // restart the entire scene
    private void CustomAwake()
    {
        Awake();
        Start();
    }
    private void OnEnable()
    {
        onHitEnemy += HitEnemy;
    }

    private void OnDisable()
    {
        onHitEnemy -= HitEnemy;
    }

    public void SpawnProjectile()
    {
        actualProjectile = Instantiate(projectile, transform.position + new Vector3(0f, -0.3f, 0f), transform.rotation);

        if (targetMask == playerMask)
            actualProjectile.Initialize(Vector3.forward, null, projectileSpeed, projectileLifetime,
                targetMask, OnHitPlayer, data.Damage, data.KnockbackForce, data.KnockbackDuration, Space.Self, ProjectileType.Normal, transform, data.HitPlayerSfx);

        else actualProjectile.Initialize(Vector3.forward, null, projectileSpeed, projectileLifetime,
                targetMask, onHitEnemy, data.Damage, data.KnockbackForce, data.KnockbackDuration, Space.Self, ProjectileType.Normal, transform, data.HitPlayerSfx);
    }
    private void HitEnemy(Transform t)
    {
        Debug.Log("hit enemy!");
        Enemy enemy = t.GetComponent<Enemy>();
        if (enemy != null)
        {
            enemy.TakeDamage(data.Damage / 10, false);

        }
    }
    public override void ThrowSpecialEffect(Transform spawnPos)
    {
        base.ThrowSpecialEffect(spawnPos);
        tProjectile.Init(tProjDamage, tKnockbackForce);
    }

    public override void MortalSpecialEffect()
    {
        base.MortalSpecialEffect();
        Vector3 euler = tProjectile.transform.eulerAngles;
        euler.z = UnityEngine.Random.Range(0, 360);
        tProjectile.transform.eulerAngles = euler;
        tProjectile.Init(tProjDamage, tKnockbackForce);
    }

}