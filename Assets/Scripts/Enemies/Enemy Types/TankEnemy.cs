﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using MoreMountains.Feedbacks;

public class TankEnemy : Enemy
{
    [HideIfGroup("VFXGroup", MemberName = "state", Value = EnemyState.Impaled)]
    [TitleGroup("VFXGroup/VFX")]
    [SerializeField, InlineEditor]
    protected MMFeedbacks[] shellHitFeedback;

    [Title("Attack Properties")]
    [HideIf("@state == EnemyState.Impaled")]
    [SerializeField]
    private float attackRadius;
    [HideIf("@state == EnemyState.Impaled")]
    [SerializeField]
    private float shieldPoints;
    [HideIf("@state == EnemyState.Impaled")]
    [SerializeField]
    private GameObject shell;

    [ShowIfGroup("state", EnemyState.Impaled)]
    [TitleGroup("state/Special Effects")]
    [HideIf("@tFeedback == null")]
    [SerializeField]
    [LabelText("Pellet Damage")]
    private float tPelletDamage;

    [TitleGroup("state/Special Effects")]
    [HideIf("@tFeedback == null")]
    [SerializeField]
    [LabelText("Knockback Force")]
    private float tKnockbackForce;

    private FriendlyParticleProjectile tProjectile;

    private float currentShield;
    private bool hasShield;

    private Action<Enemy> onHitEnemy;
    protected override void Awake()
    {
        base.Awake();
        Impalable = false;
        currentShield = shieldPoints;
        hasShield = true;
        if (state == EnemyState.Impaled)
            tProjectile = tFeedback.GetComponentInChildren<FriendlyParticleProjectile>();
    }
    // TESTING
    // Called on all saved objects when the player dies, so we don't have to
    // restart the entire scene
    private void CustomAwake()
    {
        Awake();
        Start();
    }
    private void OnEnable()
    {
        onHitEnemy += HitEnemy;
    }

    private void OnDisable()
    {
        onHitEnemy -= HitEnemy;
    }
    protected override void Attack()
    {
        base.Attack();

        Collider[] hitColliders = Physics.OverlapSphere(transform.position, attackRadius, targetMask);

        foreach (Collider col in hitColliders)
        {
            if (targetMask == playerMask)
            {
                OnHitPlayer.Raise(data.Damage);
                Scripts.PlayerMovement.GetKnockback(transform, data.KnockbackForce, data.KnockbackDuration);
            }
            else onHitEnemy.Invoke(prioritizedTarget.GetComponent<Enemy>());
        }
    }

    private void HitEnemy(Enemy t)
    {
        Debug.Log("hit enemy!");
        t.TakeDamage(data.Damage / 10, false);
    }

    public override void TakeDamage(float damage, bool hitByPlayer = true)
    {
        if (!hasShield)
            base.TakeDamage(damage, hitByPlayer);
        else
        {
            currentShield -= damage;
            if (currentShield <= 0) LoseShield();
            getHitFeedback.PlayFeedbacks();
        }
    }

    private void LoseShield()
    {
        hasShield = false;
        Impalable = true;
        shell.SetActive(false);
    }

    public override void ThrowSpecialEffect(Transform spawnPos)
    {
        base.ThrowSpecialEffect(spawnPos);
        tProjectile.Init(tPelletDamage, tKnockbackForce);
    }

    public override void MortalSpecialEffect()
    {
        base.MortalSpecialEffect();
        tProjectile.Init(tPelletDamage, tKnockbackForce);
    }
}
