﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Feedbacks;
using Sirenix.OdinInspector;

public class BomberEnemy : Enemy
{
    [Title("Feedback")]
    [HideIf("@state == EnemyState.Impaled")]
    [SerializeField, InlineEditor]
    private MMFeedbacks bomberChargeFeedback;

    [HideIf("@state == EnemyState.Impaled")]
    [SerializeField, InlineEditor]
    private MMFeedbacks bomberAttackFeedback;

    [HideIf("@state == EnemyState.Impaled")]
    [Title("Attack Properties")]
    [SerializeField]
    private float explosionRadius;
    [HideIf("@state == EnemyState.Impaled")]
    [SerializeField]
    private float detonationTime;
    [HideIf("@state == EnemyState.Impaled")]
    [SerializeField]
    private float cancelExplosionDistance;

    [ShowIfGroup("state", EnemyState.Impaled)]
    [TitleGroup("state/Special Effects")]
    [LabelText("Explosion Radius")]
    [SerializeField]
    private float throwExplosionRadius;
    [TitleGroup("state/Special Effects")]
    [LabelText("Explosion Damage")]
    [SerializeField]
    private float throwExplosionDamage;
    [TitleGroup("state/Special Effects")]
    [LabelText("Knockback Force")]
    [SerializeField]
    private float throwExplosionKnockbackForce;

    private bool canExplode;

    private Action<Enemy> onHitEnemy;

    protected override void Awake()
    {
        base.Awake();
        if (state != EnemyState.Impaled)
        {
            canExplode = true;
            anim.SetFloat("detonationTime", 10 / detonationTime);
        }
    }
    // TESTING
    // Called on all saved objects when the player dies, so we don't have to
    // restart the entire scene
    private void CustomAwake()
    {
        Awake();
        Start();
    }
    private void OnEnable()
    {
        onHitEnemy += HitEnemy;
    }

    private void OnDisable()
    {
        onHitEnemy -= HitEnemy;
    }
    protected override void Attack()
    {
        isAttacking = true;
        anim.SetBool("Charging", true);
        bomberChargeFeedback.PlayFeedbacks();
        StartCoroutine(Charge());
    }
    protected override IEnumerator AttackCooldown()
    {
        float time = 0;
        isAttacking = true;

        Attack();
        while (time < data.AttackSpeed)
        {
            time += Time.deltaTime;
            yield return null;
        }
    }
    private IEnumerator Charge()
    {
        canMove = false;
        float time = 0;

        while (time < detonationTime)
        {
            time += Time.deltaTime;

            if (Vector3.Distance(transform.position, prioritizedTarget.transform.position) > cancelExplosionDistance)
            {
                isAttacking = false;
                canExplode = false;
                anim.SetBool("Charging", false);
                bomberChargeFeedback.StopFeedbacks();
                break;
            }

            yield return null;
        }

        if (canExplode)
        {
            Debug.LogWarning("Explode");
            Explode();
        }
        canExplode = true;
        canMove = true;
    }

    public override void Die(bool hitByPlayer, bool impaled = false)
    {
        canExplode = false;
        base.Die(hitByPlayer, impaled);
    }

    private void Explode()
    {
        bomberAttackFeedback.PlayFeedbacks();

        anim.SetTrigger("explode");
        Collider[] hitColliders = Physics.OverlapSphere(transform.position,
            explosionRadius, targetMask);

        foreach (Collider col in hitColliders)
        {
            if (targetMask == playerMask)
            {
                OnHitPlayer.Raise(data.Damage);
                Scripts.PlayerMovement.GetKnockback(transform, data.KnockbackForce, data.KnockbackDuration);
            }
            else onHitEnemy.Invoke(prioritizedTarget.GetComponent<Enemy>());
        }
    }

    private void HitEnemy(Enemy t)
    {
        Debug.Log("hit enemy!");
        t.TakeDamage(data.Damage / 10, false);
    }

    public override void ThrowSpecialEffect(Transform spawnPos)
    {
        transform.rotation = Quaternion.identity;

        Collider[] hitColliders = Physics.OverlapSphere(spawnPos.position,
            throwExplosionRadius, 1 << LayerMask.NameToLayer("Enemy"));

        foreach (Collider col in hitColliders)
        {
            if (col is CapsuleCollider)
            {
                Enemy e = col.GetComponent<Enemy>();
                e.TakeDamage(throwExplosionDamage);
                e.GetKnockback(throwExplosionKnockbackForce);
            }
        }
    }

    public override void MortalSpecialEffect()
    {
        transform.position = player.position;
        transform.rotation = Quaternion.identity;

        Collider[] hitColliders = Physics.OverlapSphere(player.position,
            throwExplosionRadius, 1 << LayerMask.NameToLayer("Enemy"));

        foreach (Collider col in hitColliders)
        {
            if (col is CapsuleCollider)
            {
                Enemy e = col.GetComponent<Enemy>();
                e.TakeDamage(throwExplosionDamage);
                e.GetKnockback(throwExplosionKnockbackForce);
            }
        }
    }

    protected override void OnDrawGizmosSelected()
    {
        base.OnDrawGizmosSelected();
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, cancelExplosionDistance);
    }
}
