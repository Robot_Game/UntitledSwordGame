﻿using System;
using UnityEngine;
using Sirenix.OdinInspector;

public class BasicEnemy : Enemy
{
    [Title("Attack Properties")]
    [HideIf("@state == EnemyState.Impaled")]
    [SerializeField]
    private float attackRadius;

    [ShowIfGroup("state", EnemyState.Impaled)]
    [TitleGroup("state/Special Effects")]
    [HideIf("@tFeedback == null")]
    [SerializeField]
    [LabelText("Pellet Damage")]
    private float tPelletDamage;

    [TitleGroup("state/Special Effects")]
    [HideIf("@tFeedback == null")]
    [SerializeField]
    [LabelText("Knockback Force")]
    private float tKnockbackForce;

    private FriendlyParticleProjectile tProjectile;

    private Action<Enemy> onHitEnemy;

    protected override void Awake()
    {
        base.Awake();
        if (state == EnemyState.Impaled)
            tProjectile = tFeedback.GetComponentInChildren<FriendlyParticleProjectile>();
    }
    // TESTING
    // Called on all saved objects when the player dies, so we don't have to
    // restart the entire scene
    private void CustomAwake()
    {
        Awake();
        Start();
    }
    private void OnEnable()
    {
        onHitEnemy += HitEnemy;
    }

    private void OnDisable()
    {
        onHitEnemy -= HitEnemy;
    }

    protected override void Attack()
    {
        base.Attack();

        Vector3 pos = new Vector3(transform.position.x,
                                  transform.position.y,
                                  transform.position.z + attackRadius / 2);

        Collider[] hitColliders = Physics.OverlapSphere(pos, attackRadius / 2, targetMask, QueryTriggerInteraction.Ignore);

        foreach (Collider col in hitColliders)
        {
            if (targetMask == playerMask)
            {
                OnHitPlayer.Raise(data.Damage);
                Scripts.PlayerMovement.GetKnockback(transform, data.KnockbackForce, data.KnockbackDuration);
            }
            else onHitEnemy.Invoke(prioritizedTarget.GetComponent<Enemy>());
        }
    }

    private void HitEnemy(Enemy t)
    {
        t.TakeDamage(data.Damage / 10, false);
    }

    public override void ThrowSpecialEffect(Transform spawnPos)
    {
        base.ThrowSpecialEffect(spawnPos);
        tProjectile.Init(tPelletDamage, tKnockbackForce);
    }

    public override void MortalSpecialEffect()
    {
        base.MortalSpecialEffect();
        tProjectile.Init(tPelletDamage, tKnockbackForce);
    }
}
