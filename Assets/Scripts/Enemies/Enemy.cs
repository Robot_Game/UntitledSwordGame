﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.AI;
using Random = UnityEngine.Random;
using MoreMountains.Feedbacks;
using MoreMountains.Tools;
using Sirenix.OdinInspector;
using System;

public abstract class Enemy : MonoBehaviour
{

    #region 
#if UNITY_EDITOR
    [Button("Open Enemy Data Editor", ButtonSizes.Medium)]
    [PropertyOrder(-1)]
    public void OpenEnemyDataEditor()
    {
        EditorApplication.ExecuteMenuItem("Game Config Tools/Enemy Data Editor");
    }
#endif
    #endregion

    [Title("Enemy Data")]
    [SerializeField, InlineEditor, Required]
    protected EnemyData data;

    [Title("Events")]
    [HideIf("@this.state == EnemyState.Impaled")]
    [SerializeField, Required]
    protected FloatEvent OnHitPlayer;

    [Title("State")]
    [SerializeField] protected EnemyState state;

    [Title("Behaviour")]
    [SerializeField] protected EnemyBehaviour behaviour;

    [HideIfGroup("VFXGroup", MemberName = "state", Value = EnemyState.Impaled)]
    [TitleGroup("VFXGroup/VFX")]
    [SerializeField, InlineEditor]
    protected MMFeedbacks getHitFeedback;

    [ShowIfGroup("state", EnemyState.Impaled)]
    [TitleGroup("state/Special Effects")]
    [SerializeField, InlineEditor]
    [LabelText("Feedback")]
    protected MMFeedbacks tFeedback;

    //protected Transform mortalEffectSpawnPoint;
    // Possible Targets
    protected Transform player;
    protected List<Transform> enemiesInRange;
    // Player Masks
    protected LayerMask playerMask;
    protected LayerMask playerLayer;
    // Enemy Masks
    protected LayerMask enemyMask;
    protected LayerMask enemyLayer;
    // Cursed Masks
    protected LayerMask cursedMask;
    protected LayerMask cursedLayer;
    // Target to attack
    protected Transform prioritizedTarget;
    protected LayerMask targetMask;
    protected LayerMask targetLayer;
    protected LayerMask targetDetectionLayerMask;

    private Coroutine nearestEnemyCoroutine;
    private Coroutine stopSeekCoroutine;
    protected Animator anim;
    protected Rigidbody rb;
    private SphereCollider detectionSphere;
    private NavMeshAgent agent;
    private MeshRenderer mRenderer;
    private Vector3 initialPosition;
    protected bool canMove;
    protected bool canAttack = true;
    protected bool isAttacking;
    private bool spawnFlag;
    private bool isWandering;
    private bool isSeeking;
    private bool isFacingTarget;
    private bool isPlayerInRange;
    private bool detectedEnemy;
    private float currentHealth;

    //TEST PURPOSES
    private EnemyState lastFrameState;

    public EnemyData Data => data;
    public EnemyType Type => data.Type;
    public EnemyState State => state;
    public bool Impalable { get; protected set; }
    public bool Dead { get; private set; } = false;

    public EnemySpawner Spawner { get; set; }
    public int SpawnerInstantiationID { get; set; }

    protected EnemySaveData saveData;

    protected virtual void Awake()
    {

        if (state != EnemyState.Impaled) Init();
        player = Scripts.PlayerMovement.transform;
        playerMask = LayerMask.GetMask("Player");
        playerLayer = LayerMask.NameToLayer("Player");
        enemyMask = LayerMask.GetMask("Enemy");
        enemyLayer = LayerMask.NameToLayer("Enemy");
        cursedMask = LayerMask.GetMask("CursedEnemy");
        cursedLayer = LayerMask.NameToLayer("CursedEnemy");

        targetMask = playerMask;
        targetLayer = playerLayer;

        enemiesInRange = new List<Transform>();
        saveData = new EnemySaveData(this);
    }

    protected void Start()
    {
        ManageStartState();
    }
    private void Init()
    {

        canMove = true;

        anim = GetComponent<Animator>();
        detectionSphere = GetComponent<SphereCollider>();
        agent = GetComponent<NavMeshAgent>();
        rb = GetComponent<Rigidbody>();
        mRenderer = transform.Find("Body").GetComponent<MeshRenderer>();

        currentHealth = data.Maxhealth;

        initialPosition = transform.position;

        detectionSphere.radius = data.DetectPlayerRadius;

        agent.speed = data.MovementSpeed;
        agent.angularSpeed = data.RotationSpeed;
        agent.stoppingDistance = data.StoppingDistance;

        targetDetectionLayerMask = data.PlayerDetectionLayerMask;

        Impalable = true;
    }

    private void ManageStartState()
    {
        // If this enemy was saved, than he was already dead
        // so we destroy it
        if (Scripts.SavedObjects.IsObjectSaved(
            Scripts.SavedObjects.KilledEnemies, saveData))
            Destroy(gameObject);
    }
    private void SetBehaviour(EnemyBehaviour i) => behaviour = i;

    private void Update()
    {

        if (behaviour == EnemyBehaviour.Stop) return;

        if (state == EnemyState.Impaled) StopAllCoroutines();

        if (prioritizedTarget == null && state != EnemyState.Cursed)
        {
            SetTarget("Player");
        }

        switch (behaviour)
        {
            case EnemyBehaviour.Wander:
                Wander();
                break;

            case EnemyBehaviour.Seek:
                if (!isSeeking && stopSeekCoroutine != null) StopCoroutine(stopSeekCoroutine);
                if (!isSeeking) StopCoroutine(SetWanderDestination());
                //if(prioritizedTarget != null) Seek();
                Seek();
                break;
        }

        detectedEnemy = false;

        // TEST PURPOSES
        if (state == EnemyState.Cursed && lastFrameState == EnemyState.Blessed)
        {
            ChangeState(state);
        }

        lastFrameState = state;
    }

    private void Wander()
    {
        if (isSeeking) isSeeking = false;
        if (isWandering) return;

        agent.stoppingDistance = 0;
        StartCoroutine(SetWanderDestination());
    }

    private IEnumerator SetWanderDestination()
    {
        float time = 0;
        float timeUntilAction;
        Vector3 destinationDirection = Random.insideUnitSphere * data.PatrolRadius;
        Vector3 destination = new Vector3(
            initialPosition.x + destinationDirection.x + 4,
            transform.position.y,
            initialPosition.z + destinationDirection.z + 4);

        isWandering = true;
        if (!spawnFlag)
        {
            timeUntilAction = 0;
            spawnFlag = true;
        }
        else timeUntilAction = Random.Range(data.MinTimeUntilWander, data.MaxTimeUntilWander);

        while (time < timeUntilAction)
        {
            time += Time.deltaTime;
            yield return null;
        }

        if (canMove && !Dead) agent.SetDestination(destination);
        isWandering = false;
    }

    private void Seek()
    {
        if (!isSeeking) agent.stoppingDistance = data.StoppingDistance;

        if (canMove && prioritizedTarget != null)
        {
            isFacingTarget = RotateToTarget();
            agent.SetDestination(prioritizedTarget.position);
        }

        if (agent.remainingDistance < agent.stoppingDistance && isFacingTarget && canAttack)
            if (!isAttacking && !Dead)
            {
                Attack();
            }
        if (isWandering) isWandering = false;
        isSeeking = true;
    }

    private bool RotateToTarget()
    {
        Quaternion lookRotation = Quaternion.LookRotation(prioritizedTarget.position - transform.position);
        transform.rotation = Quaternion.Lerp(transform.rotation, lookRotation, 5 * Time.deltaTime);

        Vector3 directionToTarget = (prioritizedTarget.position - transform.position).normalized;

        bool returnValue = (Vector3.Dot(transform.forward, directionToTarget) > 0.9) ? true : false;
        return returnValue;
    }

    protected virtual IEnumerator AttackCooldown()
    {
        float time = 0;
        while (time < data.AttackSpeed && !Dead)
        {
            time += Time.deltaTime;
            yield return null;
        }

        isAttacking = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (state != EnemyState.Impaled && behaviour != EnemyBehaviour.Stop)
        {
            if (state == EnemyState.Blessed)
            {
                if (detectedEnemy) return;
                Enemy enemy = other.GetComponent<Enemy>();
                if (enemy != null)
                {
                    if (enemy.state == EnemyState.Cursed)
                    {
                        detectedEnemy = true;
                        enemiesInRange.Add(other.transform);
                        SetTarget("Enemy");
                    }
                }


                if (other.gameObject.layer == playerLayer)
                {
                    if (enemiesInRange.Count == 0) SetTarget("Player");
                    isPlayerInRange = true;
                }
            }
        }
        /*if (State == EnemyState.Cursed)
        {
            if (other.gameObject.layer == enemyLayer &&
                    State != EnemyState.Impaled && behaviour != EnemyBehaviour.Stop)
            {
                Enemy enemy = other.GetComponent<Enemy>();
                if (enemy != null)
                {
                    if (enemy.State == EnemyState.Blessed)
                    {
                        enemiesInRange.Add(other.transform);
                        prioritizedTarget = GetNearestEnemy();
                        targetLayer = enemyLayer;
                        targetMask = enemyMask;
                    }
                }
            }
        }*/
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == targetLayer && prioritizedTarget != null)
        {
            //Set Seek Behaviour
            if (state != EnemyState.Impaled && behaviour != EnemyBehaviour.Stop)
            {
                RaycastHit hit;
                // If we detect
                Debug.DrawRay(transform.position, prioritizedTarget.transform.position - transform.position, Color.red);
                if (Physics.Raycast(transform.position, prioritizedTarget.transform.position - transform.position, out hit, data.DetectPlayerRadius, targetDetectionLayerMask))
                {
                    bool flag = false;
                    if (hit.transform.gameObject.layer == targetLayer && behaviour != EnemyBehaviour.Seek)
                    {
                        flag = true;
                        SetBehaviour(EnemyBehaviour.Seek);
                    }
                    if ((hit.transform.gameObject.layer != targetLayer) && behaviour == EnemyBehaviour.Seek
                        && !flag)
                        if (stopSeekCoroutine == null) stopSeekCoroutine = StartCoroutine(StopSeekAfterInterval(3));
                }

            }
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (state != EnemyState.Impaled && behaviour != EnemyBehaviour.Stop)
        {
            if (state == EnemyState.Blessed)
            {
                if (other.gameObject.layer == targetLayer)
                {
                    Enemy enemy = other.GetComponent<Enemy>();
                    if (enemy != null)
                    {
                        if (enemy.state == EnemyState.Cursed)
                            enemiesInRange.Remove(other.transform);

                        if (enemiesInRange.Count > 0) SetTarget("Enemy");
                        else
                        {
                            if (isPlayerInRange) SetTarget("Player");
                            else SetBehaviour(EnemyBehaviour.Wander);
                        }
                    }
                }

                //If player leaves
                if (other.gameObject.layer == playerLayer)
                {
                    isPlayerInRange = false;
                    if (enemiesInRange.Count == 0)
                    {
                        if (stopSeekCoroutine == null) stopSeekCoroutine = StartCoroutine(StopSeekAfterInterval(3));
                    }
                }
            }
        }
        /*if (State == EnemyState.Cursed)
        {
            if (other.gameObject.layer == enemyLayer &&
                    State != EnemyState.Impaled && behaviour != EnemyBehaviour.Stop)
            {
                Enemy enemy = other.GetComponent<Enemy>();
                if (enemy != null)
                {
                    if (enemy.State == EnemyState.Blessed)
                    {
                        Debug.Log("REMOVED ENEMY");
                        enemiesInRange.Remove(other.transform);
                    }

                    if (enemiesInRange.Count > 0) prioritizedTarget = GetNearestEnemy();
                    else SetBehaviour(EnemyBehaviour.Wander);
                }
            }
        }*/
    }

    private void SetTarget(string s)
    {
        switch (s)
        {
            case "Player":
                prioritizedTarget = player;
                targetMask = playerMask;
                targetLayer = playerLayer;
                break;
            case "Enemy":
                prioritizedTarget = GetNearestEnemy();
                targetMask = cursedMask;
                targetLayer = cursedLayer;
                break;
        }

    }
    private Transform GetNearestEnemy()
    {
        Transform nearestEnemy = enemiesInRange[0];

        if (enemiesInRange.Count > 1)
        {
            float minDist = float.MaxValue;
            Vector3 currentPos = transform.position;
            currentPos.y = 0;
            foreach (Transform t in enemiesInRange)
            {
                if (t == null)
                {
                    enemiesInRange.Remove(t);
                    continue;
                }
                Vector3 iPos = t.transform.position;
                iPos.y = 0;
                float dist = Vector3.Distance(iPos, currentPos);
                if (dist < minDist)
                {
                    nearestEnemy = t;
                    minDist = dist;
                }
            }
        }
        return nearestEnemy;
    }

    private IEnumerator StopSeekAfterInterval(float time)
    {
        while (time > 0)
        {
            time -= Time.deltaTime;
            yield return null;
        }

        SetBehaviour(EnemyBehaviour.Wander);
        isSeeking = false;
        stopSeekCoroutine = null;
    }

    public virtual void TakeDamage(float damage, bool hitByPlayer = true)
    {
        currentHealth -= damage;
        MMHealthBar healthBar = GetComponent<MMHealthBar>();
        if (healthBar != null) healthBar.UpdateBar(currentHealth, 0, data.Maxhealth, true);
        getHitFeedback.PlayFeedbacks();
        if (hitByPlayer) DropItems(data.CursePrefab, data.CurseDropPercentage, data.MaxCurseDrops);

        if (currentHealth <= 0) Die(hitByPlayer);
    }

    public virtual void GetKnockback(float knockbackForce)
    {
        Vector3 playerPos = player.position;
        playerPos.y = 0;
        Vector3 enemyPos = transform.position;
        enemyPos.y = 0;

        Vector3 knockbackDir = (enemyPos - playerPos).normalized;
        Vector3 knockback = knockbackDir * knockbackForce;
        rb.AddForce(knockback, ForceMode.Impulse);
        anim.SetTrigger("Knockback");
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="value">True to curse, false to bless.</param>
    /// <param name="hpMultiplier"></param>
    /// <param name="sizeMultiplier"></param>
    /// <param name="duration">The amount of time the enemy will remain cursed. Set it to 0 to ignore it.</param>
    public void ChangeState(EnemyState state, bool canAttack = false, float hpMultiplier = 0, float sizeMultiplier = 0, float duration = 0)
    {
        //this.state = state;
        Vector3 pos = transform.position;
        Quaternion rot = transform.rotation;
        Transform parent = transform.parent;

        switch (state)
        {
            case EnemyState.Cursed:

                Enemy cursedEnemy = Instantiate(
                data.CursedVersion,
                pos,
                rot,
                parent).GetComponent<Enemy>();
                if (!canAttack) cursedEnemy.behaviour = EnemyBehaviour.Stop;
                cursedEnemy.AddMultipliers(hpMultiplier, sizeMultiplier);
                cursedEnemy.Spawner = cursedEnemy.GetComponentInParent<EnemySpawner>();
                if (cursedEnemy.Spawner != null) cursedEnemy.Spawner.LiveEnemies++;
                if (duration != 0)
                {
                    Debug.Log("duration is: " + duration);
                    //cursedEnemy.StartCoroutine(cursedEnemy.ChangeStateAfterInterval(EnemyState.Blessed, duration));
                    //cursedEnemy.StartCoroutine(cursedEnemy.DestroyAfterDelay(duration));
                    cursedEnemy.StartMethodAfterDelayCoroutine(() => cursedEnemy.Die(false), duration);
                }
                break;

            case EnemyState.Blessed:
                Enemy blessedEnemy = Instantiate(
                data.BlessedVersion,
                pos,
                rot,
                parent).GetComponent<Enemy>();
                break;

        }

        DestroyFarAway();
    }

    public void StartMethodAfterDelayCoroutine(Action method, float time) => StartCoroutine(CallMethodAfterDelay(() => method(), time));
    private IEnumerator CallMethodAfterDelay(Action method, float time)
    {
        while (time > 0)
        {
            time -= Time.unscaledDeltaTime;
            yield return null;
        }

        method();
    }

    private IEnumerator ChangeStateAfterInterval(EnemyState state, float time)
    {
        float t = time;
        while (t > 0)
        {
            t -= Time.deltaTime;
            yield return null;
        }

        ChangeState(state);

    }

    public void AddMultipliers(float hpMultiplier = 0, float sizeMultiplier = 0)
    {
        currentHealth += ((hpMultiplier / 100) * data.Maxhealth);
        Vector3 newSize = new Vector3(1 + (sizeMultiplier / 100),
                                      1 + (sizeMultiplier / 100),
                                      1 + (sizeMultiplier / 100));
        transform.localScale = newSize;
    }

    private void SpawnRandomRagdoll(bool impaled)
    {
        if (impaled)
        {
            if (data.ImpaleRagdolls != null)
                Instantiate(data.ImpaleRagdolls, transform.position, transform.rotation);
            return;
        }
        if (!(data.DeathRagdolls.Length > 0)) return;
        int ragdollsCount = data.DeathRagdolls.Length;
        int i = Random.Range(0, ragdollsCount);

        Instantiate(data.DeathRagdolls[i], transform.position, transform.rotation);
    }

    public virtual void Die(bool hitByPlayer, bool impaled = false)
    {

        if (!Dead && state != EnemyState.Impaled)
        {
            Debug.Log("DIE: " + gameObject.name);
            StopAllCoroutines();
            behaviour = EnemyBehaviour.Stop;
            GetComponent<CapsuleCollider>().enabled = false;
            agent.enabled = false;

            if (hitByPlayer)
            {
                DropItems(data.Heart, data.HeartDropPercentage, data.MaxHeartDrops);
                SpawnRandomRagdoll(impaled);
                DestroyFarAway();
            }
            else anim.SetTrigger("die");
        }

        Dead = true;
    }

    public virtual void DestroyFarAway()
    {
        Debug.Log("KILL FAR AWAY: " + gameObject.name);
        StopAllCoroutines();
        behaviour = EnemyBehaviour.Stop;
        NavMeshAgent nav = GetComponent<NavMeshAgent>();
        if (nav != null) nav.enabled = false;
        transform.position = new Vector3(-1000, -1000, 1000);
        Dead = true;

        if (Spawner == null)
        {
            Scripts.SavedObjects.AddObject(Scripts.SavedObjects.KilledEnemies, saveData);
            Destroy(gameObject, 5);
        }
        else
        {
            Spawner.RemoveEnemy(this, SpawnerInstantiationID);
            Destroy(gameObject, 5);
        }
    }

    private IEnumerator WaitForNullableSpawner()
    {
        while (Spawner != null) { yield return null; }

    }

    protected virtual void Attack()
    {
        isAttacking = true;
        anim.SetTrigger("attack");
        StartCoroutine(AttackCooldown());
        // Check the override of the attack in each specific enemy if 
        // you want to know how it works
    }

    public virtual void ThrowSpecialEffect(Transform spawnPos)
    {
        if (tFeedback != null)
        {
            Vector3 dir = spawnPos.position - transform.position;
            tFeedback.transform.position = spawnPos.position;
            tFeedback.transform.rotation = Quaternion.LookRotation(dir);
            tFeedback.PlayFeedbacks();
        }
    }

    public virtual void MortalSpecialEffect()
    {
        if (tFeedback != null)
        {
            tFeedback.transform.position = player.position + (Vector3.up*0.5f);
            tFeedback.transform.rotation = Quaternion.LookRotation(player.forward);

            tFeedback.PlayFeedbacks();
        }
    }

    private void DropItems(Drop item, float dropProbability, int quantity)
    {
        float percentage;

        if (item != null)
        {
            for (int i = 0; i < quantity; i++)
            {
                percentage = Random.value * 100;

                if (percentage <= dropProbability)
                    InstantiateItemToDrop(item);
            }
        }
    }

    private void InstantiateItemToDrop(Drop item)
    {
        float randomY = Random.Range(0f, 360f);
        Drop i = Instantiate(item, transform.position, Quaternion.Euler(0f, randomY, 0f));
        Rigidbody itemRb = i.GetComponent<Rigidbody>();

        itemRb.AddForce((i.transform.forward + Vector3.up) * data.PopOutForce, ForceMode.Impulse);
    }

    protected virtual void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, data.DetectPlayerRadius);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(initialPosition, data.PatrolRadius);
    }

    public void EnableCanAttack() => canAttack = true;
    public void DisableCanAttack() => canAttack = false;
}
