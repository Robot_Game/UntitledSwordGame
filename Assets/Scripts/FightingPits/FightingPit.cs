﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightingPit : MonoBehaviour
{
    [SerializeField] private EnemySpawner[] spawners;
    [SerializeField] private int rewardAmount;
    [SerializeField] private bool lockPlayer;

    [SerializeField] private AudioClip[] fightPitDoorSfx;
    private Chest chest;
    private Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();

        chest = gameObject.GetComponentInChildren<Chest>();
        if(chest != null)
        chest.SetLootAmount(rewardAmount);
        else Debug.LogError("THIS FIGHTING PIT IS MISSING A CHEST!");

        foreach (EnemySpawner s in spawners)
        {
            s.SetFightingPit(this);
        }
    }

    public void BeginFight()
    {
        if (lockPlayer) 
        {
            AudioManager.Instance.PlaySfxOneShot(fightPitDoorSfx);
            AudioManager.Instance.Fade("ambience", 0);
            AudioManager.Instance.PlayMusic();
            anim.SetTrigger("Lock");
        }

        else StartSpawning();
    }

    public void StartSpawning()
    {
        foreach (EnemySpawner s in spawners)
        {
            s.StartSpawning();
        }
    }
    public void CheckCompletionStatus()
    {
        Debug.Log("Checking Status");
        foreach (EnemySpawner s in spawners)
        {
            if (!s.Finished)
            {
                return;
            }
        }

        Debug.Log("WE COMPLETED THE PIT");
        AudioManager.Instance.PlaySfxOneShot(fightPitDoorSfx);
        AudioManager.Instance.Fade("music", 0);
        AudioManager.Instance.Fade("ambience", 1);
        anim.SetTrigger("Completed");

    }
    public void HideLoot() => chest.Submerge();
    public void GiveLoot() => chest.Emmerge();

}
