﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PitDetectionCol : MonoBehaviour
{

    private LayerMask playerLayer;
    private bool flag;
    private void Awake()
    {
        playerLayer = Scripts.PlayerActions.gameObject.layer;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!flag && other.gameObject.layer == playerLayer)
        {
            GetComponentInParent<FightingPit>().BeginFight();
            flag = true;
        }
    }
}
