﻿using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField, InlineEditor, Required] private PlayerData playerData;

    [SerializeField, Required] private VoidEvent OnExitFloor;

    private PlayerInputActions inputs;
    private DontGoThroughThings dontGoThroughThings;
    private AirDrop airDrop;
    private Rigidbody rb;
    private Vector2 moveInput = Vector3.zero;
    private Vector3 moveDirection = Vector3.zero;
    private Vector3 knockbackDirection = Vector3.zero;
    private float movementSpeed = 8;
    private float yVelocity = 0;
    private float fallingDuration = 0;
    private float mortalSpeed;
    private float airDropSpeed;
    private bool canMove = true;
    private bool canRotate = true;
    private bool canBeHit = true;
    private bool canFall = true;
    private bool isKnockedback = false;
    private bool canCoyoteJump = false;
    private bool airDropFlag = false;
    private bool jumped = false;
    private bool falling = false;
    private bool land = false;
    private bool performingMortal = false;
    private bool airDropping = false;
    private Coroutine coyoteCoroutine;
    private Animator anim;

    public Vector3 InputDirection => moveDirection;
    public Vector3 MoveInput => moveInput;
    public bool IsGrounded { get; private set; }

    private void Awake()
    {
        inputs = new PlayerInputActions();
        AddControls();
        dontGoThroughThings = GetComponent<DontGoThroughThings>();
        airDrop = GetComponentInChildren<AirDrop>();
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        anim.SetBool("moving", true);
    }

    private void OnEnable()
    {
        inputs.Enable();
    }

    private void OnDisable()
    {
        inputs.Disable();
    }

    public void DisableMovementInputs() => inputs.Disable();
    public void EnableMovementInputs() => inputs.Enable();

    private void Start()
    {
        SetMovementSpeed(playerData.WithSwordMovementSpeed);
    }
    private void Update()
    {
        if (playerData.PlayerStatus == PlayerStatus.WithSword)
            SetMovementSpeed(playerData.WithSwordMovementSpeed);
        else
            SetMovementSpeed(playerData.NoSwordMovementSpeed);
    }
    private void FixedUpdate()
    {
        if (canFall) ApplyGravity();
        Move();
        if (canRotate) Rotate();
        if (isKnockedback) ApplyKnockbackForce();

        if (airDropFlag && yVelocity <= 0 && !airDropping)
        {
            airDropFlag = false;
            anim.SetBool("airDropping", true);
        }
    }

    private void AddControls()
    {
        inputs.Gameplay.Move.performed += ctx => moveInput = ctx.ReadValue<Vector2>();
        inputs.Gameplay.Jump.performed += ctx => { if (canCoyoteJump) Jump(); };
    }

    private void Move()
    {
        float moveMagnitude = new Vector3(moveInput.x, 0, moveInput.y).magnitude;
        anim.SetFloat("moveMagnitude", moveMagnitude);


        moveDirection = new Vector3(moveInput.x, yVelocity, moveInput.y);

        if (canMove) rb.MovePosition(rb.position + moveDirection * movementSpeed * Time.fixedDeltaTime);
    }

    private void Rotate()
    {
        Vector3 rotDirection = moveDirection;
        rotDirection.y = 0;

        Quaternion previousRot = transform.rotation;
        if (rotDirection == Vector3.zero) transform.rotation = previousRot;
        else transform.rotation = Quaternion.Slerp(transform.rotation,
            Quaternion.LookRotation(rotDirection),
            playerData.RotationSpeed * Time.fixedDeltaTime);
    }

    public void Jump()
    {
        if (falling) return;
        canCoyoteJump = false;
        jumped = true;
        //GetComponentInChildren<PlayerGroundCheck>().SendMessage("ClearGrounds");
        //OnExitFloor.Raise();
        //SetMovementSpeed(movementSpeed * playerData.JumpMovementSpeedMultiplier);
        //rb.AddForce(Vector3.up * playerData.JumpPower, ForceMode.Impulse);
        yVelocity = playerData.JumpPower;
        fallingDuration = 0;
        anim.SetBool("jumping", true);
    }

    private void ApplyGravity()
    {
        if (IsGrounded)
        {
            //Debug.Log("Is grounded and velocity is: " + yVelocity);
            if (!jumped) yVelocity = 0;
            fallingDuration = 0;
        }
        else
        {

            if (!airDropping && !performingMortal)
            {
                if (yVelocity > -ScriptableObjects.GameData.MaxFallingSpeed)
                {
                    //Debug.Log("Falling right now");
                    yVelocity -= playerData.GravityForce * Time.deltaTime;
                }
                else yVelocity = -ScriptableObjects.GameData.MaxFallingSpeed;

                // If the player is actually falling
                if (yVelocity < 0)
                {
                    // Start counting the falling duration
                    fallingDuration += Time.fixedDeltaTime;

                    // If the player can no longer jump
                    if (!canCoyoteJump && !falling)
                    {
                        falling = true;
                        // Start falling animation
                        anim.SetBool("falling", falling);
                        // Stop movement animation
                        anim.SetBool("moving", false);
                    }
                }
            }
            // If player is Air Dropping
            else if (airDropping)
            {
                if (yVelocity > -airDropSpeed)
                {
                    //Debug.Log("Falling right now");
                    yVelocity -= airDropSpeed * 10 * Time.deltaTime;
                }
                else yVelocity = -airDropSpeed;
            }
            // If player is performing mortal
            else
            {
                if (yVelocity > -mortalSpeed * 2)
                {
                    //Debug.Log("Falling right now");
                    yVelocity -= mortalSpeed * Time.deltaTime;
                }
                else yVelocity = -mortalSpeed * 2;

                rb.velocity = new Vector3(rb.velocity.x, yVelocity, rb.velocity.z);

                return;
            }
        }

        rb.velocity = new Vector3(rb.velocity.x, yVelocity, rb.velocity.z);
    }


    public void GetKnockback(Transform damageSource, float knockbackForce, float duration)
    {
        if (isKnockedback || Scripts.PlayerActions.IsInvulnerable) return;
        Scripts.PlayerMovement.DisableMovement();
        Scripts.PlayerMovement.DisableRotation();
        Scripts.PlayerMovement.DisableJump();
        Scripts.PlayerAttacks.DisableAllAttacks();
        Vector3 damageSourcePosition = damageSource.position;
        damageSourcePosition.y = 0;
        Vector3 playerPos = transform.position;
        playerPos.y = 0;
        Vector3 knockbackDir = (playerPos - damageSourcePosition).normalized;
        knockbackDirection = knockbackDir * knockbackForce;
        transform.rotation = Quaternion.LookRotation(-knockbackDir);
        anim.SetTrigger("Knockback");
        anim.enabled = false; // DEPOIS DE TER A ANIMAÇÃO ISTO JÁ NÃO É PRECISO
        isKnockedback = true;
        Scripts.PlayerActions.EnableInvulnerability();
        StartCoroutine(StopKnockbackAfterInterval(duration));

    }

    private void ApplyKnockbackForce()
    {
        rb.MovePosition(rb.position + knockbackDirection * Time.fixedDeltaTime);
    }

    private IEnumerator StopKnockbackAfterInterval(float duration)
    {
        while (duration > 0)
        {
            duration -= Time.deltaTime;
            yield return null;
        }

        isKnockedback = false;
        Scripts.PlayerMovement.EnableMovement();
        Scripts.PlayerMovement.EnableRotation();
        Scripts.PlayerMovement.EnableJump();
        Scripts.PlayerAttacks.EnableAllAttacks();
        anim.enabled = true; // DEPOIS DE TER A ANIMAÇÃO ISTO JÁ NÃO É PRECISO
        rb.Sleep();
        Scripts.PlayerActions.EnableInvulnerabilityTemporarly();

    }

    public void OnFloorEnter()
    {
        if (playerData.PlayerStatus == PlayerStatus.WithSword)
            SetMovementSpeed(playerData.WithSwordMovementSpeed);
        else
            SetMovementSpeed(playerData.NoSwordMovementSpeed);

        if (falling)
        {
            //Debug.Log("Falling Duration was: " + fallingDuration);
            // If the player fell for longer than the falling duration set, play landing animation
            if (fallingDuration > playerData.MinFallDurationToLand)
            {
                anim.SetBool("land", true);
                fallingDuration = 0;
            }
            else falling = false;
            anim.SetBool("falling", false);
        }
        else if (airDropping)
        {
            anim.SetBool("airDropping", false);
            airDropping = false;
            airDropFlag = false;
            airDrop.HitFloor();
        }
        else if (performingMortal)
        {
            ResumeAnimator();
            performingMortal = false;
        }
        anim.SetBool("moving", true);
        anim.SetBool("jumping", false);

        if (coyoteCoroutine != null) StopCoroutine(coyoteCoroutine);
        canCoyoteJump = true;

        jumped = false;
        rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);
        IsGrounded = true;


    }

    public void OnFloorExit()
    {
        IsGrounded = false;
        if (coyoteCoroutine != null) StopCoroutine(coyoteCoroutine);
        coyoteCoroutine = StartCoroutine("CoyoteTimer");
        StartCoroutine(FloorNotFoundFixer());
    }

    private IEnumerator FloorNotFoundFixer()
    {
        float timer = 5;
        while (timer > 0)
        {
            timer -= Time.deltaTime;
            yield return null;
        }
        if (!IsGrounded)
            transform.position += transform.up * 0.1f;
    }

    public void SetAirDropParameters(float speed)
    {
        airDropSpeed = speed;
        if (jumped && !IsGrounded) airDropFlag = true;
    }

    public void StartAirDropFall()
    {
        airDropping = true;
    }
    public void StartMortalFall()
    {
        if (IsGrounded) return;
        mortalSpeed = 20;
        yVelocity = -mortalSpeed;
        performingMortal = true;
        DisableKinematic();
        EnableGravity();
        PauseAnimator();
    }

    private IEnumerator CoyoteTimer()
    {
        float timer = playerData.CoyoteTime;
        while (timer > 0)
        {
            timer -= Time.deltaTime;
            yield return null;
        }
        canCoyoteJump = false;

    }
    public void SetMovementSpeed(float desiredMovementSpeed) => movementSpeed = desiredMovementSpeed;
    public void SetWithSwordMovementSpeed() => movementSpeed = playerData.WithSwordMovementSpeed;
    public void SetWithoutSwordMovementSpeed() => movementSpeed = playerData.NoSwordMovementSpeed;

    public void EnableMovement() { canMove = true; anim.SetBool("moving", true); }
    public void DisableMovement() { canMove = false; anim.SetBool("moving", false); }

    public void EnableRotation() => canRotate = true;
    public void DisableRotation() => canRotate = false;

    public void EnableJump() => inputs.Gameplay.Jump.Enable();
    public void DisableJump() => inputs.Gameplay.Jump.Disable();

    public void EnableKinematic()
    {
        rb.isKinematic = true;
        rb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
    }
    public void DisableKinematic()
    {
        rb.isKinematic = false;
        rb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
    }

    public void EnableDontGoThroughThings() => dontGoThroughThings.enabled = false;
    public void DisableDontGoThroughThings() => dontGoThroughThings.enabled = false;
    public void EnableGravity() => canFall = true;
    public void DisableGravity()
    {
        yVelocity = 0;
        canFall = false;
    }

    public void EndLanding() { falling = false; anim.SetBool("land", false); }
    public void PauseAnimator() { if (performingMortal) anim.speed = 0; }
    public void ResumeAnimator() => anim.speed = 1;

    public void ShakeCameraOnLanding() => Scripts.CameraController.Shake(playerData.LandingShakeAmplitude,
                                                                         playerData.LandingShakeFrequency,
                                                                         playerData.LandingShakeDuration);
}