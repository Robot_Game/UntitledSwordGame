﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.SceneManagement;

public class PlayerActions : MonoBehaviour
{
    [Title("Player Data")]
    [SerializeField, InlineEditor, Required]
    private PlayerData playerData;

    [Title("Events")]
    [SerializeField, Required]
    private FloatEvent OnHpUpdate;
    [SerializeField, Required]
    private IntEvent OnHeartPickUp;

    private PlayerInputActions inputs;
    private Chest currentChest;
    private Checkpoint currentCheckpoint;
    private List<Interactible> interactiblesInRange;
    private Cheats cheats;
    private Rigidbody rb;
    private Animator anim;
    private float currentHp;
    private bool isNearCheckpoint;
    private bool isNearInteractible;
    private bool isNearChest;
    private bool usingInteractible;
    private bool interactibleIsLever;


    private PlayerSaveData playerSaveData;

    public bool IsAlive { get; private set; }
    public bool IsInvulnerable { get; private set; }
    public int CurrentHearts { get; private set; }
    public Interactible CurrentInteractible { get; private set; }
    public Vector3 SpawnPos { get; private set; }
    public string CurrentScene { get; private set; }

    private void Awake()
    {
        playerSaveData = new PlayerSaveData(this);

        inputs = new PlayerInputActions();
        AddControls();
        cheats = FindObjectOfType<Cheats>();
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();

        interactiblesInRange = new List<Interactible>();

        currentHp = playerData.MaxHp;
        CurrentHearts = 0;

        SpawnPos = transform.position;


    }

    protected virtual void Start()
    {
        ManageStartState();
        IsAlive = true;
    }

    /*private void ManageStartState()
    {
        SavableObject tmpData = new SavableObject();
        if (SavedObjects.IsObjectSaved(SavedObjects.SavedObjectsList, ID, ref tmpData))
        {
            playerSaveData = (PlayerSaveData)tmpData;
            Debug.Log("TMP DATA SPAWN POSITION: " + playerSaveData.SpawnPosX + "," + playerSaveData.SpawnPosY + "," + playerSaveData.SpawnPosZ);

            SpawnPos = new Vector3(
                playerSaveData.SpawnPosX,
                playerSaveData.SpawnPosY,
                playerSaveData.SpawnPosZ);

            Debug.Log("Spawn Pos is: " + SpawnPos);

            transform.position = SpawnPos;
            CurrentHearts = playerSaveData.CurrentHearts;
        }
    }*/
    private void ManageStartState()
    {
        if (Scripts.SavedObjects.IsObjectSaved(
            Scripts.SavedObjects.PlayerSavedData, ref playerSaveData))
        {
            if (Scripts.ScenesManager.CurrentScene == playerSaveData.CurrentScene)
                SpawnPos = new Vector3(
                    playerSaveData.SpawnPosX,
                    playerSaveData.SpawnPosY,
                    playerSaveData.SpawnPosZ);

            transform.position = SpawnPos;
            CurrentHearts = playerSaveData.CurrentHearts;
            OnHeartPickUp.Raise(CurrentHearts);
        }
    }

    private void OnEnable()
    {
        inputs.Enable();
        GameEvents.SaveInitiated += Save;
    }

    private void OnDisable()
    {
        inputs.Disable();
        GameEvents.SaveInitiated -= Save;
    }

    public void DisableActionInputs() => inputs.Disable();
    public void EnableActionInputs() => inputs.Enable();

    private void AddControls()
    {
        inputs.Gameplay.Interact.performed += ctx =>
        {
            if (isNearInteractible == true) Interact();
            if (isNearCheckpoint == true) OpenStore();
            if (isNearChest == true) OpenChest();
        };

        inputs.Gameplay.HoldInteract.performed += ctx => { if (isNearCheckpoint == true) ActivateCheckpoint(); };

        inputs.Gameplay.Move.performed += ctx =>
        { if (usingInteractible && interactibleIsLever && Scripts.PlayerMovement.MoveInput.x > 0.8f) ActivateInteractible(); };

        inputs.Gameplay.Move.performed += ctx =>
        { if (usingInteractible && interactibleIsLever && Scripts.PlayerMovement.MoveInput.x < -0.8f) DeactivateInteractible(); };
    }

    public void UpdateHp(float value)
    {
        if (!IsAlive || IsInvulnerable) return;

        currentHp = ClampHp(value);

        // Called on UI when holiness is updated
        Debug.Log("The current HP is: " + currentHp);
        OnHpUpdate.Raise(currentHp);
        /* ATENÇÃO: POR CAUSA DE UM BUG NOS DYNAMIC EVENTS DO UNITY, 
        ESTES MÉTODOS TÊM QUE ESTAR AQUI ATÉ O BUG SE RESOLVER*/
        //Scripts.UIManager.UpdateHealthBarSize(currentHp);
        Scripts.UIManager.UpdateHealthBarColor(currentHp);
        if (currentHp <= 0 && IsAlive) Die();
    }

    private void Die()
    {
        if (IsAlive)
        {
            IsAlive = false;
            anim.SetTrigger("Die");
            Scripts.CameraController.StopFollowing();
            UpdateHp(0);
        }
    }

    public void RestartGame() => Scripts.UIManager.ShowBlackScreen("RestartScene");

    private void Spawn()
    {
        currentHp = 0;

        Scripts.PlayerMovement.EnableJump();
        Scripts.PlayerMovement.EnableMovement();
        Scripts.PlayerMovement.EnableRotation();
        Scripts.PlayerMovement.DisableKinematic();
        Scripts.PlayerAttacks.EnableGroundedAttacks();

        CheckpointManager cm = FindObjectOfType<CheckpointManager>();
        transform.position = SpawnPos;

        // More spawn implementation
    }

    public void SetSpawnPos(Vector3 pos) => SpawnPos = pos;

    private float ClampHp(float damage)
        => Mathf.Clamp(currentHp - damage, 0, playerData.MaxHp);

    private void OnCollisionEnter(Collision collision)
    {
        Item i = collision.transform.GetComponent<Item>();

        if (i != null)
        {
            PickUpItem(i);
            return;
        }
    }

    public void PickUpItem(Item i)
    {

        Drop d = i as Drop;

        if (d != null)

            switch (d.Type)
            {
                case DropType.Curse:
                    UpdateHp(-playerData.CursePickupValue);
                    break;
                case DropType.Heart:
                    CurrentHearts++;
                    OnHeartPickUp.Raise(CurrentHearts);
                    break;
            }
        else
        {
            Loot loot = i as Loot;
            if (loot != null)
            {
                CurrentHearts += loot.Amount;
                OnHeartPickUp.Raise(CurrentHearts);
            }
        }

        // Later replace with proper event
        i.SendMessage("Vanish");
    }

    private void OnTriggerEnter(Collider other)
    {

        Interactible interactible = other.GetComponent<Interactible>();
        if (interactible != null)
        {
            interactiblesInRange.Add(interactible);
            if (isNearInteractible == false) isNearInteractible = true;

            SetCurrentInteractible();

            return;
        }

        Checkpoint checkpoint = other.GetComponent<Checkpoint>();
        if (checkpoint != null)
        {
            currentCheckpoint = checkpoint;
            if (isNearCheckpoint == false) isNearCheckpoint = true;

            return;
        }

        Chest chest = other.GetComponent<Chest>();
        if (chest != null)
        {
            currentChest = chest;
            if (isNearChest == false)
            {
                isNearChest = true;
                Scripts.UIManager.ShowActionKey("Open Chest");
            }

            return;
        }



        if (other.gameObject.layer == LayerMask.NameToLayer("Instakill"))
        {
            // Stop following the player with the camera and restart the game
            if (IsAlive)
            {
                Scripts.CameraController.StopFollowing();
                RestartGame();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {

        Interactible interactible = other.GetComponent<Interactible>();
        if (interactible != null)
        {
            interactiblesInRange.Remove(interactible);
            if (interactiblesInRange.Count == 0)
            {
                isNearInteractible = false;
                if (!isNearCheckpoint && !isNearChest) Scripts.UIManager.ShowActionKey(false);
            }
            return;
        }

        Checkpoint checkpoint = other.GetComponent<Checkpoint>();
        if (checkpoint != null)
        {
            if (isNearCheckpoint == true)
            {
                isNearCheckpoint = false;
                if (!isNearInteractible && !isNearChest) Scripts.UIManager.ShowActionKey(false);
            }
            return;
        }

        Chest chest = other.GetComponent<Chest>();
        if (chest != null)
        {

            if (isNearChest == true)
            {
                isNearChest = false;
                if (!isNearInteractible && !isNearCheckpoint) Scripts.UIManager.ShowActionKey(false);
            }

            return;
        }
    }

    private void OnElevatorDetection(Collider other)
    {
        if (other.CompareTag("Elevator/InsideTrigger"))
        {
            other.GetComponentInParent<Elevator>().Activate();
        }
    }

    private void OpenStore()
    {
        StartCoroutine(Scripts.GameManager.OpenStore());
    }

    private void OpenChest()
    {
        isNearChest = false;
        currentChest.OpenChest();
        Scripts.UIManager.ShowActionKey(false);
    }

    private void ActivateCheckpoint()
    {
        if (!currentCheckpoint.IsVirgin && !currentCheckpoint.IsActive)
        {
            currentCheckpoint.Activate();
            Scripts.UIManager.ShowActionKey("Open Store");
        }
    }

    private void Interact()
    {
        bool flag = false;
        if (!usingInteractible)
        {
            usingInteractible = true;
            anim.SetBool("MovingToInteractible", true);
            Scripts.UIManager.ShowActionKey(false);
            if (CurrentInteractible.Type == InteractibleTypes.Lever) interactibleIsLever = true;
            else if (interactiblesInRange.Count == 1)
            {
                interactiblesInRange.Remove(CurrentInteractible);
            }
            flag = true;
        }
        if (usingInteractible && flag == false)
        {
            if (CurrentInteractible.ActivatesMultipleTimes)
            {
                LeaveInteractible();
            }
        }
    }

    private void SetCurrentInteractible()
    {
        Interactible nearestinteractible = GetNearestInteractible();

        if (nearestinteractible.Type == InteractibleTypes.Lever)
        {
            CurrentInteractible = nearestinteractible.GetComponent<Lever>();
            Scripts.UIManager.ShowActionKey("Use Lever");
        }

        if (nearestinteractible.Type == InteractibleTypes.Lock)
        {
            CurrentInteractible = nearestinteractible.GetComponent<Lock>();

            Scripts.UIManager.ShowActionKey("Unlock");
        }

    }

    private Interactible GetNearestInteractible()
    {
        Interactible nearestinteractible = interactiblesInRange[0];

        if (interactiblesInRange.Count > 1)
        {
            float minDist = float.MaxValue;
            Vector3 currentPos = transform.position;
            currentPos.y = 0;
            foreach (Interactible i in interactiblesInRange)
            {
                Vector3 iPos = i.transform.position;
                iPos.y = 0;
                float dist = Vector3.Distance(iPos, currentPos);
                if (dist < minDist)
                {
                    nearestinteractible = i;
                    minDist = dist;
                }
            }
        }

        return nearestinteractible;
    }



    // Called on animation
    private void UseInteractible()
    {
        Scripts.PlayerMovement.DisableMovement();
        Scripts.PlayerMovement.DisableRotation();
        Scripts.PlayerMovement.DisableJump();
        Scripts.PlayerAttacks.DisableAllAttacks();
        StartCoroutine("MoveToInteractible");
    }

    //LEAVE IS NOT WORKING PROPERLY
    // FOR SOME REASON STOP COROUTINE IS NOT WORKING
    public void LeaveInteractible(bool legitLeaving = true)
    {
        StopCoroutine("MoveToInteractible");
        anim.SetBool("MovingToInteractible", false);

        Debug.Log("Legit leaving: " + legitLeaving);

        if (CurrentInteractible.Type == InteractibleTypes.Lever)
        {
            anim.SetBool("UsingLever", false);
            Scripts.UIManager.ShowActionKey("Use Lever");
            interactibleIsLever = false;
        }

        if (CurrentInteractible.Type == InteractibleTypes.Lock)
        {
            anim.SetBool("UsingLock", false);
            if (legitLeaving) interactiblesInRange.Remove(CurrentInteractible);
            else Scripts.UIManager.ShowActionKey("Unlock");


            if (interactiblesInRange.Count == 0)
            {
                isNearInteractible = false;
                Scripts.UIManager.ShowActionKey(false);
            }
            else SetCurrentInteractible();
        }

        transform.SetParent(null);
        usingInteractible = false;
    }

    private IEnumerator MoveToInteractible()
    {
        Transform positionPlaceholder;
        if (!CurrentInteractible.IsActive) positionPlaceholder = CurrentInteractible.transform.Find("PlayerActivatePlaceholder");
        else positionPlaceholder = CurrentInteractible.transform.Find("PlayerDeactivatePlaceholder");

        Vector3 destination = new Vector3(
            positionPlaceholder.position.x,
            transform.position.y,
            positionPlaceholder.position.z);
        Vector3 destinationRot = destination;
        destinationRot.y = 0;
        Vector3 interactiblePos = CurrentInteractible.transform.position;
        interactiblePos.y = 0;
        Vector3 playerPos = transform.position;
        playerPos.y = 0;
        float time = 0;
        bool failed = false;
        while (transform.position != destination
        || Vector3.Angle(transform.forward, destinationRot - playerPos) > 3f)
        {
            /////Movement
            destination = new Vector3(positionPlaceholder.position.x,
                                      transform.position.y,
                                      positionPlaceholder.position.z);

            float step = 5 * Time.deltaTime; // calculate distance to move
            transform.position = Vector3.MoveTowards(transform.position, destination, step);

            /////Rotation
            playerPos = transform.position;
            playerPos.y = 0;

            var lookPos = destinationRot - playerPos;
            lookPos.y = 0;

            var rotation = Quaternion.LookRotation(lookPos);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 5 * Time.deltaTime);
            time += Time.deltaTime;
            if (time > 2)
            {
                failed = true;
                break;
            }
            yield return null;
        }
        time = 0;
        if (!failed)
            while (Vector3.Angle(transform.forward, interactiblePos - playerPos) > 0.5f)
            {
                playerPos = transform.position;
                playerPos.y = 0;

                var lookPos = interactiblePos - transform.position;
                lookPos.y = 0;
                var rotation = Quaternion.LookRotation(lookPos);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 5 * Time.deltaTime);
                time += Time.deltaTime;
                if (time > 2)
                {
                    failed = true;
                    break;
                }
                yield return null;
            }
        if (!failed)
        {
            transform.SetParent(positionPlaceholder);
            if (CurrentInteractible.Type == InteractibleTypes.Lever) anim.SetBool("UsingLever", true);
            if (CurrentInteractible.Type == InteractibleTypes.Lock) 
            {
                AudioManager.Instance.PlaySfxOneShot(CurrentInteractible.InteractSfx);
                anim.SetBool("UsingLock", true);
            }
            anim.SetBool("MovingToInteractible", false);
        }
        else LeaveInteractible(false);
    }

    private void Save()
    {
        playerSaveData.SpawnPosX = SpawnPos.x;
        playerSaveData.SpawnPosY = SpawnPos.y;
        playerSaveData.SpawnPosZ = SpawnPos.z;
        playerSaveData.CurrentHearts = CurrentHearts;
        playerSaveData.CurrentScene = Scripts.ScenesManager.CurrentScene;
        Scripts.SavedObjects.AddObject(Scripts.SavedObjects.PlayerSavedData, playerSaveData);
    }

    public void UpdateCurrentHearts(int amount)
    {
        CurrentHearts += amount;
        OnHeartPickUp.Raise(CurrentHearts);
    }
    private void UsingInteractible() => CurrentInteractible.GetComponent<Animator>().SetBool("Using", true);
    private void StopUsingInteractible() => CurrentInteractible.GetComponent<Animator>().SetBool("Using", false);

    private void ActivateInteractible() => CurrentInteractible.Activate(true);
    private void DeactivateInteractible() => CurrentInteractible.Activate(false);

    public void EnableInvulnerability() => IsInvulnerable = true;
    public void DisableInvulnerability() => IsInvulnerable = false;
    public void EnableInvulnerabilityTemporarly() => StartCoroutine(EnableInvulnerableFrames(playerData.InvulnerabilityDuration));
    private IEnumerator EnableInvulnerableFrames(float duration)
    {
        IsInvulnerable = true;
        while (duration > 0)
        {
            duration -= Time.deltaTime;
            yield return null;
        }
        IsInvulnerable = false;
    }


}
