﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetSelectionController : MonoBehaviour
{
    [SerializeField, Range(0, 1)] private float slowMotionSpeed = 0.1f;
    [SerializeField] private float radius = 10;
    [SerializeField] private float duration = 5;
    [SerializeField] private LayerMask targetMask;

    private PlayerInputActions inputs;
    private List<Enemy> enemiesInRange;
    private Enemy currentEnemy;
    private Vector2 lookInput = Vector2.zero;
    private Vector2 lookDirection = Vector2.zero;
    private float timer;
    private bool canceled;

    public Enemy SelectedEnemy { get; private set; }
    public bool IsChoosingTarget { get; private set; }

    private void Awake()
    {
        inputs = new PlayerInputActions();
        AddControls();

        enemiesInRange = new List<Enemy>();
    }

    private void Start()
    {
        inputs.Disable();
    }

    private void OnEnable()
    {
        inputs.Enable();
    }

    private void OnDisable()
    {
        inputs.Disable();
    }

    private void AddControls()
    {
        inputs.Gameplay.ConfirmTargetSelection.performed += ctx => { if (currentEnemy != null) Confirm(); else Cancel(); };

        inputs.Gameplay.CancelTargetSelection.performed += ctx => Cancel();

        inputs.Gameplay.Move.performed += ctx => lookInput = ctx.ReadValue<Vector2>();

        inputs.Gameplay.Move.performed += ctx => { if (IsChoosingTarget) currentEnemy = GetEnemyInDirection(lookDirection); };
    }

    // Update is called once per frame
    void Update()
    {
        if (IsChoosingTarget && SelectedEnemy == null)
        {
            lookDirection = lookInput;

            timer += Time.unscaledDeltaTime;
            if (timer >= duration) Cancel();

            if (canceled) ResetValues();

        }
        if (IsChoosingTarget && SelectedEnemy != null) ResetValues();

        if(currentEnemy != null) Debug.DrawLine(transform.position, currentEnemy.transform.position, Color.red, 0.01f);
    }

    public bool InitializeTargetSelection(float detectionRadius = 0, float slowMoSpeed = 0, float time = 0, LayerMask mask = default)
    {
        if(detectionRadius != 0) radius = detectionRadius;
        if(slowMoSpeed != 0) slowMotionSpeed = slowMoSpeed;
        if(time != 0) duration = time;
        if(mask != (LayerMask)default) targetMask = mask;

        inputs.Enable();
        Time.timeScale = slowMotionSpeed;
        Time.fixedDeltaTime = 0.02F * Time.timeScale;
        SelectedEnemy = null;
        IsChoosingTarget = true;
        if (enemiesInRange.Count > 0)
            enemiesInRange = new List<Enemy>();
        enemiesInRange = GetEnemiesInRange();
        canceled = false;

        return IsChoosingTarget;
    }

    private List<Enemy> GetEnemiesInRange()
    {
        List<Enemy> enemies = new List<Enemy>();

        Collider[] hitColliders = Physics.OverlapSphere(transform.position,
                                                        radius,
                                                        targetMask);

        LayerMask enemyMask = LayerMask.GetMask("Enemy");
        foreach (Collider col in hitColliders)
        {
            if (targetMask == enemyMask)
            {
                Enemy e = col.GetComponent<Enemy>();
                if (e != null) enemies.Add(e);
            }
        }

        return enemies;
    }



    private Enemy GetEnemyInDirection(Vector2 dir)
    {

        if (dir == Vector2.zero) dir = transform.forward;
        Enemy closestEnemy = null;
        float minAngle = Mathf.Infinity;
        Vector3 dir3D = new Vector3(dir.x, 0, dir.y);

        foreach (Enemy e in enemiesInRange)
        {
            float angleToTarget = Vector3.Angle(dir3D, (e.transform.position - transform.position).normalized);
            if (angleToTarget < minAngle)
            {
                closestEnemy = e;
                minAngle = angleToTarget;
            }
        }

        Debug.DrawLine(transform.position, closestEnemy.transform.position, Color.red, 0.01f);

        return closestEnemy;
    }

    private void ResetValues()
    {

        inputs.Disable();
        Time.timeScale = 1;
        Time.fixedDeltaTime = 0.02f;
        currentEnemy = null;
        IsChoosingTarget = false;
        enemiesInRange = new List<Enemy>();
        timer = 0;
        canceled = false;
    }
    public void Confirm() => SelectedEnemy = currentEnemy;
    public void Cancel() => canceled = true;
}
