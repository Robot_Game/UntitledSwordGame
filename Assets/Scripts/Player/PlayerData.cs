﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "Player", menuName = "Game Entities/Player")]
public class PlayerData : ScriptableObject
{
    [Title("Basic Stats")]
    [SerializeField]
    private float maxHp;

    [Title("Movement")]
    [SerializeField]
    private float noSwordMovementSpeed;
    [SerializeField]
    private float withSwordMovementSpeed;
    [SerializeField]
    private float chargingAttackMovementSpeed;
    [SerializeField]
    private float rotationSpeed;
    [SerializeField]
    private float gravityForce;

    [Title("Jump")]
    [SerializeField]
    private float jumpPower;
    [SerializeField, Range(0, 1)]
    private float jumpMovementSpeedMultiplier = 1;
    [SerializeField]
    private float coyoteTime = 0.1f;
    [SerializeField]
    private float minFallDurationToLand = 0.3f;

    [Title("Items")]
    [SerializeField]
    private float cursePickupValue;
    [Title("On Damage Received")]
    [SerializeField]
    private float invulnerabilityDuration;
    [Title("Other")]
    [SerializeField]
    private LayerMask groundDetectionLayers;
    [Title("CameraShakes")]
    [SerializeField] private float landingShakeAmplitude;
    [SerializeField] private float landingShakeFrequency;
    [SerializeField] private float landingShakeDuration;
    [SerializeField] private float mortalShakeAmplitude;
    [SerializeField] private float mortalShakeFrequency;
    [SerializeField] private float mortalShakeDuration;

    public float MaxHp => maxHp;
    public float NoSwordMovementSpeed => noSwordMovementSpeed;
    public float WithSwordMovementSpeed => withSwordMovementSpeed;
    public float ChargingAttackMovementSpeed => chargingAttackMovementSpeed;
    public float RotationSpeed => rotationSpeed;
    public float GravityForce => gravityForce;
    public float JumpPower => jumpPower;
    public float JumpMovementSpeedMultiplier => jumpMovementSpeedMultiplier;
    public float CoyoteTime => coyoteTime;
    public float MinFallDurationToLand => minFallDurationToLand;
    public float InvulnerabilityDuration => invulnerabilityDuration;
    public LayerMask GroundDetectionLayers => groundDetectionLayers;
    public PlayerStatus PlayerStatus { get; set; } = PlayerStatus.WithSword;
    public float CursePickupValue => cursePickupValue;
    public float LandingShakeAmplitude => landingShakeAmplitude;
    public float LandingShakeFrequency => landingShakeFrequency;
    public float LandingShakeDuration => landingShakeDuration;
    public float MortalShakeAmplitude => mortalShakeAmplitude;
    public float MortalShakeFrequency => mortalShakeFrequency;
    public float MortalShakeDuration => mortalShakeDuration;
}
