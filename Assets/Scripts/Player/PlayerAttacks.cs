﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using MoreMountains.Feedbacks;

public class PlayerAttacks : MonoBehaviour
{
    [SerializeField, InlineEditor, Required]
    private PlayerData playerData;
    [Title("Combos")]
    [SerializeField, Required] private float timeToResetAttacks;
    [Title("Freeze Frames")]
    [SerializeField, Required] private float freezeFrameSpeed;
    [SerializeField, Required] private float freezeFrameDuration;
    [Title("Snap to Enemy")]
    [SerializeField, Required]
    private GameObject emptyRefObj;
    [SerializeField] private float snapRadius;
    [SerializeField] private int segments;
    [SerializeField] private float aperture;
    [SerializeField] private bool ignoreTriggers;
    [SerializeField] private LayerMask layerToCheck;

    [Title("Attacks VFX")]
    [SerializeField] Transform spawnPos;
    [SerializeField] Transform spawnPosReference;
    [SerializeField] MMFeedbacks[] slashesVFX;
    [SerializeField] MMFeedbacks mortalVFX;
    [SerializeField] MMFeedbacks mortalImpactVFX;
    [SerializeField] MMFeedbacks spinVFX;

    private PlayerInputActions inputs;
    private PlayerMovement playerMovement;
    private PlayerAbilities abilityManager;
    private Sword sword;
    private Animator animator;
    private LineOfSight snapSight;
    private Coroutine attackStateCoroutine;
    private int currentAttackState = 0;

    public bool HasSword { get; set; } = true;
    public bool AllowSlowMotion { get; set; } = false;
    public bool HasRandomSlowMoChance { get; set; } = false;
    public float SlowMoChance { get; set; }
    public float SlowMoSpeed { get; set; }
    public float SlowMoDuration { get; set; }
    private void Awake()
    {
        inputs = new PlayerInputActions();
        AddControls();

        playerMovement = GetComponent<PlayerMovement>();
        animator = GetComponent<Animator>();
        abilityManager = GetComponentInChildren<PlayerAbilities>();
        sword = FindObjectOfType<Sword>();

        snapSight = gameObject.AddComponent<LineOfSight>();
    }

    private void OnEnable()
    {
        inputs.Enable();
    }

    private void OnDisable()
    {
        inputs.Disable();
    }

    public void DisableAttackInputs() => inputs.Disable();
    public void EnableAttackInputs() => inputs.Enable();

    private void AddControls()
    {
        inputs.Gameplay.SquareAttack.performed += ctx => HandleSquareAttack();
        inputs.Gameplay.CircleAttack.performed += ctx => HandleCircleAttack();
        inputs.Gameplay.TriangleAttack.performed += ctx => HandleTriangleAttack();
        inputs.Gameplay.R1Attack.performed += ctx => HandleR1Attack();
        //inputs.Gameplay.ChargedR1Attack.performed += ctx => HandleR1ChargedAttack();
        inputs.Gameplay.ChargedL1Attack.performed += ctx => HandleL1Attack();

        inputs.Gameplay.Charging.performed += ctx => StartChargingAttack();

        inputs.Gameplay.ChargedSquareAttack.performed += ctx => ChargedSquareAttack();
        inputs.Gameplay.StopChargedSquareAttack.performed += ctx => StopChargingAttack();

        //inputs.Gameplay.ChargedR1Attack.performed += ctx => ChargedSquareAttack();
        inputs.Gameplay.StopChargedR1Attack.performed += ctx => HandleR1ChargedAttack();

        inputs.Gameplay.ChargedCircleAttack.performed += ctx => ChargedAttack("ChargedCircle"); // Doesnt exist an attack yet
        inputs.Gameplay.ChargedTriangleAttack.performed += ctx => ChargedAttack("ChargedMortal");
        //inputs.Gameplay.ChargedR1Attack.performed += ctx => ChargedAttack("ChargedThrowSword");

        inputs.Gameplay.ChargedCircleAttack.canceled += ctx => StopChargingAttack();
        inputs.Gameplay.ChargedSquareAttack.canceled += ctx => StopChargingAttack();
        inputs.Gameplay.ChargedTriangleAttack.canceled += ctx => StopChargingAttack();
    }

    private void HandleSquareAttack()
    {
        // If is grounded 
        if (playerMovement.IsGrounded)
        {
            if (HasSword)
            {
                if (currentAttackState == 2)
                {
                    AudioManager.Instance.PlaySfxOneShot(sword.Data.SpinAttackSfx);
                    Attack("Spin");
                    SetSlowMotionParameters("Spin");
                }
                else
                {
                    AudioManager.Instance.PlaySfxOneShot(sword.Data.SlashesSfx);
                    Attack("Slash", currentAttackState);
                    SetSlowMotionParameters("Slash");
                }
            }
        }
        // Air Drop
        else
        {
            abilityManager.abilities[2].Cast();
            SetSlowMotionParameters("Air Drop");
        }
    }

    private void HandleCircleAttack()
    {
        //IMPLEMENT WHATEVER CIRCLE ATTACK WE WANT
    }

    private void HandleTriangleAttack()
    {

        if (playerMovement.IsGrounded && HasSword)
        {
            if (currentAttackState == 2)
            {
                AudioManager.Instance.PlaySfxOneShot(sword.Data.MortalAttackSfx);
                Attack("Mortal");
                SetSlowMotionParameters("Mortal");
            }
        }
    }

    private void HandleR1Attack()
    {
        bool canCast = true;
        if (!playerMovement.IsGrounded)
        {
            DashImpale dash = abilityManager.abilities[0] as DashImpale;
            if (!dash.CanAerialDash) canCast = false;
        }

        SetSlowMotionParameters("Impale");

        if (canCast)
            abilityManager.abilities[0].Cast();
    }

    private void HandleR1ChargedAttack()
    {
        if (playerMovement.IsGrounded && HasSword) // DECIDIR SE PODEMOS OU NÃO FAZER O THROW QUANDO ESTAMOS NO AR
        {
            animator.SetBool("Charging", false);
            abilityManager.abilities[3].Cast();
            SetSlowMotionParameters("Throw Sword");
        }
    }

    private void HandleL1Attack()
    {
        abilityManager.abilities[1].Cast();
    }

    public void Attack(string attackType, int i = 0, bool setAttackStats = true)
    {

        if (!HasSword) return;

        SnapToEnemy(playerMovement.InputDirection, snapRadius, segments, aperture);

        if (setAttackStats) sword.SetAttackStats(attackType, i);

        if (attackType == "Slash") animator.SetTrigger(attackType + (i + 1));
        else animator.SetTrigger(attackType);

        currentAttackState++;
        if (attackStateCoroutine != null) StopCoroutine(attackStateCoroutine);
        if (currentAttackState >= 3) ResetAttackState();
        else attackStateCoroutine = StartCoroutine(ResetAttackStateAfterInterval());
    }

    public void PlayVFX(string attackType)
    {
        spawnPos.position = spawnPosReference.position;
        spawnPos.rotation = spawnPosReference.rotation;

        if (attackType == "Slash") slashesVFX[currentAttackState - 1]?.PlayFeedbacks();
        else if (attackType == "Mortal") mortalVFX?.PlayFeedbacks();
        else if (attackType == "Spin") spinVFX?.PlayFeedbacks();
        else if (attackType == "MImpact") mortalImpactVFX?.PlayFeedbacks();
    }

    public void UseAbility(string abilityType, bool canMove = false, bool canRotate = false, bool snapToEnemy = false)
    {
        if (!HasSword && abilityType != "Air Drop") return;
        if (!canMove) playerMovement.DisableMovement();
        if (!canRotate) playerMovement.DisableRotation();
        if (snapToEnemy) SnapToEnemy(playerMovement.InputDirection, snapRadius, segments, aperture);
        currentAttackState = 0;
        animator.SetTrigger(abilityType);
    }

    private void ChargedAttack(string attackType)
    {
        if (!HasSword) return;

        SnapToEnemy(playerMovement.InputDirection, snapRadius, segments, aperture);

        // Perform Charged attack
        sword.SetAttackStats(attackType);
        animator.SetTrigger(attackType);

        StopChargingAttack();
    }
    // Chamar na animation do throw sword charged
    private void ThrowSword() => sword.ThrowSword();

    private void ChargedSquareAttack()
    {
        if (HasSword)
        {
            Debug.Log("Charged SPIN WAS CALLED");
            // Perform Charged attack
            sword.SetAttackStats("ChargedSpin");
            animator.SetBool("ChargedSpin", true);
            playerMovement.SetMovementSpeed(playerData.ChargingAttackMovementSpeed);
        }
    }

    private void StartChargingAttack()
    {
        if (HasSword)
        {
            animator.SetBool("Charging", true);
            playerMovement.SetMovementSpeed(playerData.ChargingAttackMovementSpeed);
        }
    }

    private void StopChargingAttack()
    {
        animator.SetBool("Charging", false);
        animator.SetBool("ChargedSpin", false);
        playerMovement.SetMovementSpeed(playerData.WithSwordMovementSpeed);
        playerMovement.EnableRotation();
    }

    private IEnumerator ResetAttackStateAfterInterval()
    {
        float time = timeToResetAttacks;
        while (time > 0)
        {
            time -= Time.deltaTime;
            yield return null;
        }
        ResetAttackState();
    }

    public void ManageSlowMotionEffect()
    {
        if (!AllowSlowMotion) return;

        if (HasRandomSlowMoChance)
        {
            float rand = Random.Range(0f, 1f);
            if (rand > SlowMoChance)
            {
                SlowMoSpeed = freezeFrameSpeed;
                SlowMoDuration = freezeFrameDuration;
            }
        }

        Scripts.GameManager.StartSlowMotionEffect(SlowMoSpeed, SlowMoDuration);
    }
    private void SetSlowMotionParameters(string attackType = default)
    {
        switch (attackType)
        {
            case "Slash":
                AllowSlowMotion = sword.Data.SlashSlowMotionEffect;
                HasRandomSlowMoChance = sword.Data.SlashHasRandomSlowMoChance;
                SlowMoChance = sword.Data.SlashSlowMoChance;
                SlowMoSpeed = sword.Data.SlashSlowMotionSpeed;
                SlowMoDuration = sword.Data.SlashSlowMotionDuration;
                break;
            case "Spin":
                AllowSlowMotion = sword.Data.SpinSlowMotionEffect;
                HasRandomSlowMoChance = sword.Data.SpinHasRandomSlowMoChance;
                SlowMoChance = sword.Data.SpinSlowMoChance;
                SlowMoSpeed = sword.Data.SpinSlowMotionSpeed;
                SlowMoDuration = sword.Data.SpinSlowMotionDuration;
                break;
            case "Mortal":
                AllowSlowMotion = sword.Data.MortalSlowMotionEffect;
                HasRandomSlowMoChance = sword.Data.MortalHasRandomSlowMoChance;
                SlowMoChance = sword.Data.MortalSlowMoChance;
                SlowMoSpeed = sword.Data.MortalSlowMotionSpeed;
                SlowMoDuration = sword.Data.MortalSlowMotionDuration;
                break;
            case "Impale":
                AllowSlowMotion = sword.Data.ImpaleSlowMotionEffect;
                HasRandomSlowMoChance = sword.Data.ImpaleHasRandomSlowMoChance;
                SlowMoChance = sword.Data.ImpaleSlowMoChance;
                SlowMoSpeed = sword.Data.ImpaleSlowMotionSpeed;
                SlowMoDuration = sword.Data.ImpaleSlowMotionDuration;
                break;
            case "Throw Sword":
                AllowSlowMotion = sword.Data.ThrowSlowMotionEffect;
                HasRandomSlowMoChance = sword.Data.ThrowHasRandomSlowMoChance;
                SlowMoChance = sword.Data.ThrowSlowMoChance;
                SlowMoSpeed = sword.Data.ThrowSlowMotionSpeed;
                SlowMoDuration = sword.Data.ThrowSlowMotionDuration;
                break;
            case "Air Drop":
                AllowSlowMotion = sword.Data.AirDropSlowMotionEffect;
                HasRandomSlowMoChance = sword.Data.AirDropHasRandomSlowMoChance;
                SlowMoChance = sword.Data.AirDropSlowMoChance;
                SlowMoSpeed = sword.Data.AirDropSlowMotionSpeed;
                SlowMoDuration = sword.Data.AirDropSlowMotionDuration;
                break;
            default:
                AllowSlowMotion = false;
                break;
        }
    }

    public void SnapToEnemy(Vector3 snapDirection, float snapRadius, int segments, float aperture)
    {
        snapDirection = playerMovement.InputDirection;

        // If we aren't giving input, character snaps forward
        if (snapDirection == Vector3.zero) snapDirection = transform.forward;

        // Calculates points given the snap direction and radius
        snapSight.CalculatePoints(emptyRefObj, transform, snapDirection, snapRadius, segments, aperture);

        DrawLines();    // TEST ONLY!!!!!!!!!!!!!!!!

        // Checks if enemy is crossing our snap lines
        Collider target = snapSight.CheckForHits(transform, layerToCheck, ignoreTriggers);

        // If enemy is found, we snap to it
        if (target != null)
            transform.LookAt(new Vector3(
                target.transform.position.x,
                transform.position.y,
                target.transform.position.z));
    }

    // TEST ONLY!!!!!!!!!!!!!!!!
    private void DrawLines()
    {
        for (int i = 0; i < snapSight.SnapNodes.Count; i++)
        {
            Debug.DrawLine(transform.position + (transform.up * 0.5f), snapSight.SnapNodes[i], Color.red, 3f);
        }
    }

    public void ActivateEnemyEffectsOnMortal()
    {
        sword.ActivateImpalablesEffectsOnMortal();
        sword.ClearSword();
    }

    public void ResetAttackState()
    {
        currentAttackState = 0;
    }

    public void EnableGroundedAttacks()
    {
        inputs.Gameplay.SquareAttack.Enable();
        inputs.Gameplay.CircleAttack.Enable();
        inputs.Gameplay.TriangleAttack.Enable();

        inputs.Gameplay.Charging.Enable();

        inputs.Gameplay.ChargedCircleAttack.Enable();
        inputs.Gameplay.ChargedSquareAttack.Enable();
        inputs.Gameplay.ChargedTriangleAttack.Enable();
    }

    public void DisableGroundedAttacks()
    {
        inputs.Gameplay.SquareAttack.Disable();
        inputs.Gameplay.CircleAttack.Disable();
        inputs.Gameplay.TriangleAttack.Disable();

        inputs.Gameplay.Charging.Disable();

        inputs.Gameplay.ChargedCircleAttack.Disable();
        inputs.Gameplay.ChargedSquareAttack.Disable();
        inputs.Gameplay.ChargedTriangleAttack.Disable();
    }

    public void EnableAllAttacks()
    {
        EnableGroundedAttacks();
        inputs.Gameplay.R1Attack.Enable();
    }

    public void DisableAllAttacks()
    {
        DisableGroundedAttacks();
        inputs.Gameplay.R1Attack.Disable();
    }

    public void EnableSwordColliders() => sword.EnableColliders();
    public void DisableSwordColliders() => sword.DisableColliders();
    public void EnableMortalCollider()
    {
        sword.EnableMortalCollider();
        ShakeCameraOnMortal();
        PlayVFX("MImpact");
    }
    public void DisableMortalCollider() => sword.DisableMortalCollider();
    public void ThrowEnemy() => sword.ThrowImpaledObject();

    public void EnableImpale() => sword.EnableImpale();

    public void DisableImpale() => sword.DisableImpale();

    public void HorizontalChargeToIdle() => animator.SetBool("hChargedAttack", false);

    public void ShakeCameraOnSlash() => Scripts.CameraController.Shake(2, 2, 0.5f);
    public void ShakeCameraOnMortal() => Scripts.CameraController.Shake(playerData.MortalShakeAmplitude,
                                                                        playerData.MortalShakeFrequency,
                                                                        playerData.MortalShakeDuration);

}
