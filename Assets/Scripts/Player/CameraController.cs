﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera mainCam;

    private CinemachineBasicMultiChannelPerlin camNoise;
    private Coroutine stopShakeCoroutine;

    private void Start()
    {
        camNoise = mainCam.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();
        if (camNoise == null) Debug.LogError("THERE IS NO NOISE CHANNEL IN VIRTUAL CAMERA!");
        else StopShake();
    }
    public void StartShake(float amplitude = 1, float frequency = 1)
    {
        camNoise.m_AmplitudeGain = amplitude;
        camNoise.m_FrequencyGain = frequency;
    }
    public void StopShake()
    {
        camNoise.m_AmplitudeGain = 0;
        camNoise.m_FrequencyGain = 0;
    }

    public void Shake(float amplitude = 1, float frequency = 1, float duration = 1)
    {
        StartShake(amplitude, duration);
        if (stopShakeCoroutine != null) StopCoroutine(stopShakeCoroutine);
        stopShakeCoroutine = StartCoroutine(StopShakeAfterInterval(duration));
    }

    private IEnumerator StopShakeAfterInterval(float interval)
    {
        while (interval > 0)
        {
            interval -= Time.deltaTime;
            yield return null;
        }
        StopShake();
    }

    public void StopFollowing() => mainCam.Follow = null;
    public void FollowPlayer() => mainCam.Follow = Scripts.PlayerMovement.transform;
}
