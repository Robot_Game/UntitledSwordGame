﻿using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections;

public abstract class Ability : MonoBehaviour
{

    [SerializeField] protected string name;
    [SerializeField] protected int levelCount;
    [HideIf("@levelCount == 0")]
    [SerializeField] protected int[] upgradeCost;
    [SerializeField] protected bool isLocked;
    [SerializeField] protected bool hasCooldown;
    [HideIf("@hasCooldown == false")]
    [SerializeField] protected float[] cooldownTime;

    [Title("Ability sounds")]
    [SerializeField] private AudioClip[] abilitySfx;

    public string Name => name;
    public int LevelCount => levelCount;
    public bool IsLocked => isLocked;
    public bool IsCoolingDown { get; protected set; } = false;
    public float CooldownTime { get; protected set; }
    public int Level { get; protected set; } = 0;
    public int CurrentCost { get; protected set; } = 0;

    protected virtual void Awake()
    {
        SetCost();
        if (!isLocked) Upgrade();
    }

    public virtual void Unlock() => isLocked = false;

    public virtual void Upgrade()
    {
        if (Level == 0) Unlock();
        SetParameters();
        Level++;
        SetCost();
    }

    protected abstract void SetParameters();
    public virtual void Cast()
    {
        AudioManager.Instance.PlaySfxOneShot(abilitySfx);
    }

    public void StartCooldownTimer()
    {
        if (hasCooldown)
        {
            //if (cooldownTime.Length >= 1) CooldownTime = cooldownTime[0];
            //else { CooldownTime = 0; print("COOLDOWN WAS NOT SET FOR THIS ABILITY! " + gameObject.name);}
            StartCoroutine(Cooldown());
        }
    }

    protected virtual IEnumerator Cooldown()
    {
        IsCoolingDown = true;

        float time = CooldownTime;
        while (time > 0)
        {
            time -= Time.deltaTime;
            yield return null;
        }

        IsCoolingDown = false;
    }


    public void SetCost()
    {
        if (upgradeCost.Length > 0)
        {
            if (Level < levelCount) CurrentCost = upgradeCost[Level];
        }
        else CurrentCost = 0;
    }
}
