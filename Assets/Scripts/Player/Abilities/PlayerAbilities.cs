﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class PlayerAbilities : MonoBehaviour
{
    [SerializeField] private bool unlockCurse;
    [SerializeField] private bool upgradeCurse;

    /// <summary>
    /// Abilities:
    /// 0 = Dash/Impale
    /// 1 = Curse
    /// 2 = Drop Attack
    /// 3 = Throw Sword
    /// </summary>
    [DetailedInfoBox("Abilities: Press this to see order!", 
        "0 = Dash/Impale\n" +
        "1 = Curse\n" +
        "2 = Drop Attack\n" +
        "3 = Throw Sword")]
    [SerializeField] public List<Ability> abilities;

    /// <summary>
    /// 0 = Dash Impale.
    /// 1 = Curse.
    /// 2 = Drop Attack.
    /// </summary>
    public List<Ability> Abilities => abilities;

    // Update is called once per frame
    void Update()
    {
        if (unlockCurse && abilities[0].IsLocked) abilities[0].Unlock();
        if (upgradeCurse)
        {
            abilities[0].Upgrade();
            upgradeCurse = false;
        }
    }

    public void UnlockAbility(int i, bool value) => abilities[i].Unlock();
    public void UpgradeAbility(int i) => abilities[i].Upgrade();
}
