﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class DashImpale : Ability
{
    [Title("Dash Impale")]
    [SerializeField] private bool[] canImpale;
    [SerializeField] private bool[] canAerialDash;

    private PlayerAttacks playerAttacks;
    private Animator anim;

    public bool CanImpale { get; private set; }
    public bool CanAerialDash { get; private set; }

    void Start()
    {
        playerAttacks = GetComponentInParent<PlayerAttacks>();
        anim = playerAttacks.GetComponent<Animator>();
        //SetParameters();
    }
    protected override void SetParameters()
    {
        CooldownTime = cooldownTime[Level];
        CanImpale = canImpale[Level];
        CanAerialDash = canAerialDash[Level];
    }

    public override void Cast()
    {
        if (IsLocked || IsCoolingDown) return;

        base.Cast();
        playerAttacks.UseAbility("DashImpale", false, false, true);
        StartCooldownTimer();
    }
}
