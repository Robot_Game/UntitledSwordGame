﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class ThrowSword : Ability
{
    [Title("Throw")]
    [SerializeField] private float throwForce;
    [SerializeField] private float throwDuration;
    [SerializeField] private bool hasThrowDeceleration;
    [HideIf("@hasThrowDeceleration == false")]
    [SerializeField] private bool useCustomDeceleration;
    [HideIf("@useCustomDeceleration == false")]
    [SerializeField] private float throwDeceleration = 0;

    [Title("Pull")]
    [SerializeField] private float pullSpeed;
    [SerializeField] private bool hasPullAcceleration;
    [HideIf("@hasPullAcceleration == false")]
    [SerializeField] private bool useCustomAcceleration;
    [HideIf("@useCustomAcceleration == false")]
    [SerializeField] private float pullAcceleration = 0;
    [Title("Properties")]
    [SerializeField] private bool canStick;
    // Layers in which the sword gets stuck if it collides
    [HideIf("@!canStick")]
    [SerializeField] private LayerMask stickingLayers;

    private PlayerAttacks playerAttacks;
    private Animator anim;

    public float ThrowForce { get; private set; }
    public float ThrowDuration { get; private set; }
    public float ThrowDeceleration { get; private set; }
    public float PullAcceleration { get; private set; }
    public float PullSpeed { get; private set; }
    public bool HasThrowDeceleration { get; private set; }
    public bool HasPullAcceleration { get; private set; }
    public bool UseCustomDeceleration { get; private set; }
    public bool UseCustomAcceleration { get; private set; }
    public bool CanStick { get; private set; }
    public LayerMask StickingLayers { get; private set; }

    void Start()
    {
        playerAttacks = GetComponentInParent<PlayerAttacks>();
        anim = playerAttacks.GetComponent<Animator>();
        //SetParameters();
    }
    protected override void SetParameters()
    {
        CooldownTime = cooldownTime[Level];
        ThrowForce = throwForce;
        ThrowDeceleration = throwDeceleration;
        ThrowDuration = throwDuration;
        PullSpeed = pullSpeed;
        PullAcceleration = pullAcceleration;
        HasThrowDeceleration = hasThrowDeceleration;
        HasPullAcceleration = hasPullAcceleration;
        UseCustomAcceleration = useCustomAcceleration;
        UseCustomDeceleration = useCustomDeceleration;
        CanStick = canStick;
        if (stickingLayers == default)
            stickingLayers = ScriptableObjects.GameData.TerrainLayers;
        StickingLayers = stickingLayers;
    }

    public override void Cast()
    {
        if (IsLocked || IsCoolingDown) return;
        base.Cast();
        playerAttacks.UseAbility("ThrowSword", true, true);
    }


}
