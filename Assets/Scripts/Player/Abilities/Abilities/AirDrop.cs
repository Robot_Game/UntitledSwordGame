﻿using MoreMountains.Feedbacks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class AirDrop : Ability
{
    [Title("Air Drop")]
    [SerializeField] private float[] dropSpeed;
    [SerializeField] private float[] damage;
    [SerializeField] private float[] radius;
    [SerializeField] private float[] knockbackForce;

    private PlayerAttacks playerAttacks;
    private PlayerMovement playerMovement;
    private Sword sword;
    private Animator anim;
    private MMFeedbacks feedbackEffects;
    private bool slowMoFlag = false;
    public float DropSpeed { get; private set; }
    public float Damage { get; private set; }
    public float Radius { get; private set; }
    public float KnockbackForce { get; private set; }
    void Start()
    {
        playerAttacks = GetComponentInParent<PlayerAttacks>();
        playerMovement = GetComponentInParent<PlayerMovement>();
        sword = FindObjectOfType<Sword>();
        anim = playerAttacks.GetComponent<Animator>();
        feedbackEffects = GetComponent<MMFeedbacks>();
        //SetParameters();
    }
    protected override void SetParameters()
    {
        CooldownTime = cooldownTime[Level];
        DropSpeed = dropSpeed[Level];
        Damage = damage[Level];
        Radius = radius[Level];
        KnockbackForce = knockbackForce[Level];
        GetComponent<SphereCollider>().radius = Radius;
    }

    public override void Cast()
    {
        if (IsLocked || IsCoolingDown) return;
        base.Cast();
        playerAttacks.UseAbility("Air Drop");
        playerMovement.SetAirDropParameters(DropSpeed);
        slowMoFlag = true;

    }

    public void HitFloor()
    {
        print("EXPLOSION TRIGGER WAS CALLED");
        //chamar efeito explosivo
        anim.SetTrigger("AirDropExplosion");
        feedbackEffects.PlayFeedbacks();
        StartCooldownTimer();
    }

    private void OnTriggerEnter(Collider other)
    {
        Enemy e = other.GetComponent<Enemy>();
        if (e != null)
        {
            if (e.gameObject.layer == 9 && other is CapsuleCollider && !e.Dead)
            {
                e.TakeDamage(Damage, true);
                e.GetKnockback(KnockbackForce);
                if (slowMoFlag)
                {
                    Debug.Log("Call Slow Motion Effect");
                    Scripts.PlayerAttacks.ManageSlowMotionEffect();
                    slowMoFlag = false;
                }
            }
            return;
        }
        EnemyProjectile proj = other.GetComponent<EnemyProjectile>();
        if (proj != null) Destroy(proj.gameObject);
    }
}
