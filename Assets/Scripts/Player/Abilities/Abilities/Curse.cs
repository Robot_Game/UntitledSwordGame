﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Curse : Ability
{
    [Title("Curse")]
    [SerializeField] private float castRadius;
    [SerializeField] private float targetSlectionSlowMotionSpeed;
    [SerializeField] private float targetSelectionMaxTime;
    [SerializeField] private LayerMask targetLayer;
    [SerializeField] private float[] duration;
    [SerializeField] private float[] hpMultipler;
    [SerializeField] private float[] sizeMultipler;
    [SerializeField] private bool[] canAttack;

    private TargetSelectionController tsc;
    private Coroutine castCoroutine;
    private bool attackBehaviour;
    private float currentDuration;
    private float currentHpMultiplier;
    private float currentSizeMultiplier;

    protected override void Awake()
    {
        base.Awake();
        tsc = GetComponentInParent<TargetSelectionController>();
    }
    protected override void SetParameters()
    {
        CooldownTime = cooldownTime[Level];
        attackBehaviour = canAttack[Level];
        currentDuration = duration[Level];
        currentHpMultiplier = hpMultipler[Level];
        currentSizeMultiplier = sizeMultipler[Level];
    }

    public override void Cast()
    {
        Debug.Log("Level: " + Level);
        if (IsLocked || IsCoolingDown || castCoroutine != null || !AnyEnemyInRadius()) return;
        base.Cast();
        if (castCoroutine == null) castCoroutine = StartCoroutine(CurseWhenTargetSelected());
        
    }

    private bool AnyEnemyInRadius()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position,
                                                        castRadius,
                                                        targetLayer);

        if (hitColliders.Length > 0) return true;

        return false;
    }

    private IEnumerator CurseWhenTargetSelected()
    {
        Enemy enemy = null;
        bool wait = tsc.InitializeTargetSelection(castRadius,
                                                  targetSlectionSlowMotionSpeed,
                                                  targetSelectionMaxTime,
                                                  targetLayer);
        while (wait)
        {
            wait = tsc.IsChoosingTarget;
            //Debug.Log(wait);
            yield return null;
        }

        enemy = tsc.SelectedEnemy;
        if (enemy != null)
        {
            enemy.ChangeState(EnemyState.Cursed,
                              attackBehaviour,
                              currentHpMultiplier,
                              currentSizeMultiplier,
                              currentDuration);

            StartCooldownTimer();
        }

        castCoroutine = null;

    }
}
