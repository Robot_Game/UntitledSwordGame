﻿using System;
using System.Collections;
using UnityEngine;
using Sirenix.OdinInspector;

public class PlayerDropSwordController : MonoBehaviour
{
    //Reference to the player data
    [SerializeField, InlineEditor, Required] private PlayerData playerData;

    [Title("Sword references")]
	// Reference to the player's sword
	[SerializeField] private Transform sword;
	// Variable containing where the sword goes after being grabbed
	[SerializeField] private Transform swordHoldPoint;

	[Title("Grab Sword")]
	// Range in which the player has to be to grab the sword
	[SerializeField] private float distanceToGrabSword;

	// Reference to the player inputs
	private PlayerInputActions inputs;

	// Reference to the player movement
	private PlayerMovement playerMovement;
	// Reference to the player attacks
	private PlayerAttacks playerAttacks;

	// Reference to the sword animator
	private Animator swordAnim;

	// Variable that verifies if player can drop the sword
	private bool canDrop;
	// Debug variable to draw sphere
	private bool dropped;

	// Update is called once per frame
	private void Awake()
	{
		// Initialise drop input
		inputs = new PlayerInputActions();
		AddControls();

		// Get other player data
		playerMovement = GetComponent<PlayerMovement>();
		playerAttacks = GetComponent<PlayerAttacks>();

		// Get sword animator
		swordAnim = sword.GetComponent<Animator>();
	}

	private void OnEnable()
	{
		inputs.Enable();
	}

	private void OnDisable()
	{
		inputs.Disable();
	}

	private void AddControls()
	{
		// If drop input is performed, grab or drop the sword
		//inputs.Gameplay.GrabOrDropSword.performed += ctx => { if (canDrop) GrabOrDropSword(); };
	}

	public void GrabOrDropSword()
	{
		// Player will either drop or grab sword depending on state
		if (playerData.PlayerStatus == PlayerStatus.NoSword) GrabSword();
		else if (playerData.PlayerStatus == PlayerStatus.WithSword) DropSword();
	}

	private void GrabSword()
	{
		// Player can only grab the sword if he is in range
		if (Vector3.Distance(transform.position, sword.position) < distanceToGrabSword)
		{
			// Start grab animation
			swordAnim.SetTrigger("grab");

			// Set sword parent to the payer so the it follows the player
			sword.SetParent(swordHoldPoint);

			// Reset sword position, rotation and scale
			sword.localPosition = Vector3.zero;
			sword.localRotation = Quaternion.identity;
			sword.localScale = Vector3.one;

			// Wiht a sword player can now attack
			playerAttacks.EnableGroundedAttacks();

			// Change the player's stats to match the state
			ChangePlayerStats(PlayerStatus.WithSword);

			// Debug variable to draw sphere
			dropped = false;
		}
	}

	private void DropSword()
	{
		// Start drop animation
		swordAnim.SetTrigger("drop");
		// Remove parent from sword. This way the sword no longer follows the player
		sword.SetParent(null);

		// Wihtout a sword player cannot attack
		playerAttacks.DisableGroundedAttacks();

		// Change the player's stats to match the state
		ChangePlayerStats(PlayerStatus.NoSword);

		// Debug variable to draw sphere
		dropped = true;
	}

	private void ChangePlayerStats(PlayerStatus status)
	{
		// Change player status
		playerData.PlayerStatus = status;

		// Change player stats according to status
		if (playerData.PlayerStatus == PlayerStatus.NoSword)
			// Change player movement speed
			playerMovement.SetMovementSpeed(playerData.NoSwordMovementSpeed);
		else if (playerData.PlayerStatus == PlayerStatus.WithSword)
			// Change player movement speed
			playerMovement.SetMovementSpeed(playerData.WithSwordMovementSpeed);
	}

	void OnDrawGizmosSelected()
	{
		// Draw a sphere around the sword to visualize grab range
		if (sword != null && dropped)
		{
			// Draws a blue line from this transform to the target
			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere(sword.position, distanceToGrabSword);
		}
	}

	// The player is allowed to drop the sword
	public void EnableDrop() => canDrop = true;
	// The player is not allowed to drop the sword
	public void DisableDrop() => canDrop = false;

}
