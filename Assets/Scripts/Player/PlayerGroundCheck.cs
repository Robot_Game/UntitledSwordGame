﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Sirenix.OdinInspector;

public class PlayerGroundCheck : MonoBehaviour
{
    [SerializeField, Required] private PlayerData data;
    [SerializeField, Required] VoidEvent OnTouchFloor;
    [SerializeField, Required] VoidEvent OnExitFloor;

    private List<Collider> grounds;
    private List<Collider> groundsLastFrame;
    private Collider[] floorsArray;
    private Collider[] floorsLastFrame;
    private LayerMask groundLayers;
    private bool isGrounded = false;

    //private Transform currentParent;
    private void Awake()
    {
        groundLayers = data.GroundDetectionLayers;
        grounds = new List<Collider>();
        groundsLastFrame = new List<Collider>();
    }

    private void Update()
    {
        grounds = Physics.OverlapBox(transform.position, new Vector3(0.1f, 0.2f, 0.1f), transform.rotation, groundLayers, QueryTriggerInteraction.Ignore).ToList();
        if (grounds.Count > 0 && groundsLastFrame.Count < grounds.Count)
        {
            OnTouchFloor.Raise();
            isGrounded = true;
            Debug.Log("WE ENTERED A NEW FLOOR!");
        }
        if (grounds.Count == 0 && isGrounded)
        {
            OnExitFloor.Raise();
            isGrounded = false;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawCube(transform.position, new Vector3(0.1f, 0.1f, 0.1f));
    }
    private void LateUpdate()
    {
        groundsLastFrame = grounds;
    }

    /*private void OnTriggerStay(Collider other)
    {
        if (groundLayers.Contains(other.gameObject.layer))
        {
            if (grounds.Contains(other.gameObject)) return;

            grounds.Add(other.gameObject);
            OnTouchFloor.Raise();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (groundLayers.Contains(other.gameObject.layer))
        {
            if (grounds.Contains(other.gameObject))
            {
                grounds.Remove(other.gameObject);
            }

            if (grounds.Count < 1) OnExitFloor.Raise();

        }
    }*/

    private void ClearGrounds()
    {
        grounds.Clear();
        OnExitFloor.Raise();
    }
}
