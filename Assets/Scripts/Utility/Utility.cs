﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utility : MonoBehaviour
{

    public static bool HasAnimationFinished(Animator anim, string animationName)
        => anim.GetCurrentAnimatorStateInfo(0).IsName(animationName);


    /// <summary>
    /// Returns a random position inside a ring.
    /// </summary>
    /// <param name="centerPos"></param>
    /// <param name="minRadius"></param>
    /// <param name="maxRadius"></param>
    /// <param name="height"></param>
    /// <returns></returns>
    public static Vector3 GetRandomPositionInRing(Vector3 centerPos, float minRadius, float maxRadius, float height)
    {
        Vector3 pos = Vector3.zero;
        while (pos == Vector3.zero)
        {
            pos = new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f));
        }

        pos = pos.normalized;
        float magnitude = Random.Range(minRadius, maxRadius);
        pos *= magnitude;
        pos.y = height;
        return centerPos + pos;

    }
    /// <summary>
    /// Returns a random float between a (median - offset) and (median + offset)
    /// </summary>
    /// <param name="median">The middle number in the range.</param>
    /// <param name="offset">The offset from the median (under and over).</param>
    /// <returns></returns>
    public static float GetRandomFloat(float median, float offset) => Random.Range(median - offset, median + offset);
    /// <summary>
    /// Returns a random int between a (median - offset)[inclusive] and (median + offset)[exclusive].
    /// </summary>
    /// <param name="median">The middle number in the range.</param>
    /// <param name="offset">The offset from the median (under and over).</param>
    /// <returns></returns>
    public static float GetRandomInt(int median, int offset) => Random.Range(median - offset, median + offset + 1);

    /// <summary>
    /// Get a random string from the strings in the parameters.
    /// </summary>
    /// <param name="strings">The possible strings to return.</param>
    /// <returns></returns>
    public static string GetRandomString(params string[] strings)
    {
        if (strings.Length == 1) return strings[0];
        else return strings[Random.Range(0, strings.Length)];

    }
    /// <summary>
    /// Check if distance between position and  targetPosition is shorter than a certain range.
    /// </summary>
    /// <param name="position"></param>
    /// <param name="targetPosition"></param>
    /// <param name="range"></param>
    /// <returns></returns>
    public static bool IsInRange(Vector3 position, Vector3 targetPosition, float range)
        => Vector3.Distance(position, targetPosition) <= range;
    /// <summary>
    /// Check if distance between transform and targetTransform is shorter than a certain range.
    /// </summary>
    /// <param name="transform"></param>
    /// <param name="targetTransform"></param>
    /// <param name="range"></param>
    /// <returns></returns>
    public static bool IsInRange(Transform transform, Transform targetTransform, float range)
        => Vector3.Distance(transform.position, targetTransform.position) <= range;
    /// <summary>
    /// Check if distance between gameObject and targetObject is shorter than a certain range.
    /// </summary>
    /// <param name="gameObject"></param>
    /// <param name="targetObject"></param>
    /// <param name="range"></param>
    /// <returns></returns>
    public static bool IsInRange(GameObject gameObject, GameObject targetObject, float range)
        => Vector3.Distance(gameObject.transform.position, targetObject.transform.position) <= range;

}
