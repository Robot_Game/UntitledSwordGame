﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MoreMountains.Feedbacks
{
    /// <summary>
    /// This feedback will make the bound renderer flicker for the set duration when played (and restore its initial color when stopped)
    /// </summary>
    [AddComponentMenu("")]
    [FeedbackHelp("This feedback lets you flicker the specified renderer (sprite, mesh, etc) for a certain duration, at the specified octave, and with the specified color. Useful when a character gets hit, for example (but so much more!).")]
    [FeedbackPath("GameObject/FlickerVFX")]
    public class MMFeedbackFlickerVFX : MMFeedback
    {
        [Header("Flicker")]
        /// the renderer to flicker when played
        public Renderer BoundRenderer;
        /// the duration of the flicker when getting damage
        public float FlickerDuration = 0.2f;
        /// the frequency at which to flicker
        public float FlickerOctave = 0.04f;
        /// the color we should flicker the sprite to 
        public Color FlickerColor = new Color32(255, 20, 20, 255);

        protected Color _initialFlickerColor;
        protected Color _initialFresnelColor;
        protected Color _initialIntersectionColor;
        protected Texture _initialGradientMap;

        /// <summary>
        /// On init we grab our initial color and components
        /// </summary>
        /// <param name="owner"></param>
        protected override void CustomInitialization(GameObject owner)
        {
            if (Active && (BoundRenderer != null)) GetShaderColors();

            if (Active && (BoundRenderer == null) && (owner != null))
            {
                if (owner.MMFGetComponentNoAlloc<Renderer>() != null)
                    BoundRenderer = owner.GetComponent<Renderer>();

                if (BoundRenderer == null)
                    BoundRenderer = owner.GetComponentInChildren<Renderer>();

                if (BoundRenderer != null) GetShaderColors();
            }
        }

        /// <summary>
        /// On play we make our renderer flicker
        /// </summary>
        /// <param name="position"></param>
        /// <param name="attenuation"></param>
        protected override void CustomPlayFeedback(Vector3 position, float attenuation = 1.0f)
        {
            if (Active && (BoundRenderer != null))
            {
                StartCoroutine(Flicker(BoundRenderer, _initialFlickerColor, 
                    _initialFresnelColor, _initialIntersectionColor, 
                    _initialGradientMap, FlickerColor, FlickerOctave, 
                    FlickerDuration));
            }
        }

        /// <summary>
        /// On reset we make our renderer stop flickering
        /// </summary>
        protected override void CustomReset()
        {
            base.CustomReset();
            if (Active && (BoundRenderer != null)) SetShaderColors();
        }

        public virtual IEnumerator Flicker(Renderer renderer, Color initialColor, 
            Color initialFresnelColor, Color initialIntersectionColor, 
            Texture initalGradientMap, Color flickerColor, float flickerSpeed, 
            float flickerDuration)
        {
            if (renderer == null) yield break;

            if (!renderer.material.HasProperty("_Color") || 
                !renderer.material.HasProperty("_FresnelColor") ||
                !renderer.material.HasProperty("_IntersectionColor") ||
                !renderer.material.HasProperty("_GradientMap"))
            {
                yield break;
            }

            if (initialColor == flickerColor && 
                initialFresnelColor == flickerColor && 
                initialIntersectionColor == flickerColor)
            {
                yield break;
            }

            float flickerStop = Time.time + flickerDuration;

            while (Time.time < flickerStop)
            {
                renderer.material.color = flickerColor;
                renderer.material.SetColor("_FresnelColor", flickerColor);
                renderer.material.SetColor("_IntersectionColor", flickerColor);
                renderer.material.SetTexture("_GradientMap", null);
                yield return new WaitForSeconds(flickerSpeed);
                renderer.material.color = initialColor;
                renderer.material.SetColor("_FresnelColor", initialFresnelColor);
                renderer.material.SetColor("_IntersectionColor", initialIntersectionColor);
                renderer.material.SetTexture("_GradientMap", initalGradientMap);
                yield return new WaitForSeconds(flickerSpeed);
            }

            SetShaderColors();
        }

        public void GetShaderColors()
        {
            if (BoundRenderer.material.HasProperty("_Color"))
                _initialFlickerColor = BoundRenderer.material.color;

            if (BoundRenderer.material.HasProperty("_FresnelColor"))
                _initialFresnelColor = BoundRenderer.material.GetColor("_FresnelColor");

            if (BoundRenderer.material.HasProperty("_IntersectionColor"))
                _initialIntersectionColor = BoundRenderer.material.GetColor("_IntersectionColor");

            if (BoundRenderer.material.HasProperty("_GradientMap"))
                _initialGradientMap = BoundRenderer.material.GetTexture("_GradientMap");
        }

        public void SetShaderColors()
        {
            if (BoundRenderer.material.HasProperty("_Color"))
                BoundRenderer.material.color= _initialFlickerColor;

            if (BoundRenderer.material.HasProperty("_FresnelColor"))
                BoundRenderer.material.SetColor("_FresnelColor", _initialFresnelColor);

            if (BoundRenderer.material.HasProperty("_IntersectionColor"))
                BoundRenderer.material.SetColor("_IntersectionColor", _initialIntersectionColor); ;

            if (BoundRenderer.material.HasProperty("_GradientMap"))
                BoundRenderer.material.SetTexture("_GradientMap", _initialGradientMap); ;
        }
    }
}
