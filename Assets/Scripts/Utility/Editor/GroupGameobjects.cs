﻿using UnityEditor;
using UnityEngine;

/// <summary>
/// Group all selected gameobjects as childs of a new gameobject
/// </summary>
public class GroupGameobjects
{
	[MenuItem("GameObject/Group it %g")]
	private static void GroupSelected()
	{
		if (!Selection.activeTransform) return;

		GameObject go = new GameObject { name = "GroupedObjects" };

		Undo.RegisterCompleteObjectUndo(go, "Group it");

		// Set parent to selection parent
		go.transform.SetParent(Selection.activeTransform.parent, false);

		// Iterate over selected gameobjects to add to a new group go
		foreach (Transform transform in Selection.transforms)
			Undo.SetTransformParent(transform, go.transform, "Group it");

		Selection.activeGameObject = go;
	}
}
