﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UnityExtensions
{
    /// <summary>
    /// Check if a Layer is inside a LayerMask
    /// </summary>
    /// <param name="mask">The LayerMask we want to check.</param>
    /// <param name="layer">The Layer we want to check.</param>
    /// <returns></returns>
    public static bool Contains(this LayerMask mask, int layer)
    {
        return mask == (mask | (1 << layer));
    }
}