﻿using System;
using UnityEngine;

public static class FullscreenModeExtensions
{
    public static string ToFriendlyString(this FullScreenMode mode)
    {
        switch (mode)
        {
            case FullScreenMode.ExclusiveFullScreen:
                return "Fullscreen";
            case FullScreenMode.FullScreenWindow:
                return "Windowed Fullscreen";
            case FullScreenMode.MaximizedWindow:
                return "Windowed Maximized ";
            case FullScreenMode.Windowed:
                return "Windowed";
            default:
                return mode.ToString();
        }
    }
}
