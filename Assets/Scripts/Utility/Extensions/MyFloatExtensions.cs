﻿public static class MyFloatExtensions
{
    /// <summary>
    /// With a value of Range[minInput,maxInput], convert it to 
    /// its equivalent of Range[minInput, maxInput].
    /// </summary>
    /// <param name="value"></param>
    /// <param name="minInput"></param>
    /// <param name="maxInput"></param>
    /// <param name="minOutput"></param>
    /// <param name="maxOutput"></param>
    /// <returns></returns>
    public static float Lerp(this float value, float minInput, float maxInput, float minOutput, float maxOutput)
    {
        if (value > maxInput) value = maxInput;
        else if (value < minInput) value = minInput;
        return minOutput + (value - minInput) / (maxInput - minInput) * (maxOutput - minOutput);
    }
}
