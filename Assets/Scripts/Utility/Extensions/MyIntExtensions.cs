﻿using System.Collections;
using System.Collections.Generic;

public static class MyIntExtensions
{
	public static bool IntToBool(this int value)
	{
		if (value == 0) return false;
		else return true;
	}
}
