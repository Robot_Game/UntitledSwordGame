﻿using UnityEngine;
using System.Collections;

public class DontGoThroughThings : MonoBehaviour
{
    // Careful when setting this to true - it might cause double
    // events to be fired - but it won't pass through the trigger
    [SerializeField] private bool sendTriggerMessage = false;
    [SerializeField] private bool resetPositionOnEnable = false;

    public LayerMask layerMask = -1; //make sure we aren't in this layer 
    public float skinWidth = 0.1f; //probably doesn't need to be changed 

    private float minimumExtent;
    private float partialExtent;
    private float sqrMinimumExtent;
    private Vector3 previousPosition;
    private PlayerGroundCheck player;
    private Rigidbody myRigidbody;
    private Collider myCollider;

    //initialize values 
    void Start()
    {
        player = Scripts.PlayerMovement.GetComponentInChildren<PlayerGroundCheck>();
        myRigidbody = GetComponent<Rigidbody>();
        myCollider = GetComponent<Collider>();
        previousPosition = myRigidbody.position;
        Debug.Log("Previous position: " + previousPosition);
        minimumExtent = Mathf.Min(Mathf.Min(myCollider.bounds.extents.x, myCollider.bounds.extents.y), myCollider.bounds.extents.z);
        partialExtent = minimumExtent * (1.0f - skinWidth);
        sqrMinimumExtent = minimumExtent * minimumExtent;
    }
    private void OnEnable()
    {
        if (myRigidbody != null)
        {
            if(resetPositionOnEnable) previousPosition = myRigidbody.position;
        }
    }
    void FixedUpdate()
    {
        //have we moved more than our minimum extent? 
        Vector3 movementThisStep = myRigidbody.position - previousPosition;
        if (movementThisStep.magnitude > 10)
        {
            previousPosition = myRigidbody.position;
            return;
        }

        float movementSqrMagnitude = movementThisStep.sqrMagnitude;

        if (movementSqrMagnitude > sqrMinimumExtent)
        {
            float movementMagnitude = Mathf.Sqrt(movementSqrMagnitude);
            RaycastHit hitInfo;

            //check for obstructions we might have missed 
            if (Physics.Raycast(previousPosition, movementThisStep, out hitInfo, movementMagnitude, layerMask.value))
            {
                if (!hitInfo.collider)
                    return;

                if (sendTriggerMessage && hitInfo.collider.isTrigger)
                    hitInfo.collider.SendMessage("OnTriggerEnter", myCollider);

                if (!hitInfo.collider.isTrigger)
                {
                    Debug.Log("WENT THROUGH " + hitInfo.collider.gameObject.name);
                    myRigidbody.position = hitInfo.point - (movementThisStep / movementMagnitude) * partialExtent;
                    player.SendMessage("OnTriggerEnter", hitInfo.collider);
                }
            }
        }

        previousPosition = myRigidbody.position;
    }
}