﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineOfSight : MonoBehaviour
{
    public List<Vector3> SnapNodes { get; private set; }

    private void Awake()
    {
        SnapNodes = new List<Vector3>();
    }

    public void CalculatePoints(GameObject refObj, Transform origin, Vector3 dir, float snapRadius,
        int segments, float aperture)
    {
        float calcPoint;

        // Clear list
        SnapNodes.Clear();

        // Set position and rotation
        refObj.transform.position = new Vector3(
            origin.position.x + dir.normalized.x * snapRadius,
            origin.position.y + 0.5f,
            origin.position.z + dir.normalized.z * snapRadius);
        refObj.transform.rotation = Quaternion.LookRotation(dir);

        // Point starts with the minimum value
        calcPoint = -aperture / 2;

        // If segments is an even number, make it odd
        if (segments % 2 == 0) segments++;
        // Add points connecting segments to the list
        for (int i = 0; i <= segments; i++)
        {
            SnapNodes.Add(refObj.transform.position - refObj.transform.right * calcPoint);
            calcPoint += aperture / segments;
        }

    }

    public Collider CheckForHits(Transform origin, LayerMask mask, bool ignoreTriggers = false)
    {
        int initialSnapNode = SnapNodes.Count / 2;
        int currentSnapNode = initialSnapNode;
        Vector3 pos = new Vector3(origin.position.x, origin.position.y + 0.5f, origin.position.z);
        for (int i = 0; i < SnapNodes.Count; i++)
        {
            if (i % 2 == 0) currentSnapNode = initialSnapNode - (i / 2);
            else currentSnapNode = initialSnapNode + (i / 2);

            QueryTriggerInteraction detectTrigger = QueryTriggerInteraction.Collide;
            if (ignoreTriggers) detectTrigger = QueryTriggerInteraction.Ignore;

            if (Physics.Linecast(
                pos,
                SnapNodes[currentSnapNode],
                out RaycastHit hit,
                mask.value, detectTrigger))
            {
                // TEST ONLY!!!!!!!!!!!!!!!!
                Debug.DrawLine(pos, SnapNodes[i], Color.blue, 10f);
                return hit.collider;
            }

        }
        return null;
    }
}
