﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Sirenix.OdinInspector;
using System.Collections;

public class Cheats : MonoBehaviour
{
    private Transform player;
    private PlayerInputActions inputs;
    private int currentPosIndex = -1;

    [Title("Prefab Spawn")]
    [SerializeField] private GameObject[] objectsToSpawn;
    [SerializeField] private float forwardOffset;

    [Title("Checkpoints")]
    [SerializeField] Transform[] checkpoints;

    //[Title("Positions")]
    //[SerializeField] Vector3[] positions;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        inputs = new PlayerInputActions();
        AddControls();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1)) Spawn(objectsToSpawn[0]);
        if (Input.GetKeyDown(KeyCode.Alpha2)) Spawn(objectsToSpawn[1]);
        if (Input.GetKeyDown(KeyCode.Alpha3)) Spawn(objectsToSpawn[2]);
        if (Input.GetKeyDown(KeyCode.Alpha4)) Spawn(objectsToSpawn[3]);
        if (Input.GetKeyDown(KeyCode.Alpha5)) Spawn(objectsToSpawn[4]);

        if (Input.GetKeyDown(KeyCode.Tab)) GoToNextCheckpoint();

        if (Input.GetKeyDown(KeyCode.Return)) Debug.Log("Time scale is: " + Time.timeScale);
    }

    private void Spawn(GameObject obj)
    {
        Instantiate(obj, player.transform.position + (player.transform.forward * forwardOffset), Quaternion.identity);
    }

    private void GoToNextCheckpoint()
    {
        currentPosIndex++;
        if (currentPosIndex >= checkpoints.Length) currentPosIndex = 0;

        player.transform.position = new Vector3(checkpoints[currentPosIndex].position.x,
            checkpoints[currentPosIndex].position.y + 1,
            checkpoints[currentPosIndex].position.z);
    }

    private void OnEnable()
    {
        inputs.Enable();
    }

    private void OnDisable()
    {
        inputs.Disable();
    }

    private void AddControls()
    {
        inputs.Gameplay.RestartScene.performed += ctx => { if (Scripts.GameManager.IsInPause) RestartScene(); };
    }

    public void RestartScene() => StartCoroutine(RestartSceneAfterSave());

    private IEnumerator RestartSceneAfterSave()
    {
        inputs.Disable();
        Debug.Log("Let's Restart");

        Scripts.GameManager.Save();

        while (Scripts.SavedObjects.IsSaving)
        {
            //Debug.Log("IS SAVING!!!!");
            yield return null;
        }
        float timer = 0.2f;
        while (timer > 0)
        {
            timer -= Time.unscaledDeltaTime;
            yield return null;
        }
        while (Scripts.SavedObjects.IsSaving)
        {
            //Debug.Log("IS SAVING!!!!");
            yield return null;
        }
        Debug.Log("Started loading scene");
        Time.timeScale = 1;
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
    }
}
