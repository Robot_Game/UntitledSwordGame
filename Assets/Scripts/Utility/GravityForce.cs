﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityForce : MonoBehaviour
{

    private GroundCheck gc;
    private Rigidbody rb;
    private float yVelocity = 0;
    private bool isKinematic = false;
    private bool rbFlag = false;


    private void Awake()
    {
        GetRigidbody();
        GetGroundCheck();
    }

    private void FixedUpdate()
    {
        ApplyGravity();
    }
    private void ApplyGravity()
    {
        if (gc.IsGrounded)
        {
            //Debug.Log("Is grounded and velocity is: " + yVelocity);
            yVelocity = 0;
        }
        else if (yVelocity > -ScriptableObjects.GameData.MaxFallingSpeed)
        {
            yVelocity -= ScriptableObjects.GameData.GravityForce * Time.deltaTime;
        }
        else
        {
            yVelocity = -ScriptableObjects.GameData.MaxFallingSpeed;
        }
        rb.velocity = new Vector3(rb.velocity.x, yVelocity * rb.mass, rb.velocity.z);
    }

    private void GetGroundCheck()
    {
        gc = GetComponentInChildren<GroundCheck>();
        if (gc == null) gc = gameObject.AddComponent<GroundCheck>();

        if (rb.isKinematic)
        {
            gc.OnGroundEnter += EnableIsKinematic;
            gc.OnGroundExit += DisableIsKinematic;
        }
        DisableIsKinematic();
    }
    private void EnableIsKinematic() => rb.isKinematic = true;
    private void DisableIsKinematic() => rb.isKinematic = false;

    private void GetRigidbody()
    {
        rb = GetComponent<Rigidbody>();
        if(rb == null) rb = gameObject.AddComponent<Rigidbody>();

        //rb.useGravity = true;
    }
}
