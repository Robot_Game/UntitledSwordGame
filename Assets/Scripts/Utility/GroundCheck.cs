﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheck : MonoBehaviour
{

    [SerializeField] private LayerMask groundLayers;

    public Action OnGroundEnter { get; set; }
    public Action OnGroundExit { get; set; }

    private bool flag;
    public bool IsGrounded { get; private set; } = false;

    private void Start()
    {
        if (groundLayers == default) groundLayers = ScriptableObjects.GameData.GroundDetectionLayers;
    }

    private void Update()
    {
        flag = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (groundLayers.Contains(other.gameObject.layer)
            && !flag)
        {
            OnGroundEnter?.Invoke();
            IsGrounded = true;
            flag = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (groundLayers.Contains(other.gameObject.layer))
        {
            OnGroundExit?.Invoke();
            IsGrounded = false;
        }
    }
}
