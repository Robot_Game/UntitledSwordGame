﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendlyProjectile : Projectile
{
    private void OnTriggerEnter(Collider other)
    {
        if (layerToHit.Contains(other.gameObject.layer)
            && !(other is SphereCollider))
        {
            HideThrownImpaledObject(other.transform);
            DealDamage(other);
        }
    }

    private void HideThrownImpaledObject(Transform other)
    {
        transform.SetParent(null);
        Enemy e = GetComponent<Enemy>();
        if (e != null)
        {
            transform.GetChild(0).gameObject.SetActive(false);
            transform.GetChild(1).gameObject.SetActive(false);
            e.ThrowSpecialEffect(other.transform);
        }
        else
        {
            Impalable i = GetComponent<Impalable>();
            if (i != null)
            {
                GetComponent<MeshRenderer>().enabled = false;
                i.ThrowSpecialEffect(other.transform);
            }
        }

        GetComponent<Collider>().enabled = false;

        Destroy(gameObject, 10);
    }

    private void DealDamage(Collider other)
    {
        Enemy e = other.GetComponent<Enemy>();
        if (e != null)
        {
            AudioManager.Instance.PlaySfxOneShot(hitSfx);
            e.TakeDamage(damage);
            e.GetKnockback(knockbackForce);
        }
    }
}
