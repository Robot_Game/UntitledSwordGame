﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    protected Vector3 direction;
    protected float speed;
    protected float lifeTime;
    protected LayerMask layerToHit;
    protected FloatEvent OnHitTarget;
    protected Action<Transform> onHitTransform;
    protected float damage;
    protected float knockbackForce;
    protected float knockbackDuration;
    protected Space relativeTo;
    protected ProjectileType type;
    protected Transform target;
    protected Transform parent;
    protected AudioClip[] hitSfx;

    protected bool hasBeenInitialized;
    protected bool canMove = true;

    protected Sword sword;

    private Vector3 tempPos;

    protected virtual void Awake()
    {
        sword = FindObjectOfType<Sword>();
    }

    public void Initialize(Vector3 direction, Transform target, float speed, float lifeTime,
        LayerMask layerToHit, FloatEvent OnHitTarget, float damage, float knockbackForce,
        float knockbackDuration, Space relativeTo, ProjectileType type, Transform parent, AudioClip[] hitSfx)
    {
        this.direction = direction;
        this.target = target;
        this.speed = speed;
        this.lifeTime = lifeTime;
        this.layerToHit = layerToHit;
        this.OnHitTarget = OnHitTarget;
        this.damage = damage;
        this.knockbackForce = knockbackForce;
        this.knockbackDuration = knockbackDuration;
        this.relativeTo = relativeTo;
        this.type = type;
        this.parent = parent;
        this.hitSfx = hitSfx;


        hasBeenInitialized = true;

        if (target == null) type = ProjectileType.Normal;
        if (type == ProjectileType.Normal) StartCoroutine(TimeOut());
    }

    public void Initialize(Vector3 direction, Transform target, float speed, float lifeTime,
    LayerMask layerToHit, Action<Transform> onHitTarget, float damage, float knockbackForce,
    float knockbackDuration, Space relativeTo, ProjectileType type, Transform parent, AudioClip[] hitSfx)
    {
        this.direction = direction;
        this.target = target;
        this.speed = speed;
        this.lifeTime = lifeTime;
        this.layerToHit = layerToHit;
        onHitTransform = onHitTarget;
        this.damage = damage;
        this.knockbackForce = knockbackForce;
        this.knockbackDuration = knockbackDuration;
        this.relativeTo = relativeTo;
        this.type = type;
        this.parent = parent;
        this.hitSfx = hitSfx;


        hasBeenInitialized = true;

        if (target == null) type = ProjectileType.Normal;
        if (type == ProjectileType.Normal) StartCoroutine(TimeOut());
    }

    private void Update()
    {
        if (hasBeenInitialized && canMove)
        {
            Move();
        }
    }

    private void Move()
    {
        switch (type)
        {
            case ProjectileType.Normal:
                transform.Translate(direction * speed * Time.deltaTime, relativeTo);
                break;
            case ProjectileType.Tracking:
                if (target == null)
                {
                    transform.position =
                        Vector3.MoveTowards(transform.position, tempPos, speed * Time.deltaTime);
                    if (Vector3.Distance(transform.position, tempPos) < 0.1f) Destroy(gameObject);
                    break;
                }
                transform.position =
                    Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
                tempPos = target.position;
                break;
        }
    }

    private IEnumerator TimeOut()
    {
        float time = 0;

        while (time < lifeTime)
        {
            time += Time.deltaTime;
            yield return null;
        }
        // For now we destroy this game object but later we want to call a vfx or animation
        // like the body disintegrating or something
        //DestroyEffect()
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    protected void DestroyEffect()
    {
        // Implement animation and/or vfx for the destruction
    }
}
