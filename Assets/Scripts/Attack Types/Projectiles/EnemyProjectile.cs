﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : Projectile
{
    [SerializeField] private GameObject[] disableOnHit;
    private Collider col;

    protected override void Awake()
    {
        base.Awake();

        col = GetComponent<Collider>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (layerToHit.Contains(collision.gameObject.layer))
        {
            if (collision.gameObject.layer != LayerMask.NameToLayer("Player"))
            {
                Debug.Log("Hit enemy");
                onHitTransform.Invoke(collision.gameObject.transform);
                DisableProj();
            }
            else
            {
                OnHitTarget.Raise(damage);
                Scripts.PlayerMovement.GetKnockback(parent, knockbackForce, knockbackDuration);
                DisableProj();
            }
        }
    }

    private void DisableProj()
    {
        canMove = false;
        col.enabled = false;
        foreach (GameObject go in disableOnHit) go.SetActive(false);

        Destroy(gameObject, 5);
    }
}
