﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendlyParticleProjectile : MonoBehaviour
{
	private float damage;
	private float knockbackForce;

	public void Init(float damage, float knockbackForce)
	{
		this.damage = damage;
		this.knockbackForce = knockbackForce;
		Destroy(gameObject, 10);
	}

	private void OnParticleCollision(GameObject other)
	{
		DealDamage(other);
	}

	private void DealDamage(GameObject other)
	{
		if (other.GetComponent<Collider>() is CapsuleCollider)
		{
			Enemy e = other.GetComponent<Enemy>();
			e.TakeDamage(damage);
			e.GetKnockback(knockbackForce);
		}
	}
}
