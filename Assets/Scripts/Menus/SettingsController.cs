﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Doozy.Engine.UI;
using Sirenix.OdinInspector;
using UnityEngine.Audio;

public class SettingsController : MonoBehaviour
{
    private void Start()
    {
        DisableMouse();
        LoadSettings();
    }

    private void Update()
    {
        ControlOption(resolutionButton, resolutionLeftArrow, resolutionRightArrow);
        ControlOption(fullscreenButton, fullscreenLeftArrow, fullscreenRightArrow);
        ControlOption(VSyncButton, VSyncLeftArrow, VSyncRightArrow);
    }

    private void LoadSettings()
    {
        LoadResolution();
        LoadFullscreenMode();
        LoadVsync();
        LoadSoundSettings();
    }

    private void DisableMouse()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    #region Resolution

    [Title("Resolution Settings")]
    [SerializeField] private UIButton resolutionButton;
    [SerializeField] private UIButton resolutionLeftArrow;
    [SerializeField] private UIButton resolutionRightArrow;
    [SerializeField] private TextMeshProUGUI resolutionText;

    private const string RESOLUTION_PREF_KEY = "resolution";
    private Resolution[] resolutions;
    private int currentResolutionIndex;

    private void LoadResolution()
    {
        resolutions = Screen.resolutions;
        currentResolutionIndex = PlayerPrefs.GetInt(RESOLUTION_PREF_KEY, resolutions.Length - 1);
        SetResolutionText(resolutions[currentResolutionIndex]);
        ApplyResolution(resolutions[currentResolutionIndex], currentResolutionIndex);
    }

    public void SetNextResolution()
    {
        currentResolutionIndex = GetNextWrappedIndex(resolutions, currentResolutionIndex);
        SetResolutionText(resolutions[currentResolutionIndex]);
        ApplyResolution(resolutions[currentResolutionIndex], currentResolutionIndex);
    }

    public void SetPreviousResolution()
    {
        currentResolutionIndex = GetPreviousWrappedIndex(resolutions, currentResolutionIndex);
        SetResolutionText(resolutions[currentResolutionIndex]);
        ApplyResolution(resolutions[currentResolutionIndex], currentResolutionIndex);
    }

    private void ApplyResolution(Resolution resolution, int newResolutionIndex)
    {
        currentResolutionIndex = newResolutionIndex;
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
        PlayerPrefs.SetInt(RESOLUTION_PREF_KEY, currentResolutionIndex);
        PlayerPrefs.Save();
    }

    private void SetResolutionText(Resolution resolution)
        => resolutionText.text = resolution.width + " x " + resolution.height;

    #endregion

    #region Fullscreen

    [Title("Fullscreen Settings")]
    [SerializeField] private UIButton fullscreenButton;
    [SerializeField] private UIButton fullscreenLeftArrow;
    [SerializeField] private UIButton fullscreenRightArrow;
    [SerializeField] private TextMeshProUGUI fullscreenText;

    private const string FULLSCREENMODE_PREF_KEY = "fullscreenMode";

    private FullScreenMode fullscreenModes;
    private int numberOfModes;
    private int currentModeIndex;

    private void LoadFullscreenMode()
    {
        numberOfModes = Enum.GetNames(typeof(FullScreenMode)).Length;
        currentModeIndex = PlayerPrefs.GetInt(FULLSCREENMODE_PREF_KEY, 0);
        SetFullscreenText(GetFullscreenMode(currentModeIndex));
        ApplyFullscreenMode(GetFullscreenMode(currentModeIndex), currentModeIndex);
    }

    public void SetNextFullscreenMode()
    {
        currentModeIndex = GetNextWrappedIndexFullscreenModeEnum(currentModeIndex);
        SetFullscreenText(GetFullscreenMode(currentModeIndex));
        ApplyFullscreenMode(GetFullscreenMode(currentModeIndex), currentModeIndex);
    }

    public void SetPreviousFullscreenMode()
    {
        currentModeIndex = GetPreviousWrappedIndexFullscreenModeEnum(currentModeIndex);
        SetFullscreenText(GetFullscreenMode(currentModeIndex));
        ApplyFullscreenMode(GetFullscreenMode(currentModeIndex), currentModeIndex);
    }

    private void ApplyFullscreenMode(FullScreenMode mode, int newCurrentModeIndex)
    {
        currentModeIndex = newCurrentModeIndex;
        Screen.fullScreenMode = mode;
        PlayerPrefs.SetInt(FULLSCREENMODE_PREF_KEY, currentModeIndex);
        PlayerPrefs.Save();
    }

    private void SetFullscreenText(FullScreenMode mode)
        => fullscreenText.text = mode.ToFriendlyString();

    private FullScreenMode GetFullscreenMode(int index)
        => (FullScreenMode)(Enum.GetValues(fullscreenModes.GetType())).GetValue(index);

    #endregion

    #region VSync

    [Title("VSync Settings")]
    [SerializeField] private UIButton VSyncButton;
    [SerializeField] private UIButton VSyncLeftArrow;
    [SerializeField] private UIButton VSyncRightArrow;
    [SerializeField] private TextMeshProUGUI VSyncText;

    private const string VSync_PREF_KEY = "VSync";

    private FullScreenMode VSyncModes;

    private void LoadVsync()
    {
        QualitySettings.vSyncCount = PlayerPrefs.GetInt(VSync_PREF_KEY, 1);
        SetOnOffText(VSyncText, QualitySettings.vSyncCount);
    }

    public void SetVSync()
    {
        switch (QualitySettings.vSyncCount)
        {
            case 0:
                QualitySettings.vSyncCount = 1;
                break;
            case 1:
                QualitySettings.vSyncCount = 0;
                break;
        }

        SetOnOffText(VSyncText, QualitySettings.vSyncCount);

        PlayerPrefs.SetInt(VSync_PREF_KEY, QualitySettings.vSyncCount);
        PlayerPrefs.Save();
    }

    #endregion

    #region Audio Controller

    [Title("Audio Control")]
    [SerializeField] private AudioMixer audioMixer;
    [SerializeField] private Slider masterSlider;
    [SerializeField] private Slider musicSlider;
    [SerializeField] private Slider ambienceSlider;
    [SerializeField] private Slider sfxSlider;

    private string MASTERVOLUME_PREF_KEY = "masterVolume";
    private string MUSICVOLUME_PREF_KEY = "musicVolume";
    private string AMBIENCEVOLUME_PREF_KEY = "ambienceVolume";
    private string SFXVOLUME_PREF_KEY = "sfxVolume";

    public void SetMasterVolume(float volume)
    {
        audioMixer.SetFloat(MASTERVOLUME_PREF_KEY, volume);

        float masterVolume = 0;
        audioMixer.GetFloat(MASTERVOLUME_PREF_KEY, out masterVolume);
        PlayerPrefs.SetFloat(MASTERVOLUME_PREF_KEY, masterVolume);
        PlayerPrefs.Save();
    }

    public void SetMusicVolume(float volume)
    {
        audioMixer.SetFloat(MUSICVOLUME_PREF_KEY, volume);

        float musicVolume = 0;
        audioMixer.GetFloat(MUSICVOLUME_PREF_KEY, out musicVolume);
        PlayerPrefs.SetFloat(MUSICVOLUME_PREF_KEY, musicVolume);
        PlayerPrefs.Save();
    }

    public void SetAmbienceVolume(float volume)
    {
        audioMixer.SetFloat(AMBIENCEVOLUME_PREF_KEY, volume);

        float ambienceVolume = 0;
        audioMixer.GetFloat(AMBIENCEVOLUME_PREF_KEY, out ambienceVolume);
        PlayerPrefs.SetFloat(AMBIENCEVOLUME_PREF_KEY, ambienceVolume);
        PlayerPrefs.Save();
    }

    public void SetSFXVolume(float volume)
    {
        audioMixer.SetFloat(SFXVOLUME_PREF_KEY, volume);

        float sfxVolume = 0;
        audioMixer.GetFloat(SFXVOLUME_PREF_KEY, out sfxVolume);
        PlayerPrefs.SetFloat(SFXVOLUME_PREF_KEY, sfxVolume);
        PlayerPrefs.Save();
    }

    private void LoadSoundSettings()
    {
        masterSlider.value = PlayerPrefs.GetFloat(MASTERVOLUME_PREF_KEY, 0);
        musicSlider.value = PlayerPrefs.GetFloat(MUSICVOLUME_PREF_KEY, 0);
        ambienceSlider.value = PlayerPrefs.GetFloat(AMBIENCEVOLUME_PREF_KEY, 0);
        sfxSlider.value = PlayerPrefs.GetFloat(SFXVOLUME_PREF_KEY, 0);
    }

    #endregion

    #region Helpers
    private int GetNextWrappedIndex<T>(IList<T> collection, int currentIndex)
    {
        if (collection.Count < 1) return 0;
        return (currentIndex + 1) % collection.Count;
    }

    private int GetPreviousWrappedIndex<T>(IList<T> collection, int currentIndex)
    {
        if (collection.Count < 1) return 0;
        if ((currentIndex - 1) < 0) return collection.Count - 1;
        return (currentIndex - 1) % collection.Count;
    }

    private int GetNextWrappedIndexFullscreenModeEnum(int currentIndex)
    {
        if (numberOfModes < 1) return 0;
        return (currentIndex + 1) % numberOfModes;
    }

    private int GetPreviousWrappedIndexFullscreenModeEnum(int currentIndex)
    {
        if (numberOfModes < 1) return 0;
        if ((currentIndex - 1) < 0) return numberOfModes - 1;
        return (currentIndex - 1) % numberOfModes;
    }

    private void ControlOption(UIButton button, UIButton left, UIButton right)
    {
        if (button.IsSelected)
        {
            left.Interactable = true;
            right.Interactable = true;

            float input = Input.GetAxis("Settings Arrow Buttons");

            if (input < 0 || Input.GetKeyDown(KeyCode.A)) left.ExecuteClick();
            if (input > 0 || Input.GetKeyDown(KeyCode.D)) right.ExecuteClick();
        }
        else
        {
            left.Interactable = false;
            right.Interactable = false;
        }
    }

    private void SetOnOffText(TextMeshProUGUI text, bool flag) => text.text = !flag ? "On" : "Off";
    private void SetOnOffText(TextMeshProUGUI text, int flag) => text.text = flag == 1 ? "On" : "Off";

    #endregion
}
