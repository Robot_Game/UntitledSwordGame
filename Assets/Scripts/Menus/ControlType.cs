﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ControlType", menuName = "Menu/ControlType")]
public class ControlType : ScriptableObject
{
    [SerializeField] private string controlName;
    [SerializeField] private Sprite controlsSprite;

    public string ControlName => controlName;
    public Sprite ControlsSprite => controlsSprite;

}
