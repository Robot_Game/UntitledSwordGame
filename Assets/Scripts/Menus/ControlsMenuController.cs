﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Doozy.Engine.UI;
using Sirenix.OdinInspector;
using TMPro;

public class ControlsMenuController : MonoBehaviour
{
    private void Start()
    {
        LoadControlType();
    }

    private void Update()
    {
        ControlOption(controlTypeButton, controlTypeLeftArrow, controlTypeRightArrow);
    }

    [Title("Control Type Settings")]
    [SerializeField] private UIButton controlTypeButton;
    [SerializeField] private UIButton controlTypeLeftArrow;
    [SerializeField] private UIButton controlTypeRightArrow;
    [SerializeField] private TextMeshProUGUI controlTypeText;
    [SerializeField] private Image controlTypeImage;
    [SerializeField] private ControlType[] controlTypes;
    // 0 - KB & M
    // 1 - PS4

    private const string CONTROLTYPE_PREF_KEY = "controlType";
    private int currentTypeIndex;

    private void LoadControlType()
    {
        currentTypeIndex = PlayerPrefs.GetInt(CONTROLTYPE_PREF_KEY, 0);
        ApplyControlType(controlTypes[currentTypeIndex], currentTypeIndex);
    }

    public void SetNextControlType()
    {
        currentTypeIndex = GetNextWrappedIndex(controlTypes, currentTypeIndex);
        ApplyControlType(controlTypes[currentTypeIndex], currentTypeIndex);
    }

    public void SetPreviousControlType()
    {
        currentTypeIndex = GetPreviousWrappedIndex(controlTypes, currentTypeIndex);
        ApplyControlType(controlTypes[currentTypeIndex], currentTypeIndex);
    }

    private void ApplyControlType(ControlType controlType, int newResolutionIndex)
    {
        currentTypeIndex = newResolutionIndex;

        controlTypeText.text = controlType.ControlName;
        controlTypeImage.sprite = controlType.ControlsSprite;

        PlayerPrefs.SetInt(CONTROLTYPE_PREF_KEY, currentTypeIndex);
        PlayerPrefs.Save();
    }

    #region Helpers
    private int GetNextWrappedIndex<T>(IList<T> collection, int currentIndex)
    {
        if (collection.Count < 1) return 0;
        return (currentIndex + 1) % collection.Count;
    }

    private int GetPreviousWrappedIndex<T>(IList<T> collection, int currentIndex)
    {
        if (collection.Count < 1) return 0;
        if ((currentIndex - 1) < 0) return collection.Count - 1;
        return (currentIndex - 1) % collection.Count;
    }

    private void ControlOption(UIButton button, UIButton left, UIButton right)
    {
        if (button.IsSelected)
        {
            left.Interactable = true;
            right.Interactable = true;

            float input = Input.GetAxis("Settings Arrow Buttons");

            if (input < 0 || Input.GetKeyDown(KeyCode.A)) left.ExecuteClick();
            if (input > 0 || Input.GetKeyDown(KeyCode.D)) right.ExecuteClick();
        }
        else
        {
            left.Interactable = false;
            right.Interactable = false;
        }
    }

    private void SetOnOffText(TextMeshProUGUI text, bool flag) => text.text = !flag ? "On" : "Off";

    #endregion
}
