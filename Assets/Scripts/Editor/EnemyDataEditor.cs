﻿using System;
using UnityEngine;
using UnityEditor;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;
using Sirenix.Utilities;

public class EnemyDataEditor : OdinMenuEditorWindow
{
    [MenuItem("Game Config Tools/Enemy Data Editor")]
    private static void OpenWindow()
    {
        var window = GetWindow<EnemyDataEditor>();
        window.position = GUIHelper.GetEditorWindowRect().AlignCenter(600, 600);
    }

    private CreateNewEnemyData createNewEnemyData;

    protected override void OnEnable()
    {
        base.OnEnable();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();

        if (createNewEnemyData != null) DestroyImmediate(createNewEnemyData.EnemyData);
    }

    protected override OdinMenuTree BuildMenuTree()
    {
        OdinMenuTree tree = new OdinMenuTree();

        tree.AddAllAssetsAtPath("Enemy Data", "Assets/Resources/ScriptableObjects/Datas/Enemies", typeof(EnemyData))
            .AddThumbnailIcons().SortMenuItemsByName();

        return tree;
    }

    protected override void OnBeginDrawEditors()
    {
        OdinMenuTreeSelection selected = MenuTree.Selection;
        EnemyData asset = selected.SelectedValue as EnemyData;
        string path = AssetDatabase.GetAssetPath(asset);

        SirenixEditorGUI.BeginHorizontalToolbar();
        {
            // Create new Asset
            if (SirenixEditorGUI.ToolbarButton("Create New"))
            {
                createNewEnemyData = new CreateNewEnemyData();

                var createNewWindow = GUIHelper.GetCurrentLayoutRect();
                OdinEditorWindow.InspectObjectInDropDown(createNewEnemyData, createNewWindow,
                    createNewWindow.width);
            }

            // Rename Asset
            if (SirenixEditorGUI.ToolbarButton("Rename"))
            {
                if (asset != null)
                {
                    RenameAsset newName = new RenameAsset(path, asset);

                    var renameWindow = GUIHelper.GetCurrentLayoutRect();
                    OdinEditorWindow.InspectObjectInDropDown(newName, renameWindow,
                        new Vector2(renameWindow.width, 80));
                }
            }

            GUILayout.FlexibleSpace();

            // Delete asset
            if (SirenixEditorGUI.ToolbarButton("Delete"))
            {
                if (asset != null)
                {
                    AssetDatabase.DeleteAsset(path);
                    AssetDatabase.SaveAssets();
                }
            }
        }
        SirenixEditorGUI.EndHorizontalToolbar();
    }

    public class CreateNewEnemyData
    {
        [SerializeField, InlineEditor(InlineEditorObjectFieldModes.Hidden)]
        private EnemyData enemyData;

        public EnemyData EnemyData
        {
            get { return enemyData; }
            set { enemyData = value; }
        }

        public CreateNewEnemyData()
        {
            EnemyData = ScriptableObject.CreateInstance<EnemyData>();
            EnemyData.name = "New Enemy Data";
        }

        [PropertySpace(5)]
        [Button(ButtonSizes.Medium)]
        public void CreateNewData()
        {
            AssetDatabase.CreateAsset(EnemyData, "Assets/Resources/ScriptableObjects/Datas/EnemiesNew Enemy Data.asset");
            AssetDatabase.SaveAssets();

            EnemyData = ScriptableObject.CreateInstance<EnemyData>();
            EnemyData.name = "New Enemy Data";
        }
    }

    public class RenameAsset
    {
        [SerializeField]
        [HideLabel, Title("Name", horizontalLine: false, bold: false)]
        private string name;

        private string path;
        private EnemyData asset;

        public string Name => name;

        public RenameAsset(string path, EnemyData asset)
        {
            this.path = path;
            this.name = asset.name;
            this.asset = asset;
        }

        [PropertySpace(5)]
        [Button(ButtonSizes.Medium)]
        private void Rename()
        {
            if (asset != null)
            {
                AssetDatabase.RenameAsset(path, name);
                AssetDatabase.SaveAssets();
            }
        }
    }
}
