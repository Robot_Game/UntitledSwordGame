﻿using System;
using UnityEngine;
using UnityEditor;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;
using Sirenix.Utilities;

public class PlayerDataEditor : OdinMenuEditorWindow
{
    [MenuItem("Game Config Tools/Player Data Editor")]
    private static void OpenWindow()
    {
        var window = GetWindow<PlayerDataEditor>();
        window.position = GUIHelper.GetEditorWindowRect().AlignCenter(600, 600);
    }

    protected override void OnEnable()
    {
        base.OnEnable();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
    }

    protected override OdinMenuTree BuildMenuTree()
    {
        OdinMenuTree tree = new OdinMenuTree();

        tree.AddAllAssetsAtPath("Player Data", "Assets/Resources/ScriptableObjects/Datas/", typeof(PlayerData))
            .AddThumbnailIcons().SortMenuItemsByName();
        tree.AddAllAssetsAtPath("Player Data", "Assets/Resources/ScriptableObjects/Datas/", typeof(SwordData))
        .AddThumbnailIcons().SortMenuItemsByName();
        tree.AddAllAssetsAtPath("Player Data", "Assets/Resources/ScriptableObjects/Datas/", typeof(GameData))
        .AddThumbnailIcons().SortMenuItemsByName();

        return tree;
    }

    protected override void OnBeginDrawEditors()
    {
        OdinMenuTreeSelection selected = MenuTree.Selection;
        UnityEngine.Object asset = selected.SelectedValue as UnityEngine.Object;
        string path = AssetDatabase.GetAssetPath(asset);

        SirenixEditorGUI.BeginHorizontalToolbar();
        {
            // Rename Asset
            if (SirenixEditorGUI.ToolbarButton("Rename"))
            {
                if (asset != null)
                {
                    RenameAsset newName = new RenameAsset(path, asset);

                    var renameWindow = GUIHelper.GetCurrentLayoutRect();
                    OdinEditorWindow.InspectObjectInDropDown(newName, renameWindow,
                        new Vector2(renameWindow.width, 80));
                }
            }

            GUILayout.FlexibleSpace();

            // Delete asset
            if (SirenixEditorGUI.ToolbarButton("Delete"))
            {
                if (asset != null)
                {
                    AssetDatabase.DeleteAsset(path);
                    AssetDatabase.SaveAssets();
                }
            }
        }
        SirenixEditorGUI.EndHorizontalToolbar();
    }

    public class RenameAsset
    {
        [SerializeField]
        [HideLabel, Title("Name", horizontalLine: false, bold: false)]
        private string name;

        private string path;
        private UnityEngine.Object asset;

        public string Name => name;

        public RenameAsset(string path, UnityEngine.Object asset)
        {
            this.path = path;
            this.name = asset.name;
            this.asset = asset;
        }

        [PropertySpace(5)]
        [Button(ButtonSizes.Medium)]
        private void Rename()
        {
            if (asset != null)
            {
                AssetDatabase.RenameAsset(path, name);
                AssetDatabase.SaveAssets();
            }
        }
    }
}
