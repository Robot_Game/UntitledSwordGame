﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackScreen : MonoBehaviour
{
    public void RestartScene() => Scripts.ScenesManager.RestartScene();
    //public void LoadNextScene() => Scripts.UIManager.LoadNextScene();
}
