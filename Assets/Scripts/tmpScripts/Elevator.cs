﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Elevator : MonoBehaviour
{
    [SerializeField] bool isSceneChanger;

    [HideIf("@isSceneChanger == false")]
    [SerializeField] string sceneToLoad;
    private bool isOpen;
    private bool isActive;

    private Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("WE DETECTED: " + other.gameObject.name);
        if (other.CompareTag("Player"))
        {
            if (isOpen && !isActive) Activate();
            if (!isOpen) Open();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (isOpen && !isActive) Close();
        }
    }


    public void Open()
    {
        anim.SetTrigger("Open");
        isOpen = true;
        anim.SetFloat("speedMultiplier", 1);
    }
    public void Close()
    {
        anim.SetFloat("speedMultiplier", -1);
        isOpen = false;
    }
    public void Activate()
    {
        GameEvents.OnSaveInitiated();
        Scripts.CameraController.StopFollowing();
        Scripts.PlayerMovement.DisableMovement();
        Scripts.PlayerMovement.DisableJump();
        Scripts.PlayerAttacks.DisableAllAttacks();
        Scripts.PlayerMovement.GetComponent<Animator>().enabled = false;
        if (isSceneChanger) Scripts.ScenesManager.NextScene = sceneToLoad;
        anim.SetBool("Activate", true);
        anim.SetFloat("speedMultiplier", 1);

    }

    public void OnAnimStart()
    {
        if (!isOpen) anim.SetFloat("speedMultiplier", 0);
    }
    public void OnAnimEnd()
    {
        if (isOpen) anim.SetFloat("speedMultiplier", 0);
    }

    public void ChangeScenes() => Scripts.UIManager.ShowBlackScreen("NextScene");
}
