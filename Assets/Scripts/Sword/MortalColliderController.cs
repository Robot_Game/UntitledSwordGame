﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class MortalColliderController : MonoBehaviour
{
	[SerializeField, InlineEditor, Required] 
	private SwordData swordData;

	private MeshCollider col;

	private void Awake()
	{
		col = GetComponent<MeshCollider>();
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.layer == 9 && other is CapsuleCollider)
		{
			HitEnemy(other.gameObject);
		}
	}

	private void HitEnemy(GameObject enemy)
	{
		Enemy e = enemy.GetComponent<Enemy>();

		e.TakeDamage(swordData.MortalDamage);
		e.GetKnockback(swordData.MortalKnockbackForce);
	}

	public void EnableMortalCollider() => col.enabled = true;

	public void DisableMortalCollider() => col.enabled = false;
}
