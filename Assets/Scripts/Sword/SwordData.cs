﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "SwordData", menuName = "Game Entities/Sword")]
public class SwordData : ScriptableObject
{
    [Title("Attacks")]
    [SerializeField]
    private float normalKnockbackForce;

    // SLASHES
    [ListDrawerSettings(DraggableItems = false, HideRemoveButton = true)]
    [TabGroup("Slashes")]
    [SerializeField]
    [Space(2)]
    private float[] slashDamage;
    [TabGroup("Slashes")]
    [SerializeField]
    private float[] slashKnockbackForce;
    [TabGroup("Slashes")]
    [SerializeField]
    private bool slashSlowMotionEffect;
    [TabGroup("Slashes")]
    [HideIf("@slashSlowMotionEffect == false")]
    [SerializeField]
    private bool slashHasRandomSlowMoChance;
    [TabGroup("Slashes")]
    [HideIf("@slashSlowMotionEffect == false " +
        "|| slashHasRandomSlowMoChance == false")]
    [SerializeField]
    private float slashSlowMoChance;
    [TabGroup("Slashes")]
    [HideIf("@slashSlowMotionEffect == false")]
    [SerializeField]
    private float slashSlowMoSpeed;
    [TabGroup("Slashes")]
    [HideIf("@slashSlowMotionEffect == false")]
    [SerializeField]
    private float slashSlowMoDuration;
    [TabGroup("Slashes")]
    [SerializeField] private AudioClip[] slashesSfx;

    // MORTAL
    [ListDrawerSettings(DraggableItems = false, HideRemoveButton = true)]
    [TabGroup("Mortal Attack")] // Square
    [SerializeField]
    private float mortalDamage; // 2
    [TabGroup("Mortal Attack")]
    [SerializeField]
    private float mortalKnockbackForce; //20
    [TabGroup("Mortal Attack")]
    [SerializeField]
    private float chargedMortalDamage; // 2
    [TabGroup("Mortal Attack")]
    [SerializeField]
    private float chargedMortalKnockbackForce; //20
    [TabGroup("Mortal Attack")]
    [SerializeField]
    private bool mortalSlowMotionEffect;
    [TabGroup("Mortal Attack")]
    [HideIf("@mortalSlowMotionEffect == false")]
    [SerializeField]
    private bool mortalHasRandomSlowMoChance;
    [TabGroup("Mortal Attack")]
    [HideIf("@mortalSlowMotionEffect == false " +
        "|| mortalHasRandomSlowMoChance == false")]
    [SerializeField]
    private float mortalSlowMoChance;
    [TabGroup("Mortal Attack")]
    [HideIf("@mortalSlowMotionEffect == false")]
    [SerializeField]
    private float mortalSlowMoSpeed;
    [TabGroup("Mortal Attack")]
    [HideIf("@mortalSlowMotionEffect == false")]
    [SerializeField]
    private float mortalSlowMoDuration;
    [TabGroup("Mortal Attack")]
    [SerializeField] private AudioClip[] mortalAttackSfx;

    // SPIN
    [ListDrawerSettings(DraggableItems = false, HideRemoveButton = true)]
    [TabGroup("Spin Attack")] // Circle
    [SerializeField]
    private float spinDamage; // 1
    [TabGroup("Spin Attack")]
    [SerializeField]
    private float spinKnockbackForce; //20
    [TabGroup("Spin Attack")]
    [SerializeField]
    private float chargedSpinDamage; // 1
    [TabGroup("Spin Attack")]
    [SerializeField]
    private float chargedSpinKnockbackForce; //10
    [TabGroup("Spin Attack")]
    [SerializeField]
    private bool spinSlowMotionEffect;
    [TabGroup("Spin Attack")]
    [HideIf("@spinSlowMotionEffect == false")]
    [SerializeField]
    private bool spinHasRandomSlowMoChance;
    [TabGroup("Spin Attack")]
    [HideIf("@spinSlowMotionEffect == false " +
        "|| spinHasRandomSlowMoChance == false")]
    [SerializeField]
    private float spinSlowMoChance;
    [TabGroup("Spin Attack")]
    [HideIf("@spinSlowMotionEffect == false")]
    [SerializeField]
    private float spinSlowMoSpeed;
    [TabGroup("Spin Attack")]
    [HideIf("@spinSlowMotionEffect == false")]
    [SerializeField]
    private float spinSlowMoDuration;
    [TabGroup("Spin Attack")]
    [SerializeField] private AudioClip[] spinAttackSfx;

    // AIR DROP
    [ListDrawerSettings(DraggableItems = false, HideRemoveButton = true)]
    [TabGroup("Air Drop")]
    [SerializeField]
    private bool airDropSlowMotionEffect;
    [TabGroup("Air Drop")]
    [HideIf("@airDropSlowMotionEffect == false")]
    [SerializeField]
    private bool airDropHasRandomSlowMoChance;
    [TabGroup("Air Drop")]
    [HideIf("@airDropSlowMotionEffect == false " +
        "|| airDropHasRandomSlowMoChance == false")]
    [SerializeField]
    private float airDropSlowMoChance;
    [TabGroup("Air Drop")]
    [HideIf("@airDropSlowMotionEffect == false")]
    [SerializeField]
    private float airDropSlowMoSpeed;
    [TabGroup("Air Drop")]
    [HideIf("@airDropSlowMotionEffect == false")]
    [SerializeField]
    private float airDropSlowMoDuration;

    // IMPALE
    [ListDrawerSettings(DraggableItems = false, HideRemoveButton = true)]
    [TabGroup("Impale")]
    [SerializeField]
    private int maxImpaledEnemies;
    [TabGroup("Impale")]
    [SerializeField]
    private bool impaleSlowMotionEffect;
    [TabGroup("Impale")]
    [HideIf("@impaleSlowMotionEffect == false")]
    [SerializeField]
    private bool impaleHasRandomSlowMoChance;
    [TabGroup("Impale")]
    [HideIf("@impaleSlowMotionEffect == false " +
        "|| impaleHasRandomSlowMoChance == false")]
    [SerializeField]
    private float impaleSlowMoChance;
    [TabGroup("Impale")]
    [HideIf("@impaleSlowMotionEffect == false")]
    [SerializeField]
    private float impaleSlowMoSpeed;
    [TabGroup("Impale")]
    [HideIf("@impaleSlowMotionEffect == false")]
    [SerializeField]
    private float impaleSlowMoDuration;

    // THROW SWORD
    [ListDrawerSettings(DraggableItems = false, HideRemoveButton = true)]
    [TabGroup("Throw Sword")]
    [SerializeField]
    private bool throwSlowMotionEffect;
    [TabGroup("Throw Sword")]
    [HideIf("@throwSlowMotionEffect == false")]
    [SerializeField]
    private bool throwHasRandomSlowMoChance;
    [TabGroup("Throw Sword")]
    [HideIf("@throwSlowMotionEffect == false " +
        "|| throwHasRandomSlowMoChance == false")]
    [SerializeField]
    private float throwSlowMoChance;
    [TabGroup("Throw Sword")]
    [HideIf("@throwSlowMotionEffect == false")]
    [SerializeField]
    private float throwSlowMoSpeed;
    [TabGroup("Throw Sword")]
    [HideIf("@throwSlowMotionEffect == false")]
    [SerializeField]
    private float throwSlowMoDuration;

    [Title("Hit/Gore Sound Effects")]
    [SerializeField] private AudioClip[] hitEnemySfx;

    [Title("Throw Impaled Object")]
    [SerializeField]
    private float speed;

    [SerializeField] private float lifeTime;

    [SerializeField] private LayerMask layerToHit;

    [SerializeField] private FloatEvent onHitTarget;

    [SerializeField] private float damageProjectile;

    [SerializeField] private float projKnockbackForce;

    [SerializeField, EnumToggleButtons]
    private Space coordinateSpace;

    [SerializeField, EnumToggleButtons]
    private ProjectileType type;

    // Slashes
    public float[] SlashDamage => slashDamage;
    public bool SlashSlowMotionEffect => slashSlowMotionEffect;
    public bool SlashHasRandomSlowMoChance => slashHasRandomSlowMoChance;
    public float SlashSlowMoChance => slashSlowMoChance;
    public float SlashSlowMotionSpeed => slashSlowMoSpeed;
    public float SlashSlowMotionDuration => slashSlowMoDuration;
    public AudioClip[] SlashesSfx => slashesSfx;
    // Mortal
    public float MortalDamage => mortalDamage;
    public float MortalKnockbackForce => mortalKnockbackForce;
    public float ChargedMortalDamage => chargedMortalDamage;
    public bool MortalSlowMotionEffect => mortalSlowMotionEffect;
    public bool MortalHasRandomSlowMoChance => mortalHasRandomSlowMoChance;
    public float MortalSlowMoChance => mortalSlowMoChance;
    public float MortalSlowMotionSpeed => mortalSlowMoSpeed;
    public float MortalSlowMotionDuration => mortalSlowMoDuration;
    public AudioClip[] MortalAttackSfx => mortalAttackSfx;
    // Spin
    public float SpinDamage => spinDamage;
    public float ChargedSpinDamage => chargedSpinDamage;
    public bool SpinSlowMotionEffect => spinSlowMotionEffect;
    public bool SpinHasRandomSlowMoChance => spinHasRandomSlowMoChance;
    public float SpinSlowMoChance => spinSlowMoChance;
    public float SpinSlowMotionSpeed => spinSlowMoSpeed;
    public float SpinSlowMotionDuration => spinSlowMoDuration;
    public AudioClip[] SpinAttackSfx => spinAttackSfx;
    // Air Drop
    public bool AirDropSlowMotionEffect => airDropSlowMotionEffect;
    public bool AirDropHasRandomSlowMoChance => airDropHasRandomSlowMoChance;
    public float AirDropSlowMoChance => airDropSlowMoChance;
    public float AirDropSlowMotionSpeed => airDropSlowMoSpeed;
    public float AirDropSlowMotionDuration => airDropSlowMoDuration;
    // Impale
    public int MaxImpaledObjects => maxImpaledEnemies;
    public bool ImpaleSlowMotionEffect => impaleSlowMotionEffect;
    public bool ImpaleHasRandomSlowMoChance => impaleHasRandomSlowMoChance;
    public float ImpaleSlowMoChance => impaleSlowMoChance;
    public float ImpaleSlowMotionSpeed => impaleSlowMoSpeed;
    public float ImpaleSlowMotionDuration => impaleSlowMoDuration;

    // Throw Sword
    public bool ThrowSlowMotionEffect => throwSlowMotionEffect;
    public bool ThrowHasRandomSlowMoChance => throwHasRandomSlowMoChance;
    public float ThrowSlowMoChance => throwSlowMoChance;
    public float ThrowSlowMotionSpeed => throwSlowMoSpeed;
    public float ThrowSlowMotionDuration => throwSlowMoDuration;
    // Sword Stats
    public float Damage { get; private set; }
    public float KnockbackForce { get; private set; }
    // Throw Enemy On Spin Stats
    public float Speed => speed;
    public float LifeTime => lifeTime;
    public LayerMask LayerToHit => layerToHit;
    public FloatEvent OnHitTarget => onHitTarget;
    public float DamageProjectile => damageProjectile;
    public float ProjKnockbackForce => projKnockbackForce;
    public Space CoordinateSpace => coordinateSpace;
    public ProjectileType Type
    {
        get { return type; }
        set { type = value; }
    }
    public AudioClip[] HitEnemySfx => hitEnemySfx;

    // Attacks should be named properly: "Spin", "ChargedSpin", etc
    public void SetAttackStats(string attackType, int attackNumber)
    {
        // Charged attacks
        if (attackType.Contains("Charged"))
        {
            if (attackType == "ChargedMortal") SetHitStats(ChargedMortalDamage, chargedMortalKnockbackForce);
            else if (attackType == "ChargedSpin") SetHitStats(ChargedSpinDamage, chargedSpinKnockbackForce);
            else Debug.Log("There is something wrong! There is no attack named " + attackType);
        }

        // Normal attacks
        else if (attackType == "Slash") SetHitStats(SlashDamage[attackNumber], slashKnockbackForce[attackNumber]);

        // Special attacks (combos or abilities)
        else if (attackType == "Mortal") SetHitStats(MortalDamage, mortalKnockbackForce);
        else if (attackType == "Spin") SetHitStats(SpinDamage, spinKnockbackForce);

        // In case of error
        else Debug.Log("There is something wrong! There is no attack named " + attackType);
    }

    private void SetHitStats(float damage, float knockbackForce)
    {
        Damage = damage;
        KnockbackForce = knockbackForce;
    }
}
