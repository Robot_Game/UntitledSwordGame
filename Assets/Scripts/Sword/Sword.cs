﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections;
using Random = UnityEngine.Random;

public class Sword : MonoBehaviour
{
    [SerializeField, InlineEditor, Required]
    private SwordData data;

    [Title("Impale")]
    [SerializeField, Required]
    private Transform parentAfterImpale;
    [SerializeField]
    [HideIf("@parentAfterImpale == null")]
    private float distanceBetweenImpaledObjects;

    [Title("Throw Impaled Object")]
    [SerializeField, Required]
    private GameObject refObj;
    [SerializeField, Required]
    private GameObject cursedOrbPrefab;
    [SerializeField]
    [HideIf("@refObj == null")]
    private float radius;
    [SerializeField]
    [HideIf("@refObj == null")]
    private int segments;
    [SerializeField]
    [HideIf("@refObj == null")]
    private float aperture;
    [SerializeField]
    [HideIf("@refObj == null")]
    private LayerMask layersToSnap;

    private Rigidbody rb;
    private PlayerInputActions inputs;
    private BoxCollider[] colliders;
    private MortalColliderController mortalCol;
    private LineOfSight lineOfSight;
    private DashImpale dashImpaleScript;
    private ThrowSword throwSwordScript;
    private List<GameObject> impaledObjects;
    private Vector3 throwInput;
    private Transform parent;
    private Transform player;
    private Vector3 initPos;
    private Vector3 initRot;
    private Vector3 initLocalPos;
    private Vector3 initLocalRot;
    private bool canImpale;
    private bool throwing;
    private bool pulling;
    private float throwForce;
    private Coroutine currentThrowCoroutine;
    private bool triggerFlag;
    public event Action<GameObject> onHitEnemy;

    public SwordData Data => data;

    private void Awake()
    {
        inputs = new PlayerInputActions();
        AddControls();

        rb = GetComponent<Rigidbody>();
        colliders = gameObject.GetComponentsInChildren<BoxCollider>();
        mortalCol = GetComponentInChildren<MortalColliderController>();
        dashImpaleScript = FindObjectOfType<DashImpale>();
        throwSwordScript = FindObjectOfType<ThrowSword>();
        lineOfSight = gameObject.AddComponent<LineOfSight>();
        player = Scripts.PlayerMovement.transform;
        parent = transform.parent;
        initLocalPos = transform.localPosition;
        initLocalRot = transform.localRotation.eulerAngles;
    }

    private void OnEnable()
    {
        inputs.Enable();

        onHitEnemy += Impale;
        onHitEnemy += HitEnemy;
    }

    private void OnDisable()
    {
        inputs.Disable();

        onHitEnemy -= Impale;
        onHitEnemy -= HitEnemy;
    }

    private void Start()
    {
        impaledObjects = new List<GameObject>(data.MaxImpaledObjects);
        canImpale = false;
    }
    private void Update()
    {
        triggerFlag = false;
    }
    private void AddControls()
    {
        inputs.Gameplay.Move.performed += ctx => throwInput = ctx.ReadValue<Vector2>();
        inputs.Gameplay.R1Attack.performed += ctx => PullIfThrowing();
    }
    private void OnTriggerEnter(Collider other)
    {
        Enemy e = other.GetComponent<Enemy>();
        if (e != null)
        {
            if (e.gameObject.layer == 9 && other is CapsuleCollider && !e.Dead)
            {
                onHitEnemy.Invoke(e.gameObject);
                Scripts.PlayerAttacks.ManageSlowMotionEffect();
            }
        }
        Impalable i = other.GetComponent<Impalable>();
        if (i != null && i.IsImpalable && !triggerFlag)
        {
            onHitEnemy.Invoke(i.gameObject);
            triggerFlag = true;
        }

        if (throwing && throwSwordScript.CanStick
            && throwSwordScript.StickingLayers.Contains(other.gameObject.layer))
        {
            Debug.Log("TOUCHED!");
            if (currentThrowCoroutine != null) StopCoroutine(currentThrowCoroutine);
            rb.Sleep();
            rb.isKinematic = true;
            DisableImpale();
            currentThrowCoroutine = StartCoroutine(SwordPullTimer());
        }
    }


    public void HitEnemy(GameObject obj)
    {
        Enemy e = obj.GetComponent<Enemy>();
        if (e == null) return;

        AudioManager.Instance.PlaySfxOneShot(data.HitEnemySfx);
        e.TakeDamage(data.Damage);
        e.GetKnockback(data.KnockbackForce);
    }

    #region Impale
    private void Impale(GameObject obj)
    {
        if (dashImpaleScript.CanImpale && canImpale)
        {
            // Implement IsGrounded to check if aerial dash is allowed
            //if(!player.IsGrounded && !dashImpale.CanAerialDash) return;
            Enemy e = obj.GetComponent<Enemy>();
            if (e != null)
            {
                if (e.Impalable)
                    if (impaledObjects.Count < data.MaxImpaledObjects)
                    {
                        e.TakeDamage(0, true);
                        e.Die(true, true);
                        StartCoroutine(SpawnImpaledEnemy(e));
                    }
            }
            else
            {
                Impalable i = obj.GetComponent<Impalable>();
                if (i != null && impaledObjects.Count < data.MaxImpaledObjects
                && i.IsImpalable) SpawnCursedOrb(i);

            }
        }
    }

    private IEnumerator SpawnImpaledEnemy(Enemy enemy)
    {
        // Wait for enemy to die
        while (!enemy.Dead)
        {
            yield return null;
        }

        GameObject impaledEnemy = Instantiate(
            enemy.Data.ImpaledVersion,
            parentAfterImpale.position,
            Quaternion.Euler(Random.Range(0, 360), Random.Range(0, 360), 0),
            parentAfterImpale);

        enemy.DestroyFarAway();

        impaledObjects.Add(impaledEnemy);

        OrganizeImpaledObjects();
    }

    private void SpawnCursedOrb(Impalable impalable)
    {
        impalable.Activate(true);

        GameObject impaledOrb = Instantiate(
            cursedOrbPrefab,
            parentAfterImpale.position,
            Quaternion.Euler(Random.Range(0, 360), Random.Range(0, 360), 0),
            parentAfterImpale);

        impaledObjects.Add(impaledOrb);

        OrganizeImpaledObjects();

    }

    private void OrganizeImpaledObjects()
    {
        for (int i = 0; i < impaledObjects.Count; i++)
        {
            float posOffset = (data.MaxImpaledObjects - i) * distanceBetweenImpaledObjects;

            //Vector3 correctPos = new Vector3(0, 0, 0 + posOffset);
            Vector3 correctPos = new Vector3(0, posOffset, 0);

            impaledObjects[impaledObjects.Count - 1 - i].transform.localPosition = correctPos;
        }
    }

    public void ActivateImpalablesEffectsOnMortal()
    {
        foreach (GameObject i in impaledObjects)
        {
            Enemy e = i.GetComponent<Enemy>();
            if (e != null) e.MortalSpecialEffect();
            else
            {
                Impalable impalable = i.GetComponent<Impalable>();
                if (impalable != null) impalable.MortalSpecialEffect();
            }
        }
    }

    public void ClearSword()
    {
        /* Deactivate enemies on sword
		 * We cant kill them instanty because they need to be alive for their 
		 * particle effects to perform*/
        foreach (GameObject objectOnSword in impaledObjects)
        {
            objectOnSword.transform.SetParent(null);
            objectOnSword.GetComponent<Collider>().enabled = false;
            Enemy e = objectOnSword.GetComponent<Enemy>();
            if (e != null)
            {
                objectOnSword.transform.GetChild(0).gameObject.SetActive(false);
                objectOnSword.transform.GetChild(1).gameObject.SetActive(false);
            }
            else objectOnSword.GetComponent<MeshRenderer>().enabled = false;

        }

        for (int i = impaledObjects.Count - 1; i >= 0; i--)
        {
            GameObject impaledObject = impaledObjects[i];

            Destroy(impaledObject, 10);
            impaledObjects.Remove(impaledObject);
        }
    }

    public void ThrowImpaledObject()
    {
        if (impaledObjects.Count <= 0) return;
        GameObject impaledObject = impaledObjects[impaledObjects.Count - 1];

        RemoveImpalableFromSword(impaledObject);

        Tuple<Vector3, Transform> targetInfo = GetThrowDirection(impaledObject.transform, radius, segments, aperture, layersToSnap);

        data.Type = targetInfo.Item2 == null ? ProjectileType.Normal : ProjectileType.Tracking;

        impaledObject.transform.parent = null;

        Projectile proj = impaledObject.gameObject.AddComponent<FriendlyProjectile>();
        proj.Initialize(targetInfo.Item1, targetInfo.Item2, data.Speed, data.LifeTime,
            data.LayerToHit, data.OnHitTarget, data.DamageProjectile, data.ProjKnockbackForce,
            0, data.CoordinateSpace, data.Type, null, data.HitEnemySfx);
    }
    public Tuple<Vector3, Transform> GetThrowDirection(Transform projPos, float radius, int segments, float aperture, LayerMask layers, Vector3 direction = default)
    {
        Vector3 throwDirection = direction;

        if (direction == default)
        {
            throwDirection = new Vector3(throwInput.x, 0, throwInput.y);

            throwDirection = throwDirection == Vector3.zero ?
                transform.root.forward :
                throwDirection;
        }
        // Calculates points given the pointing direction and radius
        lineOfSight.CalculatePoints(refObj, transform.root, throwDirection.normalized, radius, segments, aperture);

        DrawLines();    // TEST ONLY!!!!!!!!!!!!!!!!

        // Checks if enemy is crossing our snap lines
        Collider c = lineOfSight.CheckForHits(transform.root, layers);

        if (c != null)
        {
            throwDirection = c.transform.position - projPos.position;
            return new Tuple<Vector3, Transform>(throwDirection.normalized, c.transform);
        }

        throwDirection = (refObj.transform.position - projPos.position).normalized;
        return new Tuple<Vector3, Transform>(throwDirection.normalized, null);
    }

    // TEST ONLY!!!!!!!!!!!!!!!!
    private void DrawLines()
    {
        for (int i = 0; i < lineOfSight.SnapNodes.Count; i++)
        {
            Debug.DrawLine(transform.root.position, lineOfSight.SnapNodes[i], Color.yellow, 3f);
        }
    }

    public void RemoveImpalableFromSword(GameObject obj, bool destroy = false, float timeToDestroy = 0)
    {
        //impaledObjects.RemoveAt(impaledObjects.Count - 1);
        impaledObjects.Remove(obj);

        if (destroy) Destroy(obj, 0);

    }
    #endregion

    #region Sword Throw
    public void ThrowSword()
    {
        currentThrowCoroutine = null;
        EnableImpale();
        EnableColliders();
        rb.isKinematic = false;
        rb.collisionDetectionMode = CollisionDetectionMode.Continuous;
        transform.SetParent(null, true);
        Scripts.PlayerAttacks.HasSword = false;

        Tuple<Vector3, Transform> targetInfo = GetThrowDirection(transform, 40, 30, 18, layersToSnap);
        if (targetInfo.Item2 != null)
        {
            var lookPos = targetInfo.Item1;
            lookPos.y = 0;
            var rotation = Quaternion.LookRotation(lookPos);
            transform.rotation = rotation;
        }

        //RemoveObjectFromAnimator(transform.gameObject, Scripts.PlayerMovement.GetComponent<Animator>());
        transform.eulerAngles = new Vector3(0, 180 + transform.eulerAngles.y, 0);
        transform.position += -transform.right + transform.up / 2;
        throwForce = throwSwordScript.ThrowForce;
        throwing = true;
        pulling = false;
        //if(!throwSwordScript.HasThrowDeceleration) rb.AddForce(-transform.forward * throwForce, ForceMode.Impulse);
        currentThrowCoroutine = StartCoroutine(ThrowSwordTimer());
    }

    private IEnumerator ThrowSwordTimer()
    {
        float timer = throwSwordScript.ThrowDuration;
        float deceleration;
        if (throwSwordScript.UseCustomDeceleration) deceleration = throwSwordScript.ThrowDeceleration;
        else deceleration = throwSwordScript.ThrowForce / throwSwordScript.ThrowDuration;

        while (timer > 0)
        {
            if (throwSwordScript.HasThrowDeceleration)
            {
                if (throwForce > 0)
                    throwForce -= deceleration * Time.deltaTime;
                else throwForce = 0;
            }
            else throwForce = throwSwordScript.ThrowForce;
            float step = throwForce * Time.deltaTime; // calculate distance to move
            transform.position = Vector3.MoveTowards(transform.position, transform.position - transform.forward, step);
            timer -= Time.deltaTime;
            yield return null;
        }

        StartCoroutine(PullSwordBack());
    }

    private IEnumerator SwordPullTimer()
    {
        float timer = throwSwordScript.ThrowDuration;
        while (timer > 0)
        {
            timer -= Time.deltaTime;
            yield return null;
        }
        throwForce = 0;
        StartCoroutine(PullSwordBack());
    }
    private void PullIfThrowing()
    {
        if (throwing && !pulling)
        {
            throwForce = 0;
            if (currentThrowCoroutine != null)
                StopCoroutine(currentThrowCoroutine);
            StartCoroutine(PullSwordBack());
        }
    }
    private IEnumerator PullSwordBack()
    {
        pulling = true;
        throwing = false;
        EnableImpale();
        rb.Sleep();
        rb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
        rb.isKinematic = true;
        Scripts.PlayerMovement.GetComponent<Animator>().SetBool("PullingSword", true);

        Vector3 destination = player.position;
        float acceleration;
        if (throwSwordScript.UseCustomAcceleration) acceleration = throwSwordScript.PullAcceleration;
        else acceleration = throwSwordScript.PullSpeed / throwSwordScript.ThrowDuration;
        while (Vector3.Distance(transform.position, player.position) > 1f
                || Vector3.Angle(transform.forward, destination - transform.position) > 3f)
        {
            destination = player.position;
            //Movement
            if (throwSwordScript.HasPullAcceleration)
            {
                if (throwForce < throwSwordScript.PullSpeed)
                    throwForce += acceleration * Time.deltaTime;
                else throwForce = throwSwordScript.PullSpeed;
            }
            else throwForce = throwSwordScript.PullSpeed;
            float step = throwForce * Time.deltaTime; // calculate distance to move
            transform.position = Vector3.MoveTowards(transform.position, player.position, step);

            //Rotation
            Vector3 lookPos = destination - transform.position;
            Quaternion rotation = Quaternion.LookRotation(lookPos);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, throwForce * Time.deltaTime);
            yield return null;
        }

        if (currentThrowCoroutine != null) StopCoroutine(currentThrowCoroutine);

        CatchSword();

    }

    private void CatchSword()
    {
        DisableImpale();
        rb.Sleep();
        rb.isKinematic = true;
        rb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
        transform.SetParent(parent);
        transform.localPosition = initLocalPos;
        transform.localEulerAngles = initLocalRot;
        pulling = false;
        throwing = false;
        Scripts.PlayerMovement.GetComponent<Animator>().SetBool("PullingSword", false);
        Scripts.PlayerAttacks.HasSword = true;
        throwSwordScript.StartCooldownTimer();
    }

    /*private void RemoveObjectFromAnimator(GameObject gameObject, Animator animator)
    {
        Transform parentTransform = gameObject.transform.parent;

        gameObject.transform.parent = null;

        float playbackTime = animator.playbackTime;

        animator.Rebind();

        animator.playbackTime = playbackTime;

        gameObject.transform.parent = parentTransform;
    }*/
    #endregion

    public void EnableColliders()
    {
        foreach (BoxCollider c in colliders) c.enabled = true;
    }

    public void DisableColliders()
    {
        foreach (BoxCollider c in colliders) c.enabled = false;
    }

    public void SetAttackStats(string s, int i = 0) => data.SetAttackStats(s, i);

    public void EnableMortalCollider() => mortalCol.EnableMortalCollider();

    public void DisableMortalCollider() => mortalCol.DisableMortalCollider();
    public void EnableImpale() => canImpale = true;
    public void DisableImpale() => canImpale = false;
}
