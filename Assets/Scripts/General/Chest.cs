﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Chest : MonoBehaviour
{
    [Header("Loot Drop")]
    [SerializeField] private int lootAmount;
    [SerializeField] private GameObject lootObj;
    [SerializeField] private float lootDropForce;
    [Header("Breaking")]
    [SerializeField] private LayerMask breakingLayers;
    [SerializeField] private Rigidbody[] breakedPieces;
    [SerializeField] private Transform explosionPlaceholder;
    [SerializeField] private float explosionForce;
    [SerializeField] private float upwardsModifier;
    [SerializeField] private Vector3 offset;

    [Header("Other")]
    [SerializeField] private GameObject lights;
    [SerializeField] private GameObject blueWalls;

    private Animator anim;
    private TMP_Text text;

    private Vector3 explosionPosition;


    private void Awake()
    {
        anim = GetComponent<Animator>();
        lootObj.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("COLLIDED WITH: " + other.gameObject.name);
        if (breakingLayers.Contains(other.gameObject.layer))
        {
            Transform sword = FindObjectOfType<Sword>().transform;
            explosionPosition = sword.position + (sword.up * 1.3f) + offset;
            OpenChest();
        }
    }

    public void Submerge() => anim.SetTrigger("Submerge");
    public void Emmerge() => anim.SetTrigger("Emmerge");
    public void OpenChest()
    {
        foreach (Rigidbody rb in breakedPieces)
        {
            rb.isKinematic = false;
            rb.AddExplosionForce(explosionForce, explosionPosition, 5, upwardsModifier, ForceMode.Impulse);
        }

        lights.SetActive(false);
        blueWalls.SetActive(false);
        anim.SetTrigger("Open");
        DropLoot();
    }

    public void SetLootAmount(int amount) => lootAmount = amount;


    private void DropLoot()
    {
        GetComponent<AudioSource>().Stop();
        float randRotation = Random.Range(0f, 360f);
        lootObj.transform.rotation = Quaternion.Euler(0f, randRotation, 0f);
        lootObj.transform.position += offset;
        lootObj.SetActive(true);
        Rigidbody itemRb = lootObj.GetComponent<Rigidbody>();

        itemRb.AddForce((lootObj.transform.forward + Vector3.up) * lootDropForce, ForceMode.Impulse);
    }
}
