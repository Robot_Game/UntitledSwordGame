﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "GameData", menuName = "Game Entities/Game")]
public class GameData : ScriptableObject
{
    /// <summary>
    /// Layers which are considered ground
    /// </summary>
    [SerializeField]
    private LayerMask groundDetectionLayers;
    /// <summary>
    /// Layers which are considered terrain (walls, ground,
    /// anything that should confine/restrain the player
    /// </summary>
    [SerializeField]
    private LayerMask terrainLayers;
    [SerializeField]
    private float gravityForce;
    [SerializeField]
    private float maxFallingSpeed;

    public LayerMask GroundDetectionLayers => groundDetectionLayers;
    public LayerMask TerrainLayers => terrainLayers;
    public float GravityForce => gravityForce;
    public float MaxFallingSpeed => maxFallingSpeed;
}
