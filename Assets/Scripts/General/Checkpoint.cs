﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    [SerializeField] private AudioClip[] redSymbolSpawnSfx;
    private GameObject symbolActive;
    private GameObject symbolDeactive;

    private CheckpointManager cm;
    private bool flag;
    public Vector3 SpawnPos { get; private set; }
    public bool IsActive { get; private set; } = false;
    public bool IsVirgin { get; private set; } = true;

    protected CheckpointSaveData data;

    // Start is called before the first frame update
    void Awake()
    {
        data = new CheckpointSaveData(this);

        symbolActive = transform.GetChild(0).gameObject;
        symbolDeactive = transform.GetChild(1).gameObject;

        cm = FindObjectOfType<CheckpointManager>();
        //SpawnPos = transform.Find("SpawnPosition").transform.position;
        SpawnPos = new Vector3(
            transform.position.x,
            transform.position.y + 0.7f,
            transform.position.z);
    }

    void Start()
    {
        ManageStartState();
        flag = true;
    }

    private void ManageStartState()
    {
        if (Scripts.SavedObjects.IsObjectSaved(
            Scripts.SavedObjects.SavedCheckpoints, ref data))
        {
            IsActive = data.IsActive;
            IsVirgin = data.IsVirgin;

            if (!IsVirgin)
            {
                if (IsActive)
                {
                    VisualFeedback(true);
                    cm.SetCurrentCheckpoint(this);
                }
                else VisualFeedback(false);
            }
        }
    }

    // Activate checkpoint (red fire ignites f.e.)
    public void Activate(bool saveGame = true)
    {
        if (!IsActive)
        {
            AudioManager.Instance.PlaySfxOneShot(redSymbolSpawnSfx);

            if (cm.CurrentCheckpoint != null) cm.DeactivateCurrentCheckpoint();
            cm.SetCurrentCheckpoint(this);

            /*VISUAL EFFECTS START HERE*/
            VisualFeedback(true);
            /*VISUAL EFFECTS END HERE*/

            IsActive = true;
            data.IsActive = true;
            if (IsVirgin)
            {
                IsVirgin = false;
                data.IsVirgin = false;
            }

            Scripts.PlayerActions.SetSpawnPos(SpawnPos);

            // Save the game
            if (saveGame)
            {
                Scripts.SavedObjects.AddObject(Scripts.SavedObjects.SavedCheckpoints, data);
                GameEvents.OnSaveInitiated();
            }
        }

    }

    // This method is called on the last checkpoint when the player
    // activates a new one.
    // Deactivate checkpoint (fire extints but wood stays kinda burnt
    // and with ashes or a little smoke f.e.)
    public void Deactivate()
    {
        if (IsActive)
        {
            /*VISUAL EFFECTS START HERE*/
            VisualFeedback(false);
            /*VISUAL EFFECTS END HERE*/

            IsActive = false;
            data.IsActive = false;
        }
    }

    private void VisualFeedback(bool lit)
    {
        if (lit)
        {
            symbolActive.SetActive(true);
            symbolDeactive.SetActive(true);
            //transform.Find("BlackFire").gameObject.SetActive(false);
            //transform.Find("RedFire").gameObject.SetActive(true);
        }
        else
        {
            symbolActive.SetActive(false);
            symbolDeactive.SetActive(true);
            //transform.Find("RedFire").gameObject.SetActive(false);
            //transform.Find("BlackFire").gameObject.SetActive(true);
        }
    }

    // Show the UI button that activates this checkpoint
    private void ShowInteractionButton(bool value)
    {
        // buttonSprite.SetActive(value);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Player") && flag)
        {
            bool flag = false;
            if (IsVirgin && !IsActive)
            {
                Activate();
                Scripts.UIManager.ShowActionKey("Open Store");
                flag = true;
            }
            if (!IsVirgin && !IsActive && !flag) Scripts.UIManager.ShowActionKey("[Press] Open Store\n[Hold] Lit Checkpoint");
            if (!IsVirgin && IsActive && !flag) Scripts.UIManager.ShowActionKey("Open Store");

        }
    }
}
