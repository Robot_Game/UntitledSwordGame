﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class ImpalablePlant : Activable
{
    [SerializeField, Required]
    private GameObject impalableOrbPrefab;
    [SerializeField, Required]
    private Transform orbPlaceHolder;
    [SerializeField, Required]
    private float timeToRespawnOrb;
    public override void Activate(bool value)
    {
        connectedInteractibles[0] = null;
        StartCoroutine(ResetAfterInterval());
    }

    public override void ManageActiveState(bool value)
    {
        Activate(value);
    }
    private IEnumerator ResetAfterInterval()
    {
        float timer = timeToRespawnOrb;
        while (timer > 0)
        {
            timer -= Time.deltaTime;
            yield return null;
        }

        InstantiateNewOrb();

    }

    private void InstantiateNewOrb()
    {
        connectedInteractibles[0] =
        Instantiate(impalableOrbPrefab, orbPlaceHolder.position, orbPlaceHolder.rotation, transform)
        .GetComponent<Interactible>();

        anim.Rebind();
        anim.SetTrigger("SpawnOrb");

        LinkToInteractibles();
    }
    protected override bool ActivationConditionsMet(bool value) => true;
}
