﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : Activable
{
    [SerializeField] private bool canBlowUp;
    private void OnTriggerEnter(Collider other)
    {
        if(!canBlowUp) return;
        Enemy enemy = other.gameObject.GetComponent<Enemy>();
        if (enemy != null)
        {
            if (enemy.GetComponent<Projectile>() != null)
            {
                if (enemy.Type == EnemyType.Bomber && !IsActive) anim.SetTrigger("Destroy");

            }
        }
    }
}
