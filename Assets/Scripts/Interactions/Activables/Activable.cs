﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Activable : MonoBehaviour
{
    // The Interactibles that need to be activated in order to activate this object
    [ListDrawerSettings(DraggableItems = false)]
    [SerializeField, Required, SceneObjectsOnly]
    protected Interactible[] connectedInteractibles;

    //The amount of time it takes to reset the Interactibles after 
    // the first Interactible has been activated
    [InfoBox("If this value is 0, the counter will be ignored")]
    [HideIf("@connectedInteractibles.Length < 2")]
    [SerializeField, Range(0,3)]
    protected float resetTime;
    /// <summary>
    /// Whether this activable can be enabled and disabled multiple times.
    /// </summary>
    [EnumToggleButtons]
    [ToggleGroup("activatesMultipleTimes", "Activates Multiple Times")]
    [SerializeField]
    protected bool activatesMultipleTimes;

    /// <summary>
    /// Wait for the animation to end before allowing the player to
    /// interact again.
    /// </summary>
    [EnumToggleButtons]
    [ToggleGroup("activatesMultipleTimes", "Activates Multiple Times")]
    [SerializeField]
    protected bool lockInteractibleWhileInAnimation;

    [Title("Activable sound")]
    [SerializeField] private AudioClip[] activableSfx;

    protected Animator anim;

    public bool IsActive { get; protected set; }

    protected void Awake()
    {
        anim = GetComponent<Animator>();
        if (anim == null) Debug.LogError("The Activable " + gameObject + "has no animator attached to it!!");
        if (connectedInteractibles.Length < 2) resetTime = 0;
    }

    private void Start()
    {
        if (connectedInteractibles.Length > 0) LinkToInteractibles();
        if (lockInteractibleWhileInAnimation) connectedInteractibles[0].SetWaitForActivableAnim(true);
    }
    protected void LinkToInteractibles()
    {
        if (connectedInteractibles.Length < 1) return;
        foreach (Interactible i in connectedInteractibles)
        {
            i.AddTarget(this);
        }
    }
    // Activate or Deactivate the object depending on the value
    public virtual void Activate(bool value)
    {
        AudioManager.Instance.PlaySfxOneShot(activableSfx);

        anim.SetBool("Active", value);
        if (value == true) anim.SetFloat("speedMultiplier", 1);
        else anim.SetFloat("speedMultiplier", -1);

        IsActive = value;
    }

    // Activate or Deactivate the object depending on the value
    public virtual void ManageActiveState(bool value)
    {
        if (ActivationConditionsMet(value) == true) Activate(value);
        // If this object resets after an interval, start the reset coroutine
        if (resetTime != 0) StartCoroutine(CountdownToReset());
    }

    protected IEnumerator CountdownToReset()
    {
        float time = 0;
        while (time < resetTime)
        {
            time += Time.deltaTime;
            yield return null;
        }

        // If this object is still deactivated, then it means that
        // the interactibles were not all activated on time
        if (!IsActive)
        {
            foreach (Interactible i in connectedInteractibles)
            {
                // So we reset them
                i.Activate(false);
            }
        }
    }

    protected virtual bool ActivationConditionsMet(bool value)
    {
        if (!IsActive && value == true)
        {
            foreach (Interactible i in connectedInteractibles)
            {
                if (!i.IsActive) return false;
            }
            return true;
        }
        // If this object is already active and can be deactivated 
        // and an interactible was deactivated
        if (IsActive && activatesMultipleTimes && value == false)
        {
            return true;
        }

        return false;
    }

    public void OnAnimStart()
    {
        if(!IsActive) anim.SetFloat("speedMultiplier", 0);
        connectedInteractibles[0].OnActivableAnimStart();
    }
    public void OnAnimEnd()
    {
        if (IsActive) anim.SetFloat("speedMultiplier", 0);
        connectedInteractibles[0].OnActivableAnimEnd();
    }

}
