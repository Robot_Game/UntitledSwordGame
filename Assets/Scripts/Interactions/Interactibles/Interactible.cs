﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Interactible : MonoBehaviour
{
    [SerializeField] protected InteractibleTypes type;
    
    [Title("Properties")]
    [HideIf("@type != InteractibleTypes.Lever ||" +
        "type != InteractibleTypes.Target")]
    // Variable that defines whether this interactible
    // can be activated and deactivated multiple times
    // or only once
    [SerializeField] protected bool activatesMultipleTimes;
    [SerializeField] protected bool isSavable = true;

    // An object used to visualize if the interactible is active or deactive
    [SerializeField] protected GameObject feedbackObject;

    [Title("Interactible sound")]
    [SerializeField] private AudioClip[] interactSfx;
    [SerializeField] protected AudioClip[] activateSfx;

    // The Activable that this Interactible activates
    protected List<Activable> targetObjects;
    protected Animator anim;

    protected bool waitForActivableAnim;
    protected bool activableAnimFinished = true;

    public InteractibleTypes Type => type;
    public bool IsActive { get; protected set; }
    public bool ActivatesMultipleTimes => activatesMultipleTimes;

    protected InteractibleSaveData data;

    public AudioClip[] InteractSfx => interactSfx;


    protected virtual void Awake()
    {
        if (isSavable) data = new InteractibleSaveData(this);

        anim = GetComponent<Animator>();
        targetObjects = new List<Activable>();
    }

    protected virtual void Start()
    {
        ManageStartState();
    }

    protected virtual void ManageStartState()
    {
        if (isSavable && Scripts.SavedObjects.IsObjectSaved(
            Scripts.SavedObjects.SavedInteractibles, ref data))
            if (data.IsActive) Activate(true);

    }

    public virtual void Activate(bool value)
    {
        // Check if it's ative and can be deactivated 
        // or if it's deactive and we want to activate it
        if ((IsActive && activatesMultipleTimes && value == false)
            || (!IsActive && value == true))
        {
            IsActive = value;
            if (isSavable) data.IsActive = IsActive;

            if (targetObjects.Count > 0)
            {
                foreach (Activable target in targetObjects)
                    target.ManageActiveState(value);
            }

            //Activate or deactivate feedback object
            if (feedbackObject != null)
            {
                Animator anim = feedbackObject.GetComponent<Animator>();
                if (anim != null) anim.SetBool("Feedback", value);

                else Debug.LogError("The feedback object "
                    + feedbackObject + " does not have an animator!");
            }
        }

        if(isSavable) Scripts.SavedObjects.AddObject(
            Scripts.SavedObjects.SavedInteractibles, data);
    }

    public void AddTarget(Activable target) => targetObjects.Add(target);
    public void SetWaitForActivableAnim(bool value) => waitForActivableAnim = value;

    public void OnActivableAnimStart() => activableAnimFinished = false;
    public void OnActivableAnimEnd() => activableAnimFinished = true;
}