﻿public enum InteractibleTypes
{
    Lock,
    Lever,
    Target,
    Breakable,
    Swingable,
    Impalable
}
