﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lock : Interactible
{
    private PlayerActions pa;
    protected override void Awake()
    {
        base.Awake();
        if (anim == null) Debug.LogError("Lock " + gameObject + " has no animator!!");
        pa = FindObjectOfType<PlayerActions>();
    }

    protected override void Start()
    {
        base.Start();
    }
    public override void Activate(bool value)
    {
        if (!IsActive && value == true)
        {
            AudioManager.Instance.PlaySfxOneShot(activateSfx);
            anim.SetBool("Active", value);
        }
    }

    private void LeaveLock()
    {
        if (pa.CurrentInteractible != null)
        {
            pa.LeaveInteractible();
        }
        anim.SetBool("Active", false);
    }
    private void ActivateTarget() => base.Activate(true);
}
