﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swingable : Interactible
{
    /// <summary>
    /// Layers that collide with this swingable and push it
    /// </summary>
    [SerializeField] private LayerMask swingableLayers;
    [SerializeField] private float knockbackForce;
    [SerializeField] private float knockbackDeceleration;
    [SerializeField] private bool hurtsEnemies;
    [HideIf("@!hurtsEnemies")]
    [SerializeField] private float damage;
    [SerializeField] private bool isBreakable;
    [HideIf("@!isBreakable")]
    [SerializeField] private float breakForce;
    [HideIf("@!isBreakable")]
    [SerializeField] private LayerMask breakingLayers;

    private Rigidbody rb;
    private SwingableSaveData sData;
    private GravityForce gravity;
    private Vector3 currentForce;
    private bool triggerFlag;
    private bool flag;

    public bool IsMoving { get; private set; } = false;

    public Vector3 Pos { get; private set; }
    public bool IsDestroyed { get; private set; }
    protected override void Awake()
    {
        base.Awake();
        rb = GetComponent<Rigidbody>();
        if (rb == null)
        {
            rb = gameObject.AddComponent<Rigidbody>();
            rb.constraints = RigidbodyConstraints.FreezeRotation;
        }
        sData = new SwingableSaveData(this);

        gravity = GetComponent<GravityForce>();
    }
    private void OnEnable()
    {
        //GameEvents.SaveInitiated += Save;
    }

    private void OnDisable()
    {
        //GameEvents.SaveInitiated -= Save;
    }
    protected override void ManageStartState()
    {

        if (Scripts.SavedObjects.IsObjectSaved(
            Scripts.SavedObjects.SavedSwingables, ref sData))
        {
            if (sData.IsDestroyed) Destroy(gameObject);
            else transform.position = new Vector3(sData.PosX, sData.PosY, sData.PosZ);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // Check if sword has collided with this object
        if (!triggerFlag && swingableLayers.Contains(other.gameObject.layer))
        {
            Debug.Log("DETECTED SWINGABLE!");
            GetKnockback(other.gameObject.GetComponentInParent<Sword>());
            triggerFlag = true;
        }

        if (other.gameObject.layer == LayerMask.NameToLayer("Instakill"))
        {
            StartCoroutine(DestroyAfterDelay(5));
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (isBreakable && IsMoving && currentForce.magnitude >= breakForce && !IsDestroyed && !flag)
        {
            if (breakingLayers.Contains(other.gameObject.layer))
            {
                Debug.Log("Collided with: " + LayerMask.LayerToName(other.gameObject.layer));
                Debug.Log("Force was: " + currentForce.magnitude);

                // If we're colliding with an enemy
                if (other.collider is CapsuleCollider)
                {
                    if (hurtsEnemies)
                    {
                        Enemy e = other.gameObject.GetComponent<Enemy>();
                        if (e != null)
                        {
                            e.TakeDamage(damage);
                            Debug.Log("DAMAGED ENEMY!");
                            Destroy();
                        }
                        flag = true;
                    }
                }
            }
        }
    }

    private void LateUpdate()
    {
        flag = false;
    }
    private IEnumerator ApplyKnockBackForce(Vector3 force, float deceleration)
    {
        IsMoving = true;
        //rb.isKinematic = false;
        currentForce = force;
        float timer = 0.5f;
        //if (gravity != null) gravity.enabled = false;
        while (currentForce.magnitude > 0.3f)
        {
            if (timer > 0)
            {
                timer -= Time.deltaTime;
            }
            else triggerFlag = false;

            rb.MovePosition(rb.position + currentForce * Time.fixedDeltaTime);
            currentForce = Vector3.Lerp(currentForce, Vector3.zero, deceleration * Time.fixedDeltaTime);
            yield return null;
        }

        //if (gravity != null) gravity.enabled = true;
        IsMoving = false;
        //rb.isKinematic = true;

        Save();
    }
    public virtual void GetKnockback(Sword sword)
    {
        Debug.Log("KNOCKBACK THIS SWINGABLE! " + gameObject.name);
        Vector3 knockbackDir;
        Vector3 playerPos = Scripts.PlayerMovement.transform.position;
        playerPos.y = 0;
        Vector3 swingablePos = transform.position;
        swingablePos.y = 0;

        //knockbackDir = (swingablePos - playerPos).normalized;
        knockbackDir = Vector3.Normalize(swingablePos - playerPos);

        Tuple<Vector3, Transform> targetInfo = sword.GetThrowDirection(transform, knockbackForce, 20, 15, breakingLayers, knockbackDir);

        float force = knockbackForce;
        float deceleration = knockbackDeceleration;
        // COMMENT THE IF STATEMENT (JUST THE IF, NOT THE KNOCKBACKDIR VALUE ATTRIBUTION)
        // TO MAKE THE SWINGABLE DIRECTION VARY WITH INPUT INSTEAD OF PLAYER POSITION
        if (targetInfo.Item2 != null)
        {
            //force *= 0.8f;
            deceleration *= 0.6f;
            knockbackDir = Vector3.Normalize(targetInfo.Item1);
        }

        Vector3 knockback = knockbackDir * force;
        StopAllCoroutines();
        StartCoroutine(ApplyKnockBackForce(knockback, deceleration));
    }

    public void Destroy()
    {
        //VFX and/or animations for destroy
        //anim.SetTrigger("Destroy");
        Debug.Log("Destroy!");
        IsDestroyed = true; // if there's an animation, call this at the end of it
        sData.IsDestroyed = true;

        if (isSavable) Save();

        gameObject.GetComponent<MeshRenderer>().enabled = false;
        Collider[] colliders = GetComponentsInChildren<Collider>();
        if (colliders != null)
        {
            foreach (Collider c in colliders) c.enabled = false;
        }
    }

    private IEnumerator DestroyAfterDelay(float delayTime)
    {
        while (delayTime > 0)
        {
            delayTime -= Time.deltaTime;
            yield return null;
        }

        Destroy();
    }

    private void Save()
    {
        sData.IsDestroyed = IsDestroyed;
        sData.PosX = transform.position.x;
        sData.PosY = transform.position.y;
        sData.PosZ = transform.position.z;
        Scripts.SavedObjects.AddObject(Scripts.SavedObjects.SavedSwingables, sData);
    }
}
