﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using MoreMountains.Feedbacks;

public class Impalable : Interactible
{
    [Title("Impale")]
    [InfoBox("BE CAREFUL, CURSED VERSION CAN'T BE IMPALABLE")]
    [SerializeField] private bool isImpalable;
    [SerializeField] private MMFeedbacks impaledEffect;

    [Title("Special Effect")]
    [SerializeField] private MMFeedbacks tFeedback;

    private Transform player;
    private Animator plantAnim;

    public bool IsImpalable => isImpalable;

    protected override void Awake()
    {
        base.Awake();
        player = Scripts.PlayerActions.transform;
        plantAnim = GetComponentInParent<Animator>();
    }

    public override void Activate(bool value)
    {
        base.Activate(value);
        Impale();
        Destroy(gameObject, 3);
    }

    private void Impale()
    {
        plantAnim.SetTrigger("Impale");
        if (impaledEffect)
        {
            Vector3 dir = transform.position - player.position;
            impaledEffect.transform.rotation = Quaternion.LookRotation(dir);
            impaledEffect.PlayFeedbacks();
        }
    }

    public virtual void ThrowSpecialEffect(Transform spawnPos)
    {
        if (tFeedback != null)
        {
            Vector3 dir = spawnPos.position - transform.position;
            tFeedback.transform.position = spawnPos.position;
            tFeedback.transform.rotation = Quaternion.LookRotation(dir);
            tFeedback.PlayFeedbacks();
        }
    }
    public void MortalSpecialEffect()
    {
        if (tFeedback != null)
        {
            tFeedback.transform.position = Scripts.PlayerActions.transform.position;
            tFeedback.transform.rotation = Quaternion.LookRotation(Scripts.PlayerActions.transform.forward);

            tFeedback.PlayFeedbacks();
        }
    }
    /// <summary>
    /// Later this should be some sort of VFX
    /// </summary>
    public void DestroyImpalable() => Destroy(gameObject);
}
