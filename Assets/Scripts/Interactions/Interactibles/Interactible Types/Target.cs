﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Target : Interactible
{

    [Title("Activation Variables")]
    [SerializeField] private LayerMask collisionLayers;
    [ToggleGroup("specificLayersOnly", "Specific Layers Only")]
    [SerializeField] private bool specificLayersOnly;
    [ToggleGroup("specificLayersOnly", "Specific Layers Only")]
    [SerializeField] private string[] specificTags;
    [SerializeField] private bool enemiesOnly;

    [ToggleGroup("needsSpecificEnemy", "Needs Specific Enemy")]
    [SerializeField]
    private bool needsSpecificEnemy;

    [EnumToggleButtons]
    [ToggleGroup("needsSpecificEnemy", "Needs Specific Enemy")]
    [SerializeField]
    private EnemyType enemyType;

    protected override void Awake()
    {
        base.Awake();
        if (anim == null) Debug.LogError("Target " + gameObject + " has no animator!!");
    }

    protected override void Start()
    {
        base.Start();
    }

    public override void Activate(bool value)
    {
        // Check if it's ative and can be deactivated 
        // or if it's deactive and can be activated
        if ((waitForActivableAnim && activableAnimFinished)
            || !waitForActivableAnim)
        {
            if ((IsActive && activatesMultipleTimes && value == false)
            || (!IsActive && value == true))
                anim.SetBool("Active", value);
        }
        base.Activate(value);

    }

    protected void CheckActivationConditions()
    {
        Debug.Log("LETS SEE IF IT ACTIVATES");
        bool flag = false;
        if (!IsActive)
        {
            Activate(true);
            flag = true;
        }
        if (IsActive && activatesMultipleTimes && !flag) Activate(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        //if (!enemiesOnly || specificLayersOnly)
        //{
        if (collisionLayers.Contains(other.gameObject.layer))
        {
            bool flag = false;
            if (specificTags != null)
            {
                foreach (string s in specificTags)
                {
                    if (other.CompareTag(s)) flag = true;
                }
            }
            else flag = true;

            if (flag)
            {
                CheckActivationConditions();
                // If the impalable doesn't have a parent
                if (other.transform.parent == null)
                {
                    Destroy(other.gameObject);
                    return;
                }
                Sword s = other.GetComponentInParent<Sword>();
                if (s != null) s.RemoveImpalableFromSword(other.gameObject, true, 0);
            }
        }
        //}

        if (!specificLayersOnly && other is CapsuleCollider)
        {
            Enemy enemy = other.gameObject.GetComponent<Enemy>();
            if (enemy != null)
            {
                if ((needsSpecificEnemy && enemy.Type == enemyType)
                    || !needsSpecificEnemy) CheckActivationConditions();
                //Destroy(enemy.gameObject);
            }
        }
    }
}
