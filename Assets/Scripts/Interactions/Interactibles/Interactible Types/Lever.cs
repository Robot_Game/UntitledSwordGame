﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lever : Interactible
{

    [SerializeField] private bool reversed;

    protected override void Awake()
    {
        base.Awake();
        if (anim == null) Debug.LogError("Lever " + gameObject + " has no animator!!");
    }
    protected override void Start()
    {
        base.Start();
    }
    public override void Activate(bool value)
    {
        if (reversed) value = !value;

        if ((waitForActivableAnim && activableAnimFinished)
            || !waitForActivableAnim)
        {
            // Check if it's ative and can be deactivated 
            // or if it's deactive and can be activated
            if ((IsActive && activatesMultipleTimes && value == false)
            || (!IsActive && value == true))
            {
                anim.SetBool("Active", value);

                //Activate or deactivate feedback object
                if (feedbackObject != null)
                {
                    Animator anim = feedbackObject.GetComponent<Animator>();
                    if (anim != null) anim.SetBool("Feedback", value);

                    else Debug.LogError("The feedback object "
                        + feedbackObject + " does not have an animator!");
                }
            }

        }
    }

    public void EnableUsingLever() => anim.SetBool("UsingLever", true);
    public void DisableUsingLever() => anim.SetBool("UsingLever", false);

    private void ActivateTarget() => base.Activate(true);
    private void DeactivateTarget() => base.Activate(false);

}
