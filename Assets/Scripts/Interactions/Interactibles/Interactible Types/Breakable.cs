﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using MoreMountains.Feedbacks;

public class Breakable : Interactible
{
    [SerializeField] private bool triggerCollision = true;
    /// <summary>
    /// The layers which break this object in case of a collision
    /// </summary>
    [SerializeField] private LayerMask collisionLayers;
    /// <summary>
    /// The amount of hits this object can take before getting destroyed.
    /// Set hp to 0 if you want the object to break infinite times
    /// or, by other words, if this break animation is purely cosmetic/decorative
    /// (good for a grass cutting effect f.e.).
    /// </summary>
    [SerializeField] private int hp = 1;

    [SerializeField] private bool hasVFXFeedback;
    [HideIf("@!hasVFXFeedback")]
    [SerializeField] private MMFeedbacks hitFeedback;
    [HideIf("@!hasVFXFeedback")]
    [SerializeField] private MMFeedbacks breakFeedback;

    private bool collisionFlag;

    private BreakableSaveData bData;
    public int Hp => hp;

    protected override void Start()
    {
        ManageStartState();

        if (feedbackObject != null)
        {
            Interactible i = feedbackObject.GetComponent<Interactible>();
            if (i != null)
            {
                SphereCollider iTrigger = i.GetComponent<SphereCollider>();
                if (iTrigger != null)
                {
                    i.GetComponent<Animator>().enabled = false;
                    iTrigger.enabled = false;
                }
            }
        }
    }
    protected override void ManageStartState()
    {
        // If this breakable is mearly cosmetic 
        // (the breaking animation has no effect on the gameplay)
        // then we stop here
        if (hp == 0)
        {
            return;
        }
        bData = new BreakableSaveData(this);

        if (Scripts.SavedObjects.IsObjectSaved(
            Scripts.SavedObjects.SavedBreakables, ref bData))
        {
            int hitCount = hp - bData.Hp;
            if (bData.Hp == 0)
            {
                Activate(true);
            }
            else
            {
                for (int i = 0; i < hitCount; i++)
                    hp--;
                if (anim != null)
                {
                    anim.SetInteger("Hp", hp + 1);
                    anim.SetTrigger("Hit");
                }
            }
        }
    }

    private void LateUpdate()
    {
        collisionFlag = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!triggerCollision) return;
        // Check if collisionLayers contains the layer of the collision's object
        if (collisionLayers.Contains(other.gameObject.layer) && !collisionFlag)
        {
            BoxCollider col = GetComponent<BoxCollider>();
            if (col != null && !col.isTrigger) return;
            if (other.gameObject.layer == LayerMask.NameToLayer("Swingable")
            || other.gameObject.layer == LayerMask.NameToLayer("Impalable")
            || other.gameObject.layer == LayerMask.NameToLayer("Projectiles/FriendlyProjectiles"))
            {
                other.GetComponent<Swingable>()?.Destroy();
                other.GetComponent<Impalable>()?.DestroyImpalable();
            }
            if (hp == 0 && anim != null) anim.SetTrigger("PlayBreakEffect");
            else Hit();
            collisionFlag = true;
        }

        if (other.gameObject.layer == LayerMask.NameToLayer("Instakill"))
        {
            StartCoroutine(DestroyAfterDelay(5));
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (triggerCollision) return;
        // Check if collisionLayers contains the layer of the collision's object
        if (collisionLayers.Contains(other.gameObject.layer) && !collisionFlag)
        {
            BoxCollider col = GetComponent<BoxCollider>();
            if (col != null && !col.isTrigger) return;
            Debug.Log("We hit a breakable!");
            if (other.gameObject.layer == LayerMask.NameToLayer("Swingable")
            || other.gameObject.layer == LayerMask.NameToLayer("Impalable")
            || other.gameObject.layer == LayerMask.NameToLayer("Projectiles/FriendlyProjectiles"))
            {
                other.gameObject.GetComponent<Swingable>()?.Destroy();
                other.gameObject.GetComponent<Impalable>()?.DestroyImpalable();
            }
            if (hp == 0 && anim != null) anim.SetTrigger("PlayBreakEffect");
            else Hit();
            collisionFlag = true;
        }
    }

    public override void Activate(bool value)
    {
        if (!IsActive && value == true)
        {
            if (hasVFXFeedback && breakFeedback != null) breakFeedback.PlayFeedbacks();
            if (anim == null) gameObject.SetActive(false);

            anim.SetBool("Active", value);

            // If there is a feedback object
            if (feedbackObject != null)
            {
                Interactible i = feedbackObject.GetComponent<Interactible>();
                if (i != null)
                {
                    SphereCollider iTrigger = i.GetComponent<SphereCollider>();
                    if (iTrigger != null)
                    {
                        iTrigger.enabled = true;
                        i.GetComponent<Animator>().enabled = true;
                    }
                }

                else
                {
                    Animator anim = feedbackObject.GetComponent<Animator>();
                    if (anim != null) anim.SetBool("Feedback", value);
                }
            }
        }
    }
    public void Hit(bool save = true)
    {
        if (hp == 1)
        {
            Debug.Log("Activate breakable!");
            Activate(true);
        }
        else if (anim != null)
        {
            anim.SetInteger("Hp", hp);
            anim.SetTrigger("Hit");
        }

        if (hasVFXFeedback && hitFeedback != null) hitFeedback.PlayFeedbacks();

        Debug.Log("Hit! Hp is now: " + (hp - 1));
        hp--;

        if (isSavable) Save();
    }

    private IEnumerator DestroyAfterDelay(float delayTime)
    {
        while (delayTime > 0)
        {
            delayTime -= Time.deltaTime;
            yield return null;
        }

        Activate(true);
    }
    private void Save()
    {
        bData.Hp = hp;
        Scripts.SavedObjects.AddObject(Scripts.SavedObjects.SavedBreakables, bData);
    }
    private void ActivateTarget() => base.Activate(true);
}
