﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class GameEvents : MonoBehaviour
{
    public static Action SaveInitiated;

    public static void OnSaveInitiated()
    {
        SaveInitiated?.Invoke();
    }
}
