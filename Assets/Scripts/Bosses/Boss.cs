﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Boss : MonoBehaviour
{
    [Title("General")]
    /// <summary>
    /// The ability awarded to the player after defeating this boss.
    /// </summary>
    [SerializeField] private StoreButton prizeAbility;
    /// <summary>
    /// The amount of phases this boss has.
    /// </summary>
    [SerializeField] private int phases;

    [SerializeField] private Animator bossAnimator;
    [SerializeField] private Animator arenaAnimator;


    [Title("Basic Attacks")]
    /// <summary>
    /// The amount of different basic attack animations 
    /// (1 if there is only one basic attack animation).
    /// </summary>
    [SerializeField] private int basicAttackVariations = 1;
    /// <summary>
    /// The amount of basic attacks executed before performing 
    /// a vulnerable attack (each index for each phase).
    /// </summary>
    [SerializeField] private float[] basicAttackCount;
    /// <summary>
    /// The speed of the basic attack animation (each index for each phase).
    /// </summary>
    [SerializeField] private float[] basicAttackSpeed;
    /// <summary>
    /// The cooldown between each basic attack (each index for each phase).
    /// </summary>
    [SerializeField] private float[] basicAttackCooldown;

    [Title("VulnerableAttacks")]
    [SerializeField] private int vulnerableAttackVariations = 1;
    /// <summary>
    /// The speed of the basic attack animation (each index for each phase).
    /// </summary>
    [SerializeField] private float[] vulnerableAttackSpeed;
    /// <summary>
    /// The time the boss remains vulnerable after failing 
    /// an attack (each index for each phase).
    /// </summary>
    [SerializeField] private float[] vulnerableTime;



    [Title("Special Attacks")]
    /// <summary>
    /// The amount of different special attack animations.
    /// </summary>
    [SerializeField] private int specialAttackVariations;
    /// <summary>
    /// The speed of the special attack animation (each index for each phase).
    /// </summary>
    [SerializeField] private float[] specialAttackSpeed;
    /// <summary>
    /// The time the boss remains vulnerable after failing 
    /// a basic attack (each index for each phase).
    /// </summary>
    [SerializeField] private float[] specialAttackVulnerableTime;


    private int currentPhase = 0;
    private int currentBasicAttackAnim = 1;
    private int currentBasicAttackCount = 0;

    private void Awake()
    {
    }

    private void Init()
    {
        bossAnimator.SetInteger("CurrentBasicAttackAnimation", currentBasicAttackAnim);
    }

    public void AwakeBoss() => bossAnimator.SetTrigger("Awake");
    public void LockArena() => arenaAnimator.SetTrigger("Lock");

    private void SetRandomBasicAttack()
    {
        if (basicAttackVariations == 1) return;
        int oldCurrentBasicAttack = currentBasicAttackAnim;
        while (currentBasicAttackAnim == oldCurrentBasicAttack)
        {
            currentBasicAttackAnim = Random.Range(1, basicAttackVariations + 1);
        }

        bossAnimator.SetInteger("CurrentBasicAttackAnimation", currentBasicAttackAnim);

    }

    //Attacks

    private void Attack(string attackType = "Random")
    {
        if(attackType == "Random")
        {
            attackType = Utility.GetRandomString("Basic", "Vulnerable", "Special");
        }

        switch (attackType)
        {
            case "Basic":
                BasicAttack();
                break;
            case "Vulnerable":
                break;
            case "Special":
                break;
        }
    }

    public void BasicAttack()
    {
        SetRandomBasicAttack();

        bossAnimator.SetTrigger("BasicAttack"); //at the end of the anim, call the cooldown method
    }

    public void BeginCooldown() => StartCoroutine(Cooldown());

    private IEnumerator Cooldown()
    {
        // If the boss has performed the max basic attack count set for this phase
        if (currentBasicAttackCount == basicAttackCount[currentPhase]) Attack("Vulnerable");
        else
        {
            float timer = GetRandomCooldown(basicAttackCooldown[currentPhase]);

            while (timer > 0)
            {
                timer -= Time.fixedDeltaTime;
                yield return null;
            }

            Attack("Basic");
        }
    }

    private float GetRandomCooldown(float currentCooldown)
        => Random.Range(currentCooldown, currentCooldown + (currentCooldown / 2));
}
