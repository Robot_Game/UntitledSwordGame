﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;

public class StoreButton : MonoBehaviour
{
    // The ability associated with this button
    [SerializeField, Required] private Ability ability;
    // The buttons that get unlocked when this button is upgraded
    [SerializeField] private bool startUnlockedForTesting;
    // 0 -> Unlocked 1 -> Disabled
    [SerializeField] private int startState;
    [SerializeField] private bool hasLevels;
    [SerializeField] private Sprite lockedSprite;
    [SerializeField] private Sprite unlockedSprite;
    [SerializeField] private Sprite maxLevelSprite;
    [HideIf("@hasLevels == false || startState == 1")]
    [SerializeField] private Sprite[] levelSprite;
    [SerializeField] private StoreButton[] childButtons;
    [HideIf("@hasLevels == false || startState == 1 || !(childButtons.Length > 0)")]
    [SerializeField] private bool unlockChildsOnlyAfterMaxLevelReached;

    private Button button;
    private Image abilityImage;
    private TMP_Text nameText;
    private GameObject upgradeCost;
    private TMP_Text cost;
    private string abilityName;

    private void Awake()
    {
        button = GetComponent<Button>();
        abilityImage = GetComponent<Image>();
        nameText = GetComponentInChildren<TMP_Text>();
        upgradeCost = transform.Find("Cost").gameObject;
        cost = upgradeCost.GetComponentInChildren<TMP_Text>();
        upgradeCost.SetActive(false);

        abilityName = nameText.text;

    }

    private void Start()
    {
        //TESTING ONLY
        if (startUnlockedForTesting) UnlockButton();
        /*else
        {

            if (lockedSprite != null) abilityImage.overrideSprite = lockedSprite;
            else Debug.LogError("THERE IS NO SPRITE IN THIS BUTTON!");
        }*/
    }
    public void UnlockButton()
    {
        // If button should only unlock
        if (startState == 0)
        {
            if (unlockedSprite != null) abilityImage.overrideSprite = unlockedSprite;

            else Debug.LogError("THERE IS NO SPRITE IN THIS BUTTON!");
            button.interactable = true;
            UpgradeCost();
        }
        // If ability is "bought" when unlocked
        else
        {
            UpgradeAbility();
        }

    }

    public void UpgradeAbility()
    {
        ability.Upgrade();
        UpgradeCost();

        bool maxLevelReached = false;

        if (hasLevels)
        {
            if (ability.Level < ability.LevelCount)
            {
                if (levelSprite.Length > ability.Level - 1)
                    if (levelSprite[ability.Level - 1] != null) abilityImage.overrideSprite = levelSprite[ability.Level - 1];
            }
            else
            {
                if (maxLevelSprite != null) abilityImage.overrideSprite = maxLevelSprite;
                else Debug.LogError("THERE IS NO SPRITE IN THIS BUTTON!");
                button.interactable = false;
                maxLevelReached = true;
            }

            if (nameText != null) nameText.text = abilityName + " " + ability.Level;
        }
        else
        {
            if (maxLevelSprite != null) abilityImage.overrideSprite = maxLevelSprite;
            else Debug.LogError("THERE IS NO SPRITE IN THIS BUTTON!");
            button.interactable = false;
            maxLevelReached = true;
        }

        if (childButtons.Length > 0)
        {
            if ((unlockChildsOnlyAfterMaxLevelReached && maxLevelReached)
                || !unlockChildsOnlyAfterMaxLevelReached)
            {
                foreach (StoreButton b in childButtons)
                {
                    b.UnlockButton();
                }
            }
        }

        if (maxLevelReached) UpgradeCost(false);

        // SUBTRACT WHATEVER HEARTS IT COST
        // currency -= ability.CurrentCost

    }

    private void UpgradeCost(bool show = true)
    {
        if (!show)
        {
            upgradeCost.SetActive(false);
        }
        else if (ability.CurrentCost != 0)
        {
            if (!upgradeCost.activeSelf) upgradeCost.SetActive(true);

            cost.text = "" + ability.CurrentCost;
        }
    }
}
