﻿using System.Collections;
using UnityEngine;
using Sirenix.OdinInspector;

public class Drop : Item
{
    [Title("Drop type")]
    [SerializeField, EnumToggleButtons]
    private DropType type;
    public DropType Type => type;

}
