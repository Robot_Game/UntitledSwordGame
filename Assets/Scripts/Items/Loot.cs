﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loot : Item
{
    [SerializeField] private int amount;
    public int Amount => amount;
}
