﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    [Title("Follow player")]
    [SerializeField]
    [ToggleGroup("followPlayer", "Follow Player")]
    private bool followPlayer;
    [SerializeField]
    [ToggleGroup("followPlayer", "Follow Player")]
    private float rangeUntilFollow;
    [SerializeField]
    [ToggleGroup("followPlayer", "Follow Player")]
    private bool stopFollowWithDistance;
    [SerializeField]
    [ToggleGroup("followPlayer", "Follow Player")]
    private float timeUntilFollow;
    [SerializeField]
    [ToggleGroup("followPlayer", "Follow Player")]
    private float followSpeed;

    private bool timeComplete;
    private bool isInRange;

    private bool flag;
    private bool distanceFlag = false;

    private GameObject target;

    private void Awake()
    {
        target = Scripts.PlayerMovement.gameObject;
        //if (timeUntilFollow > 0) GetComponent<Collider>().enabled = false;
    }

    private void Start()
    {
        if (followPlayer) StartCoroutine(FollowPlayerTimer());
    }

    private void Update()
    {
        if (rangeUntilFollow != 0 && !distanceFlag)
            isInRange = Utility.IsInRange(gameObject, target, rangeUntilFollow);
        else if (!isInRange) isInRange = true;

        if (timeComplete && isInRange)
        {
            if (!stopFollowWithDistance) distanceFlag = true;
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, followSpeed * Time.deltaTime);
        }
    }

    // After animation ends
    private IEnumerator FollowPlayerTimer()
    {

        float time = 0;

        while (time < timeUntilFollow)
        {
            time += Time.deltaTime;
            yield return null;
        }
        //GetComponent<Collider>().enabled = true;
        timeComplete = true;
    }

    private void Vanish()
    {
        Destroy(gameObject);
    }

}
