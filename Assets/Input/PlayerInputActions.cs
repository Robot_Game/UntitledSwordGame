// GENERATED AUTOMATICALLY FROM 'Assets/Input/PlayerInputActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerInputActions : IInputActionCollection, IDisposable
{
    private InputActionAsset asset;
    public @PlayerInputActions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerInputActions"",
    ""maps"": [
        {
            ""name"": ""Gameplay"",
            ""id"": ""f9f70f0d-3e1b-4312-a363-18f08002bf53"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""PassThrough"",
                    ""id"": ""70765810-9f0e-4e63-a80b-ac7688a5f37d"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SquareAttack"",
                    ""type"": ""Button"",
                    ""id"": ""bd9fa6ec-a587-4d09-829c-4e741a173a20"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Tap""
                },
                {
                    ""name"": ""ChargedSquareAttack"",
                    ""type"": ""Button"",
                    ""id"": ""6771028e-80dc-4362-8088-17e766f971f5"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Hold(duration=0.2)""
                },
                {
                    ""name"": ""StopChargedSquareAttack"",
                    ""type"": ""Button"",
                    ""id"": ""4163f325-ccba-4018-ac38-564de5ea47fa"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""SlowTap(duration=0.2)""
                },
                {
                    ""name"": ""CircleAttack"",
                    ""type"": ""Button"",
                    ""id"": ""ca5973e1-ae98-4b21-a711-6427dbb35bce"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Tap""
                },
                {
                    ""name"": ""ChargedCircleAttack"",
                    ""type"": ""Button"",
                    ""id"": ""2b99cb30-6042-4a38-9329-61984b3bdcd7"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""SlowTap""
                },
                {
                    ""name"": ""TriangleAttack"",
                    ""type"": ""Button"",
                    ""id"": ""82260e99-de9a-4d67-bf37-9fcfae88825f"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Tap""
                },
                {
                    ""name"": ""ChargedTriangleAttack"",
                    ""type"": ""Button"",
                    ""id"": ""b5ffcd85-606d-4485-b401-68a07d4bf941"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""SlowTap""
                },
                {
                    ""name"": ""R1Attack"",
                    ""type"": ""Button"",
                    ""id"": ""e154a3b0-58bc-4898-8be7-59189dccbc38"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Tap""
                },
                {
                    ""name"": ""ChargedR1Attack"",
                    ""type"": ""Button"",
                    ""id"": ""34eb2eb5-ddc2-4c2c-a692-4df483cbf5ba"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Hold(duration=0.2)""
                },
                {
                    ""name"": ""StopChargedR1Attack"",
                    ""type"": ""Button"",
                    ""id"": ""0679e75d-d638-4723-b30d-59fe70066d50"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""SlowTap(duration=0.2)""
                },
                {
                    ""name"": ""Charging"",
                    ""type"": ""Button"",
                    ""id"": ""38ef104f-bb24-4633-a548-c31717fb4c33"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Hold(duration=0.3)""
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""05394ae4-d257-49bf-b446-4a2f8c6386fe"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Tap""
                },
                {
                    ""name"": ""RestartScene"",
                    ""type"": ""Button"",
                    ""id"": ""72d10ecc-8cb2-4ed5-972c-94992ecb92cb"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Tap""
                },
                {
                    ""name"": ""ActivateInteractible"",
                    ""type"": ""Button"",
                    ""id"": ""509455c9-f694-499a-b1d2-e905e7d9a94a"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Tap""
                },
                {
                    ""name"": ""DeactivateInteractible"",
                    ""type"": ""Button"",
                    ""id"": ""d22d06a7-2c22-431e-a198-08e40268d3b3"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Tap""
                },
                {
                    ""name"": ""Interact"",
                    ""type"": ""Button"",
                    ""id"": ""c16cf45a-3fda-4210-8173-d2deedee172f"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Tap""
                },
                {
                    ""name"": ""HoldInteract"",
                    ""type"": ""Button"",
                    ""id"": ""480610a9-8602-4886-8ad9-68a3644e0216"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Hold(duration=0.3)""
                },
                {
                    ""name"": ""ChargedL1Attack"",
                    ""type"": ""Button"",
                    ""id"": ""0fb156cd-331a-471d-a579-116488fe572a"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Hold(duration=0.1)""
                },
                {
                    ""name"": ""ConfirmTargetSelection"",
                    ""type"": ""Button"",
                    ""id"": ""e1e69de1-b769-4fb4-be31-4b8180ca5721"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""CancelTargetSelection"",
                    ""type"": ""Button"",
                    ""id"": ""5eaed096-4352-4732-bcd1-50b307c1a300"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Tap""
                },
                {
                    ""name"": ""Pause"",
                    ""type"": ""Button"",
                    ""id"": ""048485a6-f639-48ad-b335-2d435de0f0f1"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Tap""
                },
                {
                    ""name"": ""Cancel"",
                    ""type"": ""Button"",
                    ""id"": ""6a81dd1a-5d12-44b0-8832-053345e6980f"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Tap""
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""WASD"",
                    ""id"": ""8604bfaa-901f-4abd-be7f-cf2b332a00e6"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""a16cb547-997f-44bd-8de8-234095ad48c2"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""bf49a872-13ad-456b-bbb3-bc7b035d8e3a"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""4b1131f2-03bc-44d7-b83b-fe221f254667"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""435942a9-6a1f-4aaf-a508-aa68b37d77f0"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""9440b64a-6ace-4f4f-8f0b-66a1e9388229"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""42831d2c-e513-4a37-9a43-d90f848a56fb"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""SquareAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1f365b40-e40e-49de-b45f-143873c2f456"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""SquareAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""60f9e82a-2292-44d2-82a0-67549ba8d729"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""TriangleAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b3e86c63-dbbd-4ca9-b36e-bc4f626e9b2f"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""TriangleAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""03908169-370f-4476-b860-9163cc0e868f"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""49ead85d-277d-47b8-9f21-41f11f658387"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""bacbebb6-16d1-4346-beec-504e201f18a4"",
                    ""path"": ""<Mouse>/middleButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""ChargedTriangleAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""da52065a-6a98-4633-b46b-e68825936f63"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""ChargedTriangleAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""573068e3-c7eb-4490-bacc-2197e1d68bc1"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Charging"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2f55841d-494d-4a59-8849-96d9f221bb71"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Charging"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8383b421-9bb8-45e5-bfd4-29f52b384068"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Charging"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2edff960-df00-40ea-94a1-4a6e83697fec"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""Charging"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e54a60ec-a3bc-4d1a-a7b1-5904fcdcd59e"",
                    ""path"": ""<Mouse>/middleButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""Charging"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""84940742-5daa-47b7-afb2-e7f93cc26de9"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Charging"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""dacb65fb-7c2a-49e7-a3ff-174ca18e6b19"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""RestartScene"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a7cb4eff-c9ed-49c9-8467-732b21aff2e3"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""RestartScene"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9000ef8b-051f-48a5-be87-4f4ef0bbee5f"",
                    ""path"": ""<Keyboard>/#(D)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""ActivateInteractible"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5e4c66e6-5a28-42e3-bf14-a9601770f6af"",
                    ""path"": ""<Keyboard>/#(A)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""DeactivateInteractible"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a25630cc-038b-4375-bca1-9d2c2a771c64"",
                    ""path"": ""<Keyboard>/#(E)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""283e0cb4-2e3d-4d71-b56d-d0b2d6d68311"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""af9dfaa9-5deb-4232-8ff9-893a9035e8e6"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""ChargedL1Attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fde0ade2-2097-44e7-8317-5b2e9c78bbeb"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": ""Tap"",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""ConfirmTargetSelection"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d73d901a-c38a-44f4-8528-a45546a62933"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": ""Tap"",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""ConfirmTargetSelection"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b5bc2409-c87d-4804-aede-2d639780c328"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": ""SlowTap(duration=0.2)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ConfirmTargetSelection"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e6a8cd17-25ba-4b4f-9e9b-c28b80ca4853"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""CancelTargetSelection"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""84cd40ca-68ae-4943-81b3-5fc88f154ef0"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""CancelTargetSelection"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""845dd77f-717f-4545-b5b0-c1cb192acfb1"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""ChargedCircleAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1e454308-0a81-4228-b4cf-c075b6eb122b"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""ChargedCircleAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""49d31d17-9aaf-4b5b-8902-38266dedacb0"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""R1Attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d76429b3-79ea-477d-91f4-37b0960b43a3"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""R1Attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""92cc6b6c-0936-4d79-b1b8-9184fb27b694"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""ChargedR1Attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""473d631a-03f7-43b3-a597-8cebaaecbd8a"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""ChargedR1Attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e0d4483a-1022-4a17-b6a6-92361c110012"",
                    ""path"": ""<Keyboard>/#(E)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""HoldInteract"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b2d033fe-a09f-46a7-a90d-96d8ef470404"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""HoldInteract"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""dde5b1a3-d2f9-4f62-b162-7276b7c65c13"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""841b8085-3ceb-4471-b446-39910d84336b"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1aeb485f-4033-425d-87a3-1d1450bf5264"",
                    ""path"": ""<Keyboard>/p"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ef19bab0-7403-4c3c-968b-9c45404aa65e"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""Cancel"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4cf518ab-3f3b-4565-aff6-9d6b9687e9ae"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Cancel"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3a6089c1-0a69-4713-8284-62b69156cab7"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""CircleAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a35927bf-d482-45b5-beba-f559027b08f2"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""CircleAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""35042509-8653-4e90-b947-e3bd9663e62f"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""ChargedSquareAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""96be90fa-bb6d-4a88-9798-ba306d14af58"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""ChargedSquareAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f63df38b-aa40-4fc7-b88a-40828822dc7e"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""StopChargedR1Attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fa01e774-0eeb-4864-a09f-c81df53fe1d8"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""StopChargedR1Attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""df4e72cd-9fc2-45b3-89d3-820af11cb50e"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""StopChargedSquareAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""aca1d41d-ba08-4232-a45f-e41b9381b095"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""StopChargedSquareAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Keyboard&Mouse"",
            ""bindingGroup"": ""Keyboard&Mouse"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Gamepad"",
            ""bindingGroup"": ""Gamepad"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Gameplay
        m_Gameplay = asset.FindActionMap("Gameplay", throwIfNotFound: true);
        m_Gameplay_Move = m_Gameplay.FindAction("Move", throwIfNotFound: true);
        m_Gameplay_SquareAttack = m_Gameplay.FindAction("SquareAttack", throwIfNotFound: true);
        m_Gameplay_ChargedSquareAttack = m_Gameplay.FindAction("ChargedSquareAttack", throwIfNotFound: true);
        m_Gameplay_StopChargedSquareAttack = m_Gameplay.FindAction("StopChargedSquareAttack", throwIfNotFound: true);
        m_Gameplay_CircleAttack = m_Gameplay.FindAction("CircleAttack", throwIfNotFound: true);
        m_Gameplay_ChargedCircleAttack = m_Gameplay.FindAction("ChargedCircleAttack", throwIfNotFound: true);
        m_Gameplay_TriangleAttack = m_Gameplay.FindAction("TriangleAttack", throwIfNotFound: true);
        m_Gameplay_ChargedTriangleAttack = m_Gameplay.FindAction("ChargedTriangleAttack", throwIfNotFound: true);
        m_Gameplay_R1Attack = m_Gameplay.FindAction("R1Attack", throwIfNotFound: true);
        m_Gameplay_ChargedR1Attack = m_Gameplay.FindAction("ChargedR1Attack", throwIfNotFound: true);
        m_Gameplay_StopChargedR1Attack = m_Gameplay.FindAction("StopChargedR1Attack", throwIfNotFound: true);
        m_Gameplay_Charging = m_Gameplay.FindAction("Charging", throwIfNotFound: true);
        m_Gameplay_Jump = m_Gameplay.FindAction("Jump", throwIfNotFound: true);
        m_Gameplay_RestartScene = m_Gameplay.FindAction("RestartScene", throwIfNotFound: true);
        m_Gameplay_ActivateInteractible = m_Gameplay.FindAction("ActivateInteractible", throwIfNotFound: true);
        m_Gameplay_DeactivateInteractible = m_Gameplay.FindAction("DeactivateInteractible", throwIfNotFound: true);
        m_Gameplay_Interact = m_Gameplay.FindAction("Interact", throwIfNotFound: true);
        m_Gameplay_HoldInteract = m_Gameplay.FindAction("HoldInteract", throwIfNotFound: true);
        m_Gameplay_ChargedL1Attack = m_Gameplay.FindAction("ChargedL1Attack", throwIfNotFound: true);
        m_Gameplay_ConfirmTargetSelection = m_Gameplay.FindAction("ConfirmTargetSelection", throwIfNotFound: true);
        m_Gameplay_CancelTargetSelection = m_Gameplay.FindAction("CancelTargetSelection", throwIfNotFound: true);
        m_Gameplay_Pause = m_Gameplay.FindAction("Pause", throwIfNotFound: true);
        m_Gameplay_Cancel = m_Gameplay.FindAction("Cancel", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Gameplay
    private readonly InputActionMap m_Gameplay;
    private IGameplayActions m_GameplayActionsCallbackInterface;
    private readonly InputAction m_Gameplay_Move;
    private readonly InputAction m_Gameplay_SquareAttack;
    private readonly InputAction m_Gameplay_ChargedSquareAttack;
    private readonly InputAction m_Gameplay_StopChargedSquareAttack;
    private readonly InputAction m_Gameplay_CircleAttack;
    private readonly InputAction m_Gameplay_ChargedCircleAttack;
    private readonly InputAction m_Gameplay_TriangleAttack;
    private readonly InputAction m_Gameplay_ChargedTriangleAttack;
    private readonly InputAction m_Gameplay_R1Attack;
    private readonly InputAction m_Gameplay_ChargedR1Attack;
    private readonly InputAction m_Gameplay_StopChargedR1Attack;
    private readonly InputAction m_Gameplay_Charging;
    private readonly InputAction m_Gameplay_Jump;
    private readonly InputAction m_Gameplay_RestartScene;
    private readonly InputAction m_Gameplay_ActivateInteractible;
    private readonly InputAction m_Gameplay_DeactivateInteractible;
    private readonly InputAction m_Gameplay_Interact;
    private readonly InputAction m_Gameplay_HoldInteract;
    private readonly InputAction m_Gameplay_ChargedL1Attack;
    private readonly InputAction m_Gameplay_ConfirmTargetSelection;
    private readonly InputAction m_Gameplay_CancelTargetSelection;
    private readonly InputAction m_Gameplay_Pause;
    private readonly InputAction m_Gameplay_Cancel;
    public struct GameplayActions
    {
        private @PlayerInputActions m_Wrapper;
        public GameplayActions(@PlayerInputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_Gameplay_Move;
        public InputAction @SquareAttack => m_Wrapper.m_Gameplay_SquareAttack;
        public InputAction @ChargedSquareAttack => m_Wrapper.m_Gameplay_ChargedSquareAttack;
        public InputAction @StopChargedSquareAttack => m_Wrapper.m_Gameplay_StopChargedSquareAttack;
        public InputAction @CircleAttack => m_Wrapper.m_Gameplay_CircleAttack;
        public InputAction @ChargedCircleAttack => m_Wrapper.m_Gameplay_ChargedCircleAttack;
        public InputAction @TriangleAttack => m_Wrapper.m_Gameplay_TriangleAttack;
        public InputAction @ChargedTriangleAttack => m_Wrapper.m_Gameplay_ChargedTriangleAttack;
        public InputAction @R1Attack => m_Wrapper.m_Gameplay_R1Attack;
        public InputAction @ChargedR1Attack => m_Wrapper.m_Gameplay_ChargedR1Attack;
        public InputAction @StopChargedR1Attack => m_Wrapper.m_Gameplay_StopChargedR1Attack;
        public InputAction @Charging => m_Wrapper.m_Gameplay_Charging;
        public InputAction @Jump => m_Wrapper.m_Gameplay_Jump;
        public InputAction @RestartScene => m_Wrapper.m_Gameplay_RestartScene;
        public InputAction @ActivateInteractible => m_Wrapper.m_Gameplay_ActivateInteractible;
        public InputAction @DeactivateInteractible => m_Wrapper.m_Gameplay_DeactivateInteractible;
        public InputAction @Interact => m_Wrapper.m_Gameplay_Interact;
        public InputAction @HoldInteract => m_Wrapper.m_Gameplay_HoldInteract;
        public InputAction @ChargedL1Attack => m_Wrapper.m_Gameplay_ChargedL1Attack;
        public InputAction @ConfirmTargetSelection => m_Wrapper.m_Gameplay_ConfirmTargetSelection;
        public InputAction @CancelTargetSelection => m_Wrapper.m_Gameplay_CancelTargetSelection;
        public InputAction @Pause => m_Wrapper.m_Gameplay_Pause;
        public InputAction @Cancel => m_Wrapper.m_Gameplay_Cancel;
        public InputActionMap Get() { return m_Wrapper.m_Gameplay; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(GameplayActions set) { return set.Get(); }
        public void SetCallbacks(IGameplayActions instance)
        {
            if (m_Wrapper.m_GameplayActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnMove;
                @SquareAttack.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnSquareAttack;
                @SquareAttack.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnSquareAttack;
                @SquareAttack.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnSquareAttack;
                @ChargedSquareAttack.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChargedSquareAttack;
                @ChargedSquareAttack.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChargedSquareAttack;
                @ChargedSquareAttack.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChargedSquareAttack;
                @StopChargedSquareAttack.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnStopChargedSquareAttack;
                @StopChargedSquareAttack.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnStopChargedSquareAttack;
                @StopChargedSquareAttack.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnStopChargedSquareAttack;
                @CircleAttack.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnCircleAttack;
                @CircleAttack.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnCircleAttack;
                @CircleAttack.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnCircleAttack;
                @ChargedCircleAttack.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChargedCircleAttack;
                @ChargedCircleAttack.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChargedCircleAttack;
                @ChargedCircleAttack.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChargedCircleAttack;
                @TriangleAttack.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnTriangleAttack;
                @TriangleAttack.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnTriangleAttack;
                @TriangleAttack.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnTriangleAttack;
                @ChargedTriangleAttack.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChargedTriangleAttack;
                @ChargedTriangleAttack.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChargedTriangleAttack;
                @ChargedTriangleAttack.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChargedTriangleAttack;
                @R1Attack.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnR1Attack;
                @R1Attack.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnR1Attack;
                @R1Attack.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnR1Attack;
                @ChargedR1Attack.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChargedR1Attack;
                @ChargedR1Attack.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChargedR1Attack;
                @ChargedR1Attack.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChargedR1Attack;
                @StopChargedR1Attack.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnStopChargedR1Attack;
                @StopChargedR1Attack.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnStopChargedR1Attack;
                @StopChargedR1Attack.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnStopChargedR1Attack;
                @Charging.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnCharging;
                @Charging.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnCharging;
                @Charging.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnCharging;
                @Jump.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnJump;
                @RestartScene.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnRestartScene;
                @RestartScene.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnRestartScene;
                @RestartScene.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnRestartScene;
                @ActivateInteractible.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnActivateInteractible;
                @ActivateInteractible.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnActivateInteractible;
                @ActivateInteractible.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnActivateInteractible;
                @DeactivateInteractible.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnDeactivateInteractible;
                @DeactivateInteractible.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnDeactivateInteractible;
                @DeactivateInteractible.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnDeactivateInteractible;
                @Interact.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnInteract;
                @Interact.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnInteract;
                @Interact.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnInteract;
                @HoldInteract.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnHoldInteract;
                @HoldInteract.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnHoldInteract;
                @HoldInteract.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnHoldInteract;
                @ChargedL1Attack.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChargedL1Attack;
                @ChargedL1Attack.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChargedL1Attack;
                @ChargedL1Attack.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnChargedL1Attack;
                @ConfirmTargetSelection.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnConfirmTargetSelection;
                @ConfirmTargetSelection.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnConfirmTargetSelection;
                @ConfirmTargetSelection.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnConfirmTargetSelection;
                @CancelTargetSelection.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnCancelTargetSelection;
                @CancelTargetSelection.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnCancelTargetSelection;
                @CancelTargetSelection.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnCancelTargetSelection;
                @Pause.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnPause;
                @Pause.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnPause;
                @Pause.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnPause;
                @Cancel.started -= m_Wrapper.m_GameplayActionsCallbackInterface.OnCancel;
                @Cancel.performed -= m_Wrapper.m_GameplayActionsCallbackInterface.OnCancel;
                @Cancel.canceled -= m_Wrapper.m_GameplayActionsCallbackInterface.OnCancel;
            }
            m_Wrapper.m_GameplayActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @SquareAttack.started += instance.OnSquareAttack;
                @SquareAttack.performed += instance.OnSquareAttack;
                @SquareAttack.canceled += instance.OnSquareAttack;
                @ChargedSquareAttack.started += instance.OnChargedSquareAttack;
                @ChargedSquareAttack.performed += instance.OnChargedSquareAttack;
                @ChargedSquareAttack.canceled += instance.OnChargedSquareAttack;
                @StopChargedSquareAttack.started += instance.OnStopChargedSquareAttack;
                @StopChargedSquareAttack.performed += instance.OnStopChargedSquareAttack;
                @StopChargedSquareAttack.canceled += instance.OnStopChargedSquareAttack;
                @CircleAttack.started += instance.OnCircleAttack;
                @CircleAttack.performed += instance.OnCircleAttack;
                @CircleAttack.canceled += instance.OnCircleAttack;
                @ChargedCircleAttack.started += instance.OnChargedCircleAttack;
                @ChargedCircleAttack.performed += instance.OnChargedCircleAttack;
                @ChargedCircleAttack.canceled += instance.OnChargedCircleAttack;
                @TriangleAttack.started += instance.OnTriangleAttack;
                @TriangleAttack.performed += instance.OnTriangleAttack;
                @TriangleAttack.canceled += instance.OnTriangleAttack;
                @ChargedTriangleAttack.started += instance.OnChargedTriangleAttack;
                @ChargedTriangleAttack.performed += instance.OnChargedTriangleAttack;
                @ChargedTriangleAttack.canceled += instance.OnChargedTriangleAttack;
                @R1Attack.started += instance.OnR1Attack;
                @R1Attack.performed += instance.OnR1Attack;
                @R1Attack.canceled += instance.OnR1Attack;
                @ChargedR1Attack.started += instance.OnChargedR1Attack;
                @ChargedR1Attack.performed += instance.OnChargedR1Attack;
                @ChargedR1Attack.canceled += instance.OnChargedR1Attack;
                @StopChargedR1Attack.started += instance.OnStopChargedR1Attack;
                @StopChargedR1Attack.performed += instance.OnStopChargedR1Attack;
                @StopChargedR1Attack.canceled += instance.OnStopChargedR1Attack;
                @Charging.started += instance.OnCharging;
                @Charging.performed += instance.OnCharging;
                @Charging.canceled += instance.OnCharging;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @RestartScene.started += instance.OnRestartScene;
                @RestartScene.performed += instance.OnRestartScene;
                @RestartScene.canceled += instance.OnRestartScene;
                @ActivateInteractible.started += instance.OnActivateInteractible;
                @ActivateInteractible.performed += instance.OnActivateInteractible;
                @ActivateInteractible.canceled += instance.OnActivateInteractible;
                @DeactivateInteractible.started += instance.OnDeactivateInteractible;
                @DeactivateInteractible.performed += instance.OnDeactivateInteractible;
                @DeactivateInteractible.canceled += instance.OnDeactivateInteractible;
                @Interact.started += instance.OnInteract;
                @Interact.performed += instance.OnInteract;
                @Interact.canceled += instance.OnInteract;
                @HoldInteract.started += instance.OnHoldInteract;
                @HoldInteract.performed += instance.OnHoldInteract;
                @HoldInteract.canceled += instance.OnHoldInteract;
                @ChargedL1Attack.started += instance.OnChargedL1Attack;
                @ChargedL1Attack.performed += instance.OnChargedL1Attack;
                @ChargedL1Attack.canceled += instance.OnChargedL1Attack;
                @ConfirmTargetSelection.started += instance.OnConfirmTargetSelection;
                @ConfirmTargetSelection.performed += instance.OnConfirmTargetSelection;
                @ConfirmTargetSelection.canceled += instance.OnConfirmTargetSelection;
                @CancelTargetSelection.started += instance.OnCancelTargetSelection;
                @CancelTargetSelection.performed += instance.OnCancelTargetSelection;
                @CancelTargetSelection.canceled += instance.OnCancelTargetSelection;
                @Pause.started += instance.OnPause;
                @Pause.performed += instance.OnPause;
                @Pause.canceled += instance.OnPause;
                @Cancel.started += instance.OnCancel;
                @Cancel.performed += instance.OnCancel;
                @Cancel.canceled += instance.OnCancel;
            }
        }
    }
    public GameplayActions @Gameplay => new GameplayActions(this);
    private int m_KeyboardMouseSchemeIndex = -1;
    public InputControlScheme KeyboardMouseScheme
    {
        get
        {
            if (m_KeyboardMouseSchemeIndex == -1) m_KeyboardMouseSchemeIndex = asset.FindControlSchemeIndex("Keyboard&Mouse");
            return asset.controlSchemes[m_KeyboardMouseSchemeIndex];
        }
    }
    private int m_GamepadSchemeIndex = -1;
    public InputControlScheme GamepadScheme
    {
        get
        {
            if (m_GamepadSchemeIndex == -1) m_GamepadSchemeIndex = asset.FindControlSchemeIndex("Gamepad");
            return asset.controlSchemes[m_GamepadSchemeIndex];
        }
    }
    public interface IGameplayActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnSquareAttack(InputAction.CallbackContext context);
        void OnChargedSquareAttack(InputAction.CallbackContext context);
        void OnStopChargedSquareAttack(InputAction.CallbackContext context);
        void OnCircleAttack(InputAction.CallbackContext context);
        void OnChargedCircleAttack(InputAction.CallbackContext context);
        void OnTriangleAttack(InputAction.CallbackContext context);
        void OnChargedTriangleAttack(InputAction.CallbackContext context);
        void OnR1Attack(InputAction.CallbackContext context);
        void OnChargedR1Attack(InputAction.CallbackContext context);
        void OnStopChargedR1Attack(InputAction.CallbackContext context);
        void OnCharging(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnRestartScene(InputAction.CallbackContext context);
        void OnActivateInteractible(InputAction.CallbackContext context);
        void OnDeactivateInteractible(InputAction.CallbackContext context);
        void OnInteract(InputAction.CallbackContext context);
        void OnHoldInteract(InputAction.CallbackContext context);
        void OnChargedL1Attack(InputAction.CallbackContext context);
        void OnConfirmTargetSelection(InputAction.CallbackContext context);
        void OnCancelTargetSelection(InputAction.CallbackContext context);
        void OnPause(InputAction.CallbackContext context);
        void OnCancel(InputAction.CallbackContext context);
    }
}
