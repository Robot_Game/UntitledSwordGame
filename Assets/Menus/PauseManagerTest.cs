﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doozy.Engine;

public class PauseManagerTest : MonoBehaviour
{
    private PlayerInputActions inputs;

    public bool IsInPause { get; private set; }

    private void Awake()
    {
        inputs = new PlayerInputActions();
        AddControls();
    }

    private void OnEnable()
    {
        Time.timeScale = 1;
        inputs.Enable();
    }

    private void OnDisable()
    {
        Time.timeScale = 1;
        inputs.Disable();
    }

    private void AddControls()
    {
        inputs.Gameplay.Pause.performed += ctx => StartCoroutine(Pause());
    }

    private IEnumerator Pause()
    {
        inputs.Gameplay.Pause.Disable();
        GameEventMessage.SendEvent("ShowPauseUI");

        yield return new WaitForSeconds(.2f);

        inputs.Gameplay.Pause.Enable();
    }

}
