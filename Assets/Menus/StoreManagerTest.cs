﻿using System.Collections;
using UnityEngine;
using Doozy.Engine;

public class StoreManagerTest : MonoBehaviour
{
    private PlayerInputActions inputs;

    private void Awake()
    {
        inputs = new PlayerInputActions();
        AddControls();
    }
    // Start is called before the first frame update
    private void OnEnable()
    {
        inputs.Enable();
    }

    private void OnDisable()
    {
        inputs.Disable();
    }

    private void AddControls()
    {
        inputs.Gameplay.Interact.performed += ctx => StartCoroutine(OpenStore());
    }

    private IEnumerator OpenStore()
    {
        inputs.Gameplay.Pause.Disable();
        GameEventMessage.SendEvent("ShowStoreUI");

        yield return new WaitForSeconds(.2f);

        inputs.Gameplay.Pause.Enable();
    }
}
