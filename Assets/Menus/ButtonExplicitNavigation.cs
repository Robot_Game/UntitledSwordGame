﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ButtonExplicitNavigation : MonoBehaviour
{
    [SerializeField] private Button upButton;
    [SerializeField] private Button downButton;
    [SerializeField] private Button leftButton;
    [SerializeField] private Button rightButton;

    private Button thisButton;

    private void Awake()
    {
        thisButton.GetComponent<Button>();
    }

    public void SetButtonOnUp(Button button)
    {
        var navigation = thisButton.navigation;
        navigation.selectOnUp = button;
        thisButton.navigation = navigation;
    }

    public void SetButtonOnDown(Button button)
    {
        var navigation = thisButton.navigation;
        navigation.selectOnDown = button;
        thisButton.navigation = navigation;
    }

    public void SetButtonOnLeft(Button button)
    {
        var navigation = thisButton.navigation;
        navigation.selectOnLeft = button;
        thisButton.navigation = navigation;
    }

    public void SetButtonOnRight(Button button)
    {
        var navigation = thisButton.navigation;
        navigation.selectOnRight = button;
        thisButton.navigation = navigation;
    }
}
